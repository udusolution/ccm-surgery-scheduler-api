/**
 * @description various system operations to be triggered manually
 */


const express = require('express');
const router = express.Router();
const M = require('../../resources/languages/messages');
const Enums = require('../../common/enums');
const Auth = require('../../common/middleware/auth.middleware');
const SyncWithMd = require('../../operations/syncWithMd');


/**
 * @description syn local database with advanced md
 */
router.get('/syncMd', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;

    try {

        log.info(reqId, `calling (SyncWithMd)'syncMdData'`);
        // -> Sync
        await SyncWithMd.syncMdData(log, reqId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description get last advanced MD sync date
 */
 router.get('/lastMdSync', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;

    try {

        log.info(reqId, `calling (SyncWithMd)'getLastMdSync'`);
        // -> Get
        const date = await SyncWithMd.getLastMdSync(log, reqId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: date });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;