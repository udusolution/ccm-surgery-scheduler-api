/**
 * @description event edit settings routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../config/config');
const Errors = require('../../common/errors/_CustomErrors');
const M = require('../../resources/languages/messages');
const Enums = require('../../common/enums');
const Auth = require('../../common/middleware/auth.middleware');
const Validators = require('./eventEditSettings.v');
const EventEditSettingRepo = require('../../repos/eventEditSetting.repo');
const History = require('../../repos/history.repo');
const UserRepo = require('../../repos/user.repo');
const PnOperations = require('../../operations/notifications');


/**
 * @description edit a user's event edit setting (whether they allow anyone, no one, or upon request, to edit their created events)
 */
router.post('/updateUserSetting', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin])), Validators.updateUserSetting_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { userId, setting } = req.body;

    try {

        log.info(reqId, `calling (UserRepo)'findById'`);
        // -> Get User
        const user = await UserRepo.findById(log, reqId, userId);

        // Sanity Checks, throw error in the following cases
        // ? If user not found
        if(!user) throw new Errors.CommonErrors.SanityError('user not found', M.failure.userNotFound());

        log.info(reqId, `calling (EventEditSettingRepo)'updateUserSetting'`);
        // -> Update User Event Edit Permission Setting
        await EventEditSettingRepo.updateUserSetting(log, reqId, userId, setting);

        // <> Send PN
        PnOperations.sendPnToAll(log, reqId, '', '', { type: Enums.PnEventTypes.UpdatedUserEventEditSettings, userId }, { excludeUserId: userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.UpdateEventEditSetting, M.history.updatedEventEditSetting(user.name, user.eventEditPerm, setting)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description edit my event edit setting (whether they allow anyone, no one, or upon request, to edit their created events)
 */
router.post('/updateMySetting', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin, Enums.UserRoles.Coordinator])), Validators.updateMySetting_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { setting } = req.body;

    try {

        log.info(reqId, `calling (EventEditSettingRepo)'updateUserSetting'`);
        // -> Update User Event Edit Permission Setting
        await EventEditSettingRepo.updateUserSetting(log, reqId, req.userId, setting);

        // <> Send PN
        PnOperations.sendPnToAll(log, reqId, '', '', { type: Enums.PnEventTypes.UpdatedUserEventEditSettings, userId: req.userId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.UpdateEventEditSetting, M.history.updatedMyEventEditSetting(req.user.eventEditPerm, setting)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description edit a user's event edit whitelist (which coordinators I allow to edit my events without requiring permission request)
 */
router.post('/updateUserWhitelist', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin])), Validators.updateUserWhitelist_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { userId, whitelist } = req.body;

    try {

        log.info(reqId, `calling (UserRepo)'findById'`);
        // -> Get User
        const user = await UserRepo.findById(log, reqId, userId);

        // Sanity Checks, throw error in the following cases
        // ? If user not found
        if(!user) throw new Errors.CommonErrors.SanityError('user not found', M.failure.userNotFound());

        log.info(reqId, `calling (EventEditSettingRepo)'updateUserWhitelist'`);
        // -> Update User Event Edit Whitelist
        await EventEditSettingRepo.updateUserWhitelist(log, reqId, userId, whitelist);

        // <> Send PN
        PnOperations.sendPnToAll(log, reqId, '', '', { type: Enums.PnEventTypes.UpdatedUserEventEditSettings, userId }, { excludeUserId: userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.UpdateEventEditWhitelist, M.history.updatedEventEditWhitelist(user.name)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description edit my user's event edit whitelist (which coordinators I allow to edit my events without requiring permission request)
 */
router.post('/updateMyWhitelist', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin, Enums.UserRoles.Coordinator])), Validators.updateMyWhitelist_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { whitelist } = req.body;

    try {

        log.info(reqId, `calling (EventEditSettingRepo)'updateUserSetting'`);
        // -> Update User Event Edit Whitelist
        await EventEditSettingRepo.updateUserWhitelist(log, reqId, req.userId, whitelist);

        // <> Send PN
        PnOperations.sendPnToAll(log, reqId, '', '', { type: Enums.PnEventTypes.UpdatedUserEventEditSettings, userId: req.userId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.UpdateEventEditWhitelist, M.history.updatedMyEventEditWhitelist()).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description get a user's event edit whitelist
 */
 router.post('/getUserWhitelist', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin, Enums.UserRoles.Coordinator])), Validators.getUserWhitelist_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { userId } = req.body;

    try {

        log.info(reqId, `calling (EventEditSettingRepo)'getUserWhitelist'`);
        // -> Get the User Event Edit Whitelist
        const whitelist = await EventEditSettingRepo.getUserWhitelist(log, reqId, userId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { whitelist } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;