/**
 * @description validation middleware for event edit permission settings endpoints
 */


const Validator = require('../../common/utils/validators/validator');
const Matcher = require('../../common/utils/patternMatcher');
const Errors = require('../../common/errors/_CustomErrors');
const Enums = require('../../common/enums');
const Rules = require('../../config/validation.rules');


function updateUserSetting_v(req, res, next) {

    const { log, reqId } = req;
    const { userId, setting } = req.body;
    const fnName = 'eventEditSettings/updateUserSetting_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(userId, 'user ID').required().bake(),
            new Validator.Validate(setting, 'setting').required().isString().mustBeIn(Enums.UserEventEditPermissionsArray, 'must be a valid setting value').bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function updateMySetting_v(req, res, next) {

    const { log, reqId } = req;
    const { setting } = req.body;
    const fnName = 'eventEditSettings/updateMySetting_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(setting, 'setting').required().isString().mustBeIn(Enums.UserEventEditPermissionsArray, 'must be a valid setting value').bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function updateUserWhitelist_v(req, res, next) {

    const { log, reqId } = req;
    const { userId, whitelist } = req.body;
    const fnName = 'eventEditSettings/updateUserWhitelist_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(userId, 'user ID').required().bake(),
            new Validator.Validate(whitelist, 'whitelist').required().isArray().bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function updateMyWhitelist_v(req, res, next) {

    const { log, reqId } = req;
    const { whitelist } = req.body;
    const fnName = 'eventEditSettings/updateMyWhitelist_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(whitelist, 'whitelist').required().isArray().bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}

function getUserWhitelist_v(req, res, next) {

    const { log, reqId } = req;
    const { userId } = req.body;
    const fnName = 'eventEditSettings/getUserWhitelist_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(userId, 'user ID').required().bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    updateUserSetting_v,
    updateMySetting_v,
    updateUserWhitelist_v,
    updateMyWhitelist_v,
    getUserWhitelist_v
}