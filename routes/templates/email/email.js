/**
 * @description master data routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../../config/config');
const Errors = require('../../../common/errors/_CustomErrors');
const M = require('../../../resources/languages/messages');
const Enums = require('../../../common/enums');
const Auth = require('../../../common/middleware/auth.middleware');
const MasterDataRepo = require('../../../repos/masterData.repo');
const History = require('../../../repos/history.repo');
const multer = require('multer');
const EmailTemplateRepo = require('../../../repos/template.email.repo');


/**
 * MULTER CONFIGS FOR THIS ENDPOINT
 */
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/templates/email')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    }
})
const upload = multer({ storage: storage }).any();

/**
 * @description add email template
 */
router.post('/addOne', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;

    upload(req, res, async function (err) {

        try {

            // Sanity Checks, throw errors in the following cases
            // ? If multer returned an error
            // ? If general error was returned
            if (err instanceof multer.MulterError)  throw new Errors.FileUploadErrors.FileUploadError(err.message, err);
            else if (err)                           throw new Errors.FileUploadErrors.FileUploadError(err.message, err);

            // Request Variables
            const { name, subject, body, tags } = req.body;
            
            // Template Insert Params
            const insertTemplateParams = {
                name, 
                subject, 
                body
            }
            if(tags) insertTemplateParams.tags = tags;

            log.info(reqId, `calling (EmailTemplateRepo)'add'`);
            // -> Insert Template
            const newTemplate = await EmailTemplateRepo.add(log, reqId, insertTemplateParams);

            // If has attachments
            if(req.files?.length > 0) {
                
                // Attachment Insert Params
                const insertAttachmentsParams = [];

                for(let i = 0; i < req.files.length; i++) {
                    let file = req.files[i];

                    insertAttachmentsParams.push({
                        emailId: newTemplate.id,
                        file: file.path.replace(/^public[\\\/]/, '').replace(/\\/g, '/')
                    })
                }

                log.info(reqId, `calling (EmailTemplateRepo)'addAttachments'`);
                // -> Insert Template Attachments
                await EmailTemplateRepo.addAttachments(log, reqId, insertAttachmentsParams);

            }
            

            // End
            log.info(reqId, `end [success]`);
            History.add(log, reqId, req.userId, null, Enums.HistoryEvents.AddMasterData, M.history.addedEmailTemplate(name)).catch(() => {});
            res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });

        }
        catch(e) {

            next(e);
        }
    })
    
})


/**
 * @description add email template attachments
 * @note the above "addOne" endpoint can add attachments also. This endpoint is used as a part of editing an existing template
 * @note deleting an attachment happens via the Master Data endpoints as to reduce redundancy
 */
router.post('/addAttachments', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;

    upload(req, res, async function (err) {

        try {

            // Sanity Checks, throw errors in the following cases
            // ? If multer returned an error
            // ? If general error was returned
            if (err instanceof multer.MulterError)  throw new Errors.FileUploadErrors.FileUploadError(err.message, err);
            else if (err)                           throw new Errors.FileUploadErrors.FileUploadError(err.message, err);

            // Request Variables
            const { templateId } = req.body;

            // If has attachments
            if(req.files) {
                
                // Attachment Insert Params
                const insertAttachmentsParams = [];

                for(let i = 0; i < req.files.length; i++) {
                    let file = req.files[i];

                    insertAttachmentsParams.push({
                        emailId: templateId,
                        file: file.path.replace(/^public[\\\/]/, '').replace(/\\/g, '/')
                    })
                }

                log.info(reqId, `calling (EmailTemplateRepo)'addAttachments'`);
                // -> Insert Template Attachments
                await EmailTemplateRepo.addAttachments(log, reqId, insertAttachmentsParams);

            }
            

            // End
            log.info(reqId, `end [success]`);
            // History.add(log, reqId, req.userId, null, Enums.HistoryEvents.AddMasterData, M.history.addedMasterData(Enums.MasterDataTables.PatientCommunication, title)).catch(() => {});
            res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });

        }
        catch(e) {

            next(e);
        }
    })
    
})


/**
 * @description get email templates
 */
router.get('/getAll', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;

    try {

        log.info(reqId, `calling (EmailTemplateRepo)'getAll'`);
        // -> Get All Email Templates
        const templates = await EmailTemplateRepo.getAll(log, reqId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { templates } });
    }
    catch(e) {

        next(e);
    }
    
})

/**
 * @description get email templates
 */
router.post('/findById', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { templateId } = req.body;

    try {

        log.info(reqId, `calling (EmailTemplateRepo)'getAll'`);
        // -> Get Email Template
        const template = await EmailTemplateRepo.findById(log, reqId, templateId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { template } });
    }
    catch(e) {

        next(e);
    }
    
})


/**
 * @description change patient communication file
 * @deprecated
 */
router.post('/changeFile', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;

    upload(req, res, async function (err) {

        try {

            // Sanity Checks, throw errors in the following cases
            // ? If multer returned an error
            // ? If general error was returned
            if (err instanceof multer.MulterError)  throw new Errors.FileUploadErrors.FileUploadError(err.message, err);
            else if (err)                           throw new Errors.FileUploadErrors.FileUploadError(err.message, err);

            // params
            const { rowId } = req.body;

            log.info(reqId, `calling (MasterDataRepo)'getRow'`);
            // -> Get Master Data Row
            const row = await MasterDataRepo.getRow(log, reqId, Enums.MasterDataTables.PatientCommunication, rowId);

            // Sanity Checks, throw errors in the following cases
            // ? If row not found
            if(!row) throw new Errors.CommonErrors.SanityError('master data row not found', M.failure.masterFilesEntryNotFound());
            
            const filePath = req.file?.path?.replace(/^public[\\\/]/, '').replace(`\\`, '/');
            
            log.info(reqId, `calling (MasterDataRepo)'updateCol' to update file path`);
            // -> Update
            if(filePath) await MasterDataRepo.updateCol(log, reqId, Enums.MasterDataTables.PatientCommunication, rowId, 'file', filePath);

            // End
            log.info(reqId, `end [success]`);
            History.add(log, reqId, req.userId, null, Enums.HistoryEvents.UpdateMasterData, M.history.updatedMasterData(Enums.MasterDataTables.PatientCommunication, row.title, 'file', row.file, filePath)).catch(() => {});
            res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });

        }
        catch(e) {

            next(e);
        }
    })
    
})


/**
 * Exports
 */
module.exports = router;