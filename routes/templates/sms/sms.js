/**
 * @description sms template routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../../config/config');
const Errors = require('../../../common/errors/_CustomErrors');
const M = require('../../../resources/languages/messages');
const Enums = require('../../../common/enums');
const Auth = require('../../../common/middleware/auth.middleware');
const MasterDataRepo = require('../../../repos/masterData.repo');
const History = require('../../../repos/history.repo');
const SmsTemplateRepo = require('../../../repos/template.sms.repo');
const Validators = require('./sms.v');


/**
 * @description add email template
 */
router.post('/addOne', Auth.auth(), Validators.addOne_v, async (req, res, next) => {

    const { log, reqId } = req;
    const params = req.body;
    
    try {

        log.info(reqId, `calling (SmsTemplateRepo)'add'`);
        // -> Insert Template
        const newTemplate = await SmsTemplateRepo.add(log, reqId, params);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.AddMasterData, M.history.addedSmsTemplate(params.name)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { template: newTemplate } });

    }
    catch(e) {

        next(e);
    }
})


/**
 * @description get all sms templates
 */
router.get('/getAll', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;

    try {

        log.info(reqId, `calling (SmsTemplateRepo)'getAll'`);
        // -> Get All Sms Templates
        const templates = await SmsTemplateRepo.getAll(log, reqId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { templates } });
    }
    catch(e) {

        next(e);
    }
    
})


/**
 * Exports
 */
module.exports = router;