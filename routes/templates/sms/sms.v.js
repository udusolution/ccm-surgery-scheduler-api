/**
 * @description validation middleware for sms template endpoints
 */


const Validator = require('../../../common/utils/validators/validator');
const Errors = require('../../../common/errors/_CustomErrors');


function addOne_v(req, res, next) {

    const { log, reqId } = req;
    const { name, tags, body } = req.body;
    const fnName = 'templates/sms/addOne_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(name, 'name').required().isString().bake(),
            new Validator.Validate(tags, 'tags').isString().bake(),
            new Validator.Validate(body, 'body').required().isString().bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    addOne_v
}