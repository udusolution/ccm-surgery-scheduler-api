/**
 * @description scheduled productivity reports endpoints
 */


const express = require('express');
const router = express.Router();
const config = require('../../config/config');
const Errors = require('../../common/errors/_CustomErrors');
const M = require('../../resources/languages/messages');
const Enums = require('../../common/enums');
const Auth = require('../../common/middleware/auth.middleware');
const ProductivityReportRepo = require('../../repos/productivityReport.repo');


/**
 * @description add a scheduled productivity report
 */
router.post('/addOne', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const params = req.body;

    try{

        log.info(reqId, `calling (ProductivityReportRepo)'addOne'`);
        // -> Add One
        await ProductivityReportRepo.addOne(log, reqId, params);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description update a scheduled productivity report
 */
 router.post('/updateOne', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const params = req.body;

    try{

        log.info(reqId, `calling (ProductivityReportRepo)'updateOne'`);
        // -> Update One
        await ProductivityReportRepo.updateOne(log, reqId, params);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description get all scheduled productivity reports
 */
router.get('/getAll', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;

    try{

        log.info(reqId, `calling (ProductivityReportRepo)'getAll'`);
        // -> Get All
        const reports = await ProductivityReportRepo.getAll(log, reqId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { reports } });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description delete a scheduled productivity report
 */
 router.post('/deleteOne', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { id } = req.body;

    try{

        log.info(reqId, `calling (ProductivityReportRepo)'deleteOne'`);
        // -> Delete One
        await ProductivityReportRepo.deleteOne(log, reqId, id);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;