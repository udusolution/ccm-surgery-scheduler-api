/**
 * @description address lookup routes
 * @note uses Google Places API
 */


const express = require('express');
const router = express.Router();
const M = require('../../resources/languages/messages');
const Enums = require('../../common/enums');
const Auth = require('../../common/middleware/auth.middleware');
const Validators = require('./address-lookup.v');
const AddressLookup = require('../../common/services/address-lookup');


/**
 * @description search addresses
 */
router.post('/search', Validators.search_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { query } = req.body;

    try{

        log.info(reqId, `calling (AddressLookup)'search'`);
        // -> Search Addresses
        const candidates = await AddressLookup.search(log, reqId, query);
        
        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { candidates } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description get place details
 */
router.post('/details', Validators.details_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { placeId } = req.body;

    try{

        log.info(reqId, `calling (AddressLookup)'details'`);
        // -> Search Addresses
        const details = await AddressLookup.details(log, reqId, placeId);
        
        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { details } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;