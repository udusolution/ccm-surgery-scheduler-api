/**
 * @description validation middleware for patient allergies endpoints
 */


const Validator = require('../../common/utils/validators/validator');
const Matcher = require('../../common/utils/patternMatcher');
const Errors = require('../../common/errors/_CustomErrors');
const TableDefinitions = require('../../config/db/tables.json');


function get_v(req, res, next) {

    const { log, reqId } = req;
    const { skip, limit, userId } = req.body;
    const fnName = 'history/get_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        const errors = [];
        if(skip == null) errors.push('skip required');
        if(!limit) errors.push('limit required');
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function search_v(req, res, next) {

    const { log, reqId } = req;
    const { skip, limit, fromDate, toDate, userId, eventId, patientId, types } = req.body;
    const fnName = 'history/search_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(skip, 'skip').required().isNumber().bake(),
            new Validator.Validate(limit, 'limit').required().isNumber().min(1).bake(),
            new Validator.Validate(fromDate, 'from-date').isDate().bake(),
            new Validator.Validate(toDate, 'to-date').isDate().minDate(fromDate, 'to-date must be after from-date').bake(),
            new Validator.Validate(userId, 'user id').bake(),
            new Validator.Validate(eventId, 'event id').bake(),
            new Validator.Validate(patientId, 'patient id').bake(),
            new Validator.Validate(types, 'action types').isArray().bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function add_clearanceGenerated_v(req, res, next) {

    const { log, reqId } = req;
    const { eventId, clearanceType } = req.body;
    const fnName = 'history/add_clearanceGenerated_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake(),
            new Validator.Validate(clearanceType, 'clearance type').required().isString().bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    get_v,
    search_v,
    add_clearanceGenerated_v
}