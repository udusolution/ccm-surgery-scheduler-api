/**
 * @description master data routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../config/config');
const Errors = require('../../common/errors/_CustomErrors');
const M = require('../../resources/languages/messages');
const Enums = require('../../common/enums');
const Auth = require('../../common/middleware/auth.middleware');
const Validators = require('./history.v');
const History = require('../../repos/history.repo');


/**
 * @description get history entries
 * @deprecated
 */
router.post('/get', Validators.get_v, Auth.auth(new Auth.RoleSets().all([Enums.UserRoles.Admin]).bake()), async (req, res, next) => {

    const { log, reqId } = req;
    const { skip, limit, userId = null } = req.body;

    try{

        log.info(reqId, `calling (HistoryRepo)'getEntries'`);
        // -> Get History Entries Page
        const historyEntries = await History.getEntries(log, reqId, skip, limit, userId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { historyEntries } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description search history entries
 */
router.post('/search', Validators.search_v, Auth.auth(new Auth.RoleSets().all([Enums.UserRoles.Admin]).bake()), async (req, res, next) => {

    const { log, reqId } = req;
    const { skip, limit, fromDate, toDate, userId, eventId, patientId, types } = req.body;

    try{

        log.info(reqId, `calling (HistoryRepo)'search'`);
        // -> Search History Entries
        const historyEntries = await History.search(log, reqId, { skip, limit, fromDate, toDate, userId, eventId, patientId, types });

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { historyEntries } });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description add history entry when clearance PDF is generated
 * @note used in case the action performed has no effect in the data but still requires an entry in the history
 */
router.post('/add_clearanceGenerated', Validators.add_clearanceGenerated_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId, clearanceType } = req.body;

    try{

        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.GenerateClearancePdf, M.history.generatedClearancePdf(eventId, clearanceType));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;