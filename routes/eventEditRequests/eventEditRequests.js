/**
 * @description event edit request routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../config/config');
const Errors = require('../../common/errors/_CustomErrors');
const M = require('../../resources/languages/messages');
const Enums = require('../../common/enums');
const Auth = require('../../common/middleware/auth.middleware');
const Validators = require('./eventEditRequests.v');
const EventEditRequestRepo = require('../../repos/eventEditRequest.repo');
const History = require('../../repos/history.repo');
const PnOperations = require('../../operations/notifications');
const EventRepo = require('../../repos/event.repo');


/**
 * @description add an event edit request
 */
router.post('/addOne', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Coordinator])), Validators.addOne_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try {

        log.info(reqId, `calling (EventRepo)'findById'`);
        // -> Get Event
        const event = await EventRepo.findById(log, reqId, eventId);

        // Sanity Checks, throw error in the following cases
        // ? If event not found
        if(!event) throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());

        log.info(reqId, `calling (EventEditRequestRepo)'add'`);
        // -> Add
        await EventEditRequestRepo.add(log, reqId, { eventId, requesterId: req.userId });

        // <> Send PN
        PnOperations.sendPn(log, reqId, event.coordinatorId, M.pn.sentEditRequest.title(), M.pn.sentEditRequest.message(), { eventId, type: Enums.PnEventTypes.SentEditRequest });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.RequestEventEdit, M.history.requestedEventEdit()).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description get event edit requests for logged in coordinator
 */
router.get('/getRequests', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Coordinator])), async (req, res, next) => {

    const { log, reqId } = req;

    try {

        log.info(reqId, `calling (EventEditRequestRepo)'getForCoordinator'`);
        // -> Get
        const requests = await EventEditRequestRepo.getForCoordinator(log, reqId, req.userId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { requests } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description approve an event edit request
 */
router.post('/approveRequest', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Coordinator])), Validators.approveRequest_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { id } = req.body;

    try {

        log.info(reqId, `calling (EventEditRequestRepo)'findById'`);
        // -> Get Request
        const editRequest = await EventEditRequestRepo.findById(log, reqId, id);

        // Sanity Checks, throw errors in the following cases
        // ? If request not found
        // ? If request event not the user's
        // ? If request already approved
        // ? If request already rejected
        if(!editRequest) throw new Errors.CommonErrors.SanityError('event edit request not found', M.failure.eventEditRequestNotFound());
        if(editRequest.coordinatorId != req.userId) throw new Errors.CommonErrors.SanityError(`user is not the coordinator of this edit request's event`, M.failure.eventEditRequestNotYours());
        if(editRequest.status == Enums.EventEditRequestStatuses.Approved) throw new Errors.CommonErrors.SanityError(`event edit request already approved`, M.failure.eventEditRequestAlreadyApproved());
        if(editRequest.status == Enums.EventEditRequestStatuses.Rejected) throw new Errors.CommonErrors.SanityError(`event edit request already rejected`, M.failure.eventEditRequestAlreadyRejected());

        log.info(reqId, `calling (EventEditRequestRepo)'approveRequest'`);
        // -> Approve
        await EventEditRequestRepo.approveRequest(log, reqId, id);

        // <> PN
        PnOperations.sendPn(log, reqId, editRequest.requesterId, M.pn.acceptedEditRequest.title(), M.pn.acceptedEditRequest.message(), { eventId: editRequest.eventId, type: Enums.PnEventTypes.AcceptedEditRequest });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, editRequest.eventId, Enums.HistoryEvents.ApproveEventEdit, M.history.approvedEventEdit(editRequest.requesterName)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description reject an event edit request
 */
router.post('/rejectRequest', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Coordinator])), Validators.rejectRequest_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { id } = req.body;

    try {

        log.info(reqId, `calling (EventEditRequestRepo)'findById'`);
        // -> Get Request
        const editRequest = await EventEditRequestRepo.findById(log, reqId, id);

        // Sanity Checks, throw errors in the following cases
        // ? If request not found
        // ? If request event not the user's
        // ? If request already approved
        // ? If request already rejected
        if(!editRequest) throw new Errors.CommonErrors.SanityError('event edit request not found', M.failure.eventEditRequestNotFound());
        if(editRequest.coordinatorId != req.userId) throw new Errors.CommonErrors.SanityError(`user is not the coordinator of this edit request's event`, M.failure.eventEditRequestNotYours());
        if(editRequest.status == Enums.EventEditRequestStatuses.Approved) throw new Errors.CommonErrors.SanityError(`event edit request already approved`, M.failure.eventEditRequestAlreadyApproved());
        if(editRequest.status == Enums.EventEditRequestStatuses.Rejected) throw new Errors.CommonErrors.SanityError(`event edit request already rejected`, M.failure.eventEditRequestAlreadyRejected());

        log.info(reqId, `calling (EventEditRequestRepo)'rejectRequest'`);
        // -> Reject
        await EventEditRequestRepo.rejectRequest(log, reqId, id);

        // <> PN
        PnOperations.sendPn(log, reqId, editRequest.requesterId, M.pn.rejectedEditRequest.title(), M.pn.rejectedEditRequest.message(), { eventId: editRequest.eventId, type: Enums.PnEventTypes.RejectedEditRequest });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, editRequest.eventId, Enums.HistoryEvents.RejectEventEdit, M.history.rejectedEventEdit(editRequest.requesterName)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;