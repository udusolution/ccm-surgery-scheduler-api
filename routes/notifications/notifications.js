/**
 * @description master data routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../config/config');
const M = require('../../resources/languages/messages');
const Enums = require('../../common/enums');
const Auth = require('../../common/middleware/auth.middleware');
const UserRepo = require('../../repos/user.repo');


/**
 * @description push notification subscription object for a user
 */
router.post('/subscribe', Auth.auth(), async (req, res, next) => {

    const pnSub = req.body;
    const { log, reqId } = req;

    try {

        log.info(reqId, `calling (PriorityRepo)'setAsDefault'`);
        // -> Update User's PnSub
        await UserRepo.updatePnSub(log, reqId, req.userId, pnSub);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });

    }
    catch(e) {
        next(e);
    }
});


/**
 * Exports
 */
module.exports = router;