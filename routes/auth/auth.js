const express = require('express');
const router = express.Router();
const config = require('../../config/config');
const User = require('../../repos/user.repo');
const Refresh = require('../../repos/refresh.repo');
const Errors = require('../../common/errors/_CustomErrors');
const M = require('../../resources/languages/messages');
const Enums = require('../../common/enums');
const enc = require('../../common/utils/enc');
const jwt = require('../../common/utils/jwt');
const Validators = require('./auth.v');
const Auth = require('../../common/middleware/auth.middleware');
const History = require('../../repos/history.repo');
const EmailTemplates = require('../../resources/templates/mail/emailTemplates');
const Mail = require('../../common/services/mail');
const uuid = require('uuid');
const MD = require('../../common/services/md');
const con = require('../../config/db/db');


/**
 * @description register a new user
 * @deprecated
 */
router.post('/register', Validators.register_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { email, phone, password } = req.body;

    try{

        log.info(reqId, `calling (User)'isEmailTaken'`);
        // -> Check if email taken
        const isEmailTaken = await User.isEmailTaken(log, reqId, email);

        log.info(reqId, `calling (User)'isPhoneTaken'`);
        // -> Check if phone taken
        const isPhoneTaken = await User.isPhoneTaken(log, reqId, phone);

        // Sanity checks, throw error in the following cases
        // ? If email is taken
        // ? If phone is taken
        if(isEmailTaken) throw new Errors.CommonErrors.SanityError('email taken', M.failure.emailTaken());
        if(isPhoneTaken) throw new Errors.CommonErrors.SanityError('phone taken', M.failure.phoneTaken());

        log.info(reqId, `calling (enc)'hash' to hash password`);
        // -> Hash password
        const hashedPassword = await enc.hash(log, reqId, password);

        log.info(reqId, `calling (User)'insertUser'`);
        // -> Insert user
        const newUserId = await User.add(log, reqId, email, hashedPassword, phone);

        log.info(reqId, `calling (jwt)'generateAccessToken'`);
        // -> Generate Access Token
        const accessToken = jwt.generateAccessToken(log, reqId, newUserId);

        log.info(reqId, `calling (jwt)'generateRefreshToken'`);
        // -> Generate Refresh Token
        const refreshToken = jwt.generateRefreshToken(log, reqId, newUserId);

        log.info(reqId, `calling (Refresh)'add'`);
        // -> Store Refresh Token
        await Refresh.add(log, reqId, refreshToken, newUserId);
        
        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.userRegistered(), data: { newUserId, accessToken, refreshToken } });
        next();

    } catch(e) {
        next(e);
    }
});


/**
 * @description user login
 */
router.post('/login', Validators.login_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { email, password } = req.body;

    try {

        log.info(reqId, `calling (User)'findByEmail'`);
        // -> Get user
        const user = await User.findByEmail(log, reqId, email);

        // Sanity checks, throw error in any of the following cases
        // ? If user not found or is deleted
        if(!user || !!user.isDeleted) throw new Errors.AuthErrors.UserNotFoundAuthError();

        // -> If account locked, throw error
        if(user.isLocked) throw new Errors.AuthErrors.AccountLockedAuthError();
        
        log.info(reqId, `calling (enc)'compare' to compare provided password with user's password`);
        // -> Check if correct password
        const doesPasswordMatch = await enc.compare(log, reqId, password, user.password);

        // -> If incorrect password, throw error
        if(!doesPasswordMatch) {
            log.info(reqId, `end [failure] ### incorrect password`);
            throw new Errors.AuthErrors.InvalidPasswordAuthError();
        }

        log.info(reqId, `calling (jwt)'generateAccessToken'`);
        // -> Generate Access Token
        const accessToken = jwt.generateAccessToken(log, reqId, user.id);

        log.info(reqId, `calling (jwt)'generateRefreshToken'`);
        // -> Generate Refresh Token
        const refreshToken = jwt.generateRefreshToken(log, reqId, user.id);

        log.info(reqId, `calling (Refresh)'add' to save generated refresh token into DB`);
        // -> Store Refresh Token
        await Refresh.add(log, reqId, refreshToken, user.id);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.loggedIn(), data: { accessToken, refreshToken } });
        next();
            
    } catch(e) {
        next(e);
    }
});


/**
 * @description generate a new user access token from his refresh token
 */
router.get('/refresh', Validators.refresh_v, async (req, res, next) => {

    const { log, reqId } = req;
    const refreshToken = req.header('refresh-token');

    try{

        log.info(reqId, `calling (jwt)'decryptRefreshToken'`);
        // -> Decrypt the Refresh Token
        const decryptedRefreshToken = jwt.decryptRefreshToken(log, reqId, refreshToken);
    
        // Extract user ID
        const refreshTokenUserId = decryptedRefreshToken.userId;

        // Sanity checks, throw error in the following cases
        // ? If refreshTokenUserId is undefined
        if(!refreshTokenUserId) throw new Errors.AuthErrors.InvalidTokenAuthError();

        log.info(reqId, `calling (Refresh)'doesExist' to check if provider refresh token exists in DB`);
        // -> Check if Refresh Token Exists
        const doesExist = await Refresh.doesExist(log, reqId, refreshToken, refreshTokenUserId);

        // Sanity checks, throw error in the following cases
        // ? If does not exist, throw error
        if(!doesExist) throw new Errors.AuthErrors.InvalidRefreshTokenAuthError();
    
        log.info(reqId, `calling (jwt)'generateAccessToken'`);
        // -> Generate Access Token
        const newAccessToken = jwt.generateAccessToken(log, reqId, refreshTokenUserId);

        // End
        res.status(Enums.HttpCodes.Success).json({ message: '', data: { accessToken: newAccessToken } });
        next();

    } catch(e) {

        if(e instanceof Errors.JwtErrors.JwtError) {
            next(new Errors.AuthErrors.InvalidRefreshTokenAuthError());
        }
        else {
            next(e);
        }
    }
});


/**
 * @description logout user
 */
router.get('/logout', Auth.auth([], { bypassDeletionCheck: true, bypassLockCheck: true }), async (req, res, next) => {
    
    const { log, reqId } = req;

    try{

        log.info(reqId, `calling (Refresh)'deleteUserTokens' to delete user refresh tokens`);
        // -> Delete User Refresh Tokens
        await Refresh.deleteUserTokens(log, reqId, req.userId);

        // End
        res.status(Enums.HttpCodes.Success).json({ message: '', data: {} });
        next();

    } catch(e) {

        next(e);
    }
    
});


/**
 * @description get a logged in user's details
 */
router.get('/myDetails', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;

    try {

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: '', data: { user: req.user, userRoles: req.userRoles } });
        next();
            
    } catch(e) {
        next(e);
    }
});


/**
 * @description change password
 */
router.post('/changePassword', Auth.auth(), Validators.changePassword_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { currentPassword, newPassword } = req.body;

    try {

        log.info(reqId, `calling (enc)'compare' to compare provided current password with user's password`);
        // -> Check if correct current password
        const doesPasswordMatch = await enc.compare(log, reqId, currentPassword, req.user.password);

        // -> If incorrect current password, throw error
        if(!doesPasswordMatch) {
            log.info(reqId, `end [failure] ### incorrect current password`);
            throw new Errors.AuthErrors.InvalidPasswordAuthError(M.failure.incorrectPassword());
        }

        log.info(reqId, `calling (enc)'hash' to hash new password`);
        // -> Hash new password
        const hashedNewPassword = await enc.hash(log, reqId, newPassword);

        log.info(reqId, `calling (User)'changePassword'`);
        // -> Update user password
        await User.changePassword(log, reqId, req.userId, hashedNewPassword);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.ChangeMyPassword, M.history.changedMyPassword());
        res.status(Enums.HttpCodes.Success).json({ message: '', data: {} });
        next();
            
    } catch(e) {
        next(e);
    }
});


/**
 * @description send a password reset email
 */
router.post('/forgotPassword', Validators.forgotPassword_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { email } = req.body;

    try {

        log.info(reqId, `calling (User)'findByEmail'`);
        // -> Get user
        const user = await User.findByEmail(log, reqId, email);

        // If not found, return Invalid
        if(!user || !!user.isDeleted) {
            res.status(Enums.HttpCodes.Invalid).json({ message: M.failure.userWithEmailNotFound(), data: {} });
            next();
            return;
        }

        const token = uuid.v4();

        log.info(reqId, `calling (User)'changePasswordResetToken'`);
        // -> Update user forgot password token
        await User.changePasswordResetToken(log, reqId, email, token);

        log.info(reqId, `building the forgot password URL to be included in the forgot password email`);
        // -> Build the reset URL
        const forgotPasswordUrl = `${config.app.websiteBaseUrl}/forgot-password/${token}`;

        // Email Params
        const subject = `${config.identity.name} - Forgot Password`;
        const body = EmailTemplates.forgotPassword({ forgotPasswordUrl });

        log.info(reqId, `calling (Mail)'send'`);
        // -> Send Reset Email
        await Mail.send(log, reqId, email, subject, body);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: '', data: {} });
        next();
            
    } catch(e) {
        next(e);
    }
});

/**
 * @description reset password
 * @note this is the 2nd step after "forgotPassword" since the user clicks the reset link which provides a new password form that calls this endpoint
 */
router.post('/resetPassword', Validators.resetPassword_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { token, password } = req.body;

    try {

        log.info(reqId, `calling (User)'findByForgotPasswordToken'`);
        // -> Get user
        const user = await User.findByForgotPasswordToken(log, reqId, token);

        // Sanity Checks, throw errors in the following cases
        // ? If user not found
        if(!user) throw new Errors.CommonErrors.SanityError('user with forgot password token not found', M.failure.userWithPasswordResetTokenNotFound());

        log.info(reqId, `calling (enc)'hash' to hash new password`);
        // -> Hash new password
        const hashedNewPassword = await enc.hash(log, reqId, password);

        log.info(reqId, `calling (User)'changePassword'`);
        // -> Update user password
        await User.changePassword(log, reqId, user.id, hashedNewPassword);

        log.info(reqId, `calling (User)'changePasswordResetToken' to clear it`);
        // -> Clear user forgot password token
        await User.changePasswordResetToken(log, reqId, user.email, '');

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: '', data: {} });
        next();
            
    } catch(e) {
        next(e);
    }
});


/**
 * Exports
 */
module.exports = router;