/**
 * @description validation middleware for auth endpoints
 */


const Validator = require('../../common/utils/validators/validator');
const Matcher = require('../../common/utils/patternMatcher');
const Errors = require('../../common/errors/_CustomErrors');


function register_v(req, res, next) {

    const { log, reqId } = req;
    const { email, password, phone } = req.body;
    const fnName = 'auth/register_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        const errors = [];
        if(!email)                          errors.push('email required');
        else if(!Validator.isString(email)) errors.push('email must be string');
        else if(!Matcher.isEmail(email))    errors.push('email must be valid');
        if(!password)                       errors.push('password required');
        if(!phone)                          errors.push('phone required');
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function login_v(req, res, next) {

    const { log, reqId } = req;
    const { email, password } = req.body;
    const fnName = 'auth/login_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        // Validations
        const validations = [
            new Validator.Validate(email, 'email').required().isString().regex(Matcher.regex.email).bake(),
            new Validator.Validate(password, 'password').required().isString().bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);

        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function refresh_v(req, res, next) {

    const fnName = 'auth/refresh_v';
    const { log, reqId } = req;
    const refreshToken = req.header('refresh-token');

    try {

        log.info(reqId, `${fnName} ### start`);

        // Validations
        const validations = [
            new Validator.Validate(refreshToken, 'refresh token').required().isString().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);

        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function changePassword_v(req, res, next) {

    const { log, reqId } = req;
    const { currentPassword, newPassword } = req.body;
    const fnName = 'auth/changePassword_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(currentPassword, 'current password').required().isString().bake(),
            new Validator.Validate(newPassword, 'new password').required().isString().bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function forgotPassword_v(req, res, next) {

    const { log, reqId } = req;
    const { email } = req.body;
    const fnName = 'auth/forgotPassword_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(email, 'email').required().isString().isEmail().bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function resetPassword_v(req, res, next) {

    const { log, reqId } = req;
    const { token, password } = req.body;
    const fnName = 'auth/resetPassword_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(token, 'token').required().isString().bake(),
            new Validator.Validate(password, 'password').required().isString().bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    register_v,
    login_v,
    refresh_v,
    changePassword_v,
    forgotPassword_v,
    resetPassword_v
}