/**
 * @description validation middleware for event endpoints
 */


const Validator = require('../../../common/utils/validators/validator');
const Matcher = require('../../../common/utils/patternMatcher');
const Errors = require('../../../common/errors/_CustomErrors');
const ValidationRules = require('../../../config/validation.rules');


function get_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/bloodThinners/get_v';
    const { eventId } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function addOne_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/bloodThinners/addOne_v';
    const { eventId, bloodThinnerId, bloodThinnerCode, bloodThinnerName, bloodThinnerDescription } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake(),
            new Validator.Validate(bloodThinnerId, 'blood thinner id').bake(),
            new Validator.Validate(bloodThinnerCode, 'blood thinner code').isString().minLength(ValidationRules.Code.minLength).maxLength(ValidationRules.Code.maxLength).bake(),
            new Validator.Validate(bloodThinnerName, 'blood thinner name').isString().minLength(ValidationRules.BloodThinner.name.minLength).maxLength(ValidationRules.BloodThinner.name.maxLength).bake(),
            new Validator.Validate(bloodThinnerDescription, 'blood thinner description').required().isString().minLength(ValidationRules.BloodThinner.description.minLength).maxLength(ValidationRules.BloodThinner.description.maxLength).bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}

function updateOne_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/bloodThinners/updateOne_v';
    const { id, bloodThinnerName, bloodThinnerCode, bloodThinnerDescription } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(id, 'id').required().bake(),
            new Validator.Validate(bloodThinnerCode, 'blood thinner code').isString().minLength(ValidationRules.Code.minLength).maxLength(ValidationRules.Code.maxLength).bake(),
            new Validator.Validate(bloodThinnerName, 'blood thinner name').isString().minLength(ValidationRules.BloodThinner.name.minLength).maxLength(ValidationRules.BloodThinner.name.maxLength).bake(),
            new Validator.Validate(bloodThinnerDescription, 'blood thinner description').isString().minLength(ValidationRules.BloodThinner.description.minLength).maxLength(ValidationRules.BloodThinner.description.maxLength).bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function deleteOne_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/bloodThinners/deleteOne_v';
    const { id } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(id, 'id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function deleteAllFromEvent_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/bloodThinners/deleteAllFromEvent_v';
    const { eventId } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake()
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    get_v,
    addOne_v,
    updateOne_v,
    deleteOne_v,
    deleteAllFromEvent_v
}