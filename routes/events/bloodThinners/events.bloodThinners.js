/**
 * @description event blood thinners routes
 */


const express = require('express');
const router = express.Router();
const M = require('../../../resources/languages/messages');
const Enums = require('../../../common/enums');
const Auth = require('../../../common/middleware/auth.middleware');
const Validators = require('./events.bloodThinners.v');
const EventBloodThinnerRepo = require('../../../repos/event.bloodThinner.repo');
const History = require('../../../repos/history.repo');
const { CommonErrors, AuthErrors } = require('../../../common/errors/_CustomErrors');
const EventRepo = require('../../../repos/event.repo');
const ScheduledComRuling = require('../../../operations/scheduledComsRuling');
const PnOperations = require('../../../operations/notifications');
const EventEditPerm = require('../../../operations/eventEditPerm');


/**
 * @description get event blood thinners
 */
router.post('/get', Validators.get_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try{

        log.info(reqId, `calling (EventBloodThinnerRepo)'getForEvent'`);
        // -> Get Event Checklist
        const bloodThinners = await EventBloodThinnerRepo.getForEvent(log, reqId, eventId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { bloodThinners } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description add event blood thinners item
 */
router.post('/addOne', Validators.addOne_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const params = req.body;

    try {

        // Sanity Checks, throw error in the following cases
        // ? If user does not have permission to edit this event
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, params.eventId))) throw new AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventChecklistRepo)'add'`);
        // -> Add Event Blood Thinner
        const newItem = await EventBloodThinnerRepo.add(log, reqId, params);

        log.info(reqId, `calling (ScheduledComRuling)'addRuledInScheduledEmailCommunications'`);
        // -> Check and Add Scheduled Email Patient Communications if Required by the Rulings in the Master Data
        await ScheduledComRuling.addRuledInScheduledEmailCommunications(log, reqId, params.eventId);

        log.info(reqId, `calling (ScheduledComRuling)'addRuledInScheduledSmsCommunications'`);
        // -> Check and Add Scheduled Sms Patient Communications if Required by the Rulings in the Master Data
        await ScheduledComRuling.addRuledInScheduledSmsCommunications(log, reqId, params.eventId);

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId: params.eventId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, params.eventId, Enums.HistoryEvents.AddEventBloodThinner, M.history.addedEventBloodThinner(params.eventId, params.bloodThinnerName));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { bloodThinner: newItem } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description update event blood thinners item
 */
router.post('/updateOne', Validators.updateOne_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const params = req.body;

    try{

        log.info(reqId, `calling (EventBloodThinnerRepo)'findById'`);
        // -> Get Event Blood Thinner
        const eventBloodThinner = await EventBloodThinnerRepo.findById(log, reqId, params.id);

        // Sanity Checks, throw error in the following cases
        // ? If event blood thinner not found
        // ? If user does not have permission to edit this event
        if(!eventBloodThinner) throw new CommonErrors.SanityError('event blood thinner not found', M.failure.eventBloodThinnerNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventBloodThinner.eventId))) throw new AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventChecklistRepo)'updateOne'`);
        // -> Update Event Blood Thinner
        const newItem = await EventBloodThinnerRepo.updateOne(log, reqId, params);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, params.eventId, Enums.HistoryEvents.UpdateEventBloodThinner, M.history.updatedEventBloodThinner(eventBloodThinner.eventId, eventBloodThinner.bloodThinnerName), JSON.stringify(eventBloodThinner), JSON.stringify(params));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { bloodThinner: newItem } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description delete event blood thinner
 */
router.post('/deleteOne', Validators.deleteOne_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { id } = req.body;

    try {

        log.info(reqId, `calling (EventBloodThinnerRepo)'findById'`);
        // -> Get Event Blood Thinner
        const bloodThinner = await EventBloodThinnerRepo.findById(log, reqId, id);

        // Sanity Checks, throw error in the following cases
        // ? If event blood thinner not found
        // ? If user does not have permission to edit this event
        if(!bloodThinner) throw new CommonErrors.SanityError('event blood thinner not found', M.failure.eventBloodThinnerNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, bloodThinner.eventId))) throw new AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventBloodThinnerRepo)'deleteOne'`);
        // -> Delete Event Checklist Item
        await EventBloodThinnerRepo.deleteOne(log, reqId, id);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, bloodThinner.eventId, Enums.HistoryEvents.RemoveEventBloodThinner, M.history.removedEventBloodThinner(bloodThinner.eventId, bloodThinner.bloodThinnerName), JSON.stringify(bloodThinner));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description delete all event blood thinners
 */
router.post('/deleteAllFromEvent', Validators.deleteAllFromEvent_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try {

        log.info(reqId, `calling (EventRepo)'findById'`);
        // -> Get Event
        const event = await EventRepo.findById(log, reqId, eventId);

        // Sanity Checks, throw error in the following cases
        // ? If event not found
        // ? If user does not have permission to edit this event
        if(!event) throw new CommonErrors.SanityError('event not found', M.failure.eventNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventBloodThinnerRepo)'deleteAllFromEvent'`);
        // -> Delete Event Blood Thinners
        await EventBloodThinnerRepo.deleteAllFromEvent(log, reqId, eventId);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.RemoveAllEventBloodThinners, M.history.removedAllEventBloodThinners(eventId));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;