/**
 * @description master data routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../config/config');
const Errors = require('../../common/errors/_CustomErrors');
const M = require('../../resources/languages/messages');
const Enums = require('../../common/enums');
const Auth = require('../../common/middleware/auth.middleware');
const Validators = require('./events.v');
const EventRepo = require('../../repos/event.repo');
const EventProcedureRepo = require('../../repos/event.procedure.repo');
const EventDiagnosisRepo = require('../../repos/event.diagnosis.repo');
const EventBloodThinnerRepo = require('../../repos/event.bloodThinner.repo');
const EventNoteRepo = require('../../repos/event.note.repo');
const EventChecklistRepo = require('../../repos/event.checklist.repo');
const EventClearanceRepo = require('../../repos/event.clearance.repo');
const EventAuthNoteRepo = require('../../repos/event.authNote.repo');
const PatientRepo = require('../../repos/patient.repo');
const History = require('../../repos/history.repo');
const ObjUtils = require('../../common/utils/object.utils');
const SendPatientComOperations = require('../../operations/sendPatientCom');
const TemplatesInjector = require('../../common/middleware/templatesInjectors.middleware');
const multer = require('multer');
const path = require('path');
const Mail = require('../../common/services/mail');
const EmailTemplates = require('../../resources/templates/mail/emailTemplates');
const moment = require('moment');
const EventFacilityBSEmail = require('../../repos/eventFacilityEmail.repo');
const PatientComRepo = require('../../repos/patientCom.repo');
const EventEmailScheduledComRepo = require('../../repos/event.scheduledCom.email');
const EventSmsScheduledComRepo = require('../../repos/event.scheduledCom.sms');
const ScheduledComRuling = require('../../operations/scheduledComsRuling');
const EventEditRequestRepo = require('../../repos/eventEditRequest.repo');
const UserRepo = require('../../repos/user.repo');
const PnOperations = require('../../operations/notifications');
const EventEditPerm = require('../../operations/eventEditPerm');
const createGoogleCalendarEvent = require('../../common/services/gcalendar').createEvent;
const updateGoogleCalendarEvent = require('../../common/services/gcalendar').updateEvent;
const deleteGoogleCalendarEvent = require('../../common/services/gcalendar').deleteEvent;
const GCalEventBuilder = require('../../common/utils/GCalEventBuilder')
const { putStream, listBucket, deleteFile} = require('../../common/services/storage')
const { readFileSync } = require('fs')

const storage = (path) => multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, `public/${path}`)
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    }
})
const upload = (path) => multer({ storage: storage(path) }).single('file');


/**
 * @description add event
 */
router.post('/add', Validators.add_v, Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin, Enums.UserRoles.Coordinator]).bake()), async (req, res, next) => {

    const { log, reqId } = req;
    const params = req.body;

    try{

        log.info(reqId, `calling (EventRepo)'eventsInPeriod'`);
        // -> Get Events that Intersect with this New Event
        const intersectingEvents = await EventRepo.eventsInPeriod(log, reqId, params.dateFrom, params.dateTo, params.patientId, params.physicianId);

        // Sanity Checks, throw error in the following cases
        // ? If event not emergency & intersecting events exist
        if(!params.isEmergency && intersectingEvents.length > 0) throw new Errors.CommonErrors.ReservedTimeError(null, null, intersectingEvents);

        log.info(reqId, `calling (PatientRepo)'findById'`);
        // -> Get Patient Details
        const patient = await PatientRepo.findById(log, reqId, params.patientId);

        // Add some patient details to the event insert params
        params.patientDoB = patient.dateofbirth;
        params.patientGender = patient.sex;
        params.patientAddress1 = patient.address1;
        params.patientAddress2 = patient.address2;
        params.patientCity = patient.city;
        params.patientState = patient.state;
        params.patientZipCode = patient.zipcode;
        params.patientPreferredPhone = patient.phone;
        params.patientEmail = patient.email;
        params.patientSSN = patient.ssn;
        params.patientFirstName = patient.firstName;
        params.patientMiddleName = patient.middleName;
        params.patientLastName = patient.lastName;
        params.carrierId = patient.carrierId;
        params.carrierCode = patient.carrierCode;
        params.carrierName = patient.carrierName;
        params.subscriberId = patient.subscriberId;
        params.groupName = patient.groupName;
        params.groupNumber = patient.groupNumber;
        if(patient.carrierId2) params.carrierId2 = patient.carrierId2;
        if(patient.carrierCode2) params.carrierCode2 = patient.carrierCode2;
        if(patient.carrierName2) params.carrierName2 = patient.carrierName2;
        if(patient.subscriberId2) params.subscriberId2 = patient.subscriberId2;
        if(patient.groupName2) params.groupName2 = patient.groupName2;
        if(patient.groupNumber2) params.groupNumber2 = patient.groupNumber2;
        params.insuranceAuthorizationStatus = Enums.InsuranceAuthorizationStatuses.TBD

        /////////////////// REMOVED ON 2021-08-04: Carrier Info now comes from Advanced MD ////////////////
        // log.info(reqId, `calling (PatientRepo)'findById'`);
        // // -> Get Carrier
        // const carrier = await MasterDataRepo.getRow(log, reqId, Enums.MasterDataTables.Carrier, patient.carrierId);

        // // Sanity Checks, throw error in the following cases
        // // ? If patient carrier does not exist
        // if(!carrier) throw new Errors.CommonErrors.SanityError('patient carrier does not exist', M.failure.patientCarrierNotFound());

        // // Set Event Auth Status based on patient carrier exemption
        // if(carrier.isExempt == 1) params.insuranceAuthorizationStatus = Enums.InsuranceAuthorizationStatuses.NotRequired;
        // else params.insuranceAuthorizationStatus = Enums.InsuranceAuthorizationStatuses.TBD;
        ////////////////////////////////////////////////////////////////////////////////////////////////

        log.info(reqId, `calling (EventRepo)'add'`);
        // -> Add Event
        const newEvent = await EventRepo.add(log, reqId, params);

        log.info(reqId, `calling (ScheduledComRuling)'addRuledInScheduledEmailCommunications'`);
        // -> Check and Add Scheduled Email Communications if Required by the Rulings in the Master Data
        await ScheduledComRuling.addRuledInScheduledEmailCommunications(log, reqId, newEvent.id);

        log.info(reqId, `calling (ScheduledComRuling)'addRuledInScheduledSmsCommunications'`);
        // -> Check and Add Scheduled SMS Communications if Required by the Rulings in the Master Data
        await ScheduledComRuling.addRuledInScheduledSmsCommunications(log, reqId, newEvent.id);

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.AddedEvent, eventId: newEvent.id }, { excludeUserId: req.userId });

        // <> GCal
        try {
            const gCalEvent = new GCalEventBuilder()
                .setPhysicianName(newEvent.physicianName)
                .setPatientName(newEvent.patientName)
                .setEventType(newEvent.eventTypeName)
                .setStatus(Enums.EventStatuses.WaitingForConfirmation)
                .setDateFrom(newEvent.dateFrom)
                .setDateTo(newEvent.dateTo)
                .setFacilityName(newEvent.facilityName)
                .setFacilityAddress(newEvent.facilityAddress)
                .build();
            const calendarEvent = await createGoogleCalendarEvent(gCalEvent);
            await EventRepo.update(log, reqId, newEvent.id, {
                calendar_id: calendarEvent.data.id
            })
            newEvent.calendar_id = calendarEvent.data.id
        } catch (err) {
            log.error(err.message)
        }

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, newEvent.id, Enums.HistoryEvents.AddEvent, M.history.addedEvent(newEvent.id, newEvent.patientId), JSON.stringify(newEvent), null, null, null, newEvent.patientId);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { event: newEvent } });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description search events
 */
router.post('/search', Validators.search_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const params = req.body;

    try{

        // If user is only a physician, he must not see cancelled events that are not his
        if(req.userRoles.length == 1 && req.userRoles[0] == Enums.UserRoles.Physician) params.showOnlyMyCancelled = true;

        log.info(reqId, `calling (EventRepo)'search'`);
        // -> Search Events
        const events = await EventRepo.search(log, reqId, params, req.userId);

        log.info(reqId, `calling (EventRepo)'searchCount'`);
        // -> Search Events Count
        const eventsCount = await EventRepo.searchCount(log, reqId, params, req.userId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { events, eventsCount } });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description get event by ID
 * @deprecated in favor of "getWithDetails"
 */
router.post('/findById', Validators.findById_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try{

        log.info(reqId, `calling (EventRepo)'findById'`);
        // -> Get Event
        const event = await EventRepo.findById(log, reqId, eventId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { event } });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description get event with its details
 */
router.post('/getWithDetails', Validators.getWithDetails_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try{

        // -> Event & Details Promises
        const promises =  [
            EventRepo.findById(log, reqId, eventId),
            EventProcedureRepo.getForEvent(log, reqId, eventId),
            EventDiagnosisRepo.getForEvent(log, reqId, eventId),
            EventNoteRepo.getForEvent(log, reqId, eventId),
            EventChecklistRepo.getForEvent(log, reqId, eventId),
            EventClearanceRepo.getForEvent(log, reqId, eventId),
            EventBloodThinnerRepo.getForEvent(log, reqId, eventId),
            EventAuthNoteRepo.getForEvent(log, reqId, eventId),
            EventFacilityBSEmail.getForEvent(log, reqId, eventId),
            PatientComRepo.getForEvent(log, reqId, eventId),
            EventEmailScheduledComRepo.getForEvent(log, reqId, eventId, true),
            EventSmsScheduledComRepo.getForEvent(log, reqId, eventId, true),
            EventEditRequestRepo.getForEvent(log, reqId, eventId)
        ]

        log.info(reqId, `getting event and details`);
        // -> Run Promises
        const results = await Promise.all(promises);
        const event = {
            ...results[0],
            procedures: results[1],
            diagnosis: results[2],
            notes: results[3],
            checklist: results[4],
            clearances: results[5],
            bloodThinners: results[6],
            authNotes: results[7],
            facilityBSEmails: results[8],
            comsHistory: results[9],
            emailComs: results[10],
            smsComs: results[11],
            editRequests: results[12],
        }

        // Sanity Checks, throw error in the following cases
        // ? If event not found
        if(!results[0]) throw new Errors.CommonErrors.NotFoundError('event not found', M.failure.eventNotFound());

        // -> Get Patient
        const patient = await PatientRepo.findById(log, reqId, event.patientId);
        event.patientDetails = patient;

        // -> Get Coordinator with Whitelist
        const coordinator = await UserRepo.getWithWhitelist(log, reqId, event.coordinatorId);
        event.coordinatorDetails = coordinator;

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { event } });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description update an event
 */
router.put('/update', Validators.update_v, Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin, Enums.UserRoles.Coordinator]).bake()), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId, params } = req.body;

    try{

        log.info(reqId, `calling (EventRepo)'findById'`);
        // -> Get Event
        const event = await EventRepo.findById(log, reqId, eventId);

        // Sanity Checks, throw error in the following cases
        // ? If event not found
        // ? If user does not have permission to edit this event
        if(!event) throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventRepo)'update'`);
        // -> Update Event
        await EventRepo.update(log, reqId, eventId, params);

        log.info(reqId, `calling (ScheduledComRuling)'addRuledInScheduledEmailCommunications'`);
        // -> Check and Add Scheduled Email Patient Communications if Required by the Rulings in the Master Data
        await ScheduledComRuling.addRuledInScheduledEmailCommunications(log, reqId, eventId);

        log.info(reqId, `calling (ScheduledComRuling)'addRuledInScheduledSmsCommunications'`);
        // -> Check and Add Scheduled Sms Patient Communications if Required by the Rulings in the Master Data
        await ScheduledComRuling.addRuledInScheduledSmsCommunications(log, reqId, eventId);

        // <> History
        const differences = ObjUtils.getDiff(event, params).map(d => `- Changed "${d.key}" from "${d.oldVal}" to "${d.newVal}"`).join('\n');
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.UpdateEvent, M.history.updatedEvent(eventId, differences), JSON.stringify(event), JSON.stringify(params));
        if(params.medicalClearanceStatus && event.medicalClearanceStatus != params.medicalClearanceStatus) 
            History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.UpdateEventClearance, M.history.updatedEventClearance(eventId, 'medical clearance', event.medicalClearanceStatus, params.medicalClearanceStatus));
        if(params.cardiacClearanceStatus && event.cardiacClearanceStatus != params.cardiacClearanceStatus) 
            History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.UpdateEventClearance, M.history.updatedEventClearance(eventId, 'cardiac clearance', event.cardiacClearanceStatus, params.cardiacClearanceStatus));
        if(params.otherClearanceStatus && event.otherClearanceStatus != params.otherClearanceStatus) 
            History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.UpdateEventClearance, M.history.updatedEventClearance(eventId, 'other clearance', event.otherClearanceStatus, params.otherClearanceStatus));
        if(params.insuranceAuthorizationStatus && event.insuranceAuthorizationStatus != params.insuranceAuthorizationStatus) 
            History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.UpdateEventInsuranceAuthorization, M.history.updatedEventInsuranceAuthorization(eventId, event.insuranceAuthorizationStatus, params.insuranceAuthorizationStatus, params.insuranceAuthorizationNumber));

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description set event as Boarding Slip Generated
 */
router.post('/setAsBSG', upload('boarding-slips'), Validators.setAsBSG_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;

    try {

        const { eventId } = req.body;

        log.info(reqId, `calling (EventRepo)'findById'`);
        // -> Get Event
        const event = await EventRepo.findById(log, reqId, eventId);

        // Sanity Checks, throw error in the following cases
        // ? If event not found
        // ? If user does not have permission to edit this event
        if(!event) throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventRepo)'setAsBSG'`);
        // -> Update Event As BSG
        await EventRepo.setAsBSG(log, reqId, eventId);
        try {
            await updateGoogleCalendarEvent(event.calendar_id, Enums.EventStatuses.BoardingSlipGenerated)
        } catch (err) {
            log.error(err.message)
        }

        const file = req.file;
        if (file) {
            try {
                const fileName = `${eventId}/${moment().unix()}-${file.originalname}`
                await putStream(fileName, readFileSync(file.path));
            } catch (err) {
                log.error('Error uploading file: ' + err.message)
            }
        }

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.UpdateEvent, M.history.updatedEventStatus(eventId, 'Boarding Slip Generated'));
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.GenerateBoardingSlip, M.history.generateBoardingSlip(eventId));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {
        next(e);
    }

})

/**
 * @description set event as Waiting For Confirmation
 */
router.put('/setAsWFC', Validators.setAsWFC_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try{

        log.info(reqId, `calling (EventRepo)'findById'`);
        // -> Get Event
        const event = await EventRepo.findById(log, reqId, eventId);

        // Sanity Checks, throw error in the following cases
        // ? If event not found
        // ? If user does not have permission to edit this event
        if(!event)                                                          throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventRepo)'setAsWFC'`);
        // -> Update Event As WFC
        await EventRepo.setAsWFC(log, reqId, eventId);
        try {
            await updateGoogleCalendarEvent(event.calendar_id, Enums.EventStatuses.WaitingForConfirmation)
        } catch (err) {
            log.error(err.message)
        }

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.UpdateEvent, M.history.updatedEventStatus(eventId, 'Waiting For Confirmation'));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description set event as Confirmed and send patient communications if requested
 */
router.put('/setAsConfirmed', Validators.setAsConfirmed_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try{

        log.info(reqId, `calling (EventRepo)'findById'`);
        // -> Get Event
        const event = await EventRepo.findById(log, reqId, eventId);

        // Sanity Checks, throw error in the following cases
        // ? If event not found
        // ? If user does not have permission to edit this event
        if(!event)                                                          throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventRepo)'setAsConfirmed'`);
        // -> Update Event As Confirmed
        await EventRepo.setAsConfirmed(log, reqId, eventId);
        try {
            await updateGoogleCalendarEvent(event.calendar_id, Enums.EventStatuses.Confirmed);
        } catch (err) {
            log.error(err.message)
        }

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.UpdateEvent, M.history.updatedEventStatus(eventId, 'Confirmed'));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description set event as Confirmed and reschedule and send patient communications if requested
 */
router.put('/setAsConfirmedAndReschedule', Validators.setAsConfirmedAndReschedule_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId, dateFrom, dateTo } = req.body;

    try {

        log.info(reqId, `calling (EventRepo)'findById'`);
        // -> Get Event
        const event = await EventRepo.findById(log, reqId, eventId);

        // Sanity Checks, throw error in the following cases
        // ? If event not found
        // ? If user does not have permission to edit this event
        if(!event)                                                          throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventRepo)'eventsInPeriod'`);
        // -> Get Events that Intersect with this New Event
        const intersectingEvents = await EventRepo.eventsInPeriod(log, reqId, dateFrom, dateTo, event.patientId, event.physicianId, [eventId]);

        // Sanity Checks, throw error in the following cases
        // ? If event not emergency & intersecting events exist
        if(!event.isEmergency && intersectingEvents.length > 0) throw new Errors.CommonErrors.ReservedTimeError(null, null, intersectingEvents);

        log.info(reqId, `calling (EventRepo)'update' to reschedule`);
        // -> Reschedule Event
        await EventRepo.update(log, reqId, eventId, { dateFrom, dateTo });

        log.info(reqId, `calling (EventRepo)'setAsConfirmed'`);
        // -> Update Event As Confirmed
        await EventRepo.setAsConfirmed(log, reqId, eventId);
        try {
            await updateGoogleCalendarEvent(event.calendar_id, Enums.EventStatuses.Confirmed);
        } catch (err) {
            log.error(err.message);
        }

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.UpdateEvent, M.history.setEventAsConfirmedAndRescheduled(eventId, event.dateFrom, dateFrom, dateTo));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description set event as Rejected
 * @deprecated
 */
router.put('/setAsRejected', Validators.setAsRejected_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try{

        log.info(reqId, `calling (EventRepo)'findById'`);
        // -> Get Event
        const event = await EventRepo.findById(log, reqId, eventId);

        // Sanity Checks, throw error in the following cases
        // ? If event not found
        // ? If event status is not "waiting for confirmation"
        if(!event)                                                      throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());
        // if(event.status != Enums.EventStatuses.WaitingForConfirmation)  throw new Errors.CommonErrors.SanityError('event not waiting_for_confirmation', M.failure.eventNotWFC());

        log.info(reqId, `calling (EventRepo)'setAsRejected'`);
        // -> Update Event As Rejected
        await EventRepo.setAsRejected(log, reqId, eventId);
        try {
            await deleteGoogleCalendarEvent(event.calendar_id);
        } catch (err) {
            log.error(err.message)
        }

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.UpdateEvent, M.history.updatedEventStatus(eventId, 'Rejected'));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description set event as Completed
 */
router.put('/setAsCompleted', Validators.setAsCompleted_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try{

        log.info(reqId, `calling (EventRepo)'findById'`);
        // -> Get Event
        const event = await EventRepo.findById(log, reqId, eventId);

        // Sanity Checks, throw error in the following cases
        // ? If event not found
        // ? If user is not the physician and does not have permission to edit this event
        if(!event)                                                          throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());
        if(!(req.user?.roles?.includes(Enums.UserRoles.Physician) && event.physicianId == req.userId) && !(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventRepo)'setAsCompleted'`);
        // -> Update Event As Completed
        await EventRepo.setAsCompleted(log, reqId, eventId);
        try {
            await updateGoogleCalendarEvent(event.calendar_id, Enums.EventStatuses.Completed);
        } catch (err) {
            log.error(err.message)
        }

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.UpdateEvent, M.history.updatedEventStatus(eventId, 'Completed'));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description delete event
 */
router.post('/deleteEvent', Validators.deleteEvent_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try {

        log.info(reqId, `calling (EventRepo)'findById'`);
        // -> Get Event
        const event = await EventRepo.findById(log, reqId, eventId);

        // Sanity Checks, throw error in the following cases
        // ? If event not found
        // ? If event status is not "pending"
        // ? If user does not have permission to edit this event
        if(!event)                                      throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());
        if(event.status != Enums.EventStatuses.Pending) throw new Errors.CommonErrors.SanityError('event not pending', M.failure.eventNotPendingCannotDelete());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventRepo)'deleteEvent'`);
        // -> Delete Event
        await EventRepo.deleteEvent(log, reqId, eventId);
        try {
            await deleteGoogleCalendarEvent(event.calendar_id);
        } catch (err) {
            log.error(err.message)
        }

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.deletedEvent.title(), M.pn.deletedEvent.message(), { type: Enums.PnEventTypes.DeletedEvent, eventId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.DeleteEvent, M.history.deletedEvent(eventId, event.patientName, event.physicianName, event.facilityName, event.dateFrom), JSON.stringify(event));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description cancel an event
 */
 router.post('/cancelEvent', Validators.cancelEvent_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId, cancelReason, cancelNote } = req.body;

    try {

        log.info(reqId, `calling (EventRepo)'findById'`);
        // -> Get Event
        const event = await EventRepo.findById(log, reqId, eventId);

        // Sanity Checks, throw error in the following cases
        // ? If event not found
        // ? If event status is "pending"
        // ? If user does not have permission to edit this event
        if(!event)                                       throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());
        if(!event.status == Enums.EventStatuses.Pending) throw new Errors.CommonErrors.SanityError('event still pending', M.failure.eventStillPendingCannotCancel());
        if(!(req.user?.roles?.includes(Enums.UserRoles.Physician) && event.physicianId == req.userId) && !(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventRepo)'cancelEvent'`);
        // -> Cancel Event
        await EventRepo.cancelEvent(log, reqId, eventId, cancelReason, req.userId, cancelNote);
        try {
            await deleteGoogleCalendarEvent(event.calendar_id);
        } catch (err) {
            log.error(err.message)
        }

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.CancelEvent, M.history.cancelEvent(eventId, cancelReason));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description complete an event while specifying which procedures were actually done
 */
router.post('/codeEvent', Validators.codeEvent_v, Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Physician]).bake()), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId, selectedProcedures } = req.body;

    try {

        log.info(reqId, `calling (EventRepo)'findById'`);
        // -> Get Event
        const event = await EventRepo.findById(log, reqId, eventId);

        // Sanity Checks, throw error in the following cases
        // ? If event not found
        // ? If event status is not "confirmed"
        if(!event)                                                                                  throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());
        if(![Enums.EventStatuses.Confirmed, Enums.EventStatuses.Completed].includes(event.status))  throw new Errors.CommonErrors.SanityError('event not confirmed or completed', M.failure.eventNotConfirmedOrCompleted());

        log.info(reqId, `calling (EventRepo)'setAsCompleted'`);
        // -> Set As Completed
        await EventRepo.setAsCompleted(log, reqId, eventId);
        try {
            await updateGoogleCalendarEvent(event.calendar_id, Enums.EventStatuses.Completed);
        } catch (err) {
            log.error(err.message)
        }

        log.info(reqId, `calling (EventProcedureRepo)'deleteForEvent'`);
        // -> Delete Event Procedures
        await EventProcedureRepo.deleteForEvent(log, reqId, eventId);

        log.info(reqId, `calling (EventProcedureRepo)'addMany'`);
        // -> Add New Event Procedures
        await EventProcedureRepo.addMany(log, reqId, eventId, selectedProcedures);

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.CodeEvent, M.history.codeEvent(eventId, selectedProcedures));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description reschedule an event
 */
router.post('/reschedule', Validators.reschedule_v, Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin, Enums.UserRoles.Coordinator]).bake()), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId, dateFrom, dateTo, physicianId, physicianName, physicianPhone, physicianFax, physicianOfficePhone, physicianOfficeAddress, isEmergency } = req.body;

    try{

        log.info(reqId, `calling (EventRepo)'findById'`);
        // -> Get Event
        const event = await EventRepo.findById(log, reqId, eventId);

        // Sanity Checks, throw error in the following cases
        // ? If event not found
        // ? If user does not have permission to edit this event
        if(!event) throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventRepo)'eventsInPeriod'`);
        // -> Get Events that Intersect with this New Event
        const intersectingEvents = await EventRepo.eventsInPeriod(log, reqId, dateFrom, dateTo, event.patientId, physicianId, [eventId]);

        // Sanity Checks, throw error in the following cases
        // ? If event not emergency & intersecting events exist
        if(!isEmergency && intersectingEvents.length > 0) throw new Errors.CommonErrors.ReservedTimeError(null, null, intersectingEvents);

        // Update Params
        const params = {
            dateFrom, 
            dateTo,
            isEmergency
        }
        if(event.physicianId != physicianId) {
            params.physicianId = physicianId;
            params.physicianName = physicianName;
            params.physicianPhone = physicianPhone;
            params.physicianFax = physicianFax;
            params.physicianOfficePhone = physicianOfficePhone;
            params.physicianOfficeAddress = physicianOfficeAddress;
        }

        log.info(reqId, `calling (EventRepo)'update'`);
        // -> Reschedule Event
        await EventRepo.update(log, reqId, eventId, params);
        try {
            await updateGoogleCalendarEvent(event.calendar_id, event.status, params.dateFrom, params.dateTo);
        } catch (err) {
            log.error(err)
        }

        // <> History
        const physicianDiff = event.physicianId != physicianId ? physicianName : null;
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.RescheduleEvent, M.history.rescheduledEvent(eventId, dateFrom, physicianDiff), null, null, null, null, event.patientId);
        
        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId }, { excludeUserId: req.userId });
        
        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description send patient communications of event as email and/or sms template(s) selected
 * @note this is intended as a force send. Confirmation of an event is the typical way to send patient communications
 */
router.post('/sendPatientComs', Auth.auth(), Validators.sendPatientComs_v, TemplatesInjector.injectGeneralTemplates, async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId, emailTemplateIds, smsTemplateIds } = req.body;

    try{

        log.info(reqId, `calling (EventRepo)'findById'`);
        // -> Get Event
        const event = await EventRepo.findById(log, reqId, eventId);

        // Sanity Checks, throw error in the following cases
        // ? If event not found
        // ? If user does not have permission to edit this event
        if(!event) throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        // Filter email and sms templates to be sent
        let emailTemplates = req.emailTemplates?.filter(t => emailTemplateIds?.some(t2 => t2 == t.id));
        let smsTemplates = req.smsTemplates?.filter(t => smsTemplateIds?.some(t2 => t2 == t.id));

        // If emails should be sent
        if(emailTemplates?.length > 0) {

            // Sanity Checks, throw error in the following cases
            // ? If event patient email not set
            if(!event.patientEmail) throw new Errors.CommonErrors.SanityError('event patient email not set', M.failure.eventPatientEmailNotSet());

            log.info(reqId, `gather (SendPatientComOperations)'sendEmail' promises`);
            // Gather Email Promises
            const emailPromises = emailTemplates.map(t => SendPatientComOperations.sendEmail(log, reqId, t, event, req.userId));

            log.info(reqId, `send patient com emails`);
            // -> Send Patient Com Emails (which also adds a record to the database)
            await Promise.all(emailPromises);
        }

        // If sms should be sent
        if(smsTemplates?.length > 0) {

            // Sanity Checks, throw error in the following cases
            // ? If event patient phone not set
            if(!event.patientPreferredPhone) throw new Errors.CommonErrors.SanityError('event patient phone not set', M.failure.eventPatientPhoneNotSet());

            log.info(reqId, `gather (SendPatientComOperations)'sendSms' promises`);
            // Gather Sms Promises
            const smsPromises = smsTemplates.map(t => SendPatientComOperations.sendSms(log, reqId, t, event, req.userId));

            log.info(reqId, `send patient com SMSs`);
            // -> Send Patient Com SMSs (which also adds a record to the database)
            await Promise.all(smsPromises);
        }

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId }, { excludeUserId: req.userId });

        // <> History
        // const sentComTitles = coms.map(c => c.title).join('\n');
        // const methods = (sendPatientComEmail && sendPatientComSms) ? 'Email & SMS' : sendPatientComEmail ? 'Email' : 'SMS';
        // History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.SendPatientCommunication, M.history.sentPatientCommunication(eventId, sentComTitles, methods), JSON.stringify(coms));

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description send boarding slip email to facility
 */
router.post('/sendBoardingSlipEmailToFacility', Auth.auth(), TemplatesInjector.injectBSEmailTemplate, async (req, res, next) => {

    const { log, reqId } = req;

    upload('facility-emails')(req, res, async function (err) {

        try {

            // Sanity Checks, throw errors in the following cases
            // ? If multer returned an error
            // ? If general error was returned
            if (err instanceof multer.MulterError)  throw new Errors.FileUploadErrors.FileUploadError(err.message, err);
            else if (err)                           throw new Errors.FileUploadErrors.FileUploadError(err.message, err);

            // Insert params
            const { eventId } = req.body;
            
            // Sanity Checks, throw if error in the following cases
            // ? If eventId not passed
            // ? If file not uploaded
            // ? If user does not have permission to edit this event
            // ? If boarding slip email template not set
            if(!eventId) throw new Errors.CommonErrors.SanityError('event id required', M.failure.eventNotFound());
            if(!req.file) throw new Errors.CommonErrors.SanityError('boarding slip pdf required', M.failure.boardingSlipFileRequired());
            if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();
            if(!req.bsEmailTemplate) throw new Errors.CommonErrors.SanityError('boarding slip email template not set', M.failure.boardingSlipEmailTemplateNotSet());

            const filepath = req.file.path.replace(/^public[\\\/]/, '').replace(`\\`, '/');
            const fileLocation = path.join(global.appRoot, 'public', filepath)

            log.info(reqId, `calling (EventRepo)'findById'`);
            // -> Get Event
            const event = await EventRepo.findById(log, reqId, eventId);

            // Sanity Checks, throw error in the following cases
            // ? If event not found
            // ? If event's facility does not have a scheduling email
            if(!event)                          throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());
            if(!event.facilitySchedulingEmail)  throw new Errors.CommonErrors.SanityError('event facility does not have a scheduling email', M.failure.facilityHasNoSchedulingEmail());
            
            const emailTemplate = req.bsEmailTemplate;

            log.info(reqId, `calling (EmailTemplates)'facilityBoardingSlip'`);
            // Get email content
            const subject = emailTemplate.subject;
            const comsEmailHtml = EmailTemplates.facilityBoardingSlip(emailTemplate.body, event);
            const attachments = [
                {
                    filename: path.basename(filepath),
                    path: fileLocation
                }
            ]

            log.info(reqId, `calling (mail)'send'`);
            // -> Send Facility BS Email
            await Mail.send(log, reqId, event.facilitySchedulingEmail, subject, comsEmailHtml, attachments);

            // Record params
            const recordParams = {
                eventId,
                userId: req.userId,
                facilitySchedulingEmail: event.facilitySchedulingEmail,
                emailBody: comsEmailHtml,
                bsFile: filepath
            }

            log.info(reqId, `calling (EventFacilityBSEmail)'addRecord'`);
            // -> Add Record
            const newRecord = await EventFacilityBSEmail.addRecord(log, reqId, recordParams);

            // <> PN
            PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId }, { excludeUserId: req.userId });

            // End
            log.info(reqId, `end [success]`);
            History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.SendBoardingSlipFacilityEmail, M.history.sentBoardingSlipFacilityEmail(eventId, event.facilitySchedulingEmail), JSON.stringify(newRecord));
            res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { record: newRecord } });
        
        }
        catch(e) {

            next(e);
        }
    })

})


/**
 * @description update an event's patient details to match his current details in his patients table row
 */
router.post('/syncEventPatientWithLocalPatient', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin, Enums.UserRoles.Coordinator])), Validators.syncEventPatientWithLocalPatient_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try{

        log.info(reqId, `calling (EventRepo)'findById'`);
        // -> Get Event
        const event = await EventRepo.findById(log, reqId, eventId);

        // Sanity Checks, throw error in the following cases
        // ? If event not found
        // ? If user does not have permission to edit this event
        if(!event)   throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (PatientRepo)'findById'`);
        // -> Get Patient
        const patient = await PatientRepo.findById(log, reqId, event.patientId);

        // Sanity Checks, throw error in the following cases
        // ? If patient not found
        if(!patient) throw new Errors.CommonErrors.SanityError('patient not found', M.failure.patientNotFound());

        // Update Params
        const updateParams = {};
        if(event.patientFirstName != patient.firstName)                 updateParams.patientFirstName = patient.firstName;
        if(event.patientMiddleName != patient.middleName)               updateParams.patientMiddleName = patient.middleName;
        if(event.patientLastName != patient.lastName)                   updateParams.patientLastName = patient.lastName;
        if(!moment(event.patientDoB).isSame(patient.dateofbirth))       updateParams.patientDoB = patient.dateofbirth;
        if(event.patientGender != patient.sex)                          updateParams.patientGender = patient.sex;
        if(event.patientPreferredPhone != patient.phone)                updateParams.patientPreferredPhone = patient.phone;
        if(event.patientEmail != patient.email)                         updateParams.patientEmail = patient.email;
        if(event.patientSSN != patient.ssn)                             updateParams.patientSSN = patient.ssn;
        if(event.patientAddress1 != patient.address1)                   updateParams.patientAddress1 = patient.address1;
        if(event.patientAddress2 != patient.address2)                   updateParams.patientAddress2 = patient.address2;
        if(event.patientCity != patient.city)                           updateParams.patientCity = patient.city;
        if(event.patientState != patient.state)                         updateParams.patientState = patient.state;
        if(event.patientZipCode != patient.zipcode)                     updateParams.patientZipCode = patient.zipcode;
        if(event.carrierId != patient.carrierId)                        updateParams.carrierId = patient.carrierId;
        if(event.carrierName != patient.carrierName)                    updateParams.carrierName = patient.carrierName;
        if(event.carrierCode != patient.carrierCode)                    updateParams.carrierCode = patient.carrierCode;
        if(event.subscriberId != patient.subscriberId)                  updateParams.subscriberId = patient.subscriberId;
        if(event.groupName != patient.groupName)                        updateParams.groupName = patient.groupName;
        if(event.groupNumber != patient.groupNumber)                    updateParams.groupNumber = patient.groupNumber;
        if(event.carrierId2 != patient.carrierId2)                      updateParams.carrierId2 = patient.carrierId2;
        if(event.carrierName2 != patient.carrierName2)                  updateParams.carrierName2 = patient.carrierName2;
        if(event.carrierCode2 != patient.carrierCode2)                  updateParams.carrierCode2 = patient.carrierCode2;
        if(event.subscriberId2 != patient.subscriberId2)                updateParams.subscriberId2 = patient.subscriberId2;
        if(event.groupName2 != patient.groupName2)                      updateParams.groupName2 = patient.groupName2;
        if(event.groupNumber2 != patient.groupNumber2)                  updateParams.groupNumber2 = patient.groupNumber2;

        // Remove Empty Updates
        for(let k of Object.keys(updateParams)) {
            if(updateParams[k] === undefined || updateParams[k] === null || updateParams[k] === '') delete updateParams[k];
        }

        log.info(reqId, `calling (EventRepo)'findById'`);
        // -> Update Event
        await EventRepo.update(log, reqId, eventId, updateParams);

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId }, { excludeUserId: req.userId });

        // <> History
        const differences = ObjUtils.getDiff(event, updateParams).map(d => `- Changed "${d.key}" from "${d.oldVal}" to "${d.newVal}"`).join('\n');
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.SyncEventPatientDetails, M.history.syncedEventPatientDetails(eventId, differences), JSON.stringify(event), JSON.stringify(updateParams), JSON.stringify(patient));

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})

router.get('/:eventId/boarding-slips', Auth.auth(), async (req, res, next) => {
    const { log, reqId } = req;
    const { eventId } = req.params;

    try {
        const event = await EventRepo.findById(log, reqId, eventId);

        // Sanity Checks, throw error in the following cases
        // ? If event not found
        if(!event) throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());

        try {
            const { Contents } = await listBucket(eventId);
            res.status(Enums.HttpCodes.Success).json({
                message: M.response.success(),
                data: Contents?.length ? Contents.map(slip => ({
                    path: slip.Key,
                    name: slip.Key.split('/')[1].split('-')[0]
                })) : []
            });
        } catch (err) {
            log.error('Error Getting bucket: ' + err.message)
        }
    } catch (e) {
        next(e);
    }
})

router.delete('/boarding-slip', Auth.auth(), async (req, res, next) => {
    const { log } = req;
    const { path } = req.query;

    // Sanity Checks, throw error in the following cases
    // ? If path is not set
    if(!path) throw new Errors.CommonErrors.SanityError('path not set', M.failure.boardingSlipPathNotSet());

    try {
        await deleteFile(decodeURIComponent(path));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success() });
    } catch (err) {
        log.error('Error Getting bucket: ' + err.message)
        next();
    }
})


/**
 * Exports
 */
module.exports = router;