/**
 * @description event diagnosis routes
 */


const express = require('express');
const router = express.Router();
const M = require('../../../resources/languages/messages');
const Enums = require('../../../common/enums');
const Auth = require('../../../common/middleware/auth.middleware');
const Validators = require('./events.diagnosis.v');
const EventDiagnosisRepo = require('../../../repos/event.diagnosis.repo');
const History = require('../../../repos/history.repo');
const { CommonErrors, AuthErrors } = require('../../../common/errors/_CustomErrors');
const ObjectUtils = require('../../../common/utils/object.utils');
const ScheduledComRuling = require('../../../operations/scheduledComsRuling');
const PnOperations = require('../../../operations/notifications');
const EventEditPerm = require('../../../operations/eventEditPerm');


/**
 * @description get event diagnosis
 */
router.post('/get', Validators.get_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try{

        log.info(reqId, `calling (EventDiagnosisRepo)'getForEvent'`);
        // -> Get Event Diagnosis
        const diagnosis = await EventDiagnosisRepo.getForEvent(log, reqId, eventId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { diagnosis } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description add event diagnosis
 */
router.post('/addOne', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin, Enums.UserRoles.Coordinator]).bake()), Validators.addOne_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId, diagnosisId, diagnosisCode, diagnosisDescription } = req.body;

    try{

        // Sanity Checks, throw error in the following cases
        // ? If user does not have permission to edit this event
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventDiagnosisRepo)'add'`);
        // -> Add Event Diagnosis
        const newDiagnosis = await EventDiagnosisRepo.add(log, reqId, { eventId, diagnosisId, diagnosisCode, diagnosisDescription });

        log.info(reqId, `calling (ScheduledComRuling)'addRuledInScheduledEmailCommunications'`);
        // -> Check and Add Scheduled Email Patient Communications if Required by the Rulings in the Master Data
        await ScheduledComRuling.addRuledInScheduledEmailCommunications(log, reqId, eventId);

        log.info(reqId, `calling (ScheduledComRuling)'addRuledInScheduledSmsCommunications'`);
        // -> Check and Add Scheduled Sms Patient Communications if Required by the Rulings in the Master Data
        await ScheduledComRuling.addRuledInScheduledSmsCommunications(log, reqId, eventId);

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.AddEventDiagnosis, M.history.addedEventDiagnosis(eventId, diagnosisDescription));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { diagnosis: newDiagnosis } });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description update event diagnosis
 */
router.post('/updateOne', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin, Enums.UserRoles.Coordinator]).bake()), Validators.updateOne_v, async (req, res, next) => {

    const { log, reqId } = req;
    const params = req.body;

    try{

        log.info(reqId, `calling (EventDiagnosisRepo)'getForEvent'`);
        // -> Get Event Diagnosis
        const eventDiagnosis = await EventDiagnosisRepo.findById(log, reqId, params.id);

        // Sanity Checks, throw errors in the following cases
        // ? If event diagnosis not found
        // ? If user does not have permission to edit this event
        if(!eventDiagnosis) throw new CommonErrors.SanityError('event diagnosis not found', M.failure.eventDiagnosisNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventDiagnosis.eventId))) throw new AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventDiagnosisRepo)'updateOne'`);
        // -> Update Event Diagnosis
        await EventDiagnosisRepo.updateOne(log, reqId, params);

        // History
        const differences = ObjectUtils.getDiff(eventDiagnosis, params).map(d => `- Changed "${d.key}" from "${d.oldVal}" to "${d.newVal}"`).join('\n');
        History.add(log, reqId, req.userId, eventDiagnosis.eventId, Enums.HistoryEvents.UpdateEventDiagnosis, M.history.updatedEventDiagnosis(eventDiagnosis.eventId, eventDiagnosis.diagnosisDescription, differences));

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description delete event diagnosis
 */
router.post('/deleteOne', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin, Enums.UserRoles.Coordinator]).bake()), Validators.deleteOne_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { eventDiagnosisId } = req.body;

    try{

        log.info(reqId, `calling (EventDiagnosisRepo)'getForEvent'`);
        // -> Get Event Diagnosis
        const eventDiagnosis = await EventDiagnosisRepo.findById(log, reqId, eventDiagnosisId);

        // Sanity Checks, throw errors in the following cases
        // ? If event diagnosis not found
        // ? If user does not have permission to edit this event
        if(!eventDiagnosis) throw new CommonErrors.SanityError('event diagnosis not found', M.failure.eventDiagnosisNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventDiagnosis.eventId))) throw new AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventDiagnosisRepo)'deleteOne'`);
        // -> Delete Event Diagnosis
        await EventDiagnosisRepo.deleteOne(log, reqId, eventDiagnosisId);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventDiagnosis.eventId, Enums.HistoryEvents.DeleteEventDiagnosis, M.history.deletedEventDiagnosis(eventDiagnosis.eventId, eventDiagnosis.diagnosisDescription), JSON.stringify(eventDiagnosis));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;