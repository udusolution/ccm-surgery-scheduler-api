/**
 * @description validation middleware for event diagnosis endpoints
 */


const Validator = require('../../../common/utils/validators/validator');
const Matcher = require('../../../common/utils/patternMatcher');
const Errors = require('../../../common/errors/_CustomErrors');
const ValidationRules = require('../../../config/validation.rules');


function get_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/diagnosis/get_v';
    const { eventId } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);
        const errors = [];

        // Validations
        const [v1IsValid, v1ErrorMsg] = new Validator.Validate(eventId, 'event id').required().isNumber().bake();

        // Check
        if(!v1IsValid) errors.push(v1ErrorMsg);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function addOne_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/diagnosis/addOne_v';
    const { eventId, diagnosisId, diagnosisCode, diagnosisDescription } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().isNumber().bake(),
            new Validator.Validate(diagnosisId, 'diagnosis id').isNumber().bake(),
            new Validator.Validate(diagnosisCode, 'diagnosis code').isString().bake(),
            new Validator.Validate(diagnosisDescription, 'diagnosis description').isString().minLength(ValidationRules.Diagnosis.description.minLength).maxLength(ValidationRules.Diagnosis.description.maxLength).bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function updateOne_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/diagnosis/updateOne_v';
    const { id, diagnosisCode, diagnosisDescription } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(id, 'id').required().bake(),
            new Validator.Validate(diagnosisCode, 'diagnosis code').isString().minLength(ValidationRules.Code.minLength).maxLength(ValidationRules.Code.maxLength).bake(),
            new Validator.Validate(diagnosisDescription, 'diagnosis description').isString().minLength(ValidationRules.Diagnosis.description.minLength).maxLength(ValidationRules.Diagnosis.description.maxLength).bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function deleteOne_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/diagnosis/deleteOne_v';
    const { eventDiagnosisId } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventDiagnosisId, 'event diagnosis id').required().isNumber().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    get_v,
    updateOne_v,
    addOne_v,
    deleteOne_v
}