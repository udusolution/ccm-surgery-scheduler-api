/**
 * @description event auth notes routes
 */


const express = require('express');
const router = express.Router();
const Errors = require('../../../common/errors/_CustomErrors');
const M = require('../../../resources/languages/messages');
const Enums = require('../../../common/enums');
const Auth = require('../../../common/middleware/auth.middleware');
const Validators = require('./events.authNotes.v');
const EventAuthNoteRepo = require('../../../repos/event.authNote.repo');
const History = require('../../../repos/history.repo');
const { CommonErrors } = require('../../../common/errors/_CustomErrors');
const PnOperations = require('../../../operations/notifications');
const EventEditPerm = require('../../../operations/eventEditPerm');


/**
 * @description get event auth notes
 */
router.post('/get', Validators.get_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try{

        log.info(reqId, `calling (EventAuthNoteRepo)'getForEvent'`);
        // -> Get Event Auth Notes
        const notes = await EventAuthNoteRepo.getForEvent(log, reqId, eventId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { notes } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description add event auth note
 */
router.post('/addOne', Validators.addOne_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId, note } = req.body;

    try{

        // Sanity Checks, throw error in the following cases
        // ? If user does not have permission to edit this event
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventNoteRepo)'add'`);
        // -> Add Event Note
        const newNote = await EventAuthNoteRepo.add(log, reqId, { eventId, userId: req.userId, note });

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.AddEventAuthNote, M.history.addedEventAuthNote(eventId, note));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { note: newNote } });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description delete an event auth note
 */
router.post('/deleteOne', Auth.auth(), Validators.deleteOne_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { noteId } = req.body;

    try{

        log.info(reqId, `calling (EventAuthNoteRepo)'findById'`);
        // -> Get Event Auth Note
        const note = await EventAuthNoteRepo.findById(log, reqId, noteId);

        // Sanity Checks, throw errors in the following cases
        // ? If note not found
        // ? If note creator is not the logged in user
        // ? If user does not have permission to edit this event
        if(!note)                     throw new CommonErrors.SanityError('auth note not found', M.failure.eventAuthNoteNotFound());
        if(note.userId != req.userId) throw new CommonErrors.SanityError('auth note not yours to delete', M.failure.eventAuthNoteNotYours());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, note.eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventAuthNoteRepo)'deleteOne'`);
        // -> Delete Event Auth Note
        await EventAuthNoteRepo.deleteOne(log, reqId, noteId);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, note.eventId, Enums.HistoryEvents.DeleteEventAuthNote, M.history.deletedEventAuthNote(note.eventId, note.note), JSON.stringify(note));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})

/**
 * Exports
 */
module.exports = router;