/**
 * @description validation middleware for event endpoints
 */


const Validator = require('../../../common/utils/validators/validator');
const Matcher = require('../../../common/utils/patternMatcher');
const Errors = require('../../../common/errors/_CustomErrors');
const ValidationRules = require('../../../config/validation.rules');


function get_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/clearances/get_v';
    const { eventId } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function addOne_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/clearances/addOne_v';
    const { eventId, clearanceId, clearanceName, clearanceDescription, physicianName, assigneeId, dueDate, status } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake(),
            new Validator.Validate(clearanceId, 'clearance id').bake(),
            new Validator.Validate(clearanceName, 'clearance name').required().isString().minLength(ValidationRules.Clearance.name.minLength).maxLength(ValidationRules.Clearance.name.maxLength).bake(),
            new Validator.Validate(clearanceDescription, 'clearance description').required().isString().minLength(ValidationRules.Clearance.description.minLength).maxLength(ValidationRules.Clearance.description.maxLength).bake(),
            new Validator.Validate(physicianName, 'physician name').isString().minLength(ValidationRules.Clearance.physician.minLength).maxLength(ValidationRules.Clearance.physician.maxLength).bake(),
            new Validator.Validate(assigneeId, 'assignee').required().bake(),
            new Validator.Validate(dueDate, 'due date').isDate().required().isDate().bake(),
            new Validator.Validate(status, 'status').isString().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}

function updateOne_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/clearances/updateOne_v';
    const { id, clearanceName, clearanceDescription, physicianName, assigneeId, dueDate, status, rejectionReason, statusUpdatedOn, statusUpdaterId } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(id, 'id').required().bake(),
            new Validator.Validate(clearanceName, 'clearance name').isString().minLength(ValidationRules.Clearance.name.minLength).maxLength(ValidationRules.Clearance.name.maxLength).bake(),
            new Validator.Validate(clearanceDescription, 'clearance description').isString().minLength(ValidationRules.Clearance.description.minLength).maxLength(ValidationRules.Clearance.description.maxLength).bake(),
            new Validator.Validate(physicianName, 'physician name').isString().minLength(ValidationRules.Clearance.physician.minLength).maxLength(ValidationRules.Clearance.physician.maxLength).bake(),
            new Validator.Validate(assigneeId, 'assignee').bake(),
            new Validator.Validate(dueDate, 'due date').isDate().bake(),
            new Validator.Validate(status, 'status').isString().bake(),
            new Validator.Validate(rejectionReason, 'rejection reason').isString().minLength(ValidationRules.Clearance.rejectionReason.minLength).maxLength(ValidationRules.Clearance.rejectionReason.maxLength).bake(),
            new Validator.Validate(statusUpdatedOn, 'status update date').isDate().bake(),
            new Validator.Validate(statusUpdaterId, 'status updater').bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function deleteOne_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/clearances/deleteOne_v';
    const { id } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(id, 'id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    get_v,
    addOne_v,
    updateOne_v,
    deleteOne_v
}