/**
 * @description event clearances routes
 */


const express = require('express');
const router = express.Router();
const M = require('../../../resources/languages/messages');
const Enums = require('../../../common/enums');
const Auth = require('../../../common/middleware/auth.middleware');
const Validators = require('./events.clearances.v');
const EventClearanceRepo = require('../../../repos/event.clearance.repo');
const History = require('../../../repos/history.repo');
const { CommonErrors, AuthErrors } = require('../../../common/errors/_CustomErrors');
const PnOperations = require('../../../operations/notifications');
const EventEditPerm = require('../../../operations/eventEditPerm');


/**
 * @description get event clearances
 */
router.post('/get', Validators.get_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try{

        log.info(reqId, `calling (EventClearanceRepo)'getForEvent'`);
        // -> Get Event Checklist
        const clearances = await EventClearanceRepo.getForEvent(log, reqId, eventId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { clearances } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description add event clearances item
 */
router.post('/addOne', Validators.addOne_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const params = req.body;

    try{

        // Sanity Checks, throw error in the following cases
        // ? If user does not have permission to edit this event
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, params.eventId))) throw new AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventChecklistRepo)'add'`);
        // -> Add Event Clearance
        const newItem = await EventClearanceRepo.add(log, reqId, params);

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId: params.eventId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, params.eventId, Enums.HistoryEvents.AddEventClearance, M.history.addedEventClearance(params.eventId, params.clearanceName));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { clearance: newItem } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description update event clearances item
 */
router.put('/updateOne', Validators.updateOne_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const params = req.body;

    try {

        log.info(reqId, `calling (EventClearanceRepo)'findById'`);
        // -> Get Event Clearance
        const eventClearance = await EventClearanceRepo.findById(log, reqId, params.id);

        // Sanity Checks, throw error in the following cases
        // ? If event clearance not found
        // ? If user does not have permission to edit this event
        if(!eventClearance) throw new CommonErrors.SanityError('event clearance not found', M.failure.eventClearanceNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventClearance.eventId))) throw new AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventClearanceRepo)'update'`);
        // -> Update Event Clearance
        await EventClearanceRepo.update(log, reqId, params);

        // <> History
        History.add(log, reqId, req.userId, params.eventId, Enums.HistoryEvents.UpdateEventExtraClearance, M.history.updatedEventExtraClearance(params.eventId, eventClearance.clearanceName), JSON.stringify({ old: eventClearance, new: params }));
        if(params.status)
            History.add(log, reqId, req.userId, params.eventId, Enums.HistoryEvents.UpdateEventExtraClearance, M.history.updatedEventExtraClearanceStatus(params.eventId, eventClearance.clearanceName, eventClearance.status, params.status, params.rejectionReason));

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description delete event clearance
 */
router.post('/deleteOne', Validators.deleteOne_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { id } = req.body;

    try {

        log.info(reqId, `calling (EventClearanceRepo)'findById'`);
        // -> Get Event Clearance
        const clearance = await EventClearanceRepo.findById(log, reqId, id);

        // Sanity Checks, throw error in the following cases
        // ? If event clearance not found
        // ? If user does not have permission to edit this event
        if(!clearance) throw new CommonErrors.SanityError('event clearance not found', M.failure.eventClearanceNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, clearance.eventId))) throw new AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventClearanceRepo)'deleteOne'`);
        // -> Delete Event Checklist Item
        await EventClearanceRepo.deleteOne(log, reqId, id);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, clearance.eventId, Enums.HistoryEvents.DeleteEventClearance, M.history.deletedEventClearance(clearance.eventId, clearance.clearanceName), JSON.stringify(clearance));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;