/**
 * @description master data routes
 */


const express = require('express');
const router = express.Router();
const M = require('../../../resources/languages/messages');
const Enums = require('../../../common/enums');
const Auth = require('../../../common/middleware/auth.middleware');
const Validators = require('./events.procedures.v');
const EventProcedureRepo = require('../../../repos/event.procedure.repo');
const History = require('../../../repos/history.repo');
const ObjectUtils = require('../../../common/utils/object.utils');
const { CommonErrors, AuthErrors } = require('../../../common/errors/_CustomErrors');
const ScheduledComRuling = require('../../../operations/scheduledComsRuling');
const PnOperations = require('../../../operations/notifications');
const EventEditPerm = require('../../../operations/eventEditPerm');


/**
 * @description get event procedures
 */
router.post('/get', Auth.auth(), Validators.get_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try{

        log.info(reqId, `calling (EventProcedureRepo)'getForEvent'`);
        // -> Get Event Procedures
        const procedures = await EventProcedureRepo.getForEvent(log, reqId, eventId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { procedures } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description add event procedure
 */
router.post('/addOne', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin, Enums.UserRoles.Coordinator]).bake()), Validators.addOne_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId, procedureId, procedureCode, procedureName } = req.body;

    try{

        // Sanity Checks, throw error in the following cases
        // ? If user does not have permission to edit this event
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventProcedureRepo)'add'`);
        // -> Add Event Procedure
        const newProcedure = await EventProcedureRepo.add(log, reqId, { eventId, procedureId, procedureCode, procedureName });

        log.info(reqId, `calling (ScheduledComRuling)'addRuledInScheduledEmailCommunications'`);
        // -> Check and Add Scheduled Email Patient Communications if Required by the Rulings in the Master Data
        await ScheduledComRuling.addRuledInScheduledEmailCommunications(log, reqId, eventId);

        log.info(reqId, `calling (ScheduledComRuling)'addRuledInScheduledSmsCommunications'`);
        // -> Check and Add Scheduled Sms Patient Communications if Required by the Rulings in the Master Data
        await ScheduledComRuling.addRuledInScheduledSmsCommunications(log, reqId, eventId);

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.AddEventProcedure, M.history.addedEventProcedure(eventId, procedureCode, procedureName), JSON.stringify(newProcedure));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { procedure: newProcedure } });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description update event procedure
 */
router.post('/updateOne', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin, Enums.UserRoles.Coordinator]).bake()), Validators.updateOne_v, async (req, res, next) => {

    const { log, reqId } = req;
    const params = req.body;

    try{

        log.info(reqId, `calling (EventProcedureRepo)'findById'`);
        // -> Get Event Procedure
        const eventProcedure = await EventProcedureRepo.findById(log, reqId, params.id);

        // Sanity Checks, throw error in the following cases
        // ? If event procedure not found
        // ? If user does not have permission to edit this event
        if(!eventProcedure) throw new CommonErrors.SanityError('event procedure not found', M.failure.eventProcedureNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventProcedure.eventId))) throw new AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventProcedureRepo)'updateOne'`);
        // -> Update Event Procedure
        await EventProcedureRepo.updateOne(log, reqId, params);

        // History
        const differences = ObjectUtils.getDiff(eventProcedure, params).map(d => `- Changed "${d.key}" from "${d.oldVal}" to "${d.newVal}"`).join('\n');
        History.add(log, reqId, req.userId, eventProcedure.eventId, Enums.HistoryEvents.UpdateEventProcedure, M.history.updatedEventProcedure(eventProcedure.eventId, eventProcedure.procedureName, differences), JSON.stringify(eventProcedure), JSON.stringify(params));

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description add event procedure
 */
router.post('/deleteOne', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin, Enums.UserRoles.Coordinator]).bake()), Validators.deleteOne_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { eventProcedureId } = req.body;

    try{

        log.info(reqId, `calling (EventProcedureRepo)'findById'`);
        // -> Get Event Procedure
        const eventProcedure = await EventProcedureRepo.findById(log, reqId, eventProcedureId);

        // Sanity Checks, throw error in the following cases
        // ? If event procedure not found
        // ? If user does not have permission to edit this event
        if(!eventProcedure) throw new CommonErrors.SanityError('event procedure not found', M.failure.eventProcedureNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventProcedure.eventId))) throw new AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventProcedureRepo)'deleteOne'`);
        // -> Delete Event Procedure
        await EventProcedureRepo.deleteOne(log, reqId, eventProcedureId);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventProcedure.eventId, Enums.HistoryEvents.DeleteEventProcedure, M.history.deletedEventProcedure(eventProcedure.eventId, eventProcedure.procedureName), JSON.stringify(eventProcedure));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;