/**
 * @description validation middleware for event endpoints
 */


const Validator = require('../../../common/utils/validators/validator');
const Matcher = require('../../../common/utils/patternMatcher');
const Errors = require('../../../common/errors/_CustomErrors');
const ValidationRules = require('../../../config/validation.rules');


function get_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/procedures/get_v';
    const { eventId } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function addOne_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/procedures/addOne_v';
    const { eventId, procedureId, procedureCode, procedureName } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().isNumber().bake(),
            new Validator.Validate(procedureId, 'procedure id').isNumber().bake(),
            new Validator.Validate(procedureCode, 'procedure code').isString().minLength(ValidationRules.Code.minLength).maxLength(ValidationRules.Code.maxLength).bake(),
            new Validator.Validate(procedureName, 'procedure name').isString().minLength(ValidationRules.Procedure.name.minLength).maxLength(ValidationRules.Procedure.name.maxLength).bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}

function updateOne_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/procedures/updateOne_v';
    const { id, procedureCode, procedureName } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(id, 'id').required().bake(),
            new Validator.Validate(procedureCode, 'procedure code').isString().minLength(ValidationRules.Code.minLength).maxLength(ValidationRules.Code.maxLength).bake(),
            new Validator.Validate(procedureName, 'procedure name').isString().minLength(ValidationRules.Procedure.name.minLength).maxLength(ValidationRules.Procedure.name.maxLength).bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function deleteOne_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/procedures/deleteOne_v';
    const { eventProcedureId } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventProcedureId, 'event procedure id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    get_v,
    addOne_v,
    updateOne_v,
    deleteOne_v
}