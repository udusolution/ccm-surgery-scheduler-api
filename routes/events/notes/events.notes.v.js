/**
 * @description validation middleware for event endpoints
 */


const Validator = require('../../../common/utils/validators/validator');
const Matcher = require('../../../common/utils/patternMatcher');
const Errors = require('../../../common/errors/_CustomErrors');
const ValidationRules = require('../../../config/validation.rules');


function get_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/notes/get_v';
    const { eventId } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function addOne_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/notes/addOne_v';
    const { eventId, userId, note } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake(),
            new Validator.Validate(userId, 'user id').required().bake(),
            new Validator.Validate(note, 'note').required().isString().minLength(ValidationRules.EventNote.note.minLength).maxLength(ValidationRules.EventNote.note.maxLength).bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function deleteOne_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/notes/deleteOne_v';
    const { eventNoteId } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventNoteId, 'note id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    get_v,
    addOne_v,
    deleteOne_v
}