/**
 * @description master data routes
 */


const express = require('express');
const router = express.Router();
const M = require('../../../resources/languages/messages');
const Enums = require('../../../common/enums');
const Auth = require('../../../common/middleware/auth.middleware');
const Validators = require('./events.notes.v');
const EventNoteRepo = require('../../../repos/event.note.repo');
const History = require('../../../repos/history.repo');
const { AuthErrors } = require('../../../common/errors/_CustomErrors');
const PnOperations = require('../../../operations/notifications');
const EventEditPerm = require('../../../operations/eventEditPerm');


/**
 * @description get event notes
 */
router.post('/get', Validators.get_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try{

        log.info(reqId, `calling (EventNoteRepo)'getForEvent'`);
        // -> Get Event Notes
        const notes = await EventNoteRepo.getForEvent(log, reqId, eventId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { notes } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description add event note
 */
router.post('/addOne', Validators.addOne_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId, note } = req.body;

    try{

        // Sanity Checks, throw error in the following cases
        // ? If user does not have permission to edit this event
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventNoteRepo)'add'`);
        // -> Add Event Note
        const newNote = await EventNoteRepo.add(log, reqId, { eventId, userId: req.userId, note });

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.AddEventNote, M.history.addedEventNote(eventId, note));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { note: newNote } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description add event note
 * @deprecated
 */
router.post('/deleteOne', Validators.deleteOne_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventNoteId } = req.body;

    try{

        log.info(reqId, `calling (EventNoteRepo)'deleteOne'`);
        // -> Get Event Note
        const note = await EventNoteRepo.findById(log, reqId, eventNoteId);

        // Sanity Checks, throw error in the following cases
        // ? If note is not logged in user's
        // ? If user does not have permission to edit this event
        if(req.userId != note.userId) throw new AuthErrors.ForbiddenAuthError();
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, note.eventId))) throw new AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventNoteRepo)'deleteOne'`);
        // -> Delete Event Note
        await EventNoteRepo.deleteOne(log, reqId, eventNoteId);

        // End
        log.info(reqId, `end [success]`);
        // History.add(log, reqId, req.userId, Enums.HistoryEvents.AddEvent, M.history.addedEvent(newEvent.id, newEvent.patientId));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;