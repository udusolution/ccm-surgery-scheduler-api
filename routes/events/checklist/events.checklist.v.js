/**
 * @description validation middleware for event endpoints
 */


const Validator = require('../../../common/utils/validators/validator');
const Matcher = require('../../../common/utils/patternMatcher');
const Errors = require('../../../common/errors/_CustomErrors');


function get_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/checklist/get_v';
    const { eventId } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function addOne_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/checklist/addOne_v';
    const { eventId, description, groupName, assigneeId, dueDate } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        const errors = [];
        if(!eventId) errors.push('event ID required');
        if(!groupName) errors.push('item group name required');
        if(!description) errors.push('item description required');
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function deleteOne_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/checklist/deleteOne_v';
    const { itemId } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        const errors = [];
        if(!itemId) errors.push('event note ID required');
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    get_v,
    addOne_v,
    deleteOne_v
}