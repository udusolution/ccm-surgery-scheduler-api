/**
 * @description event checklist routes
 */


const express = require('express');
const router = express.Router();
const M = require('../../../resources/languages/messages');
const Enums = require('../../../common/enums');
const Auth = require('../../../common/middleware/auth.middleware');
const Validators = require('./events.checklist.v');
const EventChecklistRepo = require('../../../repos/event.checklist.repo');
const History = require('../../../repos/history.repo');
const { AuthErrors } = require('../../../common/errors/_CustomErrors');


/**
 * @description get event checklist
 * @deprecated
 */
router.post('/get', Validators.get_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try{

        log.info(reqId, `calling (EventChecklistRepo)'getForEvent'`);
        // -> Get Event Checklist
        const notes = await EventChecklistRepo.getForEvent(log, reqId, eventId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { notes } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description add event checklist item
 * @deprecated
 */
router.post('/addOne', Validators.addOne_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId, description, groupName, assigneeId, dueDate } = req.body;

    try{

        log.info(reqId, `calling (EventChecklistRepo)'add'`);
        // -> Add Event Checklist Item
        const newItem = await EventChecklistRepo.add(log, reqId, { eventId, userId: req.userId, description, groupName, assigneeId, dueDate });

        // End
        log.info(reqId, `end [success]`);
        // History.add(log, reqId, req.userId, Enums.HistoryEvents.AddEvent, M.history.addedEvent(newEvent.id, newEvent.patientId));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { item: newItem } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description update event checklist item
 * @deprecated
 */
router.put('/updateOne', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { itemId, description, assigneeId, dueDate, status } = req.body;

    try {

        log.info(reqId, `calling (EventChecklistRepo)'update'`);
        // -> Update Event Checklist Item
        await EventChecklistRepo.update(log, reqId, itemId, { description, assigneeId, dueDate, status });

        // End
        log.info(reqId, `end [success]`);
        // History.add(log, reqId, req.userId, Enums.HistoryEvents.AddEvent, M.history.addedEvent(newEvent.id, newEvent.patientId));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description add event note
 * @deprecated
 */
router.post('/deleteOne', Validators.deleteOne_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { itemId } = req.body;

    try{

        log.info(reqId, `calling (EventChecklistRepo)'findById'`);
        // -> Get Event Checklist Item
        const item = await EventChecklistRepo.findById(log, reqId, itemId);

        // Sanity Checks, throw error in the following cases
        // ? If note is not logged in user's
        if(req.userId != item.userId) throw new AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventChecklistRepo)'deleteOne'`);
        // -> Delete Event Checklist Item
        await EventChecklistRepo.deleteOne(log, reqId, itemId);

        // End
        log.info(reqId, `end [success]`);
        // History.add(log, reqId, req.userId, Enums.HistoryEvents.AddEvent, M.history.addedEvent(newEvent.id, newEvent.patientId));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;