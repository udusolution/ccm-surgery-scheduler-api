/**
 * @description validation middleware for event endpoints
 */


const Validator = require('../../common/utils/validators/validator');
const Matcher = require('../../common/utils/patternMatcher');
const Errors = require('../../common/errors/_CustomErrors');
const TableDefinitions = require('../../config/db/tables.json');
const moment = require('moment');
const ValidationRules = require('../../config/validation.rules');


function add_v(req, res, next) {

    const { log, reqId } = req;
    const fnName = 'events/add_v';
    const {
        dateFrom,
        dateTo,
        eventTypeId,
        eventTypeCode,
        eventTypeName,
        priorityId,
        priority,
        priorityText,
        facilityId,
        facilityCode,
        facilityName,
        facilityAddress,
        physicianId,
        physicianName,
        physicianPhone,
        physicianFax,
        physicianOfficePhone,
        physicianOfficeAddress,
        coordinatorId,
        coordinatorName,
        patientId,
        patientName
    } = req.body;

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // const errors = [];
        // if(!dateFrom) errors.push('starting date required');
        // if(!moment(dateFrom).isAfter(moment())) errors.push('starting date must be in the future');
        // if(!dateTo) errors.push('end date required');
        // if(!moment(dateTo).isAfter(moment(dateFrom))) errors.push('end date must be before start date');
        // if(!eventTypeId) errors.push('event type ID required');
        // if(!eventTypeCode) errors.push('event type code required');
        // if(!eventTypeName) errors.push('event type name required');
        // if(!priorityId) errors.push('priority ID required');
        // if(priority == null) errors.push('priority required');
        // if(!priorityText) errors.push('priority name required');
        // if(!facilityId) errors.push('facility ID required');
        // if(!facilityCode) errors.push('facility code required');
        // if(!facilityName) errors.push('facility name required');
        // if(!facilityAddress) errors.push('facility address required');
        // if(!physicianId) errors.push('physician ID required');
        // if(!physicianName) errors.push('physician name required');
        // if(!coordinatorId) errors.push('coordinator ID required');
        // if(!coordinatorName) errors.push('coordinator name required');
        // if(!patientId) errors.push('patient ID required');
        // if(!patientName) errors.push('patient name required');

        // Validations
        const validations = [
            new Validator.Validate(dateFrom, 'start date').required().isDate().bake(),
            new Validator.Validate(dateTo, 'end date').required().isDate().minDate(dateFrom, 'end date-time must be after start date-time').bake(),
            new Validator.Validate(eventTypeId, 'event type').required().bake(),
            new Validator.Validate(eventTypeCode, 'event type code').isString().minLength(ValidationRules.Code.minLength).maxLength(ValidationRules.Code.maxLength).bake(),
            new Validator.Validate(eventTypeName, 'event type name').required().isString().bake(),
            new Validator.Validate(priorityId, 'priority id').required().bake(),
            new Validator.Validate(priority, 'priority number').isNumber().bake(),
            new Validator.Validate(priorityText, 'priority name').required().isString().bake(),
            new Validator.Validate(facilityId, 'facility').required().bake(),
            new Validator.Validate(facilityCode, 'facility code').required().isString().minLength(ValidationRules.Code.minLength).maxLength(ValidationRules.Code.maxLength).bake(),
            new Validator.Validate(facilityName, 'facility name').required().isString().bake(),
            new Validator.Validate(facilityAddress, 'facility address').required().isString().bake(),
            new Validator.Validate(physicianId, 'physician').required().bake(),
            new Validator.Validate(physicianName, 'physician name').required().isString().bake(),
            new Validator.Validate(physicianPhone, 'physician mobile number').isString().isUsaPhoneNumber().bake(),
            new Validator.Validate(physicianFax, 'physician office fax number').isString().isUsaPhoneNumber().bake(),
            new Validator.Validate(physicianOfficePhone, 'physician office phone number').isString().isUsaPhoneNumber().bake(),
            new Validator.Validate(physicianOfficeAddress, 'physician office address').isString().bake(),
            new Validator.Validate(coordinatorId, 'coordinator').required().bake(),
            new Validator.Validate(coordinatorName, 'coordinator name').required().isString().bake(),
            new Validator.Validate(patientId, 'patient').required().bake(),
            new Validator.Validate(patientName, 'patient name').required().isString().bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function search_v(req, res, next) {

    const { log, reqId } = req;
    const { 
        searchCol, 
        searchQuery, 
        sortCol, 
        sortIsAsc, 
        skip = 0, 
        limit = 10, 
        physicianId, 
        coordinatorId, 
        patientId, 
        facilityId,
        fromDate, 
        toDate,
        status 
    } = req.body;
    const fnName = 'events/search_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(searchCol, 'search column').isString().mustBeIn(TableDefinitions.event, 'invalid search column').bake(),
            new Validator.Validate(searchQuery, 'search query').isString().bake(),
            new Validator.Validate(sortCol, 'sort column').isString().mustBeIn(TableDefinitions.event, 'invalid sort column').bake(),
            new Validator.Validate(sortIsAsc, 'sort mode').isBoolean().bake(),
            new Validator.Validate(skip, 'skip').isNumber().bake(),
            new Validator.Validate(limit, 'limit').isNumber().bake(),
            new Validator.Validate(physicianId, 'physician').bake(),
            new Validator.Validate(coordinatorId, 'coordinator').bake(),
            new Validator.Validate(patientId, 'patient').bake(),
            new Validator.Validate(facilityId, 'facility').bake(),
            new Validator.Validate(fromDate, 'from-date').isDate().bake(),
            new Validator.Validate(toDate, 'to-date').isDate().minDate(fromDate, 'to-date must be after from-date').bake(),
            new Validator.Validate(status, 'status').bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function findById_v(req, res, next) {

    const { log, reqId } = req;
    const { eventId } = req.body;
    const fnName = 'events/findById_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function getWithDetails_v(req, res, next) {

    const { log, reqId } = req;
    const { eventId } = req.body;
    const fnName = 'events/getWithDetails_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function update_v(req, res, next) {

    const { log, reqId } = req;
    const { eventId, params } = req.body;
    const fnName = 'events/update_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        const errors = [];
        if(!eventId) errors.push('event ID required');
        for(const key in params) if(!TableDefinitions.event.includes(key)) errors.push(`"${key}" is not a valid parameter`);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function setAsBSG_v(req, res, next) {

    const { log, reqId } = req;
    const { eventId } = req.body;
    const fnName = 'events/setAsBSG_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function setAsWFC_v(req, res, next) {

    const { log, reqId } = req;
    const { eventId } = req.body;
    const fnName = 'events/setAsWFC_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function setAsConfirmed_v(req, res, next) {

    const { log, reqId } = req;
    const { eventId } = req.body;
    const fnName = 'events/setAsConfirmed_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function setAsConfirmedAndReschedule_v(req, res, next) {

    const { log, reqId } = req;
    const { eventId, dateFrom, dateTo } = req.body;
    const fnName = 'events/setAsConfirmedAndReschedule_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake(),
            new Validator.Validate(dateFrom, 'start date-time').required().isDate().bake(),
            new Validator.Validate(dateTo, 'end date-time').required().isDate().minDate(dateFrom, 'end date-time must be after start date-time').bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function setAsRejected_v(req, res, next) {

    const { log, reqId } = req;
    const { eventId } = req.body;
    const fnName = 'events/setAsRejected_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function setAsCompleted_v(req, res, next) {

    const { log, reqId } = req;
    const { eventId } = req.body;
    const fnName = 'events/setAsCompleted_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function deleteEvent_v(req, res, next) {

    const { log, reqId } = req;
    const { eventId } = req.body;
    const fnName = 'events/deleteEvent_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function cancelEvent_v(req, res, next) {

    const { log, reqId } = req;
    const { eventId, cancelReason } = req.body;
    const fnName = 'events/cancelEvent_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake(),
            new Validator.Validate(cancelReason, 'cancel reason').required().isString().minLength(ValidationRules.RejectionReason.minLength).maxLength(ValidationRules.RejectionReason.maxLength).bake(),
            new Validator.Validate(cancelReason, 'cancel note').isString().bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function codeEvent_v(req, res, next) {

    const { log, reqId } = req;
    const { eventId, selectedProcedures } = req.body;
    const fnName = 'events/codeEvent_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake(),
            new Validator.Validate(selectedProcedures, 'procedures').required().isArray().minLength(1).bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function reschedule_v(req, res, next) {

    const { log, reqId } = req;
    const { eventId, dateFrom, dateTo, physicianId, physicianName, physicianPhone, physicianFax, physicianOfficePhone, physicianOfficeAddress, isEmergency } = req.body;
    const fnName = 'events/reschedule_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake(),
            new Validator.Validate(dateFrom, 'start date-time').required().isDate().bake(),
            new Validator.Validate(dateTo, 'end date-time').required().isDate().minDate(dateFrom, 'end date-time must be after start date-time').bake(),
            new Validator.Validate(physicianId, 'physician').bake(),
            new Validator.Validate(physicianName, 'physician name').isString().bake(),
            new Validator.Validate(physicianPhone, 'physician phone number').isString().regex(Matcher.regex.usaPhoneNumber).bake(),
            new Validator.Validate(physicianFax, 'physician fax').isString().regex(Matcher.regex.usaPhoneNumber).bake(),
            new Validator.Validate(physicianOfficePhone, 'physician office phone number').isString().isUsaPhoneNumber().bake(),
            new Validator.Validate(physicianOfficeAddress, 'physician office address').isString().bake(),
            new Validator.Validate(isEmergency, 'RTMU').isBoolean().bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}

function sendPatientCommunications_v(req, res, next) {

    const { log, reqId } = req;
    const { eventId, sendPatientComEmail, sendPatientComSms, patientComIds } = req.body;
    const fnName = 'events/sendPatientCommunications_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake(),
            new Validator.Validate(sendPatientComEmail, 'send patient communication email').isBoolean().bake(),
            new Validator.Validate(sendPatientComSms, 'send patient communication sms').isBoolean().bake(),
            new Validator.Validate(patientComIds, 'patient communication IDs').required(sendPatientComEmail || sendPatientComSms).isArray().minLength(1).bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}

function sendPatientComs_v(req, res, next) {

    const { log, reqId } = req;
    const { eventId, emailTemplateIds, smsTemplateIds } = req.body;
    const fnName = 'events/sendPatientComs_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake(),
            new Validator.Validate(emailTemplateIds, 'email template IDs').isArray().bake(),
            new Validator.Validate(smsTemplateIds, 'sms template IDs').isArray().bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}

function syncEventPatientWithLocalPatient_v(req, res, next) {

    const { log, reqId } = req;
    const { eventId } = req.body;
    const fnName = 'events/syncEventPatientWithLocalPatient_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(eventId, 'event id').required().bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    add_v,
    search_v,
    findById_v,
    getWithDetails_v,
    update_v,
    setAsBSG_v,
    setAsWFC_v,
    setAsConfirmed_v,
    setAsConfirmedAndReschedule_v,
    setAsRejected_v,
    setAsCompleted_v,
    deleteEvent_v,
    cancelEvent_v,
    codeEvent_v,
    reschedule_v,
    sendPatientCommunications_v,
    sendPatientComs_v,
    syncEventPatientWithLocalPatient_v
}