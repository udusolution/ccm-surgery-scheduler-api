/**
 * @description event scheduled sms communications routes
 */


const express = require('express');
const router = express.Router();
const Errors = require('../../../common/errors/_CustomErrors');
const M = require('../../../resources/languages/messages');
const Enums = require('../../../common/enums');
const Auth = require('../../../common/middleware/auth.middleware');
const EventSmsScheduledComRepo = require('../../../repos/event.scheduledCom.sms');
const ScheduledComOperations = require('../../../operations/scheduledComsRuling');
const History = require('../../../repos/history.repo');
const ObjUtils = require('../../../common/utils/object.utils');
const ScheduledComLogic = require('../../../logic/ScheduledComLogic');
const EventEditPerm = require('../../../operations/eventEditPerm');


/**
 * @description add event scheduled sms communication
 */
router.post('/addSms', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId, templateId, templateName, templateBody, whenToSend } = req.body;

    try {

        // Sanity Checks, throw error in the following cases
        // ? If user does not have permission to edit this event
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventSmsScheduledComRepo)'getForEvent'`);
        // -> Get Email Template
        const coms = await EventSmsScheduledComRepo.getForEvent(log, reqId, eventId, true);

        // Sanity Checks, throw error in the following cases:
        // ? If this event's scheduled sms coms already contain this email template
        if(coms?.length > 0 && coms.some(c => c.templateId == templateId)) throw new Errors.CommonErrors.SanityError('template already active', M.failure.eventSmsTemplateAlreadyActive());

        log.info(reqId, `calling (EventSmsScheduledComRepo)'add'`);
        // -> Add
        await EventSmsScheduledComRepo.add(log, reqId, { eventId, templateId, templateName, templateBody, whenToSend });
        

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.EnableEventScheduledCommunication, M.history.enabledEventScheduledSmsCommunication(eventId, templateName)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });

    }
    catch(e) {

        next(e);
    }
    
})


/**
 * @description get event scheduled sms
 */
router.post('/findById', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { comId } = req.body;

    try {

        log.info(reqId, `calling (EventSmsScheduledComRepo)'findById'`);
        // -> Get Sms Template
        const com = await EventSmsScheduledComRepo.findById(log, reqId, comId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { com } });
    }
    catch(e) {

        next(e);
    }
    
})

/**
 * @description get all event scheduled SMSs
 */
router.post('/getForEvent', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try {

        log.info(reqId, `calling (EventSmsScheduledComRepo)'getForEvent'`);
        // -> Get
        const coms = await EventSmsScheduledComRepo.getForEvent(log, reqId, eventId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { coms } });
    }
    catch(e) {

        next(e);
    }
    
})


/**
 * @description update event scheduled sms communication
 */
router.post('/updateSms', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { id, templateBody } = req.body;

    try {

        log.info(reqId, `calling (EventSmsScheduledComRepo)'findById'`);
        // -> Get Event Sms Com
        const com = await EventSmsScheduledComRepo.findById(log, reqId, id);

        // Sanity Checks, throw error in the following cases
        // ? If scheduled com not found
        // ? If user does not have permission to edit this event
        if(!com) throw new Errors.CommonErrors.SanityError('scheduled sms com not found', M.failure.eventScheduledSmsNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, com.eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventSmsScheduledComRepo)'updateOne'`);
        // -> Update
        await EventSmsScheduledComRepo.updateOne(log, reqId, { id, templateBody });

        // <> History
        const differences = ObjUtils.getDiff(com, { templateBody }).map(d => `- Changed "${d.key}" from "${d.oldVal}" to "${d.newVal}"`).join('\n');
        History.add(log, reqId, req.userId, com.eventId, Enums.HistoryEvents.UpdateEventScheduledCommunication, M.history.updatedEventScheduledSmsCommunication(com.templateName, com.eventId, differences)).catch(() => {});
        

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });

    }
    catch(e) {

        next(e);
    }
    
})


/**
 * @description update event scheduled sms communication's "when to send"
 */
router.post('/updateWhenToSend', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { id, whenToSend } = req.body;

    try {

        log.info(reqId, `calling (EventSmsScheduledComRepo)'findById'`);
        // -> Get Event Sms Com
        const com = await EventSmsScheduledComRepo.findById(log, reqId, id);

        // Sanity Checks, throw error in the following cases
        // ? If scheduled com not found
        // ? If user does not have permission to edit this event
        if(!com) throw new Errors.CommonErrors.SanityError('scheduled sms com not found', M.failure.eventScheduledSmsNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, com.eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventSmsScheduledComRepo)'updateWhenToSend'`);
        // -> Update
        await EventSmsScheduledComRepo.updateWhenToSend(log, reqId, id, whenToSend);

        // <> History
        const oldWhenToSendVars = ScheduledComLogic.minutesToTimeDifference(com.whenToSend);
        const newWhenToSendVars = ScheduledComLogic.minutesToTimeDifference(whenToSend);
        const oldWhenToSend = oldWhenToSendVars.timeValue > 400 ? 'On event confirmation' : `${oldWhenToSendVars.timeValue} ${oldWhenToSendVars.timeType} ${oldWhenToSendVars.timeMode} event`;
        const newWhenToSend = newWhenToSendVars.timeValue > 400 ? 'On event confirmation' : `${newWhenToSendVars.timeValue} ${newWhenToSendVars.timeType} ${newWhenToSendVars.timeMode} event`;
        const differences = `Changed when to send from "${oldWhenToSend}" to "${newWhenToSend}"`;
        History.add(log, reqId, req.userId, com.eventId, Enums.HistoryEvents.UpdateEventScheduledCommunication, M.history.updatedEventScheduledSmsCommunication(com.templateName, com.eventId, differences)).catch(() => {});
        

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });

    }
    catch(e) {

        next(e);
    }
    
})


/**
 * @description remove event scheduled sms communication
 */
router.post('/removeSms', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { id } = req.body;

    try {

        log.info(reqId, `calling (EventSmsScheduledComRepo)'findById'`);
        // -> Get Event Sms Com
        const com = await EventSmsScheduledComRepo.findById(log, reqId, id);

        // Sanity Checks, throw error in the following cases
        // ? If scheduled com not found
        // ? If user does not have permission to edit this event
        if(!com) throw new Errors.CommonErrors.SanityError('scheduled sms com not found', M.failure.eventScheduledSmsNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, com.eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventSmsScheduledComRepo)'remove'`);
        // -> Remove
        await EventSmsScheduledComRepo.remove(log, reqId, id);
        

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, com.eventId, Enums.HistoryEvents.DeleteEventScheduledCommunication, M.history.disabledEventScheduledSmsCommunication(com.templateName, com.eventId)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });

    }
    catch(e) {

        next(e);
    }
    
})

/**
 * @description force send an event scheduled sms patient communication
 */
 router.post('/forceSendSms', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { comId } = req.body;

    try {

        log.info(reqId, `calling (EventSmsScheduledComRepo)'findById'`);
        // -> Get Event Sms Com
        const com = await EventSmsScheduledComRepo.findById(log, reqId, comId);

        // Sanity Checks, throw error in the following cases
        // ? If event scheduled email communication not found
        // ? If user does not have permission to edit this event
        if(!com) throw new Errors.CommonErrors.SanityError('event scheduled sms communication not found', M.failure.eventScheduledSmsNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, com.eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (ScheduledComOperations)'forceSendEventSmsCommunication'`);
        // -> Call the Operation
        await ScheduledComOperations.forceSendEventSmsCommunication(log, reqId, comId, req.userId);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, com.eventId, Enums.HistoryEvents.ForceSendEventScheduledCommunication, M.history.forceSendEventScheduledSmsCommunication(com.templateName, com.eventId)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }
    
})


/**
 * @description bulk update event scheduled sms communication
 */
 router.post('/bulkUpdate', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId, comStates } = req.body;

    try {

        // Sanity Checks, throw error in the following cases
        // ? If user does not have permission to edit this event
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();
        
        log.info(reqId, `calling (ScheduledComOperations)'bulkUpdateEventSmsCommunications'`);
        // -> Bulk Update
        await ScheduledComOperations.bulkUpdateEventSmsCommunications(log, reqId, comStates);

        // <> History
        History.add(log, reqId, req.userId, comStates[0]?.eventId, Enums.HistoryEvents.UpdateEventScheduledCommunication, M.history.bulkUpdatedEventScheduledSmsCommunication(comStates.map(c => c.templateName)), JSON.stringify(comStates)).catch(() => {});

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });

    }
    catch(e) {

        next(e);
    }
    
})


/**
 * Exports
 */
module.exports = router;