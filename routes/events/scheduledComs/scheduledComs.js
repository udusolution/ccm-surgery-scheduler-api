/**
 * @description event scheduled communications routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../../config/config');
const Errors = require('../../../common/errors/_CustomErrors');
const M = require('../../../resources/languages/messages');
const Enums = require('../../../common/enums');
const Auth = require('../../../common/middleware/auth.middleware');
const EventEmailScheduledComRepo = require('../../../repos/event.scheduledCom.email');
const multer = require('multer');
const ScheduledComOperations = require('../../../operations/scheduledComsRuling');
const History = require('../../../repos/history.repo');
const ObjUtils = require('../../../common/utils/object.utils');
const ScheduledComLogic = require('../../../logic/ScheduledComLogic');
const PnOperations = require('../../../operations/notifications');
const EventEditPerm = require('../../../operations/eventEditPerm');


/**
 * @description add event scheduled email communication
 */
router.post('/addEmail', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId, templateId, templateName, templateSubject, templateBody, whenToSend, attachments } = req.body;

    try {

        // Sanity Checks, throw error in the following cases
        // ? If user does not have permission to edit this event
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventEmailScheduledComRepo)'getForEvent'`);
        // -> Get Email Template
        const coms = await EventEmailScheduledComRepo.getForEvent(log, reqId, eventId, true);

        // Sanity Checks, throw error in the following cases:
        // ? If this event's scheduled email coms already contain this email template
        if(coms?.length > 0 && coms.some(c => c.templateId == templateId)) throw new Errors.CommonErrors.SanityError('template already active', M.failure.eventEmailTemplateAlreadyActive());

        log.info(reqId, `calling (EventEmailScheduledComRepo)'add'`);
        // -> Add
        await EventEmailScheduledComRepo.add(log, reqId, { eventId, templateId, templateName, templateSubject, templateBody, whenToSend, attachments });

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId }, { excludeUserId: req.userId });
        
        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, eventId, Enums.HistoryEvents.EnableEventScheduledCommunication, M.history.enabledEventScheduledEmailCommunication(eventId, templateName)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });

    }
    catch(e) {

        next(e);
    }
    
})


/**
 * @description get event scheduled email
 */
router.post('/findById', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { comId } = req.body;

    try {

        log.info(reqId, `calling (EventEmailScheduledComRepo)'findById'`);
        // -> Get Email Template
        const com = await EventEmailScheduledComRepo.findById(log, reqId, comId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { com } });
    }
    catch(e) {

        next(e);
    }
    
})

/**
 * @description get all event scheduled emails
 */
router.post('/getForEvent', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId } = req.body;

    try {

        log.info(reqId, `calling (EventEmailScheduledComRepo)'getForEventWithFiles'`);
        // -> Get Email Template
        const coms = await EventEmailScheduledComRepo.getForEventWithFiles(log, reqId, eventId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { coms } });
    }
    catch(e) {

        next(e);
    }
    
})


/**
 * @description get all event scheduled emails
 */
router.post('/removeAttachment', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { id } = req.body;

    try {

        log.info(reqId, `calling (EventEmailScheduledComRepo)'removeAttachment'`);
        // -> Delete Attachment
        await EventEmailScheduledComRepo.removeAttachment(log, reqId, id);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }
    
})


/**
 * @description update event scheduled email communication
 */
router.post('/updateEmail', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { id, templateSubject, templateBody } = req.body;

    try {

        log.info(reqId, `calling (EventEmailScheduledComRepo)'findById'`);
        // -> Get Event Email Template
        const com = await EventEmailScheduledComRepo.findById(log, reqId, id);

        // Sanity Checks, throw error in the following cases
        // ? If scheduled com not found
        // ? If user does not have permission to edit this event
        if(!com) throw new Errors.CommonErrors.SanityError('scheduled email com not found', M.failure.eventScheduledEmailNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, com.eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventEmailScheduledComRepo)'updateOne'`);
        // -> Update
        await EventEmailScheduledComRepo.updateOne(log, reqId, { id, templateSubject, templateBody });

        // <> History
        const differences = ObjUtils.getDiff(com, { templateSubject, templateBody }).map(d => `- Changed "${d.key}" from "${d.oldVal}" to "${d.newVal}"`).join('\n');
        History.add(log, reqId, req.userId, com.eventId, Enums.HistoryEvents.UpdateEventScheduledCommunication, M.history.updatedEventScheduledEmailCommunication(com.templateName, com.eventId, differences)).catch(() => {});

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId: com.eventId }, { excludeUserId: req.userId });
        
        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });

    }
    catch(e) {

        next(e);
    }
    
})


/**
 * @description update event scheduled email communication's "when to send"
 */
router.post('/updateWhenToSend', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { id, whenToSend } = req.body;

    try {

        log.info(reqId, `calling (EventEmailScheduledComRepo)'findById'`);
        // -> Get Event Email Template
        const com = await EventEmailScheduledComRepo.findById(log, reqId, id);

        // Sanity Checks, throw error in the following cases
        // ? If scheduled com not found
        // ? If user does not have permission to edit this event
        if(!com) throw new Errors.CommonErrors.SanityError('scheduled email com not found', M.failure.eventScheduledEmailNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, com.eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventEmailScheduledComRepo)'updateWhenToSend'`);
        // -> Update
        await EventEmailScheduledComRepo.updateWhenToSend(log, reqId, id, whenToSend);

        // <> History
        const oldWhenToSendVars = ScheduledComLogic.minutesToTimeDifference(com.whenToSend);
        const newWhenToSendVars = ScheduledComLogic.minutesToTimeDifference(whenToSend);
        const oldWhenToSend = oldWhenToSendVars.timeValue > 400 ? 'On event confirmation' : `${oldWhenToSendVars.timeValue} ${oldWhenToSendVars.timeType} ${oldWhenToSendVars.timeMode} event`;
        const newWhenToSend = newWhenToSendVars.timeValue > 400 ? 'On event confirmation' : `${newWhenToSendVars.timeValue} ${newWhenToSendVars.timeType} ${newWhenToSendVars.timeMode} event`;
        const differences = `Changed when to send from "${oldWhenToSend}" to "${newWhenToSend}"`;
        History.add(log, reqId, req.userId, com.eventId, Enums.HistoryEvents.UpdateEventScheduledCommunication, M.history.updatedEventScheduledEmailCommunication(com.templateName, com.eventId, differences)).catch(() => {});

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId: com.eventId }, { excludeUserId: req.userId });
        
        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });

    }
    catch(e) {

        next(e);
    }
    
})


/**
 * @description remove event scheduled email communication
 */
router.post('/removeEmail', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { id } = req.body;

    try {

        log.info(reqId, `calling (EventEmailScheduledComRepo)'findById'`);
        // -> Get Event Email Template
        const com = await EventEmailScheduledComRepo.findById(log, reqId, id);

        // Sanity Checks, throw error in the following cases
        // ? If scheduled com not found
        // ? If user does not have permission to edit this event
        if(!com) throw new Errors.CommonErrors.SanityError('scheduled email com not found', M.failure.eventScheduledEmailNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, com.eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (EventEmailScheduledComRepo)'remove'`);
        // -> Remove
        await EventEmailScheduledComRepo.remove(log, reqId, id);

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId: com.eventId }, { excludeUserId: req.userId });
        

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, com.eventId, Enums.HistoryEvents.DeleteEventScheduledCommunication, M.history.disabledEventScheduledEmailCommunication(com.templateName, com.eventId)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });

    }
    catch(e) {

        next(e);
    }
    
})

/**
 * @description force send an event scheduled email patient communication
 */
 router.post('/forceSendEmail', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { comId } = req.body;

    try {

        log.info(reqId, `calling (EventEmailScheduledComRepo)'findById'`);
        // -> Get Event Email Template
        const com = await EventEmailScheduledComRepo.findById(log, reqId, comId);

        // Sanity Checks, throw error in the following cases
        // ? If event scheduled email communication not found
        // ? If user does not have permission to edit this event
        if(!com) throw new Errors.CommonErrors.SanityError('event scheduled email communication not found', M.failure.eventScheduledEmailNotFound());
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, com.eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();

        log.info(reqId, `calling (ScheduledComOperations)'forceSendEventEmailCommunication'`);
        // -> Call the Operation
        await ScheduledComOperations.forceSendEventEmailCommunication(log, reqId, comId, req.userId);

        // <> PN
        PnOperations.sendPnToAll(log, reqId, M.pn.updatedEvent.title(), M.pn.updatedEvent.message(), { type: Enums.PnEventTypes.UpdatedEvent, eventId: com.eventId }, { excludeUserId: req.userId });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, com.eventId, Enums.HistoryEvents.ForceSendEventScheduledCommunication, M.history.forceSendEventScheduledEmailCommunication(com.templateName, com.eventId)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }
    
})


/**
 * MULTER CONFIGS FOR THIS ENDPOINT
 */
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/templates/email')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    }
})
const upload = multer({ storage: storage }).any();

/**
 * @description add event scheduled email attachments
 * @note the above "addOne" endpoint can add attachments also. This endpoint is used as a part of editing an existing scheduled event email
 */
router.post('/addAttachments', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;

    upload(req, res, async function (err) {

        try {

            // Sanity Checks, throw errors in the following cases
            // ? If multer returned an error
            // ? If general error was returned
            if (err instanceof multer.MulterError)  throw new Errors.FileUploadErrors.FileUploadError(err.message, err);
            else if (err)                           throw new Errors.FileUploadErrors.FileUploadError(err.message, err);

            // Request Variables
            const { comId } = req.body;

            // If has attachments
            if(req.files) {
                
                // Attachment Insert Params
                const insertAttachmentsParams = [];

                for(let i = 0; i < req.files.length; i++) {
                    let file = req.files[i];

                    insertAttachmentsParams.push({
                        comId: comId,
                        file: file.path.replace(/^public[\\\/]/, '').replace(/\\/g, '/')
                    })
                }

                log.info(reqId, `calling (EventEmailScheduledComRepo)'addAttachments'`);
                // -> Insert Template Attachments
                await EventEmailScheduledComRepo.addAttachments(log, reqId, insertAttachmentsParams);

            }
            

            // End
            log.info(reqId, `end [success]`);
            // History.add(log, reqId, req.userId, null, Enums.HistoryEvents.AddMasterData, M.history.addedMasterData(Enums.MasterDataTables.PatientCommunication, title)).catch(() => {});
            res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });

        }
        catch(e) {

            next(e);
        }
    })
    
})


/**
 * @description bulk update event scheduled email communication
 */
 router.post('/bulkUpdate', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { eventId, comStates } = req.body;

    try {

        // Sanity Checks, throw error in the following cases
        // ? If user does not have permission to edit this event
        if(!(await EventEditPerm.isAllowed(log, reqId, req.user, eventId))) throw new Errors.AuthErrors.ForbiddenAuthError();
        
        log.info(reqId, `calling (ScheduledComOperations)'bulkUpdateEventEmailCommunications'`);
        // -> Bulk Update
        await ScheduledComOperations.bulkUpdateEventEmailCommunications(log, reqId, comStates);

        // <> History
        History.add(log, reqId, req.userId, comStates[0]?.eventId, Enums.HistoryEvents.UpdateEventScheduledCommunication, M.history.bulkUpdatedEventScheduledEmailCommunication(comStates.map(c => c.templateName)), JSON.stringify(comStates)).catch(() => {});

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });

    }
    catch(e) {

        next(e);
    }
    
})


/**
 * Exports
 */
module.exports = router;