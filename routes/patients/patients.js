/**
 * @description patient routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../config/config');
const Patient = require('../../repos/patient.repo');
const Errors = require('../../common/errors/_CustomErrors');
const M = require('../../resources/languages/messages');
const Enums = require('../../common/enums');
const Auth = require('../../common/middleware/auth.middleware');
const Validators = require('./patients.v');
const History = require('../../repos/history.repo');
const ObjectUtils = require('../../common/utils/object.utils');
 
 
 /**
  * @description add a new patient
  */
router.post('/add', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.AddPatient]).bake()), Validators.add_v, async (req, res, next) => {

    const { log, reqId } = req;
    const params = req.body;

    try {

        log.info(reqId, `calling (PatientRepo)'add'`);
        // -> Insert Patient
        const newPatient = await Patient.add(log, reqId, params);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.AddPatient, M.history.addedPatient(newPatient.id, newPatient.name), null, null, null, null, newPatient.id);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { patient: newPatient } });
    }
    catch(e) {

        next(e);
    }

})


 /**
  * @description search patients
  * @deprecated in favor of the advanced search endpoint "searchAdvanced"
  */
router.post('/search', Auth.auth(), Validators.search_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { searchCol, searchQuery, sortCol, sortIsAsc, skip, limit } = req.body;

    try{

        log.info(reqId, `calling (PatientRepo)'search'`);
        // -> Search Patients
        const patients = await Patient.search(log, reqId, { searchCol, searchQuery, sortCol, sortIsAsc, skip, limit });

        log.info(reqId, `calling (PatientRepo)'totalCount'`);
        // -> Get total patients count matching search query
        const patientsCount = await Patient.searchCount(log, reqId, { searchCol, searchQuery });

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { patients, patientsCount } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description advanced search patients
 */
router.post('/searchAdvanced', Auth.auth(), Validators.searchAdvanced_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { query, skip, limit } = req.body;

    try{

        log.info(reqId, `calling (PatientRepo)'search'`);
        // -> Search Patients
        const patients = await Patient.searchAdvanced(log, reqId, { query, skip, limit });

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { patients } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description find patient by ID
 */
router.post('/findById', Auth.auth(), Validators.findById_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { patientId } = req.body;

    try{

        log.info(reqId, `calling (PatientRepo)'findById'`);
        // -> Find Patients
        const patient = await Patient.findById(log, reqId, patientId) ?? null;

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { patient } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description update patient data
 */
router.put('/updateOne', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.UpdatePatient]).bake()), Validators.updateOne_v, async (req, res, next) => {

    const { log, reqId } = req;
    const params = req.body;

    try{

        log.info(reqId, `calling (Patient)'findById'`);
        // -> Get Patient
        const patient = await Patient.findById(log, reqId, params.patientId);

        // Sanity Checks, throw error in the following cases
        // ? If patient not found
        if(!patient) throw new Errors.CommonErrors.SanityError('patient not found', M.failure.patientNotFound());

        log.info(reqId, `calling (PatientRepo)'updateOne'`);
        // -> Update Patient
        await Patient.updateOne(log, reqId, params.patientId, params);

        // <> History
        const differences = ObjectUtils.getDiff(patient, params).map(d => `- Changed "${d.key}" from "${d.oldVal}" to "${d.newVal}"`).join('\n');
        const name = `${patient.firstName} ${patient.middleName} ${patient.lastName}`;
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.UpdatePatient, M.history.updatedPatient(params.patientId, name, differences), JSON.stringify(patient), JSON.stringify(params), null, null, params.patientId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description delete patient
 */
router.post('/deleteOne', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin, Enums.UserRoles.Coordinator]).bake()), Validators.deleteOne_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { patientId } = req.body;

    try{

        log.info(reqId, `calling (PatientRepo)'deleteOne'`);
        // -> Delete Patient
        await Patient.deleteOne(log, reqId, patientId);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.DeletePatient, M.history.deletedPatient(patientId), null, null, null, null, patientId);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;