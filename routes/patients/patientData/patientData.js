/**
 * @description master data routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../../config/config');
const PatientData = require('../../../repos/patientData.repo');
const Errors = require('../../../common/errors/_CustomErrors');
const M = require('../../../resources/languages/messages');
const Enums = require('../../../common/enums');
const Auth = require('../../../common/middleware/auth.middleware');
const Validators = require('./patientData.v');
const History = require('../../../repos/history.repo');


/**
 * @description add a new patient data
 * @deprecated
 */
router.post('/add', Validators.add_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { table, data } = req.body;

    try{

        log.info(reqId, `calling (PatientData)'add'`);
        // -> Insert Patient Data
        const newPatientData = await PatientData.add(log, reqId, table, data);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.AddPatientData, M.history.addedPatientData(table, newPatientData.id)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { patientData: newPatientData } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description get patient data table
 * @deprecated
 */
router.post('/get', Validators.get_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { table } = req.body;

    try{

        log.info(reqId, `calling (PatientData)'getTable'`);
        // -> Get Patient Data Table
        const patientData = await PatientData.getTable(log, reqId, table);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { patientData } });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description get multiple patient data tables
 * @deprecated
 */
router.post('/getMany', Validators.getMany_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { tables } = req.body;

    try{

        log.info(reqId, `calling (PatientData)'getTables'`);
        // -> Get Patient Data Tables
        const patientData = await PatientData.getTables(log, reqId, tables);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { ...patientData } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description update patient data column
 * @deprecated
 */
router.put('/updateCol', Validators.updateCol_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { table, rowId, col, val } = req.body;

    try{

        log.info(reqId, `calling (PatientData)'updateCol'`);
        // -> Update Patient Data Column
        await PatientData.updateCol(log, reqId, table, rowId, col, val);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.UpdatePatientData, M.history.updatedPatientData(table, rowId, col, val)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description delete patient data row
 * @deprecated
 */
router.delete('/deleteRow/:table/:rowId', Validators.deleteRow_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { table, rowId } = req.params;

    try{

        log.info(reqId, `calling (PatientData)'deleteRow'`);
        // -> Delete Patient Data Entry
        await PatientData.deleteRow(log, reqId, table, rowId);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.DeletePatientData, M.history.deletedPatientData(table)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;