/**
 * @description validation middleware for patient data endpoints
 */


const Validator = require('../../../common/utils/validators/validator');
const Matcher = require('../../../common/utils/patternMatcher');
const Errors = require('../../../common/errors/_CustomErrors');
const TableDefinitions = require('../../../config/db/tables.json');


function add_v(req, res, next) {

    const { log, reqId } = req;
    const { table, data } = req.body;
    const fnName = 'masterData/add_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        const errors = [];
        if(!table) errors.push('patient data type required');
        else if(!TableDefinitions.patientData.some(d => d.table == table)) errors.push('invalid patient data type');
        // else if(!TableDefinitions.patientData.find(d => d.table == table).includes())
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function get_v(req, res, next) {

    const { log, reqId } = req;
    const { table } = req.body;
    const fnName = 'masterData/get_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        const errors = [];
        if(!table) errors.push('patient data type required');
        else if(!TableDefinitions.patientData.some(d => d.table == table)) errors.push('invalid patient data type');
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function getMany_v(req, res, next) {

    const { log, reqId } = req;
    const { tables } = req.body;
    const fnName = 'masterData/getMany_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        const errors = [];
        if(!tables) errors.push('patient data types required');
        else if(!Validator.isArray(tables)) errors.push('patient data types must be array');
        else if(!tables.every(t => TableDefinitions.patientData.some(d => d.table == t))) errors.push('invalid patient data type(s)');
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function updateCol_v(req, res, next) {

    const { log, reqId } = req;
    const { table, rowId, col, val } = req.body;
    const fnName = 'masterData/updateCol_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        const errors = [];
        if(!table) errors.push('patient data type required');
        else if(!TableDefinitions.patientData.some(d => d.table == table)) errors.push('invalid patient data type');
        if(!rowId) errors.push('row id required');
        if(!col) errors.push('col required');
        else if(!TableDefinitions.patientData.find(d => d.table == table)?.cols.includes(col)) errors.push('invalid col');
        if(!val) errors.push('val required');
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function deleteRow_v(req, res, next) {

    const { log, reqId } = req;
    const { table, rowId } = req.params;
    const fnName = 'masterData/deleteRow_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        const errors = [];
        if(!table) errors.push('patient data type required');
        else if(!TableDefinitions.patientData.some(d => d.table == table)) errors.push('invalid patient data type');
        if(!rowId) errors.push('row id required');
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    add_v,
    get_v,
    getMany_v,
    updateCol_v,
    deleteRow_v
}