/**
 * @description patient allergies routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../../config/config');
const PatientAllergy = require('../../../repos/patient.allergy.repo');
const Errors = require('../../../common/errors/_CustomErrors');
const M = require('../../../resources/languages/messages');
const Enums = require('../../../common/enums');
const Auth = require('../../../common/middleware/auth.middleware');
const Validators = require('./patients.allergies.v');


/**
 * @description add a new patient allergy
 * @deprecated
 */
router.post('/add', Validators.add_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { id, patientId, name, status, allergyDate, reaction, rxNormId } = req.body;

    try{

        log.info(reqId, `calling (PatientAllergyRepo)'add'`);
        // -> Insert Patient Allergies
        const newPatientAllergy = await PatientAllergy.add(log, reqId, { id, patientId, name, status, allergyDate, reaction, rxNormId });

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { allergy: newPatientAllergy } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description get patient allergies
 * @deprecated
 */
router.post('/get', Validators.get_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { patientId } = req.body;

    try{

        log.info(reqId, `calling (PatientAllergyRepo)'getForPatient'`);
        // -> Get Patient Allergies
        const allergies = await PatientAllergy.getForPatient(log, reqId, patientId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { allergies } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description add a new patient
 * @deprecated
 */
router.put('/updateCol', Validators.updateCol_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { rowId, col, val } = req.body;

    try{

        log.info(reqId, `calling (PatientAllergyRepo)'update'`);
        // -> Update Patient Allergy
        await PatientAllergy.updateCol(log, reqId, rowId, col, val);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description delete patient allergy
 * @deprecated
 */
router.delete('/deleteRow/:rowId', Validators.deleteRow_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { rowId } = req.params;

    try{

        log.info(reqId, `calling (PatientAllergyRepo)'deleteRow'`);
        // -> Delete Patient Allergy
        await PatientAllergy.deleteRow(log, reqId, rowId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;