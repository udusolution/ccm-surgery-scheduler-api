/**
 * @description validation middleware for patient allergies endpoints
 */


const Validator = require('../../../common/utils/validators/validator');
const Matcher = require('../../../common/utils/patternMatcher');
const Errors = require('../../../common/errors/_CustomErrors');
const TableDefinitions = require('../../../config/db/tables.json');


function add_v(req, res, next) {

    const { log, reqId } = req;
    const { id, patientId, name, status, allergyDate, reaction, rxNormId } = req.body;
    const fnName = 'patients/allergies/add_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        const errors = [];
        if(!patientId) errors.push('patient id required');
        if(!name) errors.push('name required');
        if(!status) errors.push('status required');
        if(!allergyDate) errors.push('allergy date required');
        if(!reaction) errors.push('reaction required');
        if(!rxNormId) errors.push('rx norm id required');
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function get_v(req, res, next) {

    const { log, reqId } = req;
    const { patientId } = req.body;
    const fnName = 'patients/allergies/get_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        const errors = [];
        if(!patientId) errors.push('patient id required');
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function updateCol_v(req, res, next) {

    const { log, reqId } = req;
    const { rowId, col, val } = req.body;
    const fnName = 'patients/allergies/updateCol_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        const errors = [];
        if(!rowId) errors.push('row id required');
        if(!col) errors.push('col required');
        else if(!TableDefinitions.patient_allergy.includes(col)) errors.push('invalid col');
        if(!val) errors.push('val required');
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function deleteRow_v(req, res, next) {

    const { log, reqId } = req;
    const { rowId } = req.params;
    const fnName = 'patients/allergies/deleteRow_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        const errors = [];
        if(!rowId) errors.push('row id required');
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    add_v,
    get_v,
    updateCol_v,
    deleteRow_v
}