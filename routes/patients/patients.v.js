/**
 * @description validation middleware for patient endpoints
 */


const Validator = require('../../common/utils/validators/validator');
const Matcher = require('../../common/utils/patternMatcher');
const Errors = require('../../common/errors/_CustomErrors');
const TableDefinitions = require('../../config/db/tables.json');
const ValidationRules = require('../../config/validation.rules');


function add_v(req, res, next) {

    const { log, reqId } = req;
    const {
        id = null,
        firstName,
        middleName,
        lastName,
        address1,
        address2,
        city,
        state,
        zipcode,
        dateofbirth,
        email,
        phone,
        ssn,
        sex,
        carrierId,
        carrierCode,
        carrierName,
        subscriberId,
        groupName,
        groupNumber,
        carrierId2,
        carrierCode2,
        carrierName2,
        subscriberId2,
        groupName2,
        groupNumber2
    } = req.body;
    const fnName = 'patients/add_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        // Validations
        const validations = [
            new Validator.Validate(id, 'id').bake(),
            new Validator.Validate(firstName, 'first name').required().isString().minLength(ValidationRules.Patient.firstName.minLength).maxLength(ValidationRules.Patient.firstName.maxLength).bake(),
            new Validator.Validate(middleName, 'middle name').isString().minLength(ValidationRules.Patient.middleName.minLength).maxLength(ValidationRules.Patient.middleName.maxLength).bake(),
            new Validator.Validate(lastName, 'last name').required().isString().minLength(ValidationRules.Patient.lastName.minLength).maxLength(ValidationRules.Patient.lastName.maxLength).bake(),
            new Validator.Validate(address1, 'address 1').required().isString().minLength(ValidationRules.Address.address1.minLength).maxLength(ValidationRules.Address.address1.maxLength).bake(),
            new Validator.Validate(address2, 'address 2').isString().minLength(ValidationRules.Address.address2.minLength).maxLength(ValidationRules.Address.address2.maxLength).bake(),
            new Validator.Validate(city, 'city').required().isString().minLength(ValidationRules.Address.city.minLength).maxLength(ValidationRules.Address.city.maxLength).bake(),
            new Validator.Validate(state, 'state').required().isString().minLength(ValidationRules.Address.state.minLength).maxLength(ValidationRules.Address.state.maxLength).bake(),
            new Validator.Validate(zipcode, 'zip code').required().isString().minLength(ValidationRules.Address.zipCode.minLength).maxLength(ValidationRules.Address.zipCode.maxLength).bake(),
            new Validator.Validate(dateofbirth, 'date of birth').required().isDate().maxDate(new Date()).bake(),
            new Validator.Validate(email, 'email').required().isString().isEmail().bake(),
            new Validator.Validate(phone, 'phone number').required().isString().isUsaPhoneNumber().bake(),
            new Validator.Validate(ssn, 'ssn').required().isString().regex(Matcher.regex.ssn).bake(),
            new Validator.Validate(sex, 'sex').required().isString().mustBeIn(ValidationRules.Patient.sex.options).bake(),
            new Validator.Validate(carrierId, 'carrier').required().bake(),
            new Validator.Validate(carrierCode, 'carrier code').required().isString().minLength(ValidationRules.Code.minLength).maxLength(ValidationRules.Code.maxLength).bake(),
            new Validator.Validate(carrierName, 'carrier name').required().isString().minLength(ValidationRules.Carrier.name.minLength).maxLength(ValidationRules.Carrier.name.maxLength).bake(),
            new Validator.Validate(subscriberId, 'subscriber id').required().isString().maxLength(ValidationRules.Carrier.subscriberId.maxLength).bake(),
            new Validator.Validate(groupName, 'group name').isString().maxLength(ValidationRules.Carrier.groupName.maxLength).bake(),
            new Validator.Validate(groupNumber, 'group number').isString().maxLength(ValidationRules.Carrier.groupNumber.maxLength).bake(),
            new Validator.Validate(carrierId2, 'secondary carrier').bake(),
            new Validator.Validate(carrierCode2, 'secondary carrier code').isString().minLength(ValidationRules.Code.minLength).maxLength(ValidationRules.Code.maxLength).bake(),
            new Validator.Validate(carrierName2, 'secondary carrier name').isString().minLength(ValidationRules.Carrier.name.minLength).maxLength(ValidationRules.Carrier.name.maxLength).bake(),
            new Validator.Validate(subscriberId2, 'secondary subscriber id').isString().maxLength(ValidationRules.Carrier.subscriberId.maxLength).bake(),
            new Validator.Validate(groupName2, 'secondary group name').isString().maxLength(ValidationRules.Carrier.groupName.maxLength).bake(),
            new Validator.Validate(groupNumber2, 'secondary group number').isString().maxLength(ValidationRules.Carrier.groupNumber.maxLength).bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);

        if (errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch (e) {
        next(e);
    }
}


function updateOne_v(req, res, next) {

    const { log, reqId } = req;
    const {
        patientId,
        firstName,
        middleName,
        lastName,
        address1,
        address2,
        city,
        state,
        zipcode,
        dateofbirth,
        email,
        phone,
        ssn,
        sex,
        carrierId,
        carrierCode,
        carrierName,
        subscriberId,
        groupName,
        groupNumber,
        carrierId2,
        carrierCode2,
        carrierName2,
        subscriberId2,
        groupName2,
        groupNumber2
    } = req.body;
    const fnName = 'patients/updateOne_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        // Validations
        const validations = [
            new Validator.Validate(patientId, 'patient id').required().bake(),
            new Validator.Validate(firstName, 'first name').isString().minLength(ValidationRules.Patient.firstName.minLength).maxLength(ValidationRules.Patient.firstName.maxLength).bake(),
            new Validator.Validate(middleName, 'middle name').isString().minLength(ValidationRules.Patient.middleName.minLength).maxLength(ValidationRules.Patient.middleName.maxLength).bake(),
            new Validator.Validate(lastName, 'last name').isString().minLength(ValidationRules.Patient.lastName.minLength).maxLength(ValidationRules.Patient.lastName.maxLength).bake(),
            new Validator.Validate(address1, 'address 1').isString().minLength(ValidationRules.Address.address1.minLength).maxLength(ValidationRules.Address.address1.maxLength).bake(),
            new Validator.Validate(address2, 'address 2').isString().minLength(ValidationRules.Address.address2.minLength).maxLength(ValidationRules.Address.address2.maxLength).bake(),
            new Validator.Validate(city, 'city').isString().minLength(ValidationRules.Address.city.minLength).maxLength(ValidationRules.Address.city.maxLength).bake(),
            new Validator.Validate(state, 'state').isString().minLength(ValidationRules.Address.state.minLength).maxLength(ValidationRules.Address.state.maxLength).bake(),
            new Validator.Validate(zipcode, 'zip code').isString().minLength(ValidationRules.Address.zipCode.minLength).maxLength(ValidationRules.Address.zipCode.maxLength).bake(),
            new Validator.Validate(dateofbirth, 'date of birth').isDate().maxDate(new Date()).bake(),
            new Validator.Validate(email, 'email').isString().isEmail().bake(),
            new Validator.Validate(phone, 'phone number').isString().isUsaPhoneNumber().bake(),
            new Validator.Validate(ssn, 'ssn').isString().regex(Matcher.regex.ssn).bake(),
            new Validator.Validate(sex, 'sex').isString().mustBeIn(ValidationRules.Patient.sex.options).bake(),
            new Validator.Validate(carrierId, 'carrier').bake(),
            new Validator.Validate(carrierCode, 'carrier code').isString().minLength(ValidationRules.Code.minLength).maxLength(ValidationRules.Code.maxLength).bake(),
            new Validator.Validate(carrierName, 'carrier name').isString().minLength(ValidationRules.Carrier.name.minLength).maxLength(ValidationRules.Carrier.name.maxLength).bake(),
            new Validator.Validate(subscriberId, 'subscriber id').isString().maxLength(ValidationRules.Carrier.subscriberId.maxLength).bake(),
            new Validator.Validate(groupName, 'group name').isString().maxLength(ValidationRules.Carrier.groupName.maxLength).bake(),
            new Validator.Validate(groupNumber, 'group number').isString().maxLength(ValidationRules.Carrier.groupNumber.maxLength).bake(),
            new Validator.Validate(carrierId2, 'secondary carrier').bake(),
            new Validator.Validate(carrierCode2, 'secondary carrier code').isString().minLength(ValidationRules.Code.minLength).maxLength(ValidationRules.Code.maxLength).bake(),
            new Validator.Validate(carrierName2, 'secondary carrier name').isString().minLength(ValidationRules.Carrier.name.minLength).maxLength(ValidationRules.Carrier.name.maxLength).bake(),
            new Validator.Validate(subscriberId2, 'secondary subscriber id').isString().maxLength(ValidationRules.Carrier.subscriberId.maxLength).bake(),
            new Validator.Validate(groupName2, 'secondary group name').isString().maxLength(ValidationRules.Carrier.groupName.maxLength).bake(),
            new Validator.Validate(groupNumber2, 'secondary group number').isString().maxLength(ValidationRules.Carrier.groupNumber.maxLength).bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);

        if (errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch (e) {
        next(e);
    }
}


function search_v(req, res, next) {

    const { log, reqId } = req;
    const { searchCol, searchQuery, sortCol, sortIsAsc, skip, limit } = req.body;
    const fnName = 'patients/search_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        // Validations
        const validations = [
            new Validator.Validate(searchCol, 'search column').isString().mustBeIn(TableDefinitions.patient, 'invalid search column').bake(),
            new Validator.Validate(searchQuery, 'search query').required(!!searchCol).isString().bake(),
            new Validator.Validate(sortCol, 'sort column').isString().mustBeIn(TableDefinitions.patient, 'invalid sort column').bake(),
            new Validator.Validate(sortIsAsc, 'sort mode').isBoolean().bake(),
            new Validator.Validate(skip, 'skip').isNumber().bake(),
            new Validator.Validate(limit, 'limit').isNumber().bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);

        if (errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch (e) {
        next(e);
    }
}


function searchAdvanced_v(req, res, next) {

    const { log, reqId } = req;
    const { query, skip, limit } = req.body;
    const fnName = 'patients/searchAdvanced_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        // Validations
        const validations = [
            new Validator.Validate(query, 'search query').required().isString().bake(),
            new Validator.Validate(skip, 'skip').required().isNumber().bake(),
            new Validator.Validate(limit, 'limit').required().isNumber().bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);

        if (errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch (e) {
        next(e);
    }
}


function findById_v(req, res, next) {

    const { log, reqId } = req;
    const { patientId } = req.body;
    const fnName = 'patients/findById_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        // Validations
        const validations = [
            new Validator.Validate(patientId, 'patient id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);

        if (errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch (e) {
        next(e);
    }
}


function deleteOne_v(req, res, next) {

    const { log, reqId } = req;
    const { patientId } = req.body;
    const fnName = 'patients/deleteOne_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        // Validations
        const validations = [
            new Validator.Validate(patientId, 'patient id').required().bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);

        if (errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch (e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    add_v,
    updateOne_v,
    search_v,
    searchAdvanced_v,
    findById_v,
    deleteOne_v
}