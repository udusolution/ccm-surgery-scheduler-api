/**
 * @description validation middleware for patient allergies endpoints
 */


const Validator = require('../../common/utils/validators/validator');
const Matcher = require('../../common/utils/patternMatcher');
const Errors = require('../../common/errors/_CustomErrors');


function search_v(req, res, next) {

    const { log, reqId } = req;
    const { query, limit } = req.body;
    const fnName = 'diagnosis/search_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(query, 'search query').required().isString().bake(),
            new Validator.Validate(limit, 'limit').required().isNumber().bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    search_v
}