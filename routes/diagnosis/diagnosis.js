/**
 * @description master data routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../config/config');
const Errors = require('../../common/errors/_CustomErrors');
const M = require('../../resources/languages/messages');
const Enums = require('../../common/enums');
const Auth = require('../../common/middleware/auth.middleware');
const Validators = require('./diagnosis.v');
const DiagnosisRepo = require('../../repos/diagnosis.repo');


/**
 * @description search diagnosis
 */
router.post('/search', Validators.search_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { query, limit } = req.body;

    try{

        log.info(reqId, `calling (DiagnosisRepo)'search'`);
        // -> Search
        const diagnosis = await DiagnosisRepo.search(log, reqId, { query, limit });

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { diagnosis } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;