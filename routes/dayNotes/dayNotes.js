/**
 * @description day notes routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../config/config');
const Errors = require('../../common/errors/_CustomErrors');
const M = require('../../resources/languages/messages');
const Enums = require('../../common/enums');
const Auth = require('../../common/middleware/auth.middleware');
const Validators = require('./dayNotes.v');
const DayNoteRepo = require('../../repos/dayNote.repo');
const History = require('../../repos/history.repo');
const momentTZ = require("moment-timezone");
const moment = require("moment");
const createGoogleCalendarEvent = require('../../common/services/gcalendar').createEvent;
const deleteGoogleCalendarEvent = require('../../common/services/gcalendar').deleteEvent;


/**
 * @description add a day note
 */
router.post('/addOne', Auth.auth(), Validators.addOne_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { day, note, mentions, recurrences, hourFrom, hourTo } = req.body;

    try {

        log.info(reqId, `calling (DayNoteRepo)'addOne'`);
        // -> Add a Day Note
        const noteId = await DayNoteRepo.addOne(log, reqId, {
            userId: req.userId,
            day,
            note,
            mentions,
            recurrences,
            hourFrom,
            hourTo
        });

        // <> GCal
        try {

            const date = momentTZ(moment.utc(day)).tz(Enums.ZoneFormats.MichiganDetroit)
            const startDate = hourFrom ? date.hour(hourFrom) : date.startOf('day');
            const endDate = hourTo ? date.hour(hourTo) : date.startOf('day');

            let recurrence = '';
            switch(recurrences) {
                case Enums.DayNoteRecurrence.Daily:
                    recurrence = 'RRULE:FREQ=DAILY'
                    break;
                case Enums.DayNoteRecurrence.Weekly:
                    recurrence = 'RRULE:FREQ=WEEKLY'
                    break;
                case Enums.DayNoteRecurrence.Monthly:
                    recurrence = 'RRULE:FREQ=MONTHLY'
                    break;
                case Enums.DayNoteRecurrence.Yearly:
                    recurrence = 'RRULE:FREQ=YEARLY'
                    break;
            }
            const gCalEvent = {
                summary: note,
                start: {
                    dateTime: startDate.startOf('day').toISOString(),
                    timeZone: startDate.tz()
                },
                end: {
                    dateTime: endDate.toISOString(),
                    timeZone: endDate.tz()
                },
            }
            if (recurrence) {
                gCalEvent.recurrence = [recurrence]
            }
            const calendarEvent = await createGoogleCalendarEvent(gCalEvent);
            await DayNoteRepo.update(log, reqId, noteId, {
                calendar_id: calendarEvent.data.id,
                recurring_calendar_id: calendarEvent.data.recurringEventId,
            })
        } catch (err) {
            log.error(err.message)
        }

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.AddDayNote, M.history.addedDayNote(recurrences, day, note)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description get a day's note
 */
router.post('/getForDay', Auth.auth(), Validators.getForDay_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { day } = req.body;

    try {

        log.info(reqId, `calling (DayNoteRepo)'getForDay'`);
        // -> Get the Day's Notes
        const notes = await DayNoteRepo.getForDay(log, reqId, day);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { notes } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description get a month's note
 */
router.post('/getForMonth', Auth.auth(), Validators.getForDay_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { day } = req.body;

    try {

        log.info(reqId, `calling (DayNoteRepo)'getForDay'`);
        // -> Get the Month's Notes
        const notes = await DayNoteRepo.getForMonth(log, reqId, day);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { notes } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description delete a day's note
 */
 router.post('/deleteOne', Auth.auth(), Validators.deleteOne_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { noteId } = req.body;

    try {

        log.info(reqId, `calling (DayNoteRepo)'deleteOne'`);
        // -> Get Note
        const note = await DayNoteRepo.findById(log, reqId, noteId);

        // Sanity Check, throw errors in the following cases
        // ? If note not found
        if(!note) throw new Errors.CommonErrors.SanityError('note not found', M.failure.dayNoteNotFound());

        log.info(reqId, `calling (DayNoteRepo)'deleteOne'`);
        // -> Delete Note
        await DayNoteRepo.deleteOne(log, reqId, noteId);
        await deleteGoogleCalendarEvent(note.calendar_id)

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.DeleteDayNote, M.history.deletedDayNote(note.recurrences, note.day, note.note)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;