/**
 * @description validation middleware for priorities endpoints
 */


const Validator = require('../../common/utils/validators/validator');
const Matcher = require('../../common/utils/patternMatcher');
const Errors = require('../../common/errors/_CustomErrors');
const Rules = require('../../config/validation.rules');


function addOne_v(req, res, next) {

    const { log, reqId } = req;
    const { day, note, mentions, recurrences, hourFrom, hourTo } = req.body;
    const fnName = 'dayNotes/addOne_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(day, 'day').required().isDate().bake(),
            new Validator.Validate(note, 'note').required().isString().minLength(Rules.DayNote.note.minLength).maxLength(Rules.DayNote.note.maxLength).bake(),
            new Validator.Validate(mentions, 'mentions').isArray().bake(),
            new Validator.Validate(recurrences, 'recurrences').required().isString().bake(),
            new Validator.Validate(hourFrom, 'from').required(hourTo, 'both "To" and "From" are required').isNumber().bake(),
            new Validator.Validate(hourTo, 'to').required(hourFrom, 'both "To" and "From" are required').isNumber().min(hourFrom, '"To" must be after "From').bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function getForDay_v(req, res, next) {

    const { log, reqId } = req;
    const { day } = req.body;
    const fnName = 'dayNotes/getForDay_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(day, 'day').required().isDate().bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function deleteOne_v(req, res, next) {

    const { log, reqId } = req;
    const { noteId } = req.body;
    const fnName = 'dayNotes/deleteOne_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(noteId, 'day note id').required().bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    addOne_v,
    getForDay_v,
    deleteOne_v
}