/**
 * @description manage users routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../../config/config');
const FacilityPhysicianRepo = require('../../../repos/physicianFacility.repo');
const Errors = require('../../../common/errors/_CustomErrors');
const M = require('../../../resources/languages/messages');
const Enums = require('../../../common/enums');
const Auth = require('../../../common/middleware/auth.middleware');
const Validators = require('./physicianFacilities.v');


/**
 * @description get physician facilities
 * @deprecated
 */
router.post('/getPhysicianFacilities', Validators.getPhysicianFacilities_v, Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin])), async (req, res, next) => {

    const { log, reqId } = req;
    const { physicianId } = req.body;

    try {

        log.info(reqId, `calling (FacilityPhysicianRepo)'getForPhysician'`);
        // -> Get Physician Facility Associations
        const associations = await FacilityPhysicianRepo.getForPhysician(log, reqId, physicianId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { associations } });
        next();
        
    } catch(e) {
        next(e);
    }
});


/**
 * @description add association between physician and facility
 * @deprecated
 */
router.post('/addAssociation', Validators.addAssociation_v, Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin])), async (req, res, next) => {

    const { log, reqId } = req;
    const { physicianId, facilityId } = req.body;

    try{

        log.info(reqId, `calling (FacilityPhysicianRepo)'getForPhysician' to check if already exists`);
        // -> Get Physician Facility Associations
        const associations = await FacilityPhysicianRepo.getForPhysician(log, reqId, physicianId);

        // Sanity Checks, throw error in the following cases
        // ? If association already exists
        if(associations.some(a => a.physicianId == physicianId && a.facilityId == facilityId)) throw new Errors.CommonErrors.SanityError('association already exists', M.failure.physicianFacilityAssociationAlreadyExists());

        log.info(reqId, `calling (FacilityPhysicianRepo)'addAssociation'`);
        // -> Add Association
        await FacilityPhysicianRepo.addAssociation(log, reqId, physicianId, facilityId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description delete association between facility and physician
 * @deprecated
 */
router.post('/deleteAssociation', Validators.deleteAssociation, Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin])), async (req, res, next) => {

    const { log, reqId } = req;
    const { associationId } = req.body;

    try{

        log.info(reqId, `calling (FacilityPhysicianRepo)'deleteAssociation'`);
        // -> Delete Association
        await FacilityPhysicianRepo.deleteAssociation(log, reqId, associationId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;