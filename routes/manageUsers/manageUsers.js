/**
 * @description manage users routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../config/config');
const User = require('../../repos/user.repo');
const Errors = require('../../common/errors/_CustomErrors');
const M = require('../../resources/languages/messages');
const Enums = require('../../common/enums');
const Auth = require('../../common/middleware/auth.middleware');
const Validators = require('./manageUsers.v');
const enc = require('../../common/utils/enc');
const multer = require('multer');
const path = require('path');
const History = require('../../repos/history.repo');
const ObjectUtils = require('../../common/utils/object.utils');
const PnOperations = require('../../operations/notifications');


/**
 * @description add user
 */
 router.post('/addUser', Auth.auth(new Auth.RoleSets().all([Enums.UserRoles.Admin]).bake()), Validators.addUser_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { email, name, password, phone, fax } = req.body;
    const params = req.body;

    try {

        log.info(reqId, `calling (UserRepo)'isEmailTaken'`);
        // -> Check if new email taken
        const isEmailTaken = await User.isEmailTaken(log, reqId, email);

        log.info(reqId, `calling (UserRepo)'isPhoneTaken'`);
        // -> Check if new phone taken
        const isPhoneTaken = await User.isPhoneTaken(log, reqId, phone);
        
        // Sanity Checks, throw error in the following cases
        // ? If email taken
        // ? If phone taken
        if(isEmailTaken) throw new Errors.ValidationErrors.ValidationError(M.failure.emailTaken());
        if(isPhoneTaken) throw new Errors.ValidationErrors.ValidationError(M.failure.phoneTaken());

        log.info(reqId, `calling (enc)'hash' to hash password`);
        // -> Hash password
        const hashedPassword = await enc.hash(log, reqId, password);

        log.info(reqId, `calling (User)'add'`);
        // -> Insert user
        const newUserId = await User.add(log, reqId, email, name, hashedPassword, phone, fax);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.AddUser, M.history.addedUser(newUserId, name));
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { user: { id: newUserId, email, phone } } });
        next();
         
    } catch(e) {
        next(e);
    }
});


/**
 * @description search users
 */
router.post('/search', Auth.auth(), Validators.search_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { searchCol, searchQuery, sortCol, sortIsAsc, skip, limit } = req.body;

    try{

        log.info(reqId, `calling (UserRepo)'search'`);
        // -> Search Users
        const users = await User.search(log, reqId, { searchCol, searchQuery, sortCol, sortIsAsc, skip, limit });

        log.info(reqId, `calling (UserRepo)'totalCount'`);
        // -> Get total users count matching search query
        const usersCount = await User.searchCount(log, reqId, { searchCol, searchQuery });

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { users, usersCount } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description search users with role
 */
router.post('/searchWithRole', Auth.auth(), Validators.searchWithRole_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { searchCol, searchQuery, sortCol, sortIsAsc, skip, limit, role } = req.body;

    try{

        log.info(reqId, `calling (UserRepo)'search'`);
        // -> Search Users
        const users = await User.searchWithRole(log, reqId, { searchCol, searchQuery, sortCol, sortIsAsc, skip, limit, role });

        log.info(reqId, `calling (UserRepo)'totalCount'`);
        // -> Get total users count matching search query
        const usersCount = await User.searchWithRoleCount(log, reqId, { searchCol, searchQuery, role });

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { users, usersCount } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description get a user's details
 */
router.post('/getUserDetails', Validators.getUserDetails_v, Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;
    const { userId } = req.body;

    try {

        log.info(reqId, `calling (User)'getDetails' to get user with his roles`);
        // -> Get user (with details)
        const userDetails = await User.getDetails(log, reqId, userId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: '', data: { userDetails: userDetails.user, userRoles: userDetails.roles } });
        next();
            
    } catch(e) {
        next(e);
    }
});


/**
 * @description delete a user
 */
router.post('/deleteUser', Validators.deleteUser_v, Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin])), async (req, res, next) => {

    const { log, reqId } = req;
    const { userId } = req.body;

    try {

        log.info(reqId, `calling (User)'findById'`);
        // -> Get User
        const user = await User.findById(log, reqId, userId);

        // Sanity Checks, throw errors in the following cases
        // ? If user not found
        // ? If user already deleted
        if(!user)             throw new Errors.CommonErrors.SanityError('user not found', M.failure.userNotFound());
        if(user.isDeleted)    throw new Errors.CommonErrors.SanityError('user already deleted', M.failure.userAlreadyDeleted());

        log.info(reqId, `calling (User)'deleteUser'`);
        // -> Delete user
        await User.deleteUser(log, reqId, userId);

        log.info(reqId, `calling (User)'deleteUser'`);
        // -> Delete user scheduled productivity reports
        await User.deleteUserScheduledProductivityReports(log, reqId, userId);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.DeleteUser, M.history.deletedUser(userId, user.name), JSON.stringify(user));
        res.status(Enums.HttpCodes.Success).json({ message: '', data: {} });
        next();
            
    } catch(e) {
        next(e);
    }
});


/**
 * @description update a user's details
 */
router.post('/updateUserDetails', 
    Auth.auth(new Auth.RoleSets().all([Enums.UserRoles.Admin]).bake()),
    Validators.updateUserDetails_v,
    async (req, res, next) => {

    const { log, reqId } = req;
    const { userId, email, name, phone, fax, updateRoles, roles, officePhone, officeAddress, color } = req.body;
    const params = req.body;

    try {

        log.info(reqId, `calling (User)'findById'`);
        // -> Get User
        const user = await User.findById(log, reqId, userId);

        // Sanity Checks, throw errors in the following cases
        // ? If user not found
        if(!user) throw new Errors.CommonErrors.SanityError('user not found', M.failure.userNotFound());

        if(email) {
            log.info(reqId, `calling (UserRepo)'isEmailTaken'`);
            // -> Check if new email taken
            const isNewEmailTaken = await User.isEmailTaken(log, reqId, email);

            if(isNewEmailTaken) throw new Errors.CommonErrors.SanityError('email taken', M.failure.emailTaken());
        }

        if(phone) {
            log.info(reqId, `calling (UserRepo)'isPhoneTaken'`);
            // -> Check if new phone taken
            const isNewPhoneTaken = await User.isPhoneTaken(log, reqId, phone);

            if(isNewPhoneTaken) throw new Errors.CommonErrors.SanityError('phone taken', M.failure.phoneTaken());
        }

        log.info(reqId, `calling (UserRepo)'update'`);
        // -> Update User
        await User.update(log, reqId, userId, { email, name, phone, fax, officePhone, officeAddress, color });

        if(updateRoles && roles) {
            log.info(reqId, `calling (UserRepo)'updateRoles'`);
            // -> Update User Roles
            await User.updateRoles(log, reqId, userId, roles);
        }

        // <> Send PN
        PnOperations.sendPn(log, reqId, userId, M.pn.updatedUser.title(), M.pn.updatedUser.message(), { type: Enums.PnEventTypes.UpdatedUser });

        // <> History
        const newChanges = {...params};
        if(!newChanges.email) delete newChanges.email;
        if(!newChanges.phone) delete newChanges.phone;
        const differences = ObjectUtils.getDiff(user, newChanges).map(d => `- Changed "${d.key}" from "${d.oldVal}" to "${d.newVal}"`).join('\n');
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.UpdateUser, M.history.updatedUser(userId, user.name, differences), JSON.stringify(user), JSON.stringify(params));

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
        next();
            
    } catch(e) {
        next(e);
    }
});


/**
 * @description update a user's favorite event type
 */
router.post('/updateFavoriteEventType',
Auth.auth(new Auth.RoleSets().all([Enums.UserRoles.Admin]).bake()),
    Validators.updateFavoriteEventType_v,
    async (req, res, next) => {

    const { log, reqId } = req;
    const { userId, eventTypeId } = req.body;

    try {

        log.info(reqId, `calling (UserRepo)'updateFavoriteEventType'`);
        // -> Update User
        await User.updateFavoriteEventType(log, reqId, userId, eventTypeId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
        next();
            
    } catch(e) {
        next(e);
    }
});


/**
 * @description get a user's details
 */
router.post('/getUsersWithRole', Auth.auth(), Validators.getUsersWithRole_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { role } = req.body;

    try {

        log.info(reqId, `calling (User)'getWithRole'`);
        // -> Get users with role
        const users = await User.getWithRole(log, reqId, role);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: '', data: { users } });
        next();
            
    } catch(e) {
        next(e);
    }
});


/**
 * @description get physicians with details
 */
router.get('/getPhysiciansWithDetails', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;

    try {

        log.info(reqId, `calling (User)'getPhysiciansWithDetails'`);
        // -> Get physicians with details
        const physicians = await User.getPhysiciansWithDetails(log, reqId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: '', data: { physicians } });
        next();
            
    } catch(e) {
        next(e);
    }
});


/**
 * @description get all users
 */
router.get('/getAll', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;

    try {

        log.info(reqId, `calling (User)'getAll'`);
        // -> Get All Users
        const users = await User.getAll(log, reqId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: '', data: { users } });
        next();
            
    } catch(e) {
        next(e);
    }
});

/**
 * @description get all users with their roles
 */
router.get('/getAllWithRoles', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;

    try {

        log.info(reqId, `calling (User)'getAllWithRoles'`);
        // -> Get All Users
        const users = await User.getAllWithRoles(log, reqId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: '', data: { users } });
        next();
            
    } catch(e) {
        next(e);
    }
});


/**
 * MULTER CONFIGS FOR THIS ENDPOINT
 */
 const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/signatures')
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname)
    }
})
const upload = multer({ 
    storage: storage,
    fileFilter: function (req, file, cb) {
        if (file.mimetype !== 'image/png') {
            return cb(new Error('Image must be a png'));
        }

        cb(null, true)
    }
}).single('file');

/**
 * @description set user signature
 */
router.post('/setSignature', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;

    upload(req, res, async function (err) {

        try {

            // Sanity Checks, throw errors in the following cases
            // ? If multer returned an error
            // ? If general error was returned
            if (err instanceof multer.MulterError)  throw new Errors.FileUploadErrors.FileUploadError(err.message, err);
            else if (err)                           throw new Errors.FileUploadErrors.FileUploadError(err.message, err);

            const { userId } = req.body;
            let signature = req.file.path.replace(/^public[\\\/]/, '').replace(`\\`, '/');

            log.info(reqId, `calling (User)'findById'`);
            // -> Get User
            const user = await User.findById(log, reqId, userId);

            // Sanity Checks, throw errors in the following cases
            // ? If user not found
            if(!user) throw new Errors.CommonErrors.SanityError('user not found', M.failure.userNotFound());

            log.info(reqId, `calling (UserRepo)'setSignature'`);
            // -> Set User Signature
            await User.setSignature(log, reqId, userId, signature);

            // End
            log.info(reqId, `end [success]`);
            History.add(log, reqId, req.userId, null, Enums.HistoryEvents.SetUserSignature, M.history.setUserSignature(userId, user.name));
            res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
        
        }
        catch(e) {

            next(e);
        }
    })

})


/**
 * @description change a user's password
 */
router.post('/changePassword', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin]).bake()), Validators.changePassword_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { userId, newPassword } = req.body;

    try {

        log.info(reqId, `calling (User)'findById'`);
        // -> Get User
        const user = await User.findById(log, reqId, userId);

        // Sanity Checks, throw errors in the following cases
        // ? If user not found
        if(!user) throw new Errors.CommonErrors.SanityError('user not found', M.failure.userNotFound());

        log.info(reqId, `calling (enc)'hash' to hash new password`);
        // -> Hash new password
        const hashedNewPassword = await enc.hash(log, reqId, newPassword);

        log.info(reqId, `calling (User)'changePassword'`);
        // -> Update user password
        await User.changePassword(log, reqId, userId, hashedNewPassword);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.ChangeUserPassword, M.history.changedUserPassword(userId, user.name));
        res.status(Enums.HttpCodes.Success).json({ message: '', data: {} });
        next();
            
    } catch(e) {
        next(e);
    }
});


/**
 * @description lock a user's account
 * @note this does not delete them or make them invisible, only prevents them from logging in
 */
router.post('/lockUser', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin]).bake()), Validators.lockUser_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { userId } = req.body;

    try {

        log.info(reqId, `calling (User)'findById'`);
        // -> Get User
        const user = await User.findById(log, reqId, userId);

        // Sanity Checks, throw errors in the following cases
        // ? If user not found
        // ? If user is already locked
        if(!user)           throw new Errors.CommonErrors.SanityError('user not found', M.failure.userNotFound());
        if(user.isLocked)   throw new Errors.CommonErrors.SanityError('user already locked', M.failure.userAlreadyLocked());

        log.info(reqId, `calling (User)'lock'`);
        // -> Lock the User's Account
        await User.lock(log, reqId, userId);

        // <> Send PN
        PnOperations.sendPn(log, reqId, userId, M.pn.userLocked.title(), M.pn.userLocked.message(), { type: Enums.PnEventTypes.UserLocked });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.LockUserAccount, M.history.lockedUserAccount(userId, user.name)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: '', data: {} });
        next();
            
    } catch(e) {
        next(e);
    }
});


/**
 * @description unlock a user's account
 */
router.post('/unlockUser', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin]).bake()), Validators.unlockUser_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { userId } = req.body;

    try {

        log.info(reqId, `calling (User)'findById'`);
        // -> Get User
        const user = await User.findById(log, reqId, userId);

        // Sanity Checks, throw errors in the following cases
        // ? If user not found
        // ? If user is not locked
        if(!user)            throw new Errors.CommonErrors.SanityError('user not found', M.failure.userNotFound());
        if(!user.isLocked)   throw new Errors.CommonErrors.SanityError('user already unlocked', M.failure.userAlreadyLocked());

        log.info(reqId, `calling (User)'unlock'`);
        // -> Unlock the User's Account
        await User.unlock(log, reqId, userId);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.UnlockUserAccount, M.history.unlockedUserAccount(userId, user.name)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: '', data: {} });
        next();
            
    } catch(e) {
        next(e);
    }
});


/**
 * Exports
 */
module.exports = router;