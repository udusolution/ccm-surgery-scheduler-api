/**
 * @description validation middleware for manage users endpoints
 */


const Validator = require('../../common/utils/validators/validator');
const Matcher = require('../../common/utils/patternMatcher');
const Errors = require('../../common/errors/_CustomErrors');
const TableDefinitions = require('../../config/db/tables.json');
const ValidationRules = require('../../config/validation.rules');


function addUser_v(req, res, next) {

    const { log, reqId } = req;
    const { email, password, phone } = req.body;
    const fnName = 'manageUsers/addUser_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(email, 'email').required().isString().isEmail().bake(),
            new Validator.Validate(phone, 'phone number').required().isString().isUsaPhoneNumber().bake(),
            new Validator.Validate(password, 'password').required().isString().bake(), // ! NV
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function search_v(req, res, next) {

    const { log, reqId } = req;
    const { searchCol, searchQuery, sortCol, sortIsAsc, skip, limit } = req.body;
    const fnName = 'manageUsers/search_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(searchCol, 'search column').isString().mustBeIn(TableDefinitions.user, 'invalid search column').bake(),
            new Validator.Validate(searchQuery, 'search query').required(!!searchCol).isString().bake(),
            new Validator.Validate(sortCol, 'sort column').isString().mustBeIn(TableDefinitions.user, 'invalid sort column').bake(),
            new Validator.Validate(sortIsAsc, 'sort mode').isBoolean().bake(),
            new Validator.Validate(skip, 'skip').isNumber().bake(),
            new Validator.Validate(limit, 'limit').isNumber().bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function searchWithRole_v(req, res, next) {

    const { log, reqId } = req;
    const { searchCol, searchQuery, sortCol, sortIsAsc, skip, limit, role } = req.body;
    const fnName = 'manageUsers/search_v';

    try {

        log.info(reqId, `${fnName} ### start ### request: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(searchCol, 'search column').isString().mustBeIn(TableDefinitions.user, 'invalid search column').bake(),
            new Validator.Validate(searchQuery, 'search query').required(!!searchCol).isString().bake(),
            new Validator.Validate(sortCol, 'sort column').isString().mustBeIn(TableDefinitions.user, 'invalid sort column').bake(),
            new Validator.Validate(sortIsAsc, 'sort mode').isBoolean().bake(),
            new Validator.Validate(skip, 'skip').isNumber().bake(),
            new Validator.Validate(limit, 'limit').isNumber().bake(),
            new Validator.Validate(role, 'role').isString().bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function getUserDetails_v(req, res, next) {

    const { log, reqId } = req;
    const { userId } = req.body;
    const fnName = 'manageUsers/getUserDetails_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(userId, 'user id').required().bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function deleteUser_v(req, res, next) {

    const { log, reqId } = req;
    const { userId } = req.body;
    const fnName = 'manageUsers/deleteUser_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(userId, 'user id').required().bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function updateUserDetails_v(req, res, next) {

    const { log, reqId } = req;
    const { userId, email, name, phone, fax, updateRoles, roles, officePhone, officeAddress, color } = req.body;
    const fnName = 'manageUsers/updateUserDetails_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(userId, 'user id').required().bake(),
            new Validator.Validate(email, 'email').isString().isEmail().bake(),
            new Validator.Validate(name, 'name').isString().minLength(ValidationRules.User.name.minLength).maxLength(ValidationRules.User.name.maxLength).bake(),
            new Validator.Validate(phone, 'phone number').isString().isUsaPhoneNumber().bake(),
            new Validator.Validate(fax, 'fax').isString().isUsaPhoneNumber().bake(),
            new Validator.Validate(updateRoles, 'updateRoles').isBoolean().bake(),
            new Validator.Validate(roles, 'roles').required(updateRoles == true).isArray().bake(),
            new Validator.Validate(officePhone, 'office phone number').isString().isUsaPhoneNumber().bake(),
            new Validator.Validate(officeAddress, 'office address').isString().bake(),
            new Validator.Validate(color, 'color').isString().isHexColor().bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function updateFavoriteEventType_v(req, res, next) {

    const { log, reqId } = req;
    const { userId, eventTypeId } = req.body;
    const fnName = 'manageUsers/updateFavoriteEventType_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(userId, 'user id').required().bake(),
            new Validator.Validate(eventTypeId, 'event type').bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}

function getUsersWithRole_v(req, res, next) {

    const { log, reqId } = req;
    const { role } = req.body;
    const fnName = 'manageUsers/getUsersWithRole_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(role, 'role').required().isString().bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}

function changePassword_v(req, res, next) {

    const { log, reqId } = req;
    const { userId, newPassword } = req.body;
    const fnName = 'manageUsers/changePassword_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(userId, 'user id').required().bake(),
            new Validator.Validate(newPassword, 'new password').required().isString().bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function lockUser_v(req, res, next) {

    const { log, reqId } = req;
    const { userId } = req.body;
    const fnName = 'manageUsers/lockUser_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(userId, 'user id').required().bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function unlockUser_v(req, res, next) {

    const { log, reqId } = req;
    const { userId } = req.body;
    const fnName = 'manageUsers/unlockUser_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(userId, 'user id').required().bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    addUser_v,
    search_v,
    searchWithRole_v,
    getUserDetails_v,
    updateUserDetails_v,
    updateFavoriteEventType_v,
    getUsersWithRole_v,
    changePassword_v,
    deleteUser_v,
    lockUser_v,
    unlockUser_v
}