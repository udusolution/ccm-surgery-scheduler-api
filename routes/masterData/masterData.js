/**
 * @description master data routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../config/config');
const MasterData = require('../../repos/masterData.repo');
const Errors = require('../../common/errors/_CustomErrors');
const M = require('../../resources/languages/messages');
const Enums = require('../../common/enums');
const Auth = require('../../common/middleware/auth.middleware');
const Validators = require('./masterData.v');
const History = require('../../repos/history.repo');


/**
 * @description add a new master data
 */
router.post('/add', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin]).bake()), Validators.add_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { table, data } = req.body;

    try{

        log.info(reqId, `calling (MasterDataRepo)'add'`);
        // -> Insert Master Data
        const newMasterData = await MasterData.add(log, reqId, table, data);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.AddMasterData, M.history.addedMasterData(table, data.name)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { masterData: newMasterData } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description get master data table
 */
router.post('/get', Auth.auth(), Validators.get_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { table } = req.body;

    try{

        log.info(reqId, `calling (MasterDataRepo)'getTable'`);
        // -> Get Master Data Table
        const masterData = await MasterData.getTable(log, reqId, table);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { masterData } });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description get multiple master data table
 */
router.post('/getMany', Auth.auth(), Validators.getMany_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { tables } = req.body;

    try{

        log.info(reqId, `calling (MasterDataRepo)'getTables'`);
        // -> Get Master Data Table
        const masterData = await MasterData.getTables(log, reqId, tables);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { ...masterData } });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description get multiple master data table rows
 */
 router.post('/getRows', Auth.auth(), Validators.getRows_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { table, rowIds } = req.body;

    try{

        log.info(reqId, `calling (MasterDataRepo)'getRows'`);
        // -> Get Master Data Table Rows
        const rows = await MasterData.getRows(log, reqId, table, rowIds);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { rows } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description add a new patient
 */
router.put('/updateCol', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin]).bake()), Validators.updateCol_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { table, rowId, col, val } = req.body;

    try{

        log.info(reqId, `calling (MasterDataRepo)'getRow'`);
        // -> Get Master Data Row
        const row = await MasterData.getRow(log, reqId, table, rowId);

        // Sanity Checks, throw errors in the following cases
        // ? If row not found
        if(!row) throw new Errors.CommonErrors.SanityError('master data row not found', M.failure.masterFilesEntryNotFound());

        log.info(reqId, `calling (MasterDataRepo)'updateCol'`);
        // -> Update Master Data Column
        await MasterData.updateCol(log, reqId, table, rowId, col, val);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.UpdateMasterData, M.history.updatedMasterData(table, row.name || row.title || row.description || row.code || row.id, col, row[col], val))
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description delete master data row
 */
router.delete('/deleteRow/:table/:rowId', Auth.auth(new Auth.RoleSets().any([Enums.UserRoles.Admin]).bake()), Validators.deleteRow_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { table, rowId } = req.params;

    try{

        log.info(reqId, `calling (MasterDataRepo)'getRow'`);
        // -> Get Master Data Row
        const row = await MasterData.getRow(log, reqId, table, rowId);

        // Sanity Checks, throw errors in the following cases
        // ? If row not found
        if(!row) throw new Errors.CommonErrors.SanityError('master data row not found', M.failure.masterFilesEntryNotFound());

        log.info(reqId, `calling (MasterDataRepo)'deleteRow'`);
        // -> Delete Master Data Entry
        await MasterData.deleteRow(log, reqId, table, rowId);

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.DeleteMasterData, M.history.deletedMasterData(table, row.name || row.title || row.description || row.code || row.id), JSON.stringify(row)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;