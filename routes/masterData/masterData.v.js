/**
 * @description validation middleware for master data endpoints
 */


const Validator = require('../../common/utils/validators/validator');
const Matcher = require('../../common/utils/patternMatcher');
const Errors = require('../../common/errors/_CustomErrors');
const TableDefinitions = require('../../config/db/tables.json');
const ValidationRules = require('../../config/validation.rules');


function add_v(req, res, next) {

    const { log, reqId } = req;
    const { table, data } = req.body;
    const fnName = 'masterData/add_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        // Validations
        const validations = [
            new Validator.Validate(table, 'master files type').required().isString().mustBeIn(TableDefinitions.masterData.map(m => m.table), 'invalid master file type').bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function get_v(req, res, next) {

    const { log, reqId } = req;
    const { table } = req.body;
    const fnName = 'masterData/get_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        // Validations
        const validations = [
            new Validator.Validate(table, 'master files type').required().isString().mustBeIn(TableDefinitions.masterData.map(m => m.table), 'invalid master file type').bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}

function getRows_v(req, res, next) {

    const { log, reqId } = req;
    const { table, rowIds } = req.body;
    const fnName = 'masterData/getRows_v';

    try {

        log.info(reqId, `${fnName} ### start ### request: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(table, 'master files type').required().isString().mustBeIn(TableDefinitions.masterData.map(m => m.table), 'invalid master file type').bake(),
            new Validator.Validate(rowIds, 'row IDs').required().isArray().minLength(1).bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function getMany_v(req, res, next) {

    const { log, reqId } = req;
    const { tables } = req.body;
    const fnName = 'masterData/getMany_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        // Validations
        const validations = [
            new Validator.Validate(tables, 'master files types').required().isArray().eachMustBeIn(TableDefinitions.masterData.map(m => m.table), 'invalid master file types').bake()
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function updateCol_v(req, res, next) {

    const { log, reqId } = req;
    const { table, rowId, col, val } = req.body;
    const fnName = 'masterData/updateCol_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        // const errors = [];
        // if(!table) errors.push('master data type required');
        // else if(!TableDefinitions.masterData.some(d => d.table == table)) errors.push('invalid master data type');
        // if(!rowId) errors.push('row id required');
        // if(!col) errors.push('col required');
        // else if(!TableDefinitions.masterData.find(d => d.table == table)?.cols.includes(col)) errors.push('invalid col');
        // if(!val) errors.push('val required');

        // Validations
        const validations = [
            new Validator.Validate(table, 'master files type').required().isString().mustBeIn(TableDefinitions.masterData.map(m => m.table), 'invalid master file type').bake(),
            new Validator.Validate(rowId, 'row').required().bake(),
            new Validator.Validate(col, 'column').required().isString().mustBeIn(TableDefinitions.masterData.find(d => d.table == table)?.cols, 'invalid column').bake(),
            new Validator.Validate(val, 'new value').bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function deleteRow_v(req, res, next) {

    const { log, reqId } = req;
    const { table, rowId } = req.params;
    const fnName = 'masterData/deleteRow_v';

    try {

        log.info(reqId, `${fnName} ### start`);

        // Validations
        const validations = [
            new Validator.Validate(table, 'master files type').required().isString().mustBeIn(TableDefinitions.masterData.map(m => m.table), 'invalid master file type').bake(),
            new Validator.Validate(rowId, 'row').required().bake(),
        ]

        // Check
        const errors = validations.filter(v => !v[0]).map(v => v[1]);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    add_v,
    get_v,
    getMany_v,
    updateCol_v,
    deleteRow_v,
    getRows_v
}