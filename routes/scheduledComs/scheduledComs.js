/**
 * @description master data routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../config/config');
const Errors = require('../../common/errors/_CustomErrors');
const M = require('../../resources/languages/messages');
const Enums = require('../../common/enums');
const Auth = require('../../common/middleware/auth.middleware');
const Validators = require('./scheduledComs.v');
const ScheduledComRepo = require('../../repos/scheduledCom.repo');
const MasterDataRepo = require('../../repos/masterData.repo');
const History = require('../../repos/history.repo');


/**
 * @description add email scheduled communication ruling
 */
router.post('/addEmailScheduledCommunication', Auth.auth(), Validators.addEmailScheduledCommunication_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { templateId, whenToSend, rule, forWhom } = req.body;

    try {

        log.info(reqId, `calling (MasterDataRepo)'getRow' to get email template from 'email' table`);
        // -> Get Email Template
        const template = await MasterDataRepo.getRow(log, reqId, Enums.MasterDataTables.Email, templateId);

        // Sanity Checks, throw error in the following cases
        // ? If email template not found
        if(!template) throw new Errors.CommonErrors.SanityError('email template not found', M.failure.emailTemplateNotFound());

        log.info(reqId, `calling (ScheduledComRepo)'addEmailScheduledCommunication'`);
        // -> Add Ruling
        await ScheduledComRepo.addEmailScheduledCommunication(log, reqId, { templateId, whenToSend, rule, forWhom });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.AddMasterData, M.history.addedEmailScheduledComRule(template.name)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description add email scheduled communication ruling
 */
router.get('/getAllEmailScheduledCommunications', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;

    try {

        log.info(reqId, `calling (ScheduledComRepo)'getAllEmailScheduledCommunications'`);
        // -> Get All Scheduled Email Communications
        const scheduledCommunications = await ScheduledComRepo.getAllEmailScheduledCommunications(log, reqId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { scheduledCommunications } });
    }
    catch(e) {

        next(e);
    }

})

/**
 * @description add sms scheduled communication ruling
 */
 router.post('/addSmsScheduledCommunication', Auth.auth(), Validators.addSmsScheduledCommunication_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { templateId, whenToSend, rule, forWhom } = req.body;

    try {

        log.info(reqId, `calling (MasterDataRepo)'getRow' to get sms template from 'sms' table`);
        // -> Get Sms Template
        const template = await MasterDataRepo.getRow(log, reqId, Enums.MasterDataTables.Sms, templateId);

        // Sanity Checks, throw error in the following cases
        // ? If sms template not found
        if(!template) throw new Errors.CommonErrors.SanityError('sms template not found', M.failure.smsTemplateNotFound());

        log.info(reqId, `calling (ScheduledComRepo)'addSmsScheduledCommunication'`);
        // -> Add Ruling
        await ScheduledComRepo.addSmsScheduledCommunication(log, reqId, { templateId, whenToSend, rule, forWhom });

        // End
        log.info(reqId, `end [success]`);
        History.add(log, reqId, req.userId, null, Enums.HistoryEvents.AddMasterData, M.history.addedSmsScheduledComRule(template.name)).catch(() => {});
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description add sms scheduled communication ruling
 */
router.get('/getAllSmsScheduledCommunications', Auth.auth(), async (req, res, next) => {

    const { log, reqId } = req;

    try {

        log.info(reqId, `calling (ScheduledComRepo)'getAllSmsScheduledCommunications'`);
        // -> Get All Scheduled Sms Communications
        const scheduledCommunications = await ScheduledComRepo.getAllSmsScheduledCommunications(log, reqId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { scheduledCommunications } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;