/**
 * @description validation middleware for scheduled communication endpoints
 */


const Validator = require('../../common/utils/validators/validator');
const Matcher = require('../../common/utils/patternMatcher');
const Errors = require('../../common/errors/_CustomErrors');


function addEmailScheduledCommunication_v(req, res, next) {

    const { log, reqId } = req;
    const { templateId, whenToSend, rule, forWhom } = req.body;
    const fnName = 'priorities/addEmailScheduledCommunication_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(templateId, 'email template').required().bake(),
            new Validator.Validate(whenToSend, 'when to send').required().isNumber().bake(),
            new Validator.Validate(rule, 'ruling').isString().regex(Matcher.regex.scheduledComRuling, 'Invalid ruling').bake(),
            new Validator.Validate(forWhom, 'for whom').isString().mustBeIn(['patient', 'physician'], 'Scheduled Communication must be for patient or physician').bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


function addSmsScheduledCommunication_v(req, res, next) {

    const { log, reqId } = req;
    const { templateId, whenToSend, rule, forWhom } = req.body;
    const fnName = 'priorities/addSmsScheduledCommunication_v';

    try {

        log.info(reqId, `${fnName} ### start ### request body: ${JSON.stringify(req.body)}`);

        // Validations
        const validations = [
            new Validator.Validate(templateId, 'sms template').required().bake(),
            new Validator.Validate(whenToSend, 'when to send').required().isNumber().bake(),
            new Validator.Validate(rule, 'ruling').isString().regex(Matcher.regex.scheduledComRuling, 'Invalid ruling').bake(),
            new Validator.Validate(forWhom, 'for whom').isString().mustBeIn(['patient', 'physician'], 'Scheduled Communication must be for patient or physician').bake(),
        ]

        // Check
        const errors = Validator.getErrors(validations);
        
        if(errors.length) {
            log.info(reqId, `${fnName} ### end [failure] ### errors: ${JSON.stringify(errors)}`);
            next(new Errors.ValidationErrors.ValidationError(errors.join(', '), errors));
        }
        else {
            log.info(reqId, `${fnName} ### end [success]`);
            next();
        }
    }
    catch(e) {
        next(e);
    }
}


/**
 * Exports
 */
module.exports = {
    addEmailScheduledCommunication_v,
    addSmsScheduledCommunication_v
}