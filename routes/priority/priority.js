/**
 * @description master data routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../config/config');
const Errors = require('../../common/errors/_CustomErrors');
const M = require('../../resources/languages/messages');
const Enums = require('../../common/enums');
const Auth = require('../../common/middleware/auth.middleware');
const Validators = require('./priority.v');
const PriorityRepo = require('../../repos/priority.repo');


/**
 * @description set a priority as default
 */
router.post('/setAsDefault', Auth.auth(), Validators.setAsDefault_v, async (req, res, next) => {

    const { log, reqId } = req;
    const { rowId } = req.body;

    try{

        log.info(reqId, `calling (PriorityRepo)'setAsDefault'`);
        // -> Set as Default
        await PriorityRepo.setAsDefault(log, reqId, rowId);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: {} });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;