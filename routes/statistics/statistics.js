/**
 * @description statistics routes
 */


const express = require('express');
const router = express.Router();
const config = require('../../config/config');
const Errors = require('../../common/errors/_CustomErrors');
const M = require('../../resources/languages/messages');
const Enums = require('../../common/enums');
const Auth = require('../../common/middleware/auth.middleware');
const StatisticsRepo = require('../../repos/statistics.repo');
const Reports = require('../../operations/reports');


/**
 * @description get general app statistics
 */
router.post('/all', async (req, res, next) => {

    const { log, reqId } = req;
    const filters = req.body;

    try {

        log.info(reqId, `gather promises`);
        // -> Gather Promises
        const promises = [
            StatisticsRepo.countPatients(log, reqId),
            StatisticsRepo.countEvents(log, reqId),
            StatisticsRepo.countUsers(log, reqId),
            StatisticsRepo.countEvents(log, reqId, filters),
            StatisticsRepo.countEventStatuses(log, reqId, filters),
            StatisticsRepo.countEventFacilities(log, reqId, filters),
            StatisticsRepo.countEventCancelReasons(log, reqId, filters),
            StatisticsRepo.countEventTypes(log, reqId, filters),
            StatisticsRepo.countCoordinatorEvents(log, reqId, filters),
        ]

        log.info(reqId, `run promises`);
        // -> Run Promises
        const stats = await Promise.all(promises);

        log.info(reqId, `build result`);
        // -> Build Result
        const final = {
            totalPatientsCount: stats[0],
            totalEventsCount: stats[1],
            totalUsersCount: stats[2],
            eventsCount: stats[3],
            eventStatusCount: stats[4],
            eventFacilitiesCount: stats[5],
            eventCancelReasonsCount: stats[6],
            eventTypesCount: stats[7],
            coordinatorEventsCount: stats[8],
        }

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { ...final } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * @description get general app statistics
 */
router.post('/productivity', async (req, res, next) => {

    const { log, reqId } = req;
    const { dateFrom, dateTo, role, userId } = req.body;

    
    try {

        // Filters
        const filters = {
            dateFrom,
            dateTo,
        }
        if(role == Enums.UserRoles.Coordinator) filters.coordinatorId = userId;
        if(role == Enums.UserRoles.Physician) filters.physicianId = userId;

        log.info(reqId, `calling (Reports)'getProductivityReport' with filters ${JSON.stringify(filters)}`);
        // -> Get Productivity Report
        const stats = await Reports.getProductivityReport(log, reqId, filters);

        // End
        log.info(reqId, `end [success]`);
        res.status(Enums.HttpCodes.Success).json({ message: M.response.success(), data: { ...stats } });
    }
    catch(e) {

        next(e);
    }

})


/**
 * Exports
 */
module.exports = router;