/**
 * @description middleware to inject email and sms templates defined in the database
 * @note uses the tables "email" and "sms"
 */


const EmailTemplateRepo = require('../../repos/template.email.repo');
const SmsTemplateRepo = require('../../repos/template.sms.repo');
const con = require('../../config/db/db');


async function injectGeneralTemplates(req, res, next) {

    const fnName = 'templatesInjector/injectGeneralTemplates'
    const { log, reqId } = req;
    log.info(reqId, `${fnName} ### start`);

    try {

        log.info(reqId, `${fnName} ### calling (EmailTemplateRepo)'getAll'`);
        // -> Get Email Templates
        const emailTemplates = await EmailTemplateRepo.getAll(log, reqId);

        log.info(reqId, `${fnName} ### calling (SmsTemplateRepo)'getAll'`);
        // -> Get Sms Templates
        const smsTemplates = await SmsTemplateRepo.getAll(log, reqId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        req.emailTemplates = emailTemplates;
        req.smsTemplates = smsTemplates;
        next();
    }
    catch (e) {

        next(e);
    }
}


async function injectBSEmailTemplate(req, res, next) {

    const fnName = 'templatesInjector/injectBSEmailTemplate'
    const { log, reqId } = req;
    log.info(reqId, `${fnName} ### start`);

    try {

        log.info(reqId, `${fnName} ### [query] get template id from app table`);
        // -> Get template id from app table
        const [results] = await con.query(`SELECT bsTemplateId FROM app_data WHERE id = 2`);
        const templateId = results[0]?.bsTemplateId;

        if(templateId) {
            
            log.info(reqId, `${fnName} ### calling (EmailTemplateRepo)'findById'`);
            // -> Get Email Template
            const emailTemplate = await EmailTemplateRepo.findById(log, reqId, templateId);

            
            req.bsEmailTemplate = emailTemplate;
        }
        else {
            log.info(reqId, `${fnName} ### no boarding slip email template set`);
        }


        // End
        log.info(reqId, `${fnName} ### end [success]`);
        next();
    }
    catch (e) {

        next(e);
    }
}


/**
 * Export
 */
module.exports = {
    injectGeneralTemplates,
    injectBSEmailTemplate
}