/**
 * @description authorization and authentication module that contains auth middleware
 * @note uses the Policy Factory classes
 */


const Errors = require('../errors/_CustomErrors');
const jwt = require('../utils/jwt');
const User = require('../../repos/user.repo');


/**
 * returns a middleware for authenticating and authorizing access to endpoints
 * @param {string[]} roleSets role-sets that the user must have. Example: ["role1,role2", "role3"] means user must have (role1 AND role2), OR (role3). Leave null if no roles are needed.
 */
function auth(roleSets = [], options = { bypassLockCheck: false, bypassDeletionCheck: false }) {
    return async function(req, res, next) {

        const fnName = 'auth/auth'
        const { log, reqId } = req;
        log.info(reqId, `${fnName} ### start`);

        try {

            // -> Get token from header
            const accessToken = req.header('access-token');

            // Sanity checks, throw error in following cases
            // ? if access token not provided
            if(!accessToken) throw new Errors.AuthErrors.MissingAccessTokenAuthError();

            log.info(reqId, `${fnName} ### calling (jwt)'decryptAccessToken'`);
            // -> Decrypt JWT access token
            const decryptedAccessToken = jwt.decryptAccessToken(log, reqId, accessToken);

            log.info(reqId, `${fnName} ### calling (User)'getDetails' to get user with his roles`);
            // -> Get user (with details)
            const userData = await User.getDetails(log, reqId, decryptedAccessToken.userId);

            // Sanity checks, throw error in following cases
            // ? if user not found
            // ? if user is locked (this check can be disabled using an options parameter)
            // ? if user is deleted (this check can be disabled using an options parameter)
            if(!userData) throw new Errors.AuthErrors.UserNotFoundAuthError();
            if(!options.bypassLockCheck && userData.user.isLocked) throw new Errors.AuthErrors.ForbiddenAuthError('Account locked');
            if(!options.bypassDeletionCheck && userData.user.isDeleted) throw new Errors.AuthErrors.ForbiddenAuthError('Account deleted');

            // -> If roles are needed, check if user has them
            if(roleSets && roleSets.length) {
                const doesPassRoles = checkRoles(userData.roles, roleSets);
                if(!doesPassRoles) throw new Errors.AuthErrors.FailedRolesAuthError('user lacks required roles');
            }
    
            // End
            log.info(reqId, `${fnName} ### end [success]`);
            req.userId = userData.user.id;
            req.user = userData.user;
            req.user.roles = userData.roles;
            req.userRoles = userData.roles;
            next();
        }
        catch (e) {

            // Token Expired
            if(e instanceof Errors.JwtErrors.JwtExpiredError) {
                log.info(reqId, `${fnName} ### end [failure] ### access token expired, error: ${JSON.stringify(e)}`);
                next(new Errors.AuthErrors.ExpiredAccessTokenAuthError());
            }
            // Token Invalid
            else if(e instanceof Errors.JwtErrors.JwtDecryptionError) {
                log.info(reqId, `${fnName} ### end [failure] ### access token invalid, error: ${JSON.stringify(e)}`);
                next(new Errors.AuthErrors.InvalidAccessTokenAuthError());
            }
            else {
                log.info(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
                next(e);
            }

        }
    }
}


/**
 * check if user passes the specified roles
 * @param {string[]} userRoles 
 * @param {string[]} roleSets 
 * @throws {AuthErrors.FailedRolesAuthError}
 */
function checkRoles(userRoles, roleSets) {

    const doesPass = roleSets.some(roleSet => {
        return roleSet.every(role => {
            return userRoles.includes(role);
        })
    })
    
    return doesPass;
}


class RoleSets {
    constructor() {
        this.roleSets = [];
    }

    all(roles) {
        this.roleSets.push(roles);
        return this;
    }

    orAll(roles){
        this.roleSets.push(roles);
        return this;
    }

    any(roles) {
        this.roleSets.push(...roles.map(r => [r]));
        return this;
    }

    bake() {
        return this.roleSets;
    }
}


/**
 * Export
 */
module.exports = {
    auth,
    RoleSets
}