/**
 * @description logs into the errors log (not rotated daily)
 */


const { createLogger, format, transports } = require('winston');
require('winston-daily-rotate-file');
const moment = require('moment');
const uuid = require('uuid');


// Format of each log line, used in 'createLogger' below
const logFormat = function(routePath) {
    return format.printf(function({ level, message, timestamp }) {
        return `${timestamp} [${level}]: ${routePath} ### ${message}`;
    })
};


/**
 * error logger
 */
const ErrorLogger = createLogger({
    transports: [
        new transports.File({
            dirname: 'logs/errors/',
            filename: 'errors.log',
            datePattern: 'YYYY-MM-DD',
            zippedArchive: false,
            maxsize: '10MB',
            format: format.combine(
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss'
                }),
                logFormat('errors')
            )
        }),
    ]
});


/**
 * Exports
 */
module.exports = ErrorLogger;