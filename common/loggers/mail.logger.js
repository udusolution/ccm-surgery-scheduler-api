/**
 * @description logs all emails sent (not rotated daily)
 */


const { createLogger, format, transports } = require('winston');


// Format of each log line, used in 'createLogger' below
const logFormat = function(routePath) {
    return format.printf(function({ level, message, timestamp }) {
        return `${timestamp} [${level}]: ${routePath} ### ${message}`;
    })
};


/**
 * error logger
 */
const MailLogger = createLogger({
    transports: [
        new transports.File({
            dirname: 'logs/mail/',
            filename: 'mail.log',
            datePattern: 'YYYY-MM-DD',
            zippedArchive: false,
            maxsize: '10MB',
            format: format.combine(
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss'
                }),
                logFormat('mail')
            )
        }),
    ]
});


/**
 * Exports
 */
module.exports = MailLogger;