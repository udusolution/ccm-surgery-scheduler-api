/**
 * @description holds a dictionary of loggers that log every request of each service into its own log file
 * @note there is a max log file size set and a max number of log files per service. 
 * @note automatic cleanup occurs when size is exceeded, old log files are deleted.
 */


const { createLogger, format, transports } = require('winston');
require('winston-daily-rotate-file');
const moment = require('moment');
const uuid = require('uuid');
const ErrorLogger = require('./error.logger');


let loggers = {};


/**
 * Create service logger if already not created
 * @param {string} routePath route path of the service
 * @note Uses an object defined above called 'loggers' to keep already created loggers
 * @returns {*} logger
 */
function getOrCreateLogger(routePath) {
    let logger = loggers[routePath];
    if(!logger) {
        logger = create(routePath);
        loggers[routePath] = logger;
    }
    return logger;
}


// Format of each log line, used in 'createLogger' below
const logFormat = function(routePath) {
    return format.printf(function({ level, message, timestamp }) {
        return `${timestamp} [${level}]: ${routePath} ### ${message}`;
    })
};
 
 
 
 /**
  * Create service logger
  * @param {string} routePath route path of the service
  * @note The logger created is a winston logger instance, but the logger returned is a wrapper that adds the requestId to each log line
  */
function create(routePath) {

    // Extract route info
    const routePathParts = routePath.split('/');
    const routeName = routePathParts[routePathParts.length - 1];
    const dir = `logs${routePath}/`;
    const filename = `${routeName}-%DATE%.log`;

    console.info(`logger initialization for: ${dir}${routeName}-${moment().format('YYYY-MM-DD')}.log`);

    // Initialize the winston logger
    const winstonLogger = createLogger({
        transports: [
            new transports.DailyRotateFile({
                dirname: dir,
                filename: filename,
                datePattern: 'YYYY-MM-DD',
                zippedArchive: false,
                maxFiles: '30d',
                format: format.combine(
                    format.timestamp({
                        format: 'YYYY-MM-DD HH:mm:ss'
                    }),
                    logFormat(routePath)
                )
            }),
        ]
    });

    // Wrapper logger
    const logger = {
        error: function(reqId, msg) {
            winstonLogger.error(`${reqId} ### ${msg}`);
            ErrorLogger.error(`${reqId} ### ${msg}`);
        },
        warn: function(reqId, msg) {
            winstonLogger.warn(`${reqId} ### ${msg}`);
            ErrorLogger.warn(`${reqId} ### ${msg}`);
        },
        verbose: function(reqId, msg) {
            winstonLogger.verbose(`${reqId} ### ${msg}`);
        },
        info: function(reqId, msg) {
            winstonLogger.info(`${reqId} ### ${msg}`);
        },
        debug: function(reqId, msg) {
            winstonLogger.debug(`${reqId} ### ${msg}`);
        },
        silly: function(reqId, msg) {
            winstonLogger.silly(`${reqId} ### ${msg}`);
        }
    };
    
    return logger;
}
 
 
/**
 * middleware that adds a logger to the request object for the endpoint being called
 */
function inject(req, res, next) {

    const reqId = uuid.v4();
    const fullPath = req.baseUrl + req.path;
    const logger = getOrCreateLogger(fullPath);

    req.reqId = reqId;
    req.log = logger;
    req.fullPath = fullPath;

    logger.info(reqId, 'start ###');
    next();
}
 
 
/**
 * facade for the getOrCreateLogger
 * @param {string} routePath route path of the service
 * @returns {*} logger
 */
function get(routePath) {
    return getOrCreateLogger(routePath)
}
 
 
/**
 * creates a fake logger that logs nowhere, to be used as a default value for passed in loggers during the log-adding phase
 * @returns {*} fakeLogger
 */
function fake() {
    return {
        log:        () => {},
        error:      () => {},
        warn:       () => {},
        verbose:    () => {},
        info:       () => {},
        debug:      () => {},
        silly:      () => {}
    }
}
 
 
/**
 * creates a console logger that logs in the console
 * @returns {*} consoleLogger
 */
function consoleLog() {
    return {
        log: function(reqId, level, message) { console.log(`${moment().format('YYYY-MM-DD hh:mm:ss')} [${level}] ### ${reqId} ### ${message}`) },
        error: function(reqId, message) { console.log(`${moment().format('YYYY-MM-DD hh:mm:ss')} [error] ### ${reqId} ### ${message}`) },
        warn: function(reqId, message) { console.log(`${moment().format('YYYY-MM-DD hh:mm:ss')} [warn] ### ${reqId} ### ${message}`) },
        verbose: function(reqId, message) { console.log(`${moment().format('YYYY-MM-DD hh:mm:ss')} [verbose] ### ${reqId} ### ${message}`) },
        info: function(reqId, message) { console.log(`${moment().format('YYYY-MM-DD hh:mm:ss')} [info] ### ${reqId} ### ${message}`) },
        debug: function(reqId, message) { console.log(`${moment().format('YYYY-MM-DD hh:mm:ss')} [debug] ### ${reqId} ### ${message}`) },
        silly: function(reqId, message) { console.log(`${moment().format('YYYY-MM-DD hh:mm:ss')} [silly] ### ${reqId} ### ${message}`) }
    }
}
 

/**
 * Exports
 */
module.exports = {
    inject,
    get,
    fake,
    consoleLog
}