/**
 * @description logs all SMS sent (not rotated daily)
 */


const { createLogger, format, transports } = require('winston');


// Format of each log line, used in 'createLogger' below
const logFormat = function(routePath) {
    return format.printf(function({ level, message, timestamp }) {
        return `${timestamp} [${level}]: ${routePath} ### ${message}`;
    })
};


/**
 * error logger
 */
const SmsLogger = createLogger({
    transports: [
        new transports.File({
            dirname: 'logs/sms/',
            filename: 'sms.log',
            datePattern: 'YYYY-MM-DD',
            zippedArchive: false,
            maxsize: '10MB',
            format: format.combine(
                format.timestamp({
                    format: 'YYYY-MM-DD HH:mm:ss'
                }),
                logFormat('sms')
            )
        }),
    ]
});


/**
 * Exports
 */
module.exports = SmsLogger;