/**
 * @description utility module for JWT
 * @note uses 'jsonwebtoken' npm package
 */


const config = require('../../config/config');
const jwt = require('jsonwebtoken');
const Errors = require('../errors/_CustomErrors');


/**
 * generate a JWT user access token
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} userId user ID
 * @returns {string} user JWT access token
 */
function generateAccessToken(log, reqId, userId){

    const fnName = 'jwt/generateAccessToken';
    log.info(reqId, `${fnName} ### start`);

    try {

        log.info(reqId, `${fnName} ### calling (jsonwebtoken)'sign' with userId "${userId}"`);
        // -> Generate the JWT token
        const accessToken = jwt.sign({ userId }, config.jwt.secrets.accessTokenSecret, { expiresIn: config.jwt.ttl.accessTokenTTL });

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return accessToken;
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.JwtErrors.JwtEncryptionError(e.message, e);
    }
}


/**
 * generate a JWT user refresh token
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} userId user ID
 * @returns {string} user JWT refresh token
 */
function generateRefreshToken(log, reqId, userId){

    const fnName = 'jwt/generateRefreshToken';
    log.info(reqId, `${fnName} ### start`);

    try {

        log.info(reqId, `${fnName} ### calling (jsonwebtoken)'sign' with userId "${userId}"`);
        // -> Generate the JWT token
        const refreshToken = jwt.sign({ userId }, config.jwt.secrets.refreshTokenSecret, { expiresIn: config.jwt.ttl.refreshTokenTTL });

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return refreshToken;
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.JwtErrors.JwtEncryptionError(e.message, e);
    }
}


/**
 * decrypt a JWT user access token
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} token user JWT access token to decrypt
 * @returns {{ userId: string }} decrypted user JWT access token
 * @throws {Errors.JwtErrors.JwtExpiredError}
 * @throws {Errors.JwtErrors.JwtDecryptionError}
 */
function decryptAccessToken(log, reqId, token){

    const fnName = 'jwt/decryptAccessToken';
    log.info(reqId, `${fnName} ### start`);

    try {

        log.info(reqId, `${fnName} ### calling (jsonwebtoken)'verify'`);
        // -> Decrypt the JWT token
        const decryptedAccessToken = jwt.verify(token, config.jwt.secrets.accessTokenSecret);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return decryptedAccessToken;
    }
    catch(e) {

        if(e instanceof jwt.TokenExpiredError) {
            log.info(reqId, `${fnName} ### end [token expired] ### error: ${JSON.stringify(e)}`);
            throw new Errors.JwtErrors.JwtExpiredError(e.message, e);
        }

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.JwtErrors.JwtDecryptionError(e.message, e);
    }
}


/**
 * decrypt a JWT user refresh token
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} token user JWT refresh token to decrypt
 * @returns {{ userId: string }} decrypted user JWT refresh token
 * @throws {Errors.JwtErrors.JwtExpiredError}
 * @throws {Errors.JwtErrors.JwtDecryptionError}
 */
 function decryptRefreshToken(log, reqId, token){

    const fnName = 'jwt/decryptRefreshToken';
    log.info(reqId, `${fnName} ### start`);

    try {

        log.info(reqId, `${fnName} ### calling (jsonwebtoken)'verify'`);
        // -> Decrypt the JWT token
        const decryptedAccessToken = jwt.verify(token, config.jwt.secrets.refreshTokenSecret);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return decryptedAccessToken;
    }
    catch(e) {

        if(e instanceof jwt.TokenExpiredError) {
            log.info(reqId, `${fnName} ### end [token expired] ### error: ${JSON.stringify(e)}`);
            throw new Errors.JwtErrors.JwtExpiredError(e.message, e);
        }

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.JwtErrors.JwtDecryptionError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    generateAccessToken,
    generateRefreshToken,
    decryptAccessToken,
    decryptRefreshToken
}