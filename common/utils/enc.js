/**
 * @description utility module to encrypt/decrypt/hash/compare text
 * @note uses 'bcrypt' npm package
 */


const bcrypt = require('bcrypt');
const Errors = require('../errors/_CustomErrors');


/**
 * hash a piece of data
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {*} dataToHash 
 * @returns {Promise<string>}
 */
async function hash(log, reqId, dataToHash) {

    const fnName = 'enc/hash';
    log.info(reqId, `${fnName} ### start`);

    try {

        log.info(reqId, `${fnName} ### calling (bcrypt)'hash'`);
        // -> Hash the data
        const hashedStr = await bcrypt.hash(dataToHash, 10);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return hashedStr;
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.EncErrors.HashEncError(e.message, e);
    }
}


/**
 * compare a clear string and a hashed string to see if they match
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} clearStr 
 * @param {string} hashedStr 
 * @returns {Promise<boolean>}
 */
async function compare(log, reqId, clearStr, hashedStr) {

    const fnName = 'enc/compare';
    log.info(reqId, `${fnName} ### start`);

    try {

        log.info(reqId, `${fnName} ### calling (bcrypt)'compare'`);
        // -> Compare the clear and hashed strings
        const doesMatch = await bcrypt.compare(clearStr, hashedStr);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return doesMatch;
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.EncErrors.HashEncError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    hash,
    compare
}