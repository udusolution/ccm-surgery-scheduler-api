/**
 * @description various utility functions regarding javascript objects
 */


const moment = require('moment');


/**
 * get the difference between two objects
 * @param {Object} oldObj 
 * @param {Object} newObj 
 */
function getDiff(oldObj, newObj) {

    let diff = [];

    for(let newKey in newObj) {
        if(Object.prototype.hasOwnProperty.call(oldObj, newKey) && isDifferent(oldObj[newKey], newObj[newKey])) {
            diff.push({
                key: newKey,
                oldVal: oldObj[newKey],
                newVal: newObj[newKey]
            })
        }
    }

    return diff;
}

function isDifferent(val1, val2) {
    if(moment(val1).isValid() && moment(val2).isValid()) return !moment(val1).isSame(moment(val2));
    else return (val1 != val2);
}


/**
 * Exports
 */
module.exports = {
    getDiff
}