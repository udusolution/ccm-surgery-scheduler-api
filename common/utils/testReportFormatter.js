/**
 * @description Utility functions for formatting the information displayed for each test case in the reports
 */


const moment = require('moment');


/**
 * params
 * @param {string[]} header request headers
 * @param {string[]} body request body
 * @returns {string} this service's input headers and body data
 */
function params(header, body) {
  return `Params:\n\nRequest Header:\n  ${header.join('\n  ')}\nRequest Body:\n  ${body.join('\n  ')}`;
}


/**
 * description
 * @param {string} service service being tested
 * @param {string} testCase case being tested
 * @param {string} expected expected behavior of this tested case
 * @returns {string} test case summary
 */
function description(service, testCase, expected) {
  return `Service: ${service}\nTest Case: ${testCase}\nExpected Result: ${expected}`;
}


/**
 * request
 * @param {*} header request header
 * @param {*} body request body
 * @returns {string} request as formatted string
 */
function request(header, body) {
  return `Request Header:\n${JSON.stringify(header, null, 2)}\nRequest Body:\n${JSON.stringify(body, null, 2)}`;
}


/**
 * logpath
 * @param {string} requestId requestId of the request used in the test case
 * @param {string} servicePath service path of the service being tested
 * @returns {string} relevant information for where to find the logs of this test case
 */
function logpath(requestId, servicePath) {
  return `Request ID: ${requestId}\nService Log File: logs/test/services/${servicePath}-${moment().format('YYYY-MM-DD')}.log`;
}


/**
 * response
 * @param {number} status response's status code
 * @param {*} header response header
 * @param {*} body response body
 * @returns {string} response as formatted string
 */
function response(status, header, body) {
  return `Response Status: ${status}\n\nResponse Header:\n${JSON.stringify(header, null, 2)}\n\nResponse Body:\n${JSON.stringify(body, null, 2)}`;
}


/**
 * msDescription
 * @param {string} microservice microservice being tested
 * @param {string} testCase case being tested
 * @param {string} expected expected behavior of this tested case
 * @returns {string} test case summary
 */
function msDescription(microservice, testCase, expected) {
  return `Micro-Service: ${microservice}\nTest Case: ${testCase}\nExpected Result: ${expected}`;
}


/**
 * msParams
 * @param {string[]} params given parameters
 * @returns {string} this microservice's parameters
 */
function msParams(params) {
  return `Params:\n${JSON.stringify(params, null, 2)}`;
}


module.exports = {
  params,
  description,
  request,
  logpath,
  response,
  msDescription,
  msParams
}