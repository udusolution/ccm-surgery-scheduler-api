/** 
 * @description a utility for file reading
 */


const fs = require('fs');
const Errors = require('../errors/_CustomErrors');


/**
 * read a file
 * @param {string} path path to file
 */
function read(path) {

	return Promise((resolve, reject) => {

		fs.readFile(path, function(err, data) {
			if (err){
				reject(new Errors.FileErrors.FileError(err.message));
			} else {
				resolve(data);
			}
		});
	})

}


/**
 * Exports
 */
module.exports = {
	read
}