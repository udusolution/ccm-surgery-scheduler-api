/**
 * @description utility that implements parameter checks
 * @note throws ParamError
 */


const Errors = require('../../errors/_CustomErrors');


class ParamValidator{

    constructor(el) {
        this.el = el;
        return this;
    }

    isBoolean() {
        if(typeof this.el == 'boolean') return this;
        else throw new Errors.CommonErrors.ParamError(`parameter ${this.el} must be boolean`);
    }

    isString() {
        if(typeof this.el == 'string') return this;
        else throw new Errors.CommonErrors.ParamError(`parameter ${this.el} must be string`);
    }

    isNumber() {
        if(typeof this.el == 'number') return this;
        else throw new Errors.CommonErrors.ParamError(`parameter ${this.el} must be number`);
    }

    isInteger() {
        if(typeof this.el == 'number' && this.el === parseInt(this.el, 10)) return this;
        else throw new Errors.CommonErrors.ParamError(`parameter ${this.el} must be integer`);
    }

    isPositiveNumber() {
        if(typeof this.el == 'number' && this.el > 0) return this;
        else throw new Errors.CommonErrors.ParamError(`parameter ${this.el} must be positive number`);
    }

    isNegativeNumber() {
        if(typeof this.el == 'number' && this.el < 0) return this;
        else throw new Errors.CommonErrors.ParamError(`parameter ${this.el} must be negative number`);
    }

    isPositiveNumberOrZero() {
        if(typeof this.el == 'number' && this.el >= 0) return this;
        else throw new Errors.CommonErrors.ParamError(`parameter ${this.el} must be positive number or zero`);
    }

    isNegativeNumberOrZero() {
        if(typeof this.el == 'number' && this.el <= 0) return this;
        else throw new Errors.CommonErrors.ParamError(`parameter ${this.el} must be negative number or zero`);
    }

    isNonZeroNumber() {
        if(typeof this.el == 'number' && this.el != 0) return this;
        else throw new Errors.CommonErrors.ParamError(`parameter ${this.el} must be non zero number`);
    }

    isPercentage() {
        if(typeof this.el == 'number' && this.el >= 0 && this.el <= 100) return this;
        else throw new Errors.CommonErrors.ParamError(`parameter ${this.el} must be valid percentage number equal or between 0 and 100`);
    }
}


/**
 * Exports
 */
module.exports = ParamValidator;