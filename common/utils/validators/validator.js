/**
 * @description class for data validation, but using regex is better since most values received in an API are common and shared across various endpoints.
 */


const moment = require('moment');


class Validate {

    constructor(value, name = 'value') {
        this.value = value ?? null;
        this.name = name;
        this.isValid = true;
        this.errors = [];
        this.isRequired = false;
        this.isPresent = isNullOrEmpty(value) ? false : true;
        return this;
    }

    // REQUIRED
    required(isRequired = true, errorMsg = `${this.name} required`){
        this.isRequired = isRequired;
        if(isRequired && !this.isPresent) {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    // TYPE CHECK
    isNumber(errorMsg = `${this.name} must be a number`){
        if(!this.isValid) return this;
        if(this.isPresent && typeof this.value != 'number') {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    isString(errorMsg = `${this.name} must be a string`){
        if(!this.isValid) return this;
        if(this.isPresent && typeof this.value != 'string') {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    isBoolean(errorMsg = `${this.name} must be a boolean`){
        if(!this.isValid) return this;
        if(this.isPresent && typeof this.value != 'boolean') {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    isDate(errorMsg = `${this.name} must be a date`){
        if(!this.isValid) return this;
        if(this.isPresent && !moment(this.value).isValid() && !moment.unix(this.value).isValid()) {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    isArray(errorMsg = `${this.name} must be an array`){
        if(!this.isValid) return this;
        if(this.isPresent && !Array.isArray(this.value)) {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    // VALIDATION RULES
    min(minVal, errorMsg = `${this.name} must be less than ${minVal}`) {
        if(!this.isValid) return this;
        if(this.isPresent && this.value < minVal) {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    max(maxVal, errorMsg = `${this.name} must be greater than ${maxVal}`) {
        if(!this.isValid) return this;
        if(this.isPresent && this.value > maxVal) {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    minLength(minLength, errorMsg = `${this.name} must be longer than ${minLength}`) {
        if(!this.isValid) return this;
        if(this.isPresent && this.value.length < minLength) {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    maxLength(maxLength, errorMsg = `${this.name} must be shorter than ${maxLength}`) {
        if(!this.isValid) return this;
        if(this.isPresent && this.value.length > maxLength) {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    minDate(minDate, errorMsg = `${this.name} must be after ${moment(minDate).format()}`) {
        if(!this.isValid) return this;
        if(this.isPresent && moment(this.value).isBefore(moment(minDate))) {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    maxDate(maxDate, errorMsg = `${this.name} must be before ${moment(maxDate).format()}`) {
        if(!this.isValid) return this;
        if(this.isPresent && moment(this.value).isAfter(moment(maxDate))) {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    mustBeIn(arr, errorMsg = `${this.name} must be a valid value`) {
        if(!this.isValid) return this;
        if(this.isPresent && !arr.includes(this.value)) {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    eachMustBeIn(arr, errorMsg = `${this.name} must be valid values`) {
        if(!this.isValid) return this;
        if(this.isPresent && !this.value.every(v => arr.includes(v))) {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    // REGEX
    isEmail(errorMsg = `${this.name} must be a valid email`) {
        if(!this.isValid) return this;
        if(this.isPresent && !this.value.match(/^([\w.-]+)@([\w-]+)((\.(\w)+)+)$/)) {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    isUsaPhoneNumber(errorMsg = `${this.name} must be in the USA phone number format`) {
        if(!this.isValid) return this;
        if(this.isPresent && !this.value.match(/^[0-9]{10}$/)) {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    isHexColor(errorMsg = `${this.name} must be a valid hex color`) {
        if(!this.isValid) return this;
        if(this.isPresent && !this.value.match(/^#[a-fA-F0-9]{6}$/)) {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    regex(regex, errorMsg = `${this.name} does not match the required format`) {
        if(!this.isValid) return this;
        if(this.isPresent && !this.value.match(regex)) {
            this.isValid = false;
            this.errors.push(errorMsg);
        }
        return this;
    }

    // BAKE
    bake() {
        return [this.isValid, this.errors[0]];
    }
}


function isString(el) {
    return (typeof el == 'string');
}
function isNumber(el) {
    return (typeof el == 'number');
}
function isBoolean(el) {
    return (typeof el == 'boolean');
}
function isArray(el) {
    return (Array.isArray(el));
}
function isNullOrEmpty(el) {
    return (
        el === null
        || el === undefined
        || el === ''
    )
}

function getErrors(validations) {
    return validations.filter(v => !v[0]).map(v => v[1]);
}

module.exports = {
    Validate,
    isString,
    isNumber,
    isBoolean,
    isArray,
    getErrors
}