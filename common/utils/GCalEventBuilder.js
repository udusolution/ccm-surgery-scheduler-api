const Errors = require('../errors/_CustomErrors');
const moment = require('moment');
const momentTZ = require('moment-timezone');
const Enums = require('../enums');
module.exports = class GCalEventBuilder {

    constructor() {
        this.event = {};
    }

    setPhysicianName(physicianName) {
        this.event.physicianName = physicianName;
        return this;
    }

    setPatientName(patientName) {
        this.event.patientName = patientName;
        return this;
    }

    setEventType(eventType) {
        this.event.eventType = eventType;
        return this;
    }

    setStatus(status) {
        this.event.status = status;
        return this;
    }

    setDateFrom(dateFrom) {
        this.event.dateFrom = dateFrom;
        return this;
    }

    setDateTo(dateTo) {
        this.event.dateTo = dateTo;
        return this;
    }

    setFacilityName(facilityName) {
        this.event.facilityName = facilityName;
        return this;
    }

    setFacilityAddress(facilityAddress) {
        this.event.facilityAddress = facilityAddress;
        return this;
    }

    #getInitials(name) {
        const partitions = name.trim().split(',')[0].split(' ');
        return [
            partitions.shift().charAt(0).toUpperCase(),
            partitions.length ? partitions.pop().charAt(0).toUpperCase() : ''
        ].join('')
    }

    #getStatusText(status) {
        switch(status) {
            case Enums.EventStatuses.BoardingSlipGenerated:
                return ' - BSG - ';
            case Enums.EventStatuses.WaitingForConfirmation:
                return ' - WFC - ';
            case Enums.EventStatuses.Confirmed:
                return ' ';
            default:
                return ` - ${status.toUpperCase()} - `;
        }
    }

    build() {
        if (
            !moment(this.event?.dateFrom || 'N/A').isValid() ||
            !moment(this.event?.dateTo || 'N/A').isValid()
        ) {
            throw new Errors.GCalErrors.GCalError('From/to Timestamps are required to create a google calendar event')
        }
        const startDate = momentTZ(this.event.dateFrom).tz(Enums.ZoneFormats.MichiganDetroit)
        const endDate = momentTZ(this.event.dateTo).tz(Enums.ZoneFormats.MichiganDetroit)
        return {
            summary: this.#getInitials(this.event.physicianName) + this.#getStatusText(this.event.status) + this.event.eventType,
            description: `${this.event.physicianName} has a ${this.event.eventType} for ${this.event.patientName} at ${this.event.facilityName}`,
            location: this.event.facilityAddress,
            start: {
                dateTime: startDate.toISOString(),
                timeZone: startDate.tz()
            },
            end: {
                dateTime: endDate.toISOString(),
                timeZone: endDate.tz()
            },
        }
    }
}