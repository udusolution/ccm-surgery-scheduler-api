/**
 * @description integration with Advanced MD API
 */


const config = require('../../config/config');
const axios = require('axios');
const moment = require('moment');
const Errors = require('../../common/errors/_CustomErrors');
const path = require('path');
const https = require('https');


/**
 * login to advanced md api and return the bearer token
 * @param {*} log logger
 * @param {string} reqId request ID
 * @returns {Promise<{ url: string, token: string }>} bearer token and api url to use
 */
async function login(log, reqId) {

    const fnName = 'md/login';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Login Params
        const loginParams = {
            "ppmdmsg": {
                "@action": "login",
                "@class": "login",
                "@msgtime":  moment().format('MM/DD/YYYY hh:mm:ss A'),
                "@username": config.md.username,
                "@psw": config.md.password,
                "@officecode": config.md.officeKey,
                "@appname": config.md.appName
            }
        }

        log.info(reqId, `${fnName} ### initial login request ### params: ${JSON.stringify(loginParams)}`);
        // -> Initial Login Request
        const initLoginResponse = await axios.default.post(config.md.baseURL, loginParams);
        log.info(reqId, `${fnName} ### initial login response ### params: ${JSON.stringify(initLoginResponse.data)}`);

        const initLoginData = initLoginResponse.data;
        const token = initLoginData?.PPMDResults?.Results?.usercontext['#text'];
        const altApiUrl = initLoginData.PPMDResults.Results.usercontext['@webserver'];
        const altApiFullUrl = `${altApiUrl}/xmlrpc/processrequest.asp`;
        
        // If token returned in the initial login request, return
        if(token) {
            
            // End
            log.info(reqId, `${fnName} ### end [success]`);
            return {
                url: altApiFullUrl,
                token
            };
        }


        // Main Login Params
        const mainLoginParams = {
            "ppmdmsg": {
                "@action": "login",
                "@class": "login",
                "@msgtime":  moment().format('MM/DD/YYYY hh:mm:ss A'),
                "@username": config.md.username,
                "@psw": config.md.password,
                "@officecode": config.md.officeKey,
                "@appname": config.md.appName
            }
        }

        log.info(reqId, `${fnName} ### main login request ### params: ${JSON.stringify(mainLoginParams)}`);
        // -> Main Login Request
        const mainLoginResponse = await axios.default.post(altApiFullUrl, mainLoginParams);
        log.info(reqId, `${fnName} ### main login response ### params: ${JSON.stringify(mainLoginResponse.data)}`);

        const mainLoginData = mainLoginResponse.data;
        const token2 = mainLoginData.PPMDResults.Results.usercontext['#text'];

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return {
            url: altApiFullUrl,
            token: token2
        }

    }
    catch(e) {
        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.MdErrors.MdError(e.message, e);
    }
}


/**
 * get patient data changes from specific server time
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} token advanced MD api bearer token (retrieved from the login operation)
 * @param {string} url advanced MD api url (retrieved from the login operation)
 * @param {string} dateFrom date to start checking for changes (API server time)
 * @returns {Promise<{currentServerTime: string, patientsCount: number, updatedPatientData: [], isIdsOnly: boolean}>} updated patient data
 */
async function getUpdatedPatients(log, reqId, token, url, dateFrom) {

    const fnName = 'md/getUpdatedPatients';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            "ppmdmsg": {
                "@action": "getupdatedpatients",
                "@class": "api",
                "@msgtime": moment().format('MM/DD/YYYY hh:mm:ss A'),
                "@datechanged": dateFrom,
                "@nocookie": "0",
                "patient": {
                    "@name": "Name",
                    "@firstname": "FirstName",
                    "@middlename": "MiddleName",
                    "@lastname": "LastName",
                    "@dob": "DOB",
                    "@sex": "Sex",
                    "@email": "Email",
                    "@homephone": "HomePhone",
                    "@otherphone": "OtherPhone",
                    "@ssn": "SSN",
                    "@address1": "Address1",
                    "@address2": "Address2",
                    "@city": "City",
                    "@state": "State",
                    "@zip": "Zip",
                    "@changedat": "ChangedAt",
                    "@createdat": "CreatedAt",
                    "@hipaarelationship": "HipaaRelationship"
                },
                "insurance": {
                    "@carname": "CarName",
                    "@carcode": "CarCode",
                    "@groupname": "GroupName",
                    "@groupnumber": "GroupNumber",
                    "@subidnumber": "SubIDNumber",
                    "@changedat": "ChangedAt",
                    "@createdat": "CreatedAt"
                }
            }
        }

        // Header
        const headers = {
            Cookie: `token=${token}`,
            Authorization: `Bearer ${token}`
        }

        log.info(reqId, `${fnName} ### get updated patient data ### url: "${url}", token: "${token}", params: ${JSON.stringify(params)}`);
        // -> Request
        const response = await axios.default.post(url, params, { 
            headers, 
            // httpsAgent: new https.Agent({ rejectUnauthorized: false }) 
        });
        const responseData = response.data;
        log.info(reqId, `${fnName} ### response data fetched: ${JSON.stringify(responseData)}`);

        const currentServerTime = responseData.PPMDResults.Results['@servertime'];
        const patientsCount = responseData.PPMDResults.Results['@patientcount'];
        const updatedPatientData = responseData.PPMDResults.Results.patientlist;
        const isIdsOnly = (responseData.PPMDResults?.Error?.Fault?.detail?.code == '-2147217350');

        // End
        log.info(reqId, `${fnName} ### end [success] ### changes: ${JSON.stringify(responseData)}`);
        return {
            currentServerTime,
            patientsCount,
            updatedPatientData,
            isIdsOnly
        }

    }
    catch(e) {
        throw new Errors.MdErrors.MdError(e.message, e);
    }
}

/**
 * get patient data changes from specific server time for specific patients
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} token advanced MD api bearer token (retrieved from the login operation)
 * @param {string} url advanced MD api url (retrieved from the login operation)
 * @param {string} dateFrom date to start checking for changes (API server time)
 * @param {string[]} ids patient IDs
 * @returns {Promise<{currentServerTime: string, patientsCount: number, updatedPatientData:[]}>} updated patient data
 */
 async function getUpdatedPatientsUsingIds(log, reqId, token, url, dateFrom, ids) {

    const fnName = 'md/getUpdatedPatientsUsingIds';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            "ppmdmsg": {
                "@action": "getupdatedpatients",
                "@class": "api",
                "@msgtime": moment().format('MM/DD/YYYY hh:mm:ss A'),
                "@datechanged": dateFrom,
                "@nocookie": "0",
                "patientlist": {
                    "patient": ids
                },
                "patient": {
                    "@name": "Name",
                    "@firstname": "FirstName",
                    "@middlename": "MiddleName",
                    "@lastname": "LastName",
                    "@dob": "DOB",
                    "@sex": "Sex",
                    "@email": "Email",
                    "@homephone": "HomePhone",
                    "@otherphone": "OtherPhone",
                    "@ssn": "SSN",
                    "@address1": "Address1",
                    "@address2": "Address2",
                    "@city": "City",
                    "@state": "State",
                    "@zip": "Zip",
                    "@changedat": "ChangedAt",
                    "@createdat": "CreatedAt",
                    "@hipaarelationship": "HipaaRelationship"
                },
                "insurance": {
                    "@carname": "CarName",
                    "@carcode": "CarCode",
                    "@groupname": "GroupName",
                    "@groupnumber": "GroupNumber",
                    "@subidnumber": "SubIDNumber",
                    "@changedat": "ChangedAt",
                    "@createdat": "CreatedAt"
                }
            }
        }

        // Header
        const headers = {
            Cookie: `token=${token}`,
            Authorization: `Bearer ${token}`
        }

        log.info(reqId, `${fnName} ### get updated patient data ### url: "${url}", token: "${token}", params: ${JSON.stringify(params)}`);
        // -> Request
        const response = await axios.default.post(url, params, { 
            headers, 
            // httpsAgent: new https.Agent({ rejectUnauthorized: false }) 
        });
        const responseData = response.data;
        log.info(reqId, `${fnName} ### response data fetched: ${JSON.stringify(responseData)}`);

        const currentServerTime = responseData.PPMDResults.Results['@servertime'];
        const patientsCount = responseData.PPMDResults.Results['@patientcount'];
        const updatedPatientData = responseData.PPMDResults.Results.patientlist;

        // End
        log.info(reqId, `${fnName} ### end [success] ### changes: ${JSON.stringify(responseData)}`);
        return {
            currentServerTime,
            patientsCount,
            updatedPatientData
        }

    }
    catch(e) {
        throw new Errors.MdErrors.MdError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    login,
    getUpdatedPatients,
    getUpdatedPatientsUsingIds
}