/**
 * @description address lookup using google places API
 */


const axios = require('axios');
const config = require('../../config/config');
const Errors = require('../errors/_CustomErrors');


/**
 * lookup addresses using a search query
 * @param {*} log logger
 * @param {string} reqId request Id
 * @param {string} query address search query
 * @returns {Promise<[{formatted_address: string, name: string, place_id: string}]>} search results
 * @throws {Errors.AddressLookupErrors.AddressLookupError}
 */
async function search(log, reqId, query) {

    const fnName = 'address-lookup/search';
    log.info(reqId, `${fnName} ### start`);

    try {

        log.info(reqId, `${fnName} ### calling the google places API with search query "${query}"`);
        // -> Call the google maps API
        const searchResponse = await axios.default.get(config.placesApi.searchUrl, { params: {
            inputtype: 'textquery',
            fields: 'formatted_address,name,place_id',
            language: 'en',
            key: config.placesApi.key,
            input: query
        }})

        const results = searchResponse.data.candidates;

        // -> Check Errors
        if(searchResponse.data.status == 'OVER_QUERY_LIMIT')    throw new Errors.AddressLookupErrors.TooManyRequestsAddressLookupError('too many requests', searchResponse.data);
        if(searchResponse.data.status == 'REQUEST_DENIED')      throw new Errors.AddressLookupErrors.UnauthorizedAddressLookupError('google places API denied the request', searchResponse.data);
        if(searchResponse.data.status == 'INVALID_REQUEST')     throw new Errors.AddressLookupErrors.InvalidRequestAddressLookupError('invalid request', searchResponse.data);
        if(searchResponse.data.status == 'UNKNOWN_ERROR')       throw new Errors.AddressLookupErrors.AddressLookupError('error from google maps API', searchResponse.data);

        // End
        log.info(reqId, `${fnName} ### end [success] ### response: ${JSON.stringify(searchResponse.data)}`);
        return results;

    }
    catch(e) {
        log.info(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.AddressLookupErrors.AddressLookupError(e.message, e);
    }
}


/**
 * get place details
 * @param {*} log logger
 * @param {string} reqId request Id
 * @param {string} placeId google place ID
 * @throws {Errors.AddressLookupErrors.AddressLookupError}
 */
async function details(log, reqId, placeId) {

    const fnName = 'address-lookup/details';
    log.info(reqId, `${fnName} ### start`);

    try {

        log.info(reqId, `${fnName} ### calling the google places API to get place details with place ID "${placeId}"`);
        // -> Call the google maps API
        const searchResponse = await axios.default.get(config.placesApi.detailsUrl, { params: {
            fields: 'formatted_address,name,place_id,address_components',
            language: 'en',
            key: config.placesApi.key,
            place_id: placeId
        }})

        log.info(reqId, `${fnName} ### google api response: ${JSON.stringify(searchResponse.data)}`);

        // -> Check Errors
        if(searchResponse.data.status == 'ZERO_RESULTS')        throw new Errors.AddressLookupErrors.NotFoundAddressLookupError('address not found', searchResponse.data);
        if(searchResponse.data.status == 'NOT_FOUND')           throw new Errors.AddressLookupErrors.NotFoundAddressLookupError('address not found', searchResponse.data);
        if(searchResponse.data.status == 'OVER_QUERY_LIMIT')    throw new Errors.AddressLookupErrors.TooManyRequestsAddressLookupError('too many requests', searchResponse.data);
        if(searchResponse.data.status == 'REQUEST_DENIED')      throw new Errors.AddressLookupErrors.UnauthorizedAddressLookupError('google places API denied the request', searchResponse.data);
        if(searchResponse.data.status == 'INVALID_REQUEST')     throw new Errors.AddressLookupErrors.InvalidRequestAddressLookupError('invalid request', searchResponse.data);
        if(searchResponse.data.status == 'UNKNOWN_ERROR')       throw new Errors.AddressLookupErrors.AddressLookupError('error from google maps API', searchResponse.data);

        // Format the response
        const result = searchResponse.data.result;
        const formattedAddress = result.formatted_address;
        const zipCode = result.address_components?.find(c => c.types.includes('postal_code'))?.long_name || '';
        const state = result.address_components?.find(c => c.types.includes('administrative_area_level_1'))?.long_name || '';
        const city = result.address_components?.find(c => c.types.includes('locality'))?.short_name || '';
        const route = result.address_components?.find(c => c.types.includes('route'))?.short_name || '';
        const streetNumber = result.address_components?.find(c => c.types.includes('street_number'))?.long_name || '';
        const floor = result.address_components?.find(c => c.types.includes('floor'))?.long_name || '';

        const final = {
            formattedAddress: formattedAddress,
            address1: [streetNumber, route].join(' ').trim(),
            address2: [floor, streetNumber, route].join(' ').trim(),
            state: state,
            city: city,
            zipCode: zipCode
        }

        // End
        log.info(reqId, `${fnName} ### end [success] ### response: ${JSON.stringify(searchResponse.data)} ### final: ${JSON.stringify(final)}`);
        return final;

    }
    catch(e) {
        log.info(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.AddressLookupErrors.AddressLookupError) throw e;
        else throw new Errors.AddressLookupErrors.AddressLookupError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    search,
    details
}