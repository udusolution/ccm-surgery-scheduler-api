/**
 * @description module that sends sms
 * @note uses "twilio" npm package
 */


const Errors = require('../errors/_CustomErrors');
const config = require('../../config/config');
const SmsLogger = require('../loggers/sms.logger');
const twilioClient = require('twilio')(config.twilio.accountSid, config.twilio.authToken);


/**
 * send an email
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} to phone number to send sms to
 * @param {string} body the sms message
 * @throws {Errors.SmsErrors.SmsError} generic sms error
 * @throws {Errors.SmsErrors.InvalidPhoneNumberSmsError} invalid phone number sms error
 * 
 * @example send(log, reqId, '+12223334444', 'Hello')
 */
async function send(log, reqId, to, body){

    const fnName = "sms/send";

    try {

        log.info(reqId, `${fnName} ### start`);
    
        log.info(reqId, `${fnName} ### sending sms`);
        // -> Send SMS
        await twilioClient.messages.create({ 
            messagingServiceSid: config.twilio.messagingServiceSid,
            to: `+1${to}`,
            body: body,
        })
    
        // End
        log.info(reqId, `${fnName} ### end [success]`);
        SmsLogger.info(`${reqId} ### ${fnName} ### end [success] ### to: "${to}", message: "${body}"`);
        return;
    }
    catch(e) {
        log.info(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        SmsLogger.warn(`${reqId} ### ${fnName} ### end [failure] ### error: ${JSON.stringify(e)}, to: "${to}", message: "${body}"`);

        // () Invalid Phone Number Twilio error
        if(e?.code == 21211) {
            throw new Errors.SmsErrors.InvalidPhoneNumberSmsError(e.message, e);
        }

        // () Generic Sms Error
        throw new Errors.SmsErrors.SmsError(e.message, e);
    }


}


/**
 * Exports
 */
module.exports = {
    send
}