/**
 * @description module that sends emails
 * @note uses "nodemailer" npm package and "Mailgun" service
 */


const nodemailer = require('nodemailer');
const Errors = require('../errors/_CustomErrors');
const config = require('../../config/config');
const MailLogger = require('../loggers/mail.logger');


const transporter = nodemailer.createTransport({
    service: 'Mailgun',
    auth: {
        user: config.mail.user,
        pass: config.mail.pass
    },
    tls: {
        // do not fail on invalid certs
        rejectUnauthorized: false
    },
});


/**
 * send an email
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} to email(s) to send this email to (comma separated)
 * @param {string} subject the subject of the email
 * @param {string} body the html body of the email
 * @param {*} [attachments] attachments to the email
 * @throws {Errors.MailErrors.MailError} generic mail error
 * 
 * @example send(log, reqId, 'example1@mail.com,example2@mail.com', 'Test Email', '<p>Hi</p>', [{ filename: 'text3.txt', path: '/path/to/file.txt' }])
 */
async function send(log, reqId, to, subject, body, attachments){

    const fnName = "mail/send";
    log.info(reqId, `${fnName} ### start`);

    try {

        // setup email data with unicode symbols
        const mailOptions = {
            from: `${config.identity.mailerName} <${config.mail.user}>`,
            to: to,
            subject: subject,
            html: body
        };
        if(attachments) mailOptions.attachments = attachments;

        log.info(reqId, `${fnName} ### sending email`);
        // send mail with defined transport object
        await transporter.sendMail(mailOptions);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        MailLogger.info(`${reqId} ### ${fnName} ### end [success] ### to: "${to}", subject: "${subject}", body: "${body}", attachments: ${JSON.stringify(attachments)}`);
        
    }
    catch(mailError) {
        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(mailError)}`);
        MailLogger.warn(`${reqId} ### ${fnName} ### end [failure] ### error: ${JSON.stringify(mailError)}, to: "${to}", subject: "${subject}", body: "${body}", attachments: ${JSON.stringify(attachments)}`);
        throw new Errors.MailErrors.MailError(mailError.message, mailError);
    }

    

}


/**
 * Exports
 */
module.exports = {
    send
}