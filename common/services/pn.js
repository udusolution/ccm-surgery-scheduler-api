/**
 * @description module that sends pus notifications
 */


const Errors = require('../errors/_CustomErrors');
const config = require('../../config/config');
const WebPush = require('web-push')


WebPush.setVapidDetails(config.pn.contact, config.pn.vapidPublic, config.pn.vapidPrivate);


/**
 * send an push notification
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {*} subscription user's subscription credentials
 * @param {string} title PN title
 * @param {string} message PN message
 * @param {*} data custom data to send with the payload
 * @throws {Errors.PnErrors.PnError} generic PN error
 * 
 * @example send(log, reqId, {...}, 'New offer!', 'Get two chicken sandwiches for the price of one', { restaurantId: 45, validTill: '2024-03-13' } })
 */
async function send(log, reqId, subscription, title, message, data = {}){

    const fnName = "pn/send";
    log.info(reqId, `${fnName} ### start`);

    try {

        // Payload
        const payload = {
            title, 
            message, 
            ...data
        }

        log.info(reqId, `${fnName} ### sending PN`);
        // -> send PN
        await WebPush.sendNotification(subscription, JSON.stringify(payload));

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        
    }
    catch(e) {
        log.warn(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        throw new Errors.PnErrors.PnError(e.message, e);
    }

}


/**
 * Exports
 */
module.exports = {
    send
}