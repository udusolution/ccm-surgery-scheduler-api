const process = require('process');
const {
    S3Client,
    PutObjectCommand,
    DeleteObjectCommand,
    DeleteBucketCommand,
    paginateListObjectsV2,
    GetObjectCommand,
    ListObjectsCommand, ListObjectsV2Command, ObjectCannedACL
} = require('@aws-sdk/client-s3');

function getClient() {
    return new S3Client({
        region: process.env.S3_REGION,
        credentials: {
            accessKeyId: process.env.S3_ACCESS_KEY_ID,
            secretAccessKey: process.env.S3_ACCESS_KEY_SECRET,
        }
    });
}

async function listBucket(eventId) {
    return getClient().send(
        new ListObjectsV2Command({
            Bucket: process.env.S3_BUCKET_NAME,
            Prefix: `${eventId}/`,
            Delimiter: '/'
        })
    )
}

async function putStream(name, stream) {
    return getClient().send(
        new PutObjectCommand({
            Bucket: process.env.S3_BUCKET_NAME,
            Key: name,
            Body: stream,
            ACL: ObjectCannedACL.public_read
        })
    )
}

async function deleteFile(name) {
    return getClient().send(
        new DeleteObjectCommand({
            Bucket: process.env.S3_BUCKET_NAME,
            Key: name,
        })
    )
}

module.exports = {
    putStream,
    listBucket,
    deleteFile,
}