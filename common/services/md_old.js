/**
 * @description integration with the AdvancedMD API
 * @note uses 'axios' npm package
 * @note connection and auth params are in the config
 */


const axios = require('axios').default;
const config = require('../../config/config');
const Errors = require('../errors/_CustomErrors');


// MD API Endpoints
const endpoints = {
    AUTHENTICATE:       '/authenticate',
    DEMOGRAPHICS:       '/demographics/patients/',
    ALLERGIES:          '/clinical/allergies',
    ASSESSMENTS:        '/clinical/assessments',
    GOALS:              '/clinical/goals',
    HEALTH_CONCERNS:    '/clinical/healthconcerns',
    IMMUNIZATIONS:      '/clinical/immunizations',
    IMPLANTED_DEVICES:  '/clinical/implanteddevices',
    MEDICATIONS:        '/clinical/medications',
    ORDERS:             '/clinical/orders',
    PLANS:              '/clinical/plans',
    PROBLEMS:           '/clinical/problems',
    PROCEDURES:         '/clinical/procedures',
    PROVIDERS:          '/clinical/providers',
    RESULTS:            '/clinical/results',
    SMOKING_STATUS:     '/clinical/smokingstatus',
    VITAL_SIGNS:        '/clinical/vitalsigns'
}

// MD API Headers
const headers = {
    apikey: config.md.apiKey,
    Authorization: 'FIGURE OUT WHERE TO SAVE THIS SINCE IT IS RETURNED FROM THE MD AUTH ENDPOINT'
}


/**
 * custom error handler for the AdvancedMD API integration code
 * @param {*} error error object thrown by the HTTP handler
 */
function mdHttpErrorHandler(error) {

    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    if (error.response) {

        // Throw custom error depending on returned status code
        switch(error.response.status) {
            case 401:
                throw new Errors.MdErrors.UnauthorizedMdError(error.message, error);
            case 403:
                throw new Errors.MdErrors.ForbiddenMdError(error.message, error);
            case 429:
                throw new Errors.MdErrors.TooManyRequestsMdError(error.message, error);
            default:
                throw new Errors.MdErrors.MdError(error.message, error);
        }
    } 
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    else if (error.request) {
        throw new Errors.HttpErrors.HttpError(error.message, error);
    } 
    // Something happened in setting up the request that triggered an Error
    else {
        throw error;
    }
}


/**
 * Log in using AdvancedMD credentials (username, password, office key)
 * @param {*} log 
 * @param {*} reqId
 * @returns {{patientdata: [{patientid: number, name: string, address1: string, address2: string, city: string, state: string, zipcode: string, dateofbirth: string}], token: string}}
 */
async function authenticate(log, reqId) {

    const fnName = 'md/authenticate';
    log.info(reqId, `${fnName} ### start`);

    // -> Http Request
    const result = await axios.post(config.md.baseURL + endpoints.AUTHENTICATE, {
        username: config.md.username,
        password: config.md.password,
        officekey: config.md.officeKey
    }).catch(mdHttpErrorHandler);

    // End
    return result.data;
}


/**
 * get the patient's detailed demographic information
 * @param {*} log 
 * @param {*} reqId 
 * @param {string} patientId MD patient's ID
 * @returns {[{id: string, name: string, dob: string, sex: string, language: string, ethnicity: string, races: string, address: {address1: string, address2: string, city: string, state: string, zip: string}}]}
 */
async function patientDemographics(log, reqId, patientId) {

    const fnName = 'md/patientDemographics';
    log.info(reqId, `${fnName} ### start`);

    // -> Http Request
    const result = await axios.get(config.md.baseURL + endpoints.DEMOGRAPHICS + patientId, {
        headers
    }).catch(mdHttpErrorHandler);

    // End
    return result.data;
}


/**
 * get the patient's Allergies
 * @param {*} log 
 * @param {*} reqId 
 * @param {string} patientId MD patient ID
 * @param {string} startDate The beginning (m/d/yyyy) of the date range to return data for
 * @param {string} endDate The end (m/d/yyyy) of the date range to return data for
 * @returns {[{id: string, name: string, status: string, allergyDate: string, reaction: string, rxNormId: string}]}
 */
async function patientAllergies(log, reqId, patientId, startDate, endDate) {

    const fnName = 'md/patientAllergies';
    log.info(reqId, `${fnName} ### start`);

    // -> Http Request
    const result = await axios.get(config.md.baseURL + endpoints.ALLERGIES, {
        headers,
        params: {
            patientid: patientId,
            startDate,
            endDate
        }
    }).catch(mdHttpErrorHandler);

    // End
    return result.data;
}


/**
 * get the patient's assessments
 * @param {*} log 
 * @param {*} reqId 
 * @param {string} patientId MD patient ID
 * @param {string} startDate The beginning (m/d/yyyy) of the date range to return data for
 * @param {string} endDate The end (m/d/yyyy) of the date range to return data for
 * @returns {[{date: string, assessment: string}]}
 */
async function patientAssessments(log, reqId, patientId, startDate, endDate) {

    const fnName = 'md/patientAssessments';
    log.info(reqId, `${fnName} ### start`);

    // -> Http Request
    const result = await axios.get(config.md.baseURL + endpoints.ASSESSMENTS, {
        headers,
        params: {
            patientid: patientId,
            startDate,
            endDate
        }
    }).catch(mdHttpErrorHandler);

    // End
    return result.data;
}


/**
 * get the patient's goals
 * @param {*} log 
 * @param {*} reqId 
 * @param {string} patientId MD patient ID
 * @param {string} startDate The beginning (m/d/yyyy) of the date range to return data for
 * @param {string} endDate The end (m/d/yyyy) of the date range to return data for
 * @returns {[{date: string, goalText: string}]}
 */
async function patientGoals(log, reqId, patientId, startDate, endDate) {

    const fnName = 'md/patientGoals';
    log.info(reqId, `${fnName} ### start`);

    // -> Http Request
    const result = await axios.get(config.md.baseURL + endpoints.GOALS, {
        headers,
        params: {
            patientid: patientId,
            startDate,
            endDate
        }
    }).catch(mdHttpErrorHandler);

    // End
    return result.data;
}


/**
 * get the patient's health concerns
 * @param {*} log 
 * @param {*} reqId 
 * @param {string} patientId MD patient ID
 * @param {string} startDate The beginning (m/d/yyyy) of the date range to return data for
 * @param {string} endDate The end (m/d/yyyy) of the date range to return data for
 * @returns {[{date: string, concern: string}]}
 */
async function patientHealthConcerns(log, reqId, patientId, startDate, endDate) {

    const fnName = 'md/patientHealthConcerns';
    log.info(reqId, `${fnName} ### start`);

    // -> Http Request
    const result = await axios.get(config.md.baseURL + endpoints.HEALTH_CONCERNS, {
        headers,
        params: {
            patientid: patientId,
            startDate,
            endDate
        }
    }).catch(mdHttpErrorHandler);

    // End
    return result.data;
}


/**
 * get the patient's immunizations
 * @param {*} log 
 * @param {*} reqId 
 * @param {string} patientId MD patient ID
 * @param {string} startDate The beginning (m/d/yyyy) of the date range to return data for
 * @param {string} endDate The end (m/d/yyyy) of the date range to return data for
 * @returns {[{id: string, name: string, date: string, status: string, quantity: string, unit: string, lotNumber: string, cvxCode: string, details: string}]}
 */
async function patientImmunizations(log, reqId, patientId, startDate, endDate) {

    const fnName = 'md/patientImmunizations';
    log.info(reqId, `${fnName} ### start`);

    // -> Http Request
    const result = await axios.get(config.md.baseURL + endpoints.IMMUNIZATIONS, {
        headers,
        params: {
            patientid: patientId,
            startDate,
            endDate
        }
    }).catch(mdHttpErrorHandler);

    // End
    return result.data;
}


/**
 * get the patient's implanted devices
 * @param {*} log 
 * @param {*} reqId 
 * @param {string} patientId MD patient ID
 * @param {string} startDate The beginning (m/d/yyyy) of the date range to return data for
 * @param {string} endDate The end (m/d/yyyy) of the date range to return data for
 * @returns {[{id: string, deviceId: string, issuingAgency: string, siteName: string, siteCode: string, deviceName: string, deviceDescription: string, manufacturer: string, model: string, serialNumber: string, lot: string, isActive: string, date: string}]}
 */
async function patientImplantedDevices(log, reqId, patientId, startDate, endDate) {

    const fnName = 'md/patientImplantedDevices';
    log.info(reqId, `${fnName} ### start`);

    // -> Http Request
    const result = await axios.get(config.md.baseURL + endpoints.IMPLANTED_DEVICES, {
        headers,
        params: {
            patientid: patientId,
            startDate,
            endDate
        }
    }).catch(mdHttpErrorHandler);

    // End
    return result.data;
}


/**
 * get the patient's medications
 * @param {*} log 
 * @param {*} reqId 
 * @param {string} patientId MD patient ID
 * @param {string} startDate The beginning (m/d/yyyy) of the date range to return data for
 * @param {string} endDate The end (m/d/yyyy) of the date range to return data for
 * @returns {[{rxNormId: string, name: string, instructions: string, quantity: string, doseForm: string, date: string, expirationDate: string, currentStatus: string, type: string}]}
 */
async function patientMedications(log, reqId, patientId, startDate, endDate) {

    const fnName = 'md/patientMedications';
    log.info(reqId, `${fnName} ### start`);

    // -> Http Request
    const result = await axios.get(config.md.baseURL + endpoints.MEDICATIONS, {
        headers,
        params: {
            patientid: patientId,
            startDate,
            endDate
        }
    }).catch(mdHttpErrorHandler);

    // End
    return result.data;
}


/**
 * get the patient's orders
 * @param {*} log 
 * @param {*} reqId 
 * @param {string} patientId MD patient ID
 * @param {string} startDate The beginning (m/d/yyyy) of the date range to return data for
 * @param {string} endDate The end (m/d/yyyy) of the date range to return data for
 * @returns {[{date: string, testDescription: string, loincCode: string}]}
 */
async function patientOrders(log, reqId, patientId, startDate, endDate) {

    const fnName = 'md/patientOrders';
    log.info(reqId, `${fnName} ### start`);

    // -> Http Request
    const result = await axios.get(config.md.baseURL + endpoints.ORDERS, {
        headers,
        params: {
            patientid: patientId,
            startDate,
            endDate
        }
    }).catch(mdHttpErrorHandler);

    // End
    return result.data;
}


/**
 * get the patient's plans
 * @param {*} log 
 * @param {*} reqId 
 * @param {string} patientId MD patient ID
 * @param {string} startDate The beginning (m/d/yyyy) of the date range to return data for
 * @param {string} endDate The end (m/d/yyyy) of the date range to return data for
 * @returns {[{date: string, key: string, plan: string, code: string}]}
 */
async function patientPlans(log, reqId, patientId, startDate, endDate) {

    const fnName = 'md/patientPlans';
    log.info(reqId, `${fnName} ### start`);

    // -> Http Request
    const result = await axios.get(config.md.baseURL + endpoints.PLANS, {
        headers,
        params: {
            patientid: patientId,
            startDate,
            endDate
        }
    }).catch(mdHttpErrorHandler);

    // End
    return result.data;
}


/**
 * get the patient's problems
 * @param {*} log 
 * @param {*} reqId 
 * @param {string} patientId MD patient ID
 * @param {string} startDate The beginning (m/d/yyyy) of the date range to return data for
 * @param {string} endDate The end (m/d/yyyy) of the date range to return data for
 * @returns {[{code: string, codeSystem: string, name: string, description: string, status: string, date: string}]}
 */
async function patientProblems(log, reqId, patientId, startDate, endDate) {

    const fnName = 'md/patientProblems';
    log.info(reqId, `${fnName} ### start`);

    // -> Http Request
    const result = await axios.get(config.md.baseURL + endpoints.PROBLEMS, {
        headers,
        params: {
            patientid: patientId,
            startDate,
            endDate
        }
    }).catch(mdHttpErrorHandler);

    // End
    return result.data;
}


/**
 * get the patient's procedures
 * @param {*} log 
 * @param {*} reqId 
 * @param {string} patientId MD patient ID
 * @param {string} startDate The beginning (m/d/yyyy) of the date range to return data for
 * @param {string} endDate The end (m/d/yyyy) of the date range to return data for
 * @returns {[{date: string, code: string, type: string, description: string}]}
 */
async function patientProcedures(log, reqId, patientId, startDate, endDate) {

    const fnName = 'md/patientProcedures';
    log.info(reqId, `${fnName} ### start`);

    // -> Http Request
    const result = await axios.get(config.md.baseURL + endpoints.PROCEDURES, {
        headers,
        params: {
            patientid: patientId,
            startDate,
            endDate
        }
    }).catch(mdHttpErrorHandler);

    // End
    return result.data;
}


/**
 * get the patient's providers
 * @param {*} log 
 * @param {*} reqId 
 * @param {string} patientId MD patient ID
 * @param {string} startDate The beginning (m/d/yyyy) of the date range to return data for
 * @param {string} endDate The end (m/d/yyyy) of the date range to return data for
 * @returns {[{npiNumber: string, address1: string, address2: string, city: string, state: string, zip: string, country: string, title: string, lastName: string, firstName: string, middleName: string}]}
 */
async function patientProviders(log, reqId, patientId, startDate, endDate) {

    const fnName = 'md/patientProviders';
    log.info(reqId, `${fnName} ### start`);

    // -> Http Request
    const result = await axios.get(config.md.baseURL + endpoints.PROVIDERS, {
        headers,
        params: {
            patientid: patientId,
            startDate,
            endDate
        }
    }).catch(mdHttpErrorHandler);

    // End
    return result.data;
}


/**
 * get the patient's results
 * @param {*} log 
 * @param {*} reqId 
 * @param {string} patientId MD patient ID
 * @param {string} startDate The beginning (m/d/yyyy) of the date range to return data for
 * @param {string} endDate The end (m/d/yyyy) of the date range to return data for
 * @returns {[{code: string, name: string, date: string, results: {code: string, name: string, value: string, unit: string, interpretationCode: string, range: string, rangeFrom: string, rangeTo: string}}]}
 */
async function patientResults(log, reqId, patientId, startDate, endDate) {

    const fnName = 'md/patientResults';
    log.info(reqId, `${fnName} ### start`);

    // -> Http Request
    const result = await axios.get(config.md.baseURL + endpoints.RESULTS, {
        headers,
        params: {
            patientid: patientId,
            startDate,
            endDate
        }
    }).catch(mdHttpErrorHandler);

    // End
    return result.data;
}


/**
 * get the patient's smoking status
 * @param {*} log 
 * @param {*} reqId 
 * @param {string} patientId MD patient ID
 * @param {string} startDate The beginning (m/d/yyyy) of the date range to return data for
 * @param {string} endDate The end (m/d/yyyy) of the date range to return data for
 * @returns {[{value: string, description: string, date: string}]}
 */
async function patientSmokingStatus(log, reqId, patientId, startDate, endDate) {

    const fnName = 'md/patientSmokingStatus';
    log.info(reqId, `${fnName} ### start`);

    // -> Http Request
    const result = await axios.get(config.md.baseURL + endpoints.SMOKING_STATUS, {
        headers,
        params: {
            patientid: patientId,
            startDate,
            endDate
        }
    }).catch(mdHttpErrorHandler);

    // End
    return result.data;
}


/**
 * get the patient's vital signs
 * @param {*} log 
 * @param {*} reqId 
 * @param {string} patientId MD patient ID
 * @param {string} startDate The beginning (m/d/yyyy) of the date range to return data for
 * @param {string} endDate The end (m/d/yyyy) of the date range to return data for
 * @returns {[{date: string, measurements: {name: string, value: string, metricValue: string}}]}
 */
async function patientVitalSigns(log, reqId, patientId, startDate, endDate) {

    const fnName = 'md/patientVitalSigns';
    log.info(reqId, `${fnName} ### start`);

    // -> Http Request
    const result = await axios.get(config.md.baseURL + endpoints.VITAL_SIGNS, {
        headers,
        params: {
            patientid: patientId,
            startDate,
            endDate
        }
    }).catch(mdHttpErrorHandler);

    // End
    return result.data;
}


/**
 * Exports
 */
module.exports = {
    authenticate,
    patientDemographics,
    patientAllergies,
    patientAssessments,
    patientGoals,
    patientHealthConcerns,
    patientImmunizations,
    patientImplantedDevices,
    patientMedications,
    patientOrders,
    patientPlans,
    patientProblems,
    patientProcedures,
    patientProviders,
    patientResults,
    patientSmokingStatus,
    patientVitalSigns
}