const process = require('process');
const {google} = require('googleapis');
const Errors = require('../../common/errors/_CustomErrors');
const Logger = require("../loggers/route.logger");
const momentTZ = require("moment-timezone");
const Enums = require("../enums");

async function createClient() {
    const log = Logger.get('/gcal/client');

    try {
        log.info("Creating google calendar client");
        const auth = new google.auth.JWT(
            process.env.GOOGLE_CALENDAR_CLIENT_EMAIL,
            null,
            process.env.GOOGLE_CALENDAR_PRIVATE_KEY,
            ['https://www.googleapis.com/auth/calendar']
        )
        return google.calendar({
            version: 'v3',
            project: process.env.GOOGLE_CALENDAR_PROJECT_NAME,
            auth
        });
    } catch (err) {
        log.error('Error creating the google calendar client', err)
        throw err
    }
}

async function createEvent(event) {
    const log = Logger.get('/gcal/createEvent');


    try {
        const client = await createClient();

        log.info('Creating an event in Google Calendar')
        const res = await client.events.insert({
            calendarId: process.env.GOOGLE_CALENDAR_ID,
            resource: event,
        });

        log.info(`Successfully created event ${event.summary} in Google Calendar with id ${res.data.id} `)
        return res;
    } catch (err) {
        log.info('Could not created an event in Google Calendar: ' + event.summary)
        throw new Errors.GCalErrors.GCalError(err.message, err)
    }
}

async function updateEvent(eventId, newStatus, dateFrom = null, dateTo = null) {
    const log = Logger.get('/gcal/updateEvent');

    try {
        const client = await createClient();

        log.info('Updating an event in Google Calendar')
        const res = await client.events.get({
            calendarId: process.env.GOOGLE_CALENDAR_ID,
            eventId
        });
        const event = res.data;

        let initials = ''
        let eventType = ''

        let summaryPartitions = event.summary.split(' - ')
        if (summaryPartitions.length !== 3) {
            summaryPartitions = event.summary.split(' ')
            initials = summaryPartitions.shift()
            eventType = summaryPartitions.join(' ')
        } else {
            initials = summaryPartitions[0]
            eventType = summaryPartitions[2]
        }

        switch(newStatus) {
            case Enums.EventStatuses.BoardingSlipGenerated:
                newStatus = ' - BSG - ';
                break;
            case Enums.EventStatuses.WaitingForConfirmation:
                newStatus = ' - WFC - ';
                break;
            case Enums.EventStatuses.Confirmed:
                newStatus = ' ';
                break;
            default:
                newStatus = ` - ${newStatus.toUpperCase()} - `;
        }

        event.summary = initials + newStatus + eventType

        if (dateFrom) {
            event.start.dateTime = momentTZ(dateFrom).tz(Enums.ZoneFormats.MichiganDetroit).toISOString()
        }

        if (dateTo) {
            event.end.dateTime = momentTZ(dateTo).tz(Enums.ZoneFormats.MichiganDetroit).toISOString()
        }

        await client.events.update({
            calendarId: process.env.GOOGLE_CALENDAR_ID,
            eventId,
            resource: event
        })

        log.info(`Successfully updated event ${event.summary}`)
        return res;
    } catch (err) {
        if (err?.code === '404') {
            log.info(`Could not delete event ${eventId} in Google Calendar: Not found`)
            return;
        }
        log.info(`Could not update event ${eventId} in Google Calendar`)
        throw new Errors.GCalErrors.GCalError(err.message, err)
    }
}

async function deleteEvent(eventId) {
    const log = Logger.get('/gcal/deleteEvent');

    try {
        const client = await createClient();

        log.info(`Deleting event ${eventId} in Google Calendar`)
        const res = await client.events.delete({
            calendarId: process.env.GOOGLE_CALENDAR_ID,
            eventId
        });
        log.info(`Successfully deleted event ${eventId}`)
        return res;
    } catch (err) {
        if (err?.code === '404') {
            log.info(`Could not delete event ${eventId} in Google Calendar: Not found`)
            return;
        }
        log.info(`Could not delete event ${eventId} in Google Calendar`)
        throw new Errors.GCalErrors.GCalError(err.message, err)
    }
}

module.exports = {
    createEvent,
    updateEvent,
    deleteEvent
}