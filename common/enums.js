/**
 * @description all the enums used in the backend
 */


const HttpCodes = {
    Success: 200,
    BadRequest: 400,
    Unauthorized: 401,
    Forbidden: 403,
    NotFound: 404,
    Invalid: 420,
    Expired: 421,
    TooManyRequests: 423,
    Retry: 442,
    InvalidOperation: 444,
    InvalidRefreshToken: 477,
    InternalServerError: 500,
}

const UserRoles = {
    Admin: 'admin',
    Coordinator: 'coordinator',
    Physician: 'physician',
    AddPatient: 'add_patient',
    UpdatePatient: 'update_patient'
}

const DateFormats = {
    StandardDate: 'YYYY-MM-DD',
    StandardDateTime: 'YYYY-MM-DD hh:mm',
    date: 'MM/DD/YYYY',
    time: 'hh:mm a',
    dateTime: 'MM/DD/YYYY hh:mm a',
    dateTimeZ: 'MM/DD/YYYY hh:mm a z',
    dateInputDate: 'YYYY-MM-DD',
    dateInputDateTime: 'YYYY-MM-DD[T]HH:mm',
    apiDateTime: 'YYYY-MM-DD HH:mm:ss',
    friendlyDate: 'dddd, MMMM Do, YYYY',
    isoDate: 'YYYY-MM-DD',
    headerDateTime: 'dddd, MMMM Do, YYYY hh:mm:ss a [GMT]ZZ'
}

const ZoneFormats = {
    MichiganDetroit: 'US/Michigan'
}

const RateLimiterGroups = {
    OTP: 'OTP'
}

const HistoryEvents = {
    AddMasterData: 'AddMasterData',
    DeleteMasterData: 'DeleteMasterData',
    AddPatientData: 'AddPatientData',
    UpdatePatientData: 'UpdatePatientData',
    DeletePatientData: 'DeletePatientData',
    AddEvent: 'AddEvent',
    AddEventClearance: 'AddEventClearance',
    AddEventBloodThinner: 'AddEventBloodThinner',
    UpdateEventBloodThinner: 'UpdateEventBloodThinner',
    UpdateEvent: 'UpdateEvent',
    UpdateEventClearance: 'UpdateEventClearance',
    UpdateEventExtraClearance: 'UpdateEventExtraClearance',
    UpdateEventInsuranceAuthorization: 'UpdateEventInsuranceAuthorization',
    DeleteEvent: 'DeleteEvent',
    DeleteEventClearance: 'DeleteEventClearance',
    CancelEvent: 'CancelEvent',
    CodeEvent: 'CodeEvent',
    RescheduleEvent: 'RescheduleEvent',
    AddPatient: 'AddPatient',
    UpdatePatient: 'UpdatePatient',
    DeletePatient: 'DeletePatient',
    AddEventAuthNote: 'AddEventAuthNote',
    GenerateClearancePdf: 'GenerateClearancePdf',
    SendBoardingSlipFacilityEmail: 'SendBoardingSlipFacilityEmail',
    RemoveEventBloodThinner: 'RemoveEventBloodThinner',
    RemoveAllEventBloodThinners: 'RemoveAllEventBloodThinners',
    AddEventDiagnosis: 'AddEventDiagnosis',
    UpdateEventDiagnosis: 'UpdateEventDiagnosis',
    DeleteEventDiagnosis: 'DeleteEventDiagnosis',
    DeleteEventAuthNote: 'DeleteEventAuthNote',
    AddEventNote: 'AddEventNote',
    AddEventPatientCommunication: 'AddEventPatientCommunication',
    UpdateEventPatientCommunication: 'UpdateEventPatientCommunication',
    DeleteEventPatientCommunication: 'DeleteEventPatientCommunication',
    AddEventProcedure: 'AddEventProcedure',
    UpdateEventProcedure: 'UpdateEventProcedure',
    DeleteEventProcedure: 'DeleteEventProcedure',
    SendPatientCommunication: 'SendPatientCommunication',
    SetUserSignature: 'SetUserSignature',
    ChangeUserPassword: 'ChangeUserPassword',
    ChangeMyPassword: 'ChangeMyPassword',
    UpdateMasterData: 'UpdateMasterData',
    UpdateUser: 'UpdateUser',
    AddUser: 'AddUser',
    SyncEventPatientDetails: 'SyncEventPatientDetails',
    DeleteUser: 'DeleteUser',
    EnableEventScheduledCommunication: 'EnableEventScheduledCommunication',
    UpdateEventScheduledCommunication: 'UpdateEventScheduledCommunication',
    DeleteEventScheduledCommunication: 'DeleteEventScheduledCommunication',
    ForceSendEventScheduledCommunication: 'ForceSendEventScheduledCommunication',
    GenerateBoardingSlip: 'GenerateBoardingSlip',
    AddDayNote: 'AddDayNote',
    DeleteDayNote: 'DeleteDayNote',
    RequestEventEdit: 'RequestEventEdit',
    ApproveEventEdit: 'ApproveEventEdit',
    RejectEventEdit: 'RejectEventEdit',
    UpdateEventEditSetting: 'UpdateEventEditSetting',
    UpdateEventEditWhitelist: 'UpdateEventEditWhitelist',
    LockUserAccount: 'LockUserAccount',
    UnlockUserAccount: 'UnlockUserAccount',
}

const EventStatuses = {
    Pending: 'pending',
    Draft: 'draft',
    BoardingSlipGenerated: 'boarding_slip_generated',
    WaitingForConfirmation: 'waiting_for_confirmation',
    Confirmed: 'confirmed',
    Rejected: 'rejected',
    Completed: 'completed',
    Cancelled: 'cancelled',
}

const InsuranceAuthorizationStatuses = {
    NotRequired: 'not-required',
    TBD: 'tbd',
    Approved: 'approved'
}

const MasterDataTables = {
    Facility: 'facility',
    FacilityRoom: 'facility_room',
    EventType: 'event_type',
    BloodThinner: 'blood_thinner',
    Diagnosis: 'diagnosis',
    Priority: 'priority',
    Procedures: 'procedures',
    Carrier: 'carrier',
    PatientCommunication: 'patient_communication',
    SmsTemplate: 'sms_template',
    Email: 'email',
    EmailAttachment: 'email_attachment',
    Sms: 'sms',
    EmailScheduledCommunication: 'email_scheduled_com',
    EventCancelReason: 'event_cancel_reason',
    Announcement: 'announcement',
}

const EmailTemplates = {
    PatientCommunication: {
        key: 'PatientCommunication',
        label: 'Patient Communication Email'
    },
    EventReminder: {
        key: 'EventReminder',
        label: 'Event Reminder Email'
    },
    FacilityBoardingSlip: {
        key: 'FacilityBoardingSlip',
        label: 'Facility Boarding Slip Email'
    }
}
const SmsTemplates = {
    PatientCommunication: {
        key: 'PatientCommunication',
        label: 'Patient Communication SMS'
    },
    EventReminder: {
        key: 'EventReminder',
        label: 'Event Reminder SMS'
    },
}

const DayNoteRecurrence = {
    Once: 'Once',
    Daily: 'Daily',
    Weekly: 'Weekly',
    Monthly: 'Monthly',
    Yearly: 'Yearly',
}

const ProductivityReportIntervals = {
    Daily: 'Daily',
    Monday: 'Monday',
    Tuesday: 'Tuesday',
    Wednesday: 'Wednesday',
    Thursday: 'Thursday',
    Friday: 'Friday',
    Saturday: 'Saturday',
    Sunday: 'Sunday',
    BiMonthly: 'BiMonthly',
    FirstDayOfMonth: 'FirstDayOfMonth'
}

const EventEditRequestStatuses = {
    Pending: 'pending',
    Approved: 'approved',
    Rejected: 'rejected'
}

const UserEventEditPermissions = {
    AllowAll: 'allow-all',
    AllowNone: 'allow-none',
    UponRequest: 'upon-request'
}
const UserEventEditPermissionsArray = ['allow-all', 'allow-none', 'upon-request'];

const AnnouncementShowTo = {
    All: 'all',
    Admins: 'admins',
    Coordinators: 'coordinators',
    Physicians: 'physicians'
}

const PnEventTypes = {
    DiagnosisAddedToEvent: 'DiagnosisAddedToEvent',
    UpdatedEvent: 'UpdatedEvent',
    DeletedEvent: 'DeletedEvent',
    AddedEvent: 'AddedEvent',
    SentEditRequest: 'SentEditRequest',
    AcceptedEditRequest: 'AcceptedEditRequest',
    RejectedEditRequest: 'AcceptedEditRequest',
    UpdatedUser: 'UpdatedUser',
    UserLocked: 'UserLocked',
    UpdatedUserEventEditSettings: 'UpdatedUserEventEditSettings'
}


/**
 * Export
 */
module.exports = {
    HttpCodes,
    UserRoles,
    DateFormats,
    ZoneFormats,
    RateLimiterGroups,
    HistoryEvents,
    EventStatuses,
    InsuranceAuthorizationStatuses,
    MasterDataTables,
    EmailTemplates,
    SmsTemplates,
    DayNoteRecurrence,
    ProductivityReportIntervals,
    EventEditRequestStatuses,
    UserEventEditPermissions,
    UserEventEditPermissionsArray,
    AnnouncementShowTo,
    PnEventTypes,
}