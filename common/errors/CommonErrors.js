/**
 * @description common custom errors
 */


class SanityError extends Error {
    constructor(message, userMessage) {
        super(message);
        this.name = "SanityError";
        if(userMessage) this.userMessage = userMessage;
    }
}


class ReservedTimeError extends Error {
    constructor(message, userMessage = null, intersectingEvents = null) {
        super(message);
        this.name = "ReservedTimeError";
        if(userMessage) this.userMessage = userMessage;
        if(intersectingEvents) this.intersectingEvents = intersectingEvents;
    }
}


class ParamError extends Error {
    constructor(message) {
        super(message);
        this.name = "ParamError";
    }
}

class NotFoundError extends Error {
    constructor(message, userMessage) {
        super(message);
        this.name = "NotFoundError";
        if(userMessage) this.userMessage = userMessage;
    }
}


/**
 * Export
 */
module.exports = {
    SanityError,
    ReservedTimeError,
    ParamError,
    NotFoundError
}