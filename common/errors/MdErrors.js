/**
 * @description Advanced MD API custom errors
 */


class MdError extends Error {
    constructor(message, originalError = null) {
        super(message);
        this.name = "MdError";
        this.originalError = originalError;
    }
}

class UnauthorizedMdError extends MdError {
    constructor(message, originalError = null) {
        super(message, originalError);
        this.name = "UnauthorizedMdError";
    }
}

class ForbiddenMdError extends MdError {
    constructor(message, originalError = null) {
        super(message, originalError);
        this.name = "ForbiddenMdError";
    }
}

class TooManyRequestsMdError extends MdError {
    constructor(message, originalError = null) {
        super(message, originalError);
        this.name = "TooManyRequestsMdError";
    }
}


/**
 * Export
 */
module.exports = {
    MdError,
    UnauthorizedMdError,
    ForbiddenMdError,
    TooManyRequestsMdError
}