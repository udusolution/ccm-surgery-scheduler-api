/**
 * @description jwt custom errors
 */

class GCalError extends Error {
    constructor(message, originalError = null) {
        super(message);
        this.name = "GCalError";
        this.originalError = originalError;
    }
}

/**
 * Export
 */
module.exports = {
    GCalError
}
