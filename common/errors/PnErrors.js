/**
 * @description push notifications custom errors
 */


class PnError extends Error {
    constructor(message, originalError = null) {
        super(message);
        this.name = "PnError";
        this.originalError = originalError;
    }
}


/**
 * Export
 */
module.exports = {
    PnError
}