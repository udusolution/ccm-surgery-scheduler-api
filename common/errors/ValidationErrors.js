/**
 * @description data validation custom errors
 */


 class ValidationError extends Error {
    constructor(message, errors = []) {
        super(message);
        this.name = "ValidationError";
        this.errors = errors;
    }
}


/**
 * Export
 */
module.exports = {
    ValidationError
}