/**
 * @description jwt custom errors
 */


class JwtError extends Error {
    constructor(message, originalError = null) {
        super(message);
        this.name = "JwtError";
        this.originalError = originalError;
    }
}

class JwtEncryptionError extends JwtError {
    constructor(message, originalError = null) {
        super(message, originalError);
        this.name = "JwtEncryptionError";
    }
}

class JwtDecryptionError extends JwtError {
    constructor(message, originalError = null) {
        super(message, originalError);
        this.name = "JwtDecryptionError";
    }
}

class JwtExpiredError extends JwtDecryptionError {
    constructor(message, originalError = null) {
        super(message, originalError);
        this.name = "JwtExpiredError";
    }
}


/**
 * Export
 */
module.exports = {
    JwtError,
    JwtEncryptionError,
    JwtDecryptionError,
    JwtExpiredError
}