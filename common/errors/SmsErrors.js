/**
 * @description sms custom errors
 */


class SmsError extends Error {
    constructor(message, originalError = null) {
        super(message);
        this.name = "SmsError";
        this.originalError = originalError;
    }
}

class InvalidPhoneNumberSmsError extends SmsError {
    constructor(message, originalError = null) {
        super(message);
        this.name = "InvalidPhoneNumberSmsError";
        this.originalError = originalError;
    }
}


/**
 * Export
 */
module.exports = {
    SmsError,
    InvalidPhoneNumberSmsError
}