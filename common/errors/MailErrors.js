/**
 * @description mail custom errors
 */


class MailError extends Error {
    constructor(message, originalError = null) {
        super(message);
        this.name = "MailError";
        this.originalError = originalError;
    }
}


/**
 * Export
 */
module.exports = {
    MailError
}