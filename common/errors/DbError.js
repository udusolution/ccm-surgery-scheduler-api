/**
 * @description database custom errors
 */


class DbError extends Error {
    constructor(message, originalError = null) {
        super(message);
        this.name = "DbError";
        this.originalError = originalError;
    }
}

class InvalidColumnError extends DbError {
    constructor(message, originalError = null) {
        super(message);
        this.name = "InvalidColumnError";
        this.originalError = originalError;
    }
}

class MigrationsError extends DbError {
    constructor(message, originalError = null) {
        super(message);
        this.name = "MigrationFailure";
        this.originalError = originalError;
        console.error("Migrations failed")
        process.exit(0);
    }
}


/**
 * Export
 */
module.exports = {
    DbError,
    InvalidColumnError,
    MigrationsError,
}