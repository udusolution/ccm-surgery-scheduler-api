/**
 * @description HTTP custom errors
 */


class HttpError extends Error {
    constructor(message, originalError = null) {
        super(message);
        this.name = "HttpError";
        this.originalError = originalError;
    }
}


/**
 * Export
 */
module.exports = {
    HttpError
}