/**
 * @description custom errors defined for this application
 * @note uses the errors defined in the same directory
 */


const DbErrors = require('./DbError');
const RateErrors = require('./RateError');
const AuthErrors = require('./AuthError');
const ValidationErrors = require('./ValidationErrors');
const CommonErrors = require('./CommonErrors');
const MailErrors = require('./MailErrors');
const SmsErrors = require('./SmsErrors');
const FileErrors = require('./FileErrors');
const JwtErrors = require('./JwtErrors');
const EncErrors = require('./EncErrors');
const MdErrors = require('./MdErrors');
const HttpErrors = require('./HttpErrors');
const AddressLookupErrors = require('./AddressLookupErrors');
const FileUploadErrors = require('./FileUploadErrors');
const PnErrors = require('./PnErrors');
const GCalErrors = require('./GCalErrors')

/**
 * Export
 */
module.exports = {
    DbErrors,
    RateErrors,
    AuthErrors,
    ValidationErrors,
    CommonErrors,
    MailErrors,
    SmsErrors,
    FileErrors,
    JwtErrors,
    EncErrors,
    MdErrors,
    HttpErrors,
    AddressLookupErrors,
    FileUploadErrors,
    PnErrors,
    GCalErrors,
}