/**
 * @description file custom errors
 */


class FileError extends Error {
    constructor(message) {
        super(message);
        this.name = "FileError";
    }
}


/**
 * Export
 */
module.exports = {
    FileError
}