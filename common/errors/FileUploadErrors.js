/**
 * @description file upload custom errors
 */


class FileUploadError extends Error {
    constructor(message, originalError = null) {
        super(message);
        this.name = "FileUploadError";
        this.originalError = originalError;
    }
}


/**
 * Export
 */
module.exports = {
    FileUploadError
}