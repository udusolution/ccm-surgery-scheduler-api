/**
 * @description auth custom errors
 */


class AuthError extends Error {
    constructor(message) {
        super(message);
        this.name = "AuthError";
    }
}

class FailedRolesAuthError extends AuthError {
    constructor(message) {
        super(message);
        this.name = "FailedRolesAuthError";
    }
}

class UserNotFoundAuthError extends AuthError {
    constructor(message) {
        super(message);
        this.name = "UserNotFoundAuthError";
    }
}

class InvalidPasswordAuthError extends AuthError {
    constructor(message) {
        super(message);
        this.name = "InvalidPasswordAuthError";
    }
}

class ExpiredAccessTokenAuthError extends AuthError {
    constructor(message) {
        super(message);
        this.name = "ExpiredAccessTokenAuthError";
    }
}

class InvalidTokenAuthError extends AuthError {
    constructor(message) {
        super(message);
        this.name = "InvalidTokenAuthError";
    }
}

class InvalidAccessTokenAuthError extends InvalidTokenAuthError {
    constructor(message) {
        super(message);
        this.name = "InvalidAccessTokenAuthError";
    }
}

class InvalidRefreshTokenAuthError extends InvalidTokenAuthError {
    constructor(message) {
        super(message);
        this.name = "InvalidRefreshTokenAuthError";
    }
}

class MissingAccessTokenAuthError extends AuthError {
    constructor(message) {
        super(message);
        this.name = "MissingAccessTokenAuthError";
    }
}

class ForbiddenAuthError extends AuthError {
    constructor(message) {
        super(message);
        this.name = "ForbiddenAuthError";
    }
}

class AccountLockedAuthError extends AuthError {
    constructor(message) {
        super(message);
        this.name = "AccountLockedAuthError";
    }
}


/**
 * Export
 */
module.exports = {
    AuthError,
    FailedRolesAuthError,
    UserNotFoundAuthError,
    ExpiredAccessTokenAuthError,
    InvalidPasswordAuthError,
    InvalidTokenAuthError,
    InvalidAccessTokenAuthError,
    InvalidRefreshTokenAuthError,
    MissingAccessTokenAuthError,
    ForbiddenAuthError,
    AccountLockedAuthError
}