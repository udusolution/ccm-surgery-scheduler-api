/**
 * @description address lookup errors
 */


class AddressLookupError extends Error {
    constructor(message, originalError = null) {
        super(message);
        this.name = "AddressLookupError";
        this.originalError = originalError;
    }
}

class NotFoundAddressLookupError extends AddressLookupError {
    constructor(message, originalError = null) {
        super(message, originalError);
        this.name = "NotFoundAddressLookupError";
    }
}

class InvalidRequestAddressLookupError extends AddressLookupError {
    constructor(message, originalError = null) {
        super(message, originalError);
        this.name = "InvalidRequestAddressLookupError";
    }
}

class UnauthorizedAddressLookupError extends AddressLookupError {
    constructor(message, originalError = null) {
        super(message, originalError);
        this.name = "UnauthorizedAddressLookupError";
    }
}

class TooManyRequestsAddressLookupError extends AddressLookupError {
    constructor(message, originalError = null) {
        super(message, originalError);
        this.name = "TooManyRequestsAddressLookupError";
    }
}


/**
 * Export
 */
module.exports = {
    AddressLookupError,
    NotFoundAddressLookupError,
    InvalidRequestAddressLookupError,
    UnauthorizedAddressLookupError,
    TooManyRequestsAddressLookupError
}