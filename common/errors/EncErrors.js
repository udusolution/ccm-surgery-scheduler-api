/**
 * @description encryption custom errors
 */


class EncError extends Error {
    constructor(message, originalError = null) {
        super(message);
        this.name = "EncError";
        this.originalError = originalError;
    }
}

class HashEncError extends EncError {
    constructor(message, originalError = null) {
        super(message, originalError);
        this.name = "HashEncError";
    }
}


/**
 * Export
 */
module.exports = {
    EncError,
    HashEncError
}