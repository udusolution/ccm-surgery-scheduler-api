/**
 * @description rate limiter custom errors
 */


class RateError extends Error {
    constructor(message) {
        super(message);
        this.name = "RateError";
    }
}

class RateLimiterSyntaxError extends RateError {
    constructor(message) {
        super(message);
        this.name = "RateLimiterSyntaxError";
    }
}

class RateLimitKeyExtractorMissingError extends RateError {
    constructor(message) {
        super(message);
        this.name = "RateLimitKeyExtractorMissingError";
    }
}

class RateLimitKeyNotFoundError extends RateError {
    constructor(message) {
        super(message);
        this.name = "RateLimitKeyNotFoundError";
    }
}

class InvalidKeyError extends RateError {
    constructor(message) {
        super(message);
        this.name = "InvalidKeyError";
    }
}

class TooManyRequestsError extends RateError {
    constructor(message) {
        super(message);
        this.name = "TooManyRequestsError";
    }
}


/**
 * Export
 */
module.exports = {
    RateError,
    RateLimiterSyntaxError,
    RateLimitKeyExtractorMissingError,
    RateLimitKeyNotFoundError,
    InvalidKeyError,
    TooManyRequestsError
}