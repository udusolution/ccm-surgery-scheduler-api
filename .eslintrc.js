module.exports = {
    env: {
      commonjs: true,
      es6: true,
      node: true
    },
    extends: "eslint:recommended",
    globals: {
    },
    parserOptions: {
      ecmaVersion: 2021
    },
    rules: {
    }
};