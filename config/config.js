/**
 * @description global configurations for the app
 * @note gets most variables from ENV, which is defined in index.js
 */


const config = {
    identity: {
        name: process.env.APP_NAME,
        mailerName: process.env.MAILER_NAME,
    },
    app: {
        port: process.env.PORT,
        websiteBaseUrl: process.env.WEBSITE_BASE_URL
    },
    db: {
        host: process.env.DB_HOST,
        name: process.env.DB_NAME,
        user: process.env.DB_USER,
        pass: process.env.DB_PASS
    },
    jwt: {
        secrets: {
            accessTokenSecret: process.env.ACCESS_TOKEN_SECRET,
            refreshTokenSecret: process.env.REFRESH_TOKEN_SECRET,
        },
        ttl: {
            accessTokenTTL: '5m',
            refreshTokenTTL: '30d',
        }
    },
    pn: {
        contact: process.env.WEB_PUSH_CONTACT,
        vapidPublic: process.env.VAPID_PUBLIC,
        vapidPrivate: process.env.VAPID_PRIVATE
    },
    mail: {
        user: process.env.MAIL_USER,
        pass: process.env.MAIL_PASS
    },
    md: {
        baseURL: process.env.MD_BASE_URL,
        apiKey: process.env.MD_API_KEY,
        username: process.env.MD_USERNAME,
        password: process.env.MD_PASSWORD,
        officeKey: process.env.MD_OFFICE_KEY,
        appName: process.env.MD_APP_NAME,
        sourceHost: process.env.MD_SOURCE_HOST
    },
    cron: {
        intervals: {
            eventReminder: process.env.EVENT_REMINDER_CRON_INTERVAL,
            mdDataSync: process.env.MD_DATA_SYNC_CRON_INTERVAL,
            automatedPatientCom: process.env.AUTOMATED_PATIENT_COM_CRON_INTERVAL,
            scheduledProductivityReports: process.env.AUTOMATED_PRODUCTIVITY_REPORTS_CRON_INTERVAL,
        },
        eventRemindersBeforeXMinutes: process.env.EVENT_REMINDERS_BEFORE_X_MINUTES,
        eventRemindersEveryXMinutes: process.env.EVENT_REMINDERS_EVERY_X_MINUTES
    },
    formats: {
        date: 'MM/DD/YYYY',
        dateTime: 'MM/DD/YYYY hh:mm a',
        dateTimeZ: 'MM/DD/YYYY hh:mm a Z',
    },
    twilio: {
        accountSid: process.env.TWILIO_ACCOUNT_SID,
        authToken: process.env.TWILIO_AUTH_TOKEN,
        messagingServiceSid: process.env.MESSAGING_SERVICE_SID
    },
    placesApi: {
        searchUrl: process.env.PLACES_API_SEARCH_URL,
        detailsUrl: process.env.PLACES_API_DETAILS_URL,
        key: process.env.PLACES_API_KEY
    }
}


/**
 * Exports
 */
module.exports = config;