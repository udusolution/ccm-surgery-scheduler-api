/**
 * @description database connection object for the app
 * @note uses 'mysql2' npm package
 */


const mysql = require('mysql2');
const config = require('../config');

 
// Create the connection pool. The pool-specific settings are the defaults
const pool = mysql.createPool({
    host:                 config.db.host,
    user:                 config.db.user,
    password:             config.db.pass,
    database:             config.db.name,
    waitForConnections:   true,
    connectionLimit:      10,
    queueLimit:           0,
    timezone:             '+00:00'
}).promise();


/**
 * Exports
 */
module.exports = pool;