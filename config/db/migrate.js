const fs = require('fs');
const path = require('path');
const Errors = require('../../common/errors/_CustomErrors');

class Migration {
    #conn = null;

    constructor(conn) {
        this.#conn = conn;
        return this.run()
    }

    async run() {
        try {
            const [result] = await this.#conn.query("SHOW TABLES LIKE 'migrations'");
            if (result.length) {
                return this.#migrateNewFiles()
            } else {
                return this.#migrateAll()
            }
        } catch (err) {
            throw new Errors.DbErrors.MigrationsError('Could not run migrations', err)
        }
    }

    async #migrateAll(conn) {
        const res = fs.readdirSync(__dirname + '/migrations', { withFileTypes: true })
        for (let i = 0; i < res.length; i++) {
            await this.#migrateFile(res[i].name)
        }
    }

    async #migrateNewFiles() {
        const res = fs.readdirSync(__dirname + '/migrations', { withFileTypes: true })

        const availableMigrations = res.map(f => f.name);
        const [result] = await this.#conn.query('SELECT name FROM migrations');
        const migratedFiles = result.map(r => r.name)

        let remainingMigrations = availableMigrations
            .filter(x => !migratedFiles.includes(x))
            .concat(migratedFiles.filter(x => !availableMigrations.includes(x)));

        if (!remainingMigrations.length) {
            console.info('No new migrations found.')
            return;
        }

        for (let i = 0; i < remainingMigrations.length; i++) {
            await this.#migrateFile(remainingMigrations[i])
        }
    }

    async #migrateFile(name) {
        const file = fs.readFileSync(__dirname + '/migrations/' + name);
        await this.#conn.query(file.toString())
        await this.#conn.query('INSERT INTO migrations (name) VALUES (?)', [name])
        console.log(name + ' migrated successfully')
    }
}

module.exports = {
    runMigrations: conn => new Migration(conn)
}