CREATE TABLE migrations (
    id         INT AUTO_INCREMENT,
    name       VARCHAR(225)                       NOT NULL,
    created_at DATETIME default CURRENT_TIMESTAMP NOT NULL,
    CONSTRAINT migrations_pk
        PRIMARY KEY (id)
);

