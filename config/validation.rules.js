/**
 * @description validation rules used in the application
 */


const rules = {
    User: {
        name: {
            minLength: 3,
            maxLength: 30
        }
    },
    Patient: {
        firstName: {
            minLength: 1,
            maxLength: 30
        },
        middleName: {
            minLength: 1,
            maxLength: 30
        },
        lastName: {
            minLength: 1,
            maxLength: 30
        },
        sex: {
            options: ['male', 'female']
        }
    },
    Address: {
        address: {
            minLength: 2,
            maxLength: 100
        },
        address1: {
            minLength: 2,
            maxLength: 100
        },
        address2: {
            minLength: 2,
            maxLength: 100
        },
        city: {
            minLength: 2,
            maxLength: 30
        },
        state: {
            minLength: 2,
            maxLength: 30
        },
        zipCode: {
            minLength: 4,
            maxLength: 14
        },
    },
    Code: {
        minLength: 1,
        maxLength: 30
    },
    Clearance: {
        name: {
            minLength: 5,
            maxLength: 100
        },
        description: {
            minLength: 20,
            maxLength: 500
        },
        physician: {
            minLength: 2,
            maxLength: 20
        },
        rejectionReason: {
            minLength: 10,
            maxLength: 500
        },
    },
    BloodThinner: {
        name: {
            minLength: 2,
            maxLength: 100
        },
        description: {
            minLength: 5,
            maxLength: 500
        },
    },
    Diagnosis: {
        description: {
            minLength: 2,
            maxLength: 500
        },
    },
    EventNote: {
        note: {
            minLength: 5,
            maxLength: 500
        },
    },
    PatientCommunication: {
        title: {
            minLength: 5,
            maxLength: 50
        },
        description: {
            minLength: 20,
            maxLength: 200
        }
    },
    Procedure: {
        name: {
            minLength: 2,
            maxLength: 200
        }
    },
    RejectionReason: {
        minLength: 10,
        maxLength: 300
    },
    Carrier: {
        name: {
            minLength: 1,
            maxLength: 50
        },
        subscriberId: {
            maxLength: 20
        },
        groupName: {
            maxLength: 20
        },
        groupNumber: {
            maxLength: 20
        },
    },
    DayNote: {
        note: {
            minLength: 2,
            maxLength: 300
        }
    }
}


/**
 * Exports
 */
module.exports = rules;