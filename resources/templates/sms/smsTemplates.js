/**
 * @description Sms templates
 * @note Uses template files in same directory
 */


const eventReminder = require('./eventReminder.template');
const patientCom = require('./patientCom.template');
const patientComGeneral = require('./patientComGeneral.template');
const physicianComGeneral = require('./physicianComGeneral.template');


/**
 * Exports
 */
module.exports = {
    eventReminder,
    patientCom,
    patientComGeneral,
    physicianComGeneral
}