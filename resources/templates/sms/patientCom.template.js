/**
 * @description SMS template for patient communications
 */


/**
 * patient communication sms template
 * @param {string} template sms template define in the master data
 * @param {{patient: string, facility: string, department: string, dateFrom: string, coms: [{ title: string, description: string }]}} params template parameters
 * @returns {function} template function
 */
const Template = (template, {patient, facility, department, dateFrom, coms}) => {
    
    let final = template;
    final = final.replace('{PATIENT}', patient);
    final = final.replace('{FACILITY}', facility);
    final = final.replace('{DEPARTMENT}', department);
    final = final.replace('{DATE_FROM}', dateFrom);

    const comsFormatted = coms.map(c => `${c.title}${c.description ? `: ${c.description}` : ''}.`).join('')

    final = final.replace('{COMMUNICATIONS}', comsFormatted);

    return final;

}


/**
 * Exports
 */
module.exports = Template;