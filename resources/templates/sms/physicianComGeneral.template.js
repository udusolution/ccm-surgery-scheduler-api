/**
 * @description SMS template for physician communications
 */


const TemplateVariables = require('../../../logic/templateVariables');


/**
 * physician communication sms template
 */
const Template = (template, eventDetails) => {
    
    const final = TemplateVariables.bake(template, eventDetails);

    return final;

}


/**
 * Exports
 */
module.exports = Template;