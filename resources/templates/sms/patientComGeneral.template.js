/**
 * @description SMS template for patient communications
 */


const TemplateVariables = require('../../../logic/templateVariables');


/**
 * patient communication sms template
 */
const Template = (template, eventDetails) => {
    
    const final = TemplateVariables.bake(template, eventDetails);

    return final;

}


/**
 * Exports
 */
module.exports = Template;