/**
 * @description example PDF generator
 */


const PDFDocument = require('pdfkit');
const fs = require('fs');


function generate() {

    // Movement
    const paddingLeft = 40;
    const logoPadding = 100;
    let y = 100;
    let x = paddingLeft;

    const down = (by=20) => y = y + by;
    const right = (by=50) => x = x + by;
    const newLine = () => { x = paddingLeft; y = y + 50; }

    // Create a document
    const doc = new PDFDocument();

    // Pipe its output somewhere, like to a file or HTTP response
    // See below for browser usage
    doc.pipe(fs.createWriteStream('example.pdf'));

    doc.font('Helvetica');
    doc.initForm();

    right(logoPadding);
    doc.text('Some text!', x, y);
    down();
    doc.formText('firstname', x, y, 50, 10);
    down();
    doc.text('loooooooooooooooooooooooooooooooooooooooooong', x, y);
    right(350);
    doc.formText('lastname', x, y, 50, 10);
    newLine();
    doc.formRadioButton('allergy', x, y, 10, 10, { defaultStatus: true });

    // Finalize PDF file
    doc.end();

    
}
generate();

/**
 * Exports
 */
module.exports = {
    generate
}