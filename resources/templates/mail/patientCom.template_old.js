const Main = require('./main.template');

/**
 * Style (css)
 */
const Style =
`

`

/**
 * patient communications email template
 * @param {{patient, facility, department, dateFrom, dateTo, coms: [{title: string, description?: string}]}} params template parameters
 * @returns {function} template function
 */
const Template = (template, {patient, facility, department, dateFrom, coms}) => Main(Style,
`
<h2>Dear ${patient}</h2>
<p>Your event in ${facility} (${department}) has been scheduled for ${dateFrom}.</p>
<p>Please follow the below instructions:</p>
${coms.map(c => `
<h3>${c.title}</h3>
<p>${c.description || ''}</p>
`).join('')}
`
)


/**
 * Export
 */
module.exports = Template;
