const Main = require('./main.template');

/**
 * Style (css)
 */
const Style =
`
span {

}
p {

}
`

/**
 * example email template
 * @param {{arg1: string, arg2: string}} params template parameters
 * @returns {function} template function
 */
const Template = ({arg1, arg2}) => Main(Style,
`
<span>${arg1}</span>
<p>${arg2}</p>
`
)


/**
 * Export
 */
module.exports = Template;
