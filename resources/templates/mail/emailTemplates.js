/**
 * @description Email templates
 * @note Uses template files in same directory
 */


const eventReminder = require('./eventReminder.template');
const patientCom = require('./patientCom.template');
const patientComGeneral = require('./patientComGeneral.template');
const physicianComGeneral = require('./physicianComGeneral.template');
const facilityBoardingSlip = require('./facilityBSEmail.template');
const forgotPassword = require('./forgotPassword.template');
const productivityReport = require('./productivityReport.template');


/**
 * Exports
 */
module.exports = {
    eventReminder,
    patientCom,
    patientComGeneral,
    physicianComGeneral,
    facilityBoardingSlip,
    forgotPassword,
    productivityReport
}