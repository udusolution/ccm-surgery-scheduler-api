const Main = require('./main.template');

/**
 * Style (css)
 */
const Style =
`

`

/**
 * patient communications email template
 * @param {string} template template structure
 * @param {{patient, facility, department, dateFrom, coms: [{title: string, description?: string}]}} params template parameters
 * @returns {function} template function
 */
const Template = (template, {patient, facility, department, dateFrom, coms}) => {
    
    let final = template;
    final = final.replace('{PATIENT}', patient);
    final = final.replace('{FACILITY}', facility);
    final = final.replace('{DEPARTMENT}', department);
    final = final.replace('{DATE_FROM}', dateFrom);

    const comsFormatted = coms.map(c => `
        <h3>${c.title}</h3>
        <p>${c.description || ''}</p>
    `).join('')

    final = final.replace('{COMMUNICATIONS}', comsFormatted);

    return Main(Style, final);

}


/**
 * Export
 */
module.exports = Template;
