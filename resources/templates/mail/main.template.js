/**
 * @description email template, takes in the style and body
 */


const MainEmailTemplate = (style, body) =>
`
<html>
    <head>
        <style>
            ${style}
        </style>
    </head>
    <body>
        ${body}
    </body>
</html>
`


/**
 * Exports
 */
module.exports = MainEmailTemplate;
