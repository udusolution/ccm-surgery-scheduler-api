const Main = require('./main.template');

/**
 * Style (css)
 */
const Style =
`

`

/**
 * events reminder email template
 * @param {{patient: string, facility: string, department: string, dateFrom: string, dateTo: string}} params template parameters
 * @returns {function} template function
 */
const Template = (template, {patient, facility, department, dateFrom, dateTo}) => {
    
    let final = template;
    final = final.replace('{PATIENT}', patient);
    final = final.replace('{FACILITY}', facility);
    final = final.replace('{DEPARTMENT}', department);
    final = final.replace('{DATE_FROM}', dateFrom);
    final = final.replace('{DATE_TO}', dateTo);

    return Main(Style, final);

}


/**
 * Export
 */
module.exports = Template;
