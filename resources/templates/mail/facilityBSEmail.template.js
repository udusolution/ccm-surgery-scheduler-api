const Main = require('./main.template');
const TemplateVariables = require('../../../logic/templateVariables');


/**
 * Style (css)
 */
const Style =
`

`

/**
 * facility boarding slip email template
 * @param {string} template template structure
 * @param {*} eventDetails template parameters
 * @returns {function} template function
 */
const Template = (template, eventDetails) => {
    
    const final = TemplateVariables.bake(template, eventDetails);

    return Main(Style, final);

}


/**
 * Export
 */
module.exports = Template;
