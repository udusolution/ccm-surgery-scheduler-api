const Main = require('./main.template');
const TemplateVariables = require('../../../logic/templateVariables');


/**
 * Style (css)
 */
const Style =
`

`

/**
 * physician communications email template
 * @param {string} template email body template
 * @param {*} eventDetails event details
 * @returns {function} template function
 */
const Template = (template, eventDetails) => {
    
    const final = TemplateVariables.bake(template, eventDetails);

    return Main(Style, final);

}


/**
 * Export
 */
module.exports = Template;
