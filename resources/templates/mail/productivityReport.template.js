const Main = require('./main.template');
const moment = require('moment-timezone');
const Enums = require('../../../common/enums');

/**
 * Style (css)
 */
const Style =
`
span {

}
p {

}
`

/**
 * example email template
 * @param {{arg1: string, arg2: string}} params template parameters
 * @returns {function} template function
 */
const Template = (stats) => Main(Style,
`
    <p>Productivity report for "${stats?.user?.name || 'N/A'}" ${stats.dateFrom ? `from ${moment(stats.dateFrom).tz(Enums.ZoneFormats.MichiganDetroit).format(Enums.DateFormats.dateTimeZ)}` : ''} ${stats.dateTo ? `till ${moment(stats.dateTo).tz(Enums.ZoneFormats.MichiganDetroit).format(Enums.DateFormats.dateTimeZ)}` : ''}</p>
    <h4>General</h4>
    <table>
        <thead>
            <tr>
                <th>Statistic</th>
                <th>Value</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Events Created</td>
                <td>${stats.eventsCount || 0}</td>
            </tr>
            <tr>
                <td>Events in BSG</td>
                <td>${stats.eventStatusCount[Enums.EventStatuses.BoardingSlipGenerated] || 0}</td>
            </tr>
            <tr>
                <td>Events in WFC</td>
                <td>${stats.eventStatusCount[Enums.EventStatuses.WaitingForConfirmation] || 0}</td>
            </tr>
            <tr>
                <td>Events in Confirmed</td>
                <td>${stats.eventStatusCount[Enums.EventStatuses.Confirmed] || 0}</td>
            </tr>
            <tr>
                <td>Events in Completed</td>
                <td>${stats.eventStatusCount[Enums.EventStatuses.Completed] || 0}</td>
            </tr>
            <tr>
                <td>Events in Cancelled</td>
                <td>${stats.eventStatusCount[Enums.EventStatuses.Cancelled] || 0}</td>
            </tr>
        </tbody>
    </table>

    ${stats.departmentEventsCount?.length > 0 ? stats.departmentEventsCount.map(d => (
        `
            <h4>${d.eventTypeName}</h4>
            <table>
                <thead>
                    <tr>
                        <th>Statistic</th>
                        <th>Value</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Events Created</td>
                        <td>${d.eventsCount || 0}</td>
                    </tr>
                    <tr>
                        <td>Events in BSG</td>
                        <td>${d[Enums.EventStatuses.BoardingSlipGenerated] || 0}</td>
                    </tr>
                    <tr>
                        <td>Events in WFC</td>
                        <td>${d[Enums.EventStatuses.WaitingForConfirmation] || 0}</td>
                    </tr>
                    <tr>
                        <td>Events in Confirmed</td>
                        <td>${d[Enums.EventStatuses.Confirmed] || 0}</td>
                    </tr>
                    <tr>
                        <td>Events in Completed</td>
                        <td>${d[Enums.EventStatuses.Completed] || 0}</td>
                    </tr>
                    <tr>
                        <td>Events in Cancelled</td>
                        <td>${d[Enums.EventStatuses.Cancelled] || 0}</td>
                    </tr>
                </tbody>
            </table>
        `
    )).join('') : ''}
`
)


/**
 * Export
 */
module.exports = Template;
