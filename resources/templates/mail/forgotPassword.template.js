const Main = require('./main.template');
const config = require('../../../config/config');

/**
 * Style (css)
 */
const Style =
`
.reset-btn {
    padding: 10px 20px;
    border-radius: 8px;
    background-color: #0075ff;
    color: #f1f1f1;
    font-weight: bold;
    font-size: 18px;
    text-decoration: none;
    outline: none;
}
`

/**
 * forgot password email template
 * @param {{forgotPasswordUrl: string}} params template parameters
 * @returns {function} template function
 */
const Template = ({forgotPasswordUrl}) => Main(Style,
`
<p>You recently requested to reset your password for your ${config.identity.name} account.</p>
<p>You can reset your password by clicking the link below:</p>
<a class="reset-btn" href="${forgotPasswordUrl}">Reset your password</a>
<p>If you did not request a new password, then you can ignore this email.</p>
`
)


/**
 * Export
 */
module.exports = Template;
