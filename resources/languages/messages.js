/**
 * @description messages used throughout the application
 * @note uses environment variable to determine whether to load a language other than english
 * @note languages available are placed in the folders in this directory (eg "en" for english, "ar" for arabic, etc)
 */


const failureMessagesEN = require('./en/failure.messages');
const responseMessagesEN = require('./en/response.messages');
const historyMessagesEN = require('./en/history.messages');
const pnMessagesEN = require('./en/pn.messages');


const messages = {
    failure: failureMessagesEN,
    response: responseMessagesEN,
    history: historyMessagesEN,
    pn: pnMessagesEN
}


/**
 * Exports
 */
module.exports = messages;