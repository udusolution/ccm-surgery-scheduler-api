/**
 * @description english failure messages
 */


const messages = {
    generic:            () => 'Something went wrong, please try again later',
    unauthorized:       () => 'Unauthorized',
    forbidden:          () => 'You are forbidden from performing this action',
    emailTaken:         () => `Email taken, please use another one`,
    phoneTaken:         () => `Phone number taken, please use another one`,
    tooManyRequests:    () => `You are sending too many requests`,
    reservedTime:       () => `The time you chose is reserved. Please choose another one`,
    eventNotFound:      () => `Event not found`,
    eventCoordinatorNotFound:      () => `Event coordinator not found`,
    patientNotFound:      () => `Patient not found`,
    dayNoteNotFound:      () => `Note not found`,
    userNotFound:      () => `User not found`,
    userAlreadyDeleted:      () => `This user has already been deleted`,
    userWithEmailNotFound:      () => `User with this email not found`,
    eventClearanceNotFound:      () => `Event clearance not found`,
    eventProcedureNotFound:      () => `Event procedure not found`,
    eventBloodThinnerNotFound:      () => `Event blood thinner not found`,
    eventPatientCommunicationNotFound:      () => `Event patient communication not found`,
    eventStillPendingCannotCancel: () => `Event is still in a pending status, cannot cancel`,
    eventNotPending:    () => `This event is not in a pending status`,
    eventNotBSG:        () => `Boarding slip not generated for this event`,
    eventNotBSGorRejected: () => `Event must be in BSG or Rejected status`,
    eventNotWFC:        () => `Event is not waiting for confirmation`,
    eventNotConfirmed:        () => `Event is not confirmed yet`,
    eventNotConfirmedOrCompleted:        () => `Event is not confirmed or completed yet`,
    physicianFacilityAssociationAlreadyExists: () => `Association between this physician and facility already exists`,
    eventNotPendingCannotDelete: () => `This event is in an active status, cannot delete`,
    patientCarrierNotFound: () => `Patient carrier not found in master files`,
    failedToUploadFile: () => `Failed to upload file`,
    boardingSlipFileRequired: () => `Boarding Slip PDF file required`,
    facilityHasNoSchedulingEmail: () => `The event's facility does not have a scheduling email`,
    facilityBSEmailTemplateNotFound: () => `Boarding slip email template not found`,
    eventAuthNoteNotFound: () => `Authorization note not found`,
    eventAuthNoteNotYours: () => `This authorization note is not yours`,
    incorrectPassword: () => `Incorrect password`,
    masterFilesEntryNotFound: () => `Master files entry not found`,
    userWithPasswordResetTokenNotFound: () => `Reset request invalid. Make sure you used the latest reset link if you requested multiple times.`,
    eventPatientPhoneNotSet: () => `Patient phone number is not set in the event.`,
    eventPatientEmailNotSet: () => `Patient email is not set in the event.`,
    invalidPhoneNumber: () => `Phone number appears to be invalid.`,
    smsFailure: () => `Failed to send SMS, please try again later.`,
    emailTemplateNotFound: () => `Email template not found.`,
    smsTemplateNotFound: () => `Sms template not found.`,
    eventScheduledEmailNotFound: () => `Event scheduled email not found.`,
    eventScheduledSmsNotFound: () => `Event scheduled sms not found.`,
    boardingSlipEmailTemplateNotSet: () => `The boarding slip email template is not set in the Master Files`,
    eventEmailTemplateAlreadyActive: () => `Selected email template is already active for this event`,
    eventSmsTemplateAlreadyActive: () => `Selected SMS template is already active for this event`,
    eventEditRequestNotFound: () => `Event edit request not found`,
    eventEditRequestNotYours: () => `You are not the coordinator of this event`,
    eventEditRequestAlreadyApproved: () => `This event edit request has already been approved`,
    eventEditRequestAlreadyRejected: () => `This event edit request has already been rejected`,
    userAlreadyLocked: () => `This user's account is already locked`,
    userAlreadyUnlocked: () => `This user's account is already unlocked`,
    accountLocked: () => 'Your account access has been locked, please contact the administration.',
    boardingSlipPathNotSet: () => `The boarding slip is not set`,
}


/**
 * Exports
 */
module.exports = messages;