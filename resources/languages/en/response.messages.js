/**
 * @description english failure messages
 */


const messages = {
    success: () => `Success`,
    userRegistered: () => `Successfully registered`,
    loggedIn: () => `Successfully logged in`,
}


/**
 * Exports
 */
module.exports = messages;