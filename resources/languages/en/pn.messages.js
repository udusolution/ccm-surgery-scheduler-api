/**
 * @description english pn messages
 */


const messages = {
    diagnosisAdded: {
        title: () => `Diagnosis`,
        message: () => `A new diagnosis has been added to the event`,
    },
    updatedEvent: {
        title: () => `Event was Updated`,
        message: () => `Event was updated`
    },
    deletedEvent: {
        title: () => `Event was Deleted`,
        message: () => `Event was deleted`
    },
    addedEvent: {
        title: () => `New Event`,
        message: () => `New event added`
    },
    sentEditRequest: {
        title: () => `Event Edit Request`,
        message: () => `A user requested permission to edit one of your events`
    },
    acceptedEditRequest: {
        title: () => `Event Edit Request`,
        message: () => `You gained permission to edit an event`
    },
    rejectedEditRequest: {
        title: () => `Event Edit Request`,
        message: () => `You were rejected permission to edit an event`
    },
    updatedUser: {
        title: () => `Updated User`,
        message: () => `Your account was updated`
    },
    userLocked: {
        title: () => `Your Account was Locked`,
        message: () => `Your account has been locked. If you think this is a mistake, please contact the administration.`
    },
}


/**
 * Exports
 */
module.exports = messages;