const express = require('express');
const app = express();
const cors = require('cors');
require('dotenv').config();
const config = require('./config/config');
const GlobalErrorHandler = require('./globalErrorHandler');
const Logger = require('./common/loggers/route.logger');
const path = require('path');
const con = require('./config/db/db');
const runMigrations = require('./config/db/migrate').runMigrations


// <> App ROOT
global.appRoot = path.resolve(__dirname);


// <> Body Parser
app.use(express.json());
app.use(express.urlencoded({ extended: false }));


// <> CORS
app.use(cors({
    origin: '*'
}));


// <> Static Route
app.use(express.static('public'));


// <> Routes
// Auth
app.use('/api/auth', Logger.inject, require('./routes/auth/auth'));

// Patients
app.use('/api/patients', Logger.inject, require('./routes/patients/patients'));
app.use('/api/patients/allergies', Logger.inject, require('./routes/patients/allergies/patients.allergies'));
app.use('/api/patients/data', Logger.inject, require('./routes/patients/patientData/patientData'));

// Master Data
app.use('/api/masterData', Logger.inject, require('./routes/masterData/masterData'));
app.use('/api/diagnosis', Logger.inject, require('./routes/diagnosis/diagnosis'));
app.use('/api/priorities', Logger.inject, require('./routes/priority/priority'));
app.use('/api/scheduledComs', Logger.inject, require('./routes/scheduledComs/scheduledComs'));

// Manage Users
app.use('/api/manageUsers', Logger.inject, require('./routes/manageUsers/manageUsers'));
app.use('/api/physicianFacilities', Logger.inject, require('./routes/manageUsers/physicianFacilities/physicianFacilities'));

// History
app.use('/api/history', Logger.inject, require('./routes/history/history'));

// Operations
app.use('/api/operations', Logger.inject, require('./routes/operations/operations'));

// Events
app.use('/api/events', Logger.inject, require('./routes/events/events'));
app.use('/api/events/procedures', Logger.inject, require('./routes/events/procedures/events.procedures'));
app.use('/api/events/diagnosis', Logger.inject, require('./routes/events/diagnosis/events.diagnosis'));
app.use('/api/events/bloodThinners', Logger.inject, require('./routes/events/bloodThinners/events.bloodThinners'));
app.use('/api/events/notes', Logger.inject, require('./routes/events/notes/events.notes'));
app.use('/api/events/checklist', Logger.inject, require('./routes/events/checklist/events.checklist'));
app.use('/api/events/clearances', Logger.inject, require('./routes/events/clearances/events.clearances'));
app.use('/api/events/authNotes', Logger.inject, require('./routes/events/authNotes/events.authNotes'));
app.use('/api/events/scheduledComs', Logger.inject, require('./routes/events/scheduledComs/scheduledComs'));
app.use('/api/events/scheduledComsSms', Logger.inject, require('./routes/events/scheduledComs/scheduledComsSms'));

// Address Lookup
app.use('/api/address-lookup', Logger.inject, require('./routes/address-lookup/address-lookup'));

// Statistics
app.use('/api/statistics', Logger.inject, require('./routes/statistics/statistics'));

// Templates
app.use('/api/templates/email', Logger.inject, require('./routes/templates/email/email'));
app.use('/api/templates/sms', Logger.inject, require('./routes/templates/sms/sms'));

// Day Notes
app.use('/api/dayNotes', Logger.inject, require('./routes/dayNotes/dayNotes'));

// Scheduled Productivity
app.use('/api/productivityReports', Logger.inject, require('./routes/productivityReports/productivityReports'));

// Event Edit Requests
app.use('/api/eventEditRequests', Logger.inject, require('./routes/eventEditRequests/eventEditRequests'));
app.use('/api/eventEditSettings', Logger.inject, require('./routes/eventEditSettings/eventEditSettings'));

// PN
app.use('/api/notifications', Logger.inject, require('./routes/notifications/notifications'));


// <> Global Error Handler
app.use(GlobalErrorHandler);


// <> Cron Jobs
require('./cronjobs');


// <> Listen
const PORT = config.app.port;
app.listen(PORT, console.log(`Server started on port ${PORT}`));

// <> Check DB Connection
console.log(`Verifying database connection...`);
con.query('SELECT 1 + 1 AS solution')
    .then(() => {
        console.log(`Database connection working`)
        return runMigrations(con)
    })
    .then(() => console.log("Migration completed"))
    .catch(dbErr => {
        console.error(`Database error: ${JSON.stringify(dbErr)}`)
    });