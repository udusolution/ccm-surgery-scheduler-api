/**
 * @description all the cronjobs used for this app
 * @note uses the "cron" npm package
 */


const cron = require('cron');
const moment = require('moment');
const config = require('./config/config');
const Logger = require('./common/loggers/route.logger');
const SyncWithMd = require('./operations/syncWithMd');
const ScheduledComRuling = require('./operations/scheduledComsRuling');
const Reports = require('./operations/reports');


async function eventPhysicianReminders() {

    console.log(`eventPhysicianReminders ${moment().format('YYYY-MM-DD hh:mm:ss')} ### start`);

    const fnName = "eventPhysicianReminders";
    const log = Logger.get('/cron/eventPhysicianReminders');
    const reqId = 'N/A';

    try {

        log.info(reqId, `${fnName} ### start`);

        log.info(reqId, `${fnName} ### calling (ScheduledComRuling)'sendPendingEventEmailPhysicianReminders'`);
        // -> Send Pending Event Physician Email Reminders
        await ScheduledComRuling.sendPendingEventEmailPhysicianReminders(log, reqId);

        log.info(reqId, `${fnName} ### calling (ScheduledComRuling)'sendPendingEventSmsPhysicianReminders'`);
        // -> Send Pending Event Physician Sms Reminders
        await ScheduledComRuling.sendPendingEventSmsPhysicianReminders(log, reqId);
        
    }
    catch(e) {
        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
    }
}

// --------------------------- AdvancedMD Patient Sync -------------------------------- //
// ------------------------------------------------------------------------------------ //

async function syncMdData() {

    console.log(`cronjob/syncAdvancedMdDatabase ${moment().format('YYYY-MM-DD hh:mm:ss')} ### start`);

    const fnName = "cronjob/syncAdvancedMdDatabase";
    const log = Logger.get('/cron/syncAdvancedMdDatabase');
    const reqId = 'N/A';

    try {

        log.info(reqId, `${fnName} ### calling (SyncWithMd)'syncMdData'`);
        // -> Sync Md Data
        await SyncWithMd.syncMdData(log, reqId);

            
    } catch(e) {
        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
    }
}

// --------------------------- /AdvancedMD Patient Sync/ -------------------------------- //
// -------------------------------------------------------------------------------------- //

async function scheduledEventPatientComs() {

    console.log(`cronjob/scheduledEventPatientComs ${moment().format('YYYY-MM-DD hh:mm:ss')} ### start`);

    const fnName = "cronjob/scheduledEventPatientComs";
    const log = Logger.get('/cron/scheduledEventPatientComs');
    const reqId = 'N/A';

    try {

        log.info(reqId, `${fnName} ### calling (ScheduledComRuling)'sendPendingEventEmailCommunications'`);
        // -> Send Pending Event Patient Email Communications
        await ScheduledComRuling.sendPendingEventEmailCommunications(log, reqId);

        log.info(reqId, `${fnName} ### calling (ScheduledComRuling)'sendPendingEventSmsCommunications'`);
        // -> Send Pending Event Patient Sms Communications
        await ScheduledComRuling.sendPendingEventSmsCommunications(log, reqId);

    }
    catch(e) {
        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
    }
}

async function scheduledProductivityReports() {

    console.log(`cronjob/scheduledProductivityReports ${moment().format('YYYY-MM-DD hh:mm:ss')} ### start`);

    const fnName = "cronjob/scheduledProductivityReports";
    const log = Logger.get('/cron/scheduledProductivityReports');
    const reqId = 'N/A';

    try {

        log.info(reqId, `${fnName} ### calling (Reports)'sendPendingProductivityReports'`);
        // -> Send Pending Productivity Reports
        await Reports.sendPendingProductivityReports(log, reqId);

    }
    catch(e) {
        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
    }
}


/**
 * START THE JOBS
 */
if(process.env.ENABLE_EVENT_REMINDERS == 'TRUE') {
    new cron.CronJob({
        cronTime: config.cron.intervals.eventReminder,
        onTick: eventPhysicianReminders,
        start: true
    })
}

if(process.env.SYNC_MD_DATA == 'TRUE') {
    new cron.CronJob({
        cronTime: config.cron.intervals.mdDataSync,
        onTick: syncMdData,
        start: true
    })
}

if(process.env.SEND_AUTOMATED_PATIENT_COM == 'TRUE') {
    new cron.CronJob({
        cronTime: config.cron.intervals.automatedPatientCom,
        onTick: scheduledEventPatientComs,
        start: true
    })
}

if(process.env.SEND_AUTOMATED_PRODUCTIVITY_REPORTS == 'TRUE') {
    new cron.CronJob({
        cronTime: config.cron.intervals.scheduledProductivityReports,
        onTick: scheduledProductivityReports,
        start: true
    })
}