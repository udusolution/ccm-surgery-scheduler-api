/**
 * @description helpers for scheduled communication ruling logic
 */


const Matcher = require('../common/utils/patternMatcher');


/**
 * get the time difference in minutes from time variables
 * @param {number} timeValue 
 * @param {('hours'|'days')} timeType 
 * @param {('before'|'after')} timeMode 
 * @returns 
 */
function timeDifferenceToSeconds(timeValue, timeType, timeMode) {

    if(typeof timeValue !== 'number') throw Error('timeValue must be of type number');

    let final = 0;

    // -> Convert time value to seconds
    if(timeType == 'hours') final = timeValue * 60;
    else if(timeType == 'days') final = timeValue * 24 * 60;

    // -> If mode is "before" then convert time value to negative
    if(timeMode == 'before') final = -final;

    // End
    return Math.floor(final);
}


/**
 * get the time variables from minutes
 * @param {number} minutes 
 * @returns {{ timeValue: number, timeType: ('hours'|'days'), timeMode: ('before'|'after') }}
 */
function minutesToTimeDifference(minutes) {

    if(typeof minutes !== 'number') throw Error('minutes must be of type number');

    let hours = Math.round(Math.abs(minutes) / 60);
    let timeType = (hours > 23 ? 'days' : 'hours');
    let timeValue = (timeType == 'hours' ? hours : Math.round(hours / 24));
    let timeMode = (minutes < 0 ? 'before' : 'after');

    // End
    return {
        timeValue,
        timeType,
        timeMode
    };
}

/**
 * get the ruling formula from ruling variables
 * @param {number[]} procedureIds 
 * @param {('and'|'or')} procedureRulingType
 * @param {number[]} bloodThinnerIds 
 * @param {('and'|'or')} bloodThinnerRulingType
 * @returns 
 */
function rulingVariablesToFormula(procedureIds, procedureRulingType, bloodThinnerIds, bloodThinnerRulingType) {

    let final = `P{${procedureIds?.length > 0 ? procedureIds.join(procedureRulingType == 'and' ? '&' : '|') : ''}}:B{${bloodThinnerIds?.length > 0 ? bloodThinnerIds.join(bloodThinnerRulingType == 'and' ? '&' : '|') : ''}}`;
    return final == 'P{}:B{}' ? null : final;

}

/**
 * convert a scheduled communication ruling formula into its equivalent variables object
 * @param {string} formula scheduled communication ruling formula to convert
 * @returns {{ 
 *  procedureIds: number[],
 *  procedureRulingType: ('and'|'or'),
 *  bloodThinnerIds: number[],
 *  bloodThinnerRulingType: ('and'|'or'),
 *  facilityId?: number,
 *  eventTypeId?: number,
 *  diagnosisIds: number[],
 *  diagnosisRulingType: ('and'|'or')
 * }}
 */
function formulaToRulingVariables(formula) {

    if(!formula) return '';
    if(typeof formula !== 'string' || !Matcher.isScheduledCommunicationFormula(formula)) throw Error('Invalid ruling format');

    // -> Parse
    const pieces = formula.split(':');

    const proceduresRulePiece = pieces[0];
    const bloodThinnersRulePiece = pieces[1];
    const facilityIdRulePiece = pieces[2];
    const eventTypeIdRulePiece = pieces[3];
    const diagnosisRulePiece = pieces[4];

    const proceduresRule = proceduresRulePiece?.replace('P{', '')?.replace('}', '') ?? '';
    const bloodThinnersRule = bloodThinnersRulePiece?.replace('B{', '')?.replace('}', '') ?? '';
    const facilityIdRule = facilityIdRulePiece?.replace('F{', '')?.replace('}', '') ?? '';
    const eventTypeIdRule = eventTypeIdRulePiece?.replace('E{', '')?.replace('}', '') ?? '';
    const diagnosisRule = diagnosisRulePiece?.replace('D{', '')?.replace('}', '') ?? '';

    let procedureIds = [];
    let bloodThinnerIds = [];
    let procedureRulingType = '';
    let bloodThinnerRulingType = '';
    let facilityId = null;
    let eventTypeId = null;
    let diagnosisIds = [];
    let diagnosisRulingType = '';

    // Procedures
    if(proceduresRule.includes('&')) {
        procedureRulingType = 'and';
        procedureIds = proceduresRule.split('&').map(p => parseInt(p));
    }
    else if(proceduresRule.includes('|')) {
        procedureRulingType = 'or';
        procedureIds = proceduresRule.split('|').map(p => parseInt(p));
    }
    else if(proceduresRule.length > 0) {
        procedureIds = [parseInt(proceduresRule)];
    }

    // Blood Thinners
    if(bloodThinnersRule.includes('&')) {
        bloodThinnerRulingType = 'and';
        bloodThinnerIds = bloodThinnersRule.split('&').map(p => parseInt(p));
    }
    else if(bloodThinnersRule.includes('|')) {
        bloodThinnerRulingType = 'or';
        bloodThinnerIds = bloodThinnersRule.split('|').map(p => parseInt(p));
    }
    else if(bloodThinnersRule.length > 0) {
        bloodThinnerIds = [parseInt(bloodThinnersRule)];
    }

    // Diagnosis
    if(diagnosisRule.includes('&')) {
        diagnosisRulingType = 'and';
        diagnosisIds = diagnosisRule.split('&').map(p => parseInt(p));
    }
    else if(diagnosisRule.includes('|')) {
        diagnosisRulingType = 'or';
        diagnosisIds = diagnosisRule.split('|').map(p => parseInt(p));
    }
    else if(diagnosisRule.length > 0) {
        diagnosisIds = [parseInt(diagnosisRule)];
    }

    // Facility
    try {
        facilityId = facilityIdRule && facilityIdRule != 0 ? parseInt(facilityIdRule) : null;
    } catch(e) { /* Swallow */ }

    // Event Type
    try {
        eventTypeId = eventTypeIdRule && eventTypeIdRule != 0 ? parseInt(eventTypeIdRule) : null;
    } catch(e) { /* Swallow */ }

    // End
    return {
        procedureIds,
        procedureRulingType,
        bloodThinnerIds,
        bloodThinnerRulingType,
        facilityId,
        eventTypeId,
        diagnosisIds,
        diagnosisRulingType
    }

}


/**
 * Exports
 */
module.exports = {
    timeDifferenceToSeconds,
    minutesToTimeDifference,
    rulingVariablesToFormula,
    formulaToRulingVariables
}