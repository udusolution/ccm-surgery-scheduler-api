/**
 * @description module to replace email/sms template event variables with the actual event data
 */


const momentTZ = require('moment-timezone');
const Enums = require('../common/enums');


/**
 * replace event template variables with the actual event variables
 * @param {string} template template
 * @param {*} eventDetails event details
 * @returns {string} template with the event data injected into it
 */
function bake(template, eventDetails) {

    let final = template;

    // -> Replace
    final = final.replace('{PATIENT_FN}', eventDetails.patientFirstName || '');
    final = final.replace('{PATIENT_MN}', eventDetails.patientMiddleName || '');
    final = final.replace('{PATIENT_LN}', eventDetails.patientLastName || '');
    final = final.replace('{FACILITY}', eventDetails.facilityName || '');
    final = final.replace('{DEPARTMENT}', eventDetails.eventTypeName || '');
    final = final.replace('{PHYSICIAN}', eventDetails.physicianName || '');
    final = final.replace('{COORDINATOR}', eventDetails.coordinatorName || '');
    final = final.replace('{DATE_FROM}', momentTZ(eventDetails.dateFrom).tz(Enums.ZoneFormats.MichiganDetroit).format(Enums.DateFormats.dateTimeZ) || '');

    // -> Clear duplicate spaces
    final = final.replace(/ +/, ' ');

    return final;
}


/**
 * Exports
 */
module.exports = {
    bake
}