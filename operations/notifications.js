const PN = require('../common/services/pn');
const UserRepo = require('../repos/user.repo');


async function sendPnToAll(log, reqId, title, message, data, { excludeUserId }) {

    const fnName = "operations/notifications/sendPnToAll";
    log.info(reqId, `${fnName} ### start`);

    try {

        log.info(reqId, `${fnName} ### calling (UserRepo)'getAll'`);
        // -> Get all Users
        const allUsers = await UserRepo.getAll(log, reqId);

        // Filter ones with PN Sub
        const registeredUsers = allUsers.filter(u => u.pnSub);

        log.info(reqId, `${fnName} ### calling (PN)'send' for each user that has a registration`);
        // Send the PN to them
        registeredUsers.forEach(u => {

            // Don't send to excluded user if specified
            if(excludeUserId && u.id == excludeUserId) return;

            const pnSubJSON = JSON.parse(u.pnSub);
            PN.send(log, reqId, pnSubJSON, title, message, data)
                .then(() => {})
                .catch(() => {
                    // Swallow Error
                })
        })

        // End
        log.info(reqId, `${fnName} ### end`);

    }
    catch(e) {
        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`)
        // Swallow
    }
}

async function sendPn(log, reqId, userId, title, message, data) {

    const fnName = "operations/notifications/sendPn";
    log.info(reqId, `${fnName} ### start`);

    try {

        log.info(reqId, `${fnName} ### calling (UserRepo)'findById'`);
        // -> Get User
        const user = await UserRepo.findById(log, reqId, userId);

        // Return if no user found or has no PN subscription
        if(!user) {
            log.info(reqId, `${fnName} ### end [user not found]`);
            return;
        }
        else if(!user.pnSub) {
            log.info(reqId, `${fnName} ### end [user not subscribed to push notifications]`);
            return;
        }

        log.info(reqId, `${fnName} ### calling (PN)'send'`);
        // -> Send PN
        const pnSubJSON = JSON.parse(user.pnSub);
        PN.send(log, reqId, pnSubJSON, title, message, data)
            .then(() => {})
            .catch(() => {
                // Swallow Error
            })

        // End
        log.info(reqId, `${fnName} ### end`);

    }
    catch(e) {
        log.warn(reqId, `${fnName} ### end [failure] ### message: "${e.message}", error: ${JSON.stringify(e)}`);
        // Swallow
    }
}


/**
 * Exports
 */
module.exports = {
    sendPnToAll,
    sendPn
}