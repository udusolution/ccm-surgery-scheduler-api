/**
 * @description operations for implementing scheduled report mailing
 */


const Enums = require('../common/enums');
const Errors = require('../common/errors/_CustomErrors');
const StatisticsRepo = require('../repos/statistics.repo');
const UserRepo = require('../repos/user.repo');
const con = require('../config/db/db');
const moment = require('moment');
const EmailTemplates = require('../resources/templates/mail/emailTemplates');
const config = require('../config/config');
const mail = require('../common/services/mail');


/**
 * evaluate a coordinator or a physician's productivity statistics, mostly the number of event status updates
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {(string|Date|moment.Moment)?} dateFrom date to start checking for event updates
 * @param {(string|Date|moment.Moment)?} dateTo date to end checking for event updates
 * @param {number} coordinatorId coordinator's ID
 * @param {number} physicianId physician's ID
 * @param {number?} facilityId facility's ID to filter
 * @returns {Promise<{user: any,
 *       eventsCount: number,
 *       eventStatusCount: {
 *           pending: number,
 *           boarding_slip_generated: number,
 *           waiting_for_confirmation: number,
 *           confirmed: number,
 *           completed: number,
 *           cancelled: number
 *       },
 *       boardingSlipsGeneratedCount: number,
 *       departmentEventsCount: [{
 *           pending: number,
 *           boarding_slip_generated: number,
 *           waiting_for_confirmation: number,
 *           confirmed: number,
 *           completed: number,
 *           cancelled: number,
 *           eventsCount: number
 *       }]
 *   }>} productivity report
 * @note must provide only a coordinatorId or physicianId at a time, not both
 * @note events are counted based on when their status was last updated, and not when they were created or when they are scheduled to
 */
async function getProductivityReport(log, reqId, { dateFrom = null, dateTo = null, coordinatorId = null, physicianId = null, facilityId = null }) {

    const fnName = 'operations/reports/getProductivityReport';
    log.info(reqId, `${fnName} ### start`);

    const filters = {
        fromDate: dateFrom,
        toDate: dateTo,
    }
    if(coordinatorId) filters.coordinatorId = coordinatorId;
    if(physicianId) filters.physicianId = physicianId;
    const userId = coordinatorId || physicianId;

    log.info(reqId, `${fnName} ### filters: ${JSON.stringify(filters)}`);

    // Event Status SQL
    const eventStatusesSql = `
        SELECT COUNT(*) AS count, status AS eventStatus
        FROM event
        WHERE 1 = 1
        ${physicianId ? `AND physicianId = ?` : ''}
        ${coordinatorId ? `AND coordinatorId = ?` : ''}
        ${facilityId ? `AND facilityId = ?` : ''}
        ${dateFrom ? `AND statusUpdatedOn > ?` : ''}
        ${dateTo ? `AND statusUpdatedOn < ?` : ''}
        GROUP BY status
    `
    const eventStatusesParams = [];
    if(physicianId) eventStatusesParams.push(physicianId);
    if(coordinatorId) eventStatusesParams.push(coordinatorId);
    if(facilityId) eventStatusesParams.push(facilityId);
    if(dateFrom) eventStatusesParams.push(moment(dateFrom).toDate());
    if(dateTo) eventStatusesParams.push(moment(dateTo).toDate());

    log.info(reqId, `${fnName} ### [query] get event statuses`);
    // -> Get Event Statuses
    const [eventStatuses] = await con.query(eventStatusesSql, eventStatusesParams);

    // Format response
    const eventStatusCount = {};
    eventStatusCount[Enums.EventStatuses.Pending]                  = eventStatuses?.find(r => r.eventStatus == Enums.EventStatuses.Pending)?.count ?? 0;
    eventStatusCount[Enums.EventStatuses.BoardingSlipGenerated]    = eventStatuses?.find(r => r.eventStatus == Enums.EventStatuses.BoardingSlipGenerated)?.count ?? 0;
    eventStatusCount[Enums.EventStatuses.WaitingForConfirmation]   = eventStatuses?.find(r => r.eventStatus == Enums.EventStatuses.WaitingForConfirmation)?.count ?? 0;
    eventStatusCount[Enums.EventStatuses.Confirmed]                = eventStatuses?.find(r => r.eventStatus == Enums.EventStatuses.Confirmed)?.count ?? 0;
    eventStatusCount[Enums.EventStatuses.Completed]                = eventStatuses?.find(r => r.eventStatus == Enums.EventStatuses.Completed)?.count ?? 0;
    eventStatusCount[Enums.EventStatuses.Cancelled]                = eventStatuses?.find(r => r.eventStatus == Enums.EventStatuses.Cancelled)?.count ?? 0;
    const eventsCount = eventStatusCount[Enums.EventStatuses.Pending]
        + eventStatusCount[Enums.EventStatuses.BoardingSlipGenerated]
        + eventStatusCount[Enums.EventStatuses.WaitingForConfirmation]
        + eventStatusCount[Enums.EventStatuses.Confirmed]
        + eventStatusCount[Enums.EventStatuses.Completed]
        + eventStatusCount[Enums.EventStatuses.Cancelled];

    // Event Department Status SQL
    const eventDepartmentStatusesSql = `
        SELECT COUNT(*) AS count, e.status AS eventStatus, dep.id AS eventTypeId, dep.name AS departmentName
        FROM event AS e
        LEFT JOIN event_type AS dep ON e.eventTypeId = dep.id
        WHERE 1 = 1
        ${physicianId ? `AND e.physicianId = ?` : ''}
        ${coordinatorId ? `AND e.coordinatorId = ?` : ''}
        ${facilityId ? `AND e.facilityId = ?` : ''}
        ${dateFrom ? `AND e.statusUpdatedOn > ?` : ''}
        ${dateTo ? `AND e.statusUpdatedOn < ?` : ''}
        GROUP BY eventTypeId, eventStatus
    `
    const eventDepartmentStatusesParams = [];
    if(physicianId) eventDepartmentStatusesParams.push(physicianId);
    if(coordinatorId) eventDepartmentStatusesParams.push(coordinatorId);
    if(facilityId) eventDepartmentStatusesParams.push(facilityId);
    if(dateFrom) eventDepartmentStatusesParams.push(moment(dateFrom).toDate());
    if(dateTo) eventDepartmentStatusesParams.push(moment(dateTo).toDate());

    log.info(reqId, `${fnName} ### [query] get event department statuses`);
    // -> Get Event Department Statuses
    const [eventDepartmentStatuses] = await con.query(eventDepartmentStatusesSql, eventDepartmentStatusesParams);

    // Format response
    const departmentEventsCount = [];
    for(let i = 0; i < eventDepartmentStatuses.length; i++) {
        const row = eventDepartmentStatuses[i];

        if(departmentEventsCount.some(f => f.eventTypeId == row.eventTypeId)) continue;

        const departmentStats = {
            eventTypeId: row.eventTypeId,
            eventTypeName: row.departmentName,
        }
        departmentStats[Enums.EventStatuses.Pending] = eventDepartmentStatuses.find(r => r.eventTypeId == row.eventTypeId && r.eventStatus == Enums.EventStatuses.Pending)?.count || 0;
        departmentStats[Enums.EventStatuses.BoardingSlipGenerated] = eventDepartmentStatuses.find(r => r.eventTypeId == row.eventTypeId && r.eventStatus == Enums.EventStatuses.BoardingSlipGenerated)?.count || 0;
        departmentStats[Enums.EventStatuses.WaitingForConfirmation] = eventDepartmentStatuses.find(r => r.eventTypeId == row.eventTypeId && r.eventStatus == Enums.EventStatuses.WaitingForConfirmation)?.count || 0;
        departmentStats[Enums.EventStatuses.Confirmed] = eventDepartmentStatuses.find(r => r.eventTypeId == row.eventTypeId && r.eventStatus == Enums.EventStatuses.Confirmed)?.count || 0;
        departmentStats[Enums.EventStatuses.Completed] = eventDepartmentStatuses.find(r => r.eventTypeId == row.eventTypeId && r.eventStatus == Enums.EventStatuses.Completed)?.count || 0;
        departmentStats[Enums.EventStatuses.Cancelled] = eventDepartmentStatuses.find(r => r.eventTypeId == row.eventTypeId && r.eventStatus == Enums.EventStatuses.Cancelled)?.count || 0;
        departmentStats.eventsCount = departmentStats[Enums.EventStatuses.Pending]
            + departmentStats[Enums.EventStatuses.BoardingSlipGenerated]
            + departmentStats[Enums.EventStatuses.WaitingForConfirmation]
            + departmentStats[Enums.EventStatuses.Confirmed]
            + departmentStats[Enums.EventStatuses.Completed]
            + departmentStats[Enums.EventStatuses.Cancelled];

            departmentEventsCount.push(departmentStats);
    }

    // BSG Count SQL
    const bsgCountSql = `
        SELECT COUNT(*) AS count
        FROM history
        WHERE userId = ?
        AND actionType = ?
        ${dateFrom ? `AND createdOn > ?` : ''}
        ${dateTo ? `AND createdOn < ?` : ''}
    `;
    const bsgCountParams = [
        userId,
        Enums.HistoryEvents.GenerateBoardingSlip
    ]
    if(dateFrom) bsgCountParams.push(moment(dateFrom).toDate());
    if(dateTo) bsgCountParams.push(moment(dateTo).toDate());

    log.info(reqId, `${fnName} ### [query] get amount of times boarding slip was generated`);
    // -> Get BSG Count
    const [bsgCount] = await con.query(bsgCountSql, bsgCountParams);

    // -> Get User
    const user = await UserRepo.findById(log, reqId, userId);

    log.info(reqId, `${fnName} ### build result`);
    // -> Build Result
    const final = {
        user,
        eventsCount,
        eventStatusCount,
        boardingSlipsGeneratedCount: bsgCount[0]?.count || 0,
        departmentEventsCount,
    }

    // End
    log.info(reqId, `${fnName} ### end [success]`);
    return final;
}

async function getProductivityReport_deprecated(log, reqId, dateFrom, dateTo, coordinatorId, physicianId, facilityId) {

    const fnName = 'operations/reports/getProductivityReport';
    log.info(reqId, `${fnName} ### start`);

    const filters = {
        fromDate: dateFrom,
        toDate: dateTo,
    }
    if(coordinatorId) filters.coordinatorId = coordinatorId;
    if(physicianId) filters.physicianId = physicianId;
    const userId = coordinatorId || physicianId;

    log.info(reqId, `${fnName} ### filters: ${JSON.stringify(filters)}`);


    log.info(reqId, `${fnName} ### gather promises`);
    // -> Gather Promises
    const promises = [
        StatisticsRepo.countEvents(log, reqId, filters),
        StatisticsRepo.countEventStatuses(log, reqId, filters),
        StatisticsRepo.countBoardingSlipsGenerated(log, reqId, userId, dateFrom, dateTo),
        StatisticsRepo.countDepartmentEvents(log, reqId, filters),
    ]

    log.info(reqId, `${fnName} ### run promises`);
    // -> Run Promises
    const stats = await Promise.all(promises);

    // -> Get User
    const user = await UserRepo.findById(log, reqId, userId);

    log.info(reqId, `${fnName} ### build result`);
    // -> Build Result
    const final = {
        user,
        eventsCount: stats[0],
        eventStatusCount: stats[1],
        boardingSlipsGeneratedCount: stats[2],
        departmentEventsCount: stats[3],
    }

    // End
    log.info(reqId, `${fnName} ### end [success]`);
    return final;
}


async function sendPendingProductivityReports(log, reqId) {

    /**
     * Date Modes:
     *  _Daily: 
     *      _24 hours passed
     *  _Weekday:
     *      _Is the weekday
     *      _More than 5 days passed (duplicate sending prevention)
     *  _BiMonthly:
     *      _Day of month is 1 or 15
     *      _More than 5 days passed (duplicate sending prevention)
     *  _BiMonthly:
     *      _Day of month is 1
     *      _More than 5 days passed (duplicate sending prevention)
     *      
     *  All modes also check if an hour is set, then it must be that hour
     */

    const fnName = 'operations/scheduledReports/sendPendingProductivityReports';
    log.info(reqId, `${fnName} ### start`);

    /**
     * MySQL Helpers:
     * DAYOFMONTH(): 1 -> 31
     * WEEKDAY(): 0 = Monday, 1 = Tuesday, 2 = Wednesday, 3 = Thursday, 4 = Friday, 5 = Saturday, 6 = Sunday.
     * 
     * Moment Helpers:
     * date(): 1 -> 31
     * isoWeekday(): 1 -> 7 1 being Monday
     */

    const sql = `
        SELECT *
        FROM productivity_report
        WHERE
            (
                (sendEvery = "${Enums.ProductivityReportIntervals.Daily}" AND 
                    (
                        (lastSentOn IS NOT NULL AND TIMESTAMPDIFF(HOUR, lastSentOn, NOW()) >= 24)
                        OR
                        lastSentOn IS NULL
                    )
                )
                OR
                (sendEvery = "${Enums.ProductivityReportIntervals.Monday}" AND 
                    WEEKDAY(NOW()) = 0
                    AND
                    (
                        (lastSentOn IS NOT NULL AND TIMESTAMPDIFF(DAY, lastSentOn, NOW()) > 5)
                        OR
                        lastSentOn IS NULL
                    )
                )
                OR
                (sendEvery = "${Enums.ProductivityReportIntervals.Tuesday}" AND 
                    WEEKDAY(NOW()) = 1
                    AND
                    (
                        (lastSentOn IS NOT NULL AND TIMESTAMPDIFF(DAY, lastSentOn, NOW()) > 5)
                        OR
                        lastSentOn IS NULL
                    )
                )
                OR
                (sendEvery = "${Enums.ProductivityReportIntervals.Wednesday}" AND 
                    WEEKDAY(NOW()) = 2
                    AND
                    (
                        (lastSentOn IS NOT NULL AND TIMESTAMPDIFF(DAY, lastSentOn, NOW()) > 5)
                        OR
                        lastSentOn IS NULL
                    )
                )
                OR
                (sendEvery = "${Enums.ProductivityReportIntervals.Thursday}" AND 
                    WEEKDAY(NOW()) = 3
                    AND
                    (
                        (lastSentOn IS NOT NULL AND TIMESTAMPDIFF(DAY, lastSentOn, NOW()) > 5)
                        OR
                        lastSentOn IS NULL
                    )
                )
                OR
                (sendEvery = "${Enums.ProductivityReportIntervals.Friday}" AND 
                    WEEKDAY(NOW()) = 4
                    AND
                    (
                        (lastSentOn IS NOT NULL AND TIMESTAMPDIFF(DAY, lastSentOn, NOW()) > 5)
                        OR
                        lastSentOn IS NULL
                    )
                )
                OR
                (sendEvery = "${Enums.ProductivityReportIntervals.Saturday}" AND 
                    WEEKDAY(NOW()) = 5
                    AND
                    (
                        (lastSentOn IS NOT NULL AND TIMESTAMPDIFF(DAY, lastSentOn, NOW()) > 5)
                        OR
                        lastSentOn IS NULL
                    )
                )
                OR
                (sendEvery = "${Enums.ProductivityReportIntervals.Sunday}" AND 
                    WEEKDAY(NOW()) = 6
                    AND
                    (
                        (lastSentOn IS NOT NULL AND TIMESTAMPDIFF(DAY, lastSentOn, NOW()) > 5)
                        OR
                        lastSentOn IS NULL
                    )
                )
                OR
                (sendEvery = "${Enums.ProductivityReportIntervals.BiMonthly}" AND 
                    DAYOFMONTH(NOW()) IN (1, 15)
                    AND
                    (
                        (lastSentOn IS NOT NULL AND TIMESTAMPDIFF(DAY, lastSentOn, NOW()) > 5)
                        OR
                        lastSentOn IS NULL
                    )
                )
                OR
                (sendEvery = "${Enums.ProductivityReportIntervals.FirstDayOfMonth}" AND 
                    DAYOFMONTH(NOW()) = 1
                    AND
                    (
                        (lastSentOn IS NOT NULL AND TIMESTAMPDIFF(DAY, lastSentOn, NOW()) > 5)
                        OR
                        lastSentOn IS NULL
                    )
                )
            )
            AND ((hour IS NOT NULL AND HOUR(NOW()) = HOUR(hour)) OR hour IS NULL)
    `

    log.info(reqId, `${fnName} ### [query] get pending productivity reports`);
    // -> Get Pending Productivity Reports
    const [reports] = await con.query(sql);

    for(let i = 0; i < reports.length; i++) {
        const report = reports[i];

        const dateFrom = report.lastSentOn;
        const dateTo = new Date();

        // Productivity report params
        const reportParams = {};
        if(dateFrom) reportParams.dateFrom = dateFrom;
        if(dateTo) reportParams.dateTo = dateTo;
        if(report.coordinatorId) reportParams.coordinatorId = report.coordinatorId;
        if(report.physicianId) reportParams.physicianId = report.physicianId;
        if(report.facilityId) reportParams.facilityId = report.facilityId;
        
        log.info(reqId, `${fnName} ### calling getProductivityReport`);
        // -> Get Productivity Report Stats
        const stats = await getProductivityReport(log, reqId, reportParams);
        
        // Email Params
        const templateParams = {
            ...stats,
            dateFrom,
            dateTo
        }
        console.log(templateParams)

        log.info(reqId, `${fnName} ### calling (EmailTemplates)'productivityReport'`);
        // -> Bake the Email Template
        const html = EmailTemplates.productivityReport(templateParams);
        console.log(html)
        
        const subject = `${config.identity.name} - Productivity Report for ${templateParams.user.name}`;

        log.info(reqId, `${fnName} ### calling (mail)'send'`);
        // -> Send Email
        mail.send(log, reqId, report.sendTo, subject, html)
            .then(() => {
                log.info(reqId, `${fnName} ### [query] update lastSentOn`);
                // -> Update lastSentOn
                con.query(`UPDATE productivity_report SET lastSentOn = NOW() WHERE id = ${report.id}`);
            })
            .catch(emailError => {
                log.info(reqId, `${fnName} ### could not send email ### error: ${JSON.stringify(emailError)}`);
            })


    }

    // End
    log.info(reqId, `${fnName} ### end`);

}





/**
 * Exports
 */
module.exports = {
    getProductivityReport,
    sendPendingProductivityReports
}