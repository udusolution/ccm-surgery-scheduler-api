/**
 * @description operations for implementing scheduled communication automated alteration based on event rulings defined in Master Data
 */


const Enums = require('../common/enums');
const Errors = require('../common/errors/_CustomErrors');
const M = require('../resources/languages/messages');
const MasterDataRepo = require('../repos/masterData.repo');
const EventRepo = require('../repos/event.repo');
const EventProcedureRepo = require('../repos/event.procedure.repo');
const EventBloodThinnerRepo = require('../repos/event.bloodThinner.repo');
const EventDiagnosisRepo = require('../repos/event.diagnosis.repo');
const EventScheduledEmailComRepo = require('../repos/event.scheduledCom.email');
const EventScheduledSmsComRepo = require('../repos/event.scheduledCom.sms');
const EmailTemplateRepo = require('../repos/template.email.repo');
const SmsTemplateRepo = require('../repos/template.sms.repo');
const ScheduledComRepo = require('../repos/scheduledCom.repo');
const ScheduledComLogic = require('../logic/ScheduledComLogic');
const con = require('../config/db/db');
const SendPatientCom = require('./sendPatientCom');
const SendPhysicianCom = require('./sendPhysicianCom');
const { failure } = require('../resources/languages/messages');


async function addRuledInScheduledEmailCommunications(log, reqId, eventId) {

    /**
     * Steps:
     *      -> Get the event
     *      -> Get the event's:
     *          _procedures
     *          _blood thinners
     *          _scheduled email communications
     *      -> Get all master data email templates
     *      -> Get all master data scheduled email communication rulings
     *      -> For each ruling, if it matches the event's procedures and blood thinners and diagnosis and facility and event type, add its template to the array
     *      !! A ruling that references an email template id that does not exist in the email template master data will not be included even if it matches
     *      -> For each email template in the resulted array, if it is not included in the event's scheduled email communications already, add it with the ruling's "whenToSend"
     */

    const fnName = 'operations/scheduledComsRuling/addRuledInScheduledEmailCommunications';
    log.info(reqId, `${fnName} ### start`);

    log.info(reqId, `${fnName} ### calling (EventRepo)'findById'`);
    // -> Get Event
    const event = await EventRepo.findById(log, reqId, eventId);

    // Sanity Checks, throw error in the following cases
    // ? If event not found
    if(!event) throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());

    log.info(reqId, `${fnName} ### build the necessary data call promises array`);
    // -> Build the Data Promises Array
    const promises = [
        EventProcedureRepo.getForEvent(log, reqId, eventId),
        EventBloodThinnerRepo.getForEvent(log, reqId, eventId),
        EventScheduledEmailComRepo.getForEvent(log, reqId, eventId),
        EmailTemplateRepo.getAll(log, reqId),
        ScheduledComRepo.getAllEmailScheduledCommunications(log, reqId),
        EventDiagnosisRepo.getForEvent(log, reqId, eventId)
    ]

    log.info(reqId, `${fnName} ### run the promises`);
    // -> Run the Promises
    const results = await Promise.all(promises);

    log.info(reqId, `${fnName} ### extract the data`);
    // -> Extract the Data
    const eventProcedures = results[0];
    const eventBloodThinners = results[1];
    const eventScheduledEmails = results[2];
    const emailTemplates = results[3];
    const emailScheduledComRulings = results[4];
    const eventDiagnosis = results[5];

    let emailComsToBeAdded = [];


    // Loop through rulings and build array of scheduled emails that should be present (with their template data and attachments)
    // Add them to the event scheduled emails if not present
    for(let i = 0; i < emailScheduledComRulings.length; i++) {

        
        const rulingRow = emailScheduledComRulings[i];
        const rule = rulingRow.rule;
        let shouldBeAdded = false;

        // if(rulingRow.forWhom != 'patient') continue;

        if(!rule) {
            shouldBeAdded = true
        }
        else {
            let doProceduresPass = false;
            let doBloodThinnersPass = false;
            let doDiagnosisPass = false;
            let doesFacilityPass = false;
            let doesEventTypePass = false;

            const ruleVars = ScheduledComLogic.formulaToRulingVariables(rule);
            console.log(ruleVars)

            // Check if procedure rule passes
            if(ruleVars.procedureIds?.length > 1) {
                if(ruleVars.procedureRulingType == 'and') doProceduresPass = (ruleVars.procedureIds.every(rp => eventProcedures.some(ep => ep.procedureId == rp)));
                else if(ruleVars.procedureRulingType == 'or') doProceduresPass = (ruleVars.procedureIds.some(rp => eventProcedures.some(ep => ep.procedureId == rp)));
            }
            else if(ruleVars.procedureIds?.length == 1) {
                doProceduresPass = (eventProcedures.some(ep => ep.procedureId == ruleVars.procedureIds[0]));
            }
            else {
                doProceduresPass = true;
            }

            // Check if blood thinner rule passes
            if(ruleVars.bloodThinnerIds?.length > 1) {
                if(ruleVars.bloodThinnerRulingType == 'and') doBloodThinnersPass = (ruleVars.bloodThinnerIds.every(rp => eventBloodThinners.some(ebt => ebt.bloodThinnerId == rp)));
                else if(ruleVars.bloodThinnerRulingType == 'or') doBloodThinnersPass = (ruleVars.bloodThinnerIds.some(rp => eventBloodThinners.some(ebt => ebt.bloodThinnerId == rp)));
            }
            else if(ruleVars.bloodThinnerIds?.length == 1) {
                doBloodThinnersPass = (eventBloodThinners.some(ebt => ebt.bloodThinnerId == ruleVars.bloodThinnerIds[0]));
            }
            else {
                doBloodThinnersPass = true;
            }

            // Check if diagnosis rule passes
            if(ruleVars.diagnosisIds?.length > 1) {
                if(ruleVars.diagnosisRulingType == 'and') doDiagnosisPass = (ruleVars.diagnosisIds.every(rp => eventDiagnosis.some(ed => ed.diagnosisId == rp)));
                else if(ruleVars.diagnosisRulingType == 'or') doDiagnosisPass = (ruleVars.diagnosisIds.some(rp => eventDiagnosis.some(ed => ed.diagnosisId == rp)));
            }
            else if(ruleVars.diagnosisIds?.length == 1) {
                doDiagnosisPass = (eventDiagnosis.some(ed => ed.diagnosisId == ruleVars.diagnosisIds[0]));
            }
            else {
                doDiagnosisPass = true;
            }

            // Check if facility passes
            if(ruleVars.facilityId && ruleVars.facilityId == event.facilityId) doesFacilityPass = true;
            else if(ruleVars.facilityId) doesFacilityPass = false;
            else doesFacilityPass = true;

            // Check if event type passes
            if(ruleVars.eventTypeId && ruleVars.eventTypeId == event.eventTypeId) doesEventTypePass = true;
            else if(ruleVars.eventTypeId) doesEventTypePass = false;
            else doesEventTypePass = true;

            shouldBeAdded = doProceduresPass && doBloodThinnersPass && doesFacilityPass && doesEventTypePass && doDiagnosisPass;

        }

        // If should be added, include in the toBeAdded array
        if(shouldBeAdded) {

            // If email template not found, skip
            const emailTemplate = emailTemplates.find(t => t.id == rulingRow.templateId);
            if(!emailTemplate) continue;

            // If this template was already added from a previous ruling, skip
            if(emailComsToBeAdded.some(com => com.templateId == emailTemplate.id)) continue;

            emailComsToBeAdded.push({
                eventId: eventId,
                templateId: emailTemplate.id,
                templateName: emailTemplate.name,
                templateSubject: emailTemplate.subject,
                templateBody: emailTemplate.body,
                whenToSend: rulingRow.whenToSend,
                attachments: emailTemplate.attachments,
                forWhom: rulingRow.forWhom
            })
    
        }
        
    }

    log.info(reqId, `${fnName} ### Filter out the coms that already are enabled in this event`);
    // -> Filter out the coms that already are enabled in this event
    emailComsToBeAdded = emailComsToBeAdded.filter(com => !eventScheduledEmails.some(e => e.templateId == com.templateId));
    
    log.info(reqId, `${fnName} ### Add the Coms`);
    // -> Add the Coms
    await Promise.all(emailComsToBeAdded.map(com => EventScheduledEmailComRepo.add(log, reqId, com)));

    // End
    log.info(reqId, `${fnName} ### end [success] ### ${emailComsToBeAdded.length} scheduled email communication(s) added`);

}


async function sendPendingEventEmailCommunications(log, reqId) {

    /**
     * Steps:
     *      -> Query event scheduled email communications on:
     *          _not sent yet
     *          _event's date crossed the date that the com should be sent on
     *          _event has a patient email
     *      -> For each returned communication:
     *          -> Get its attachments
     *          -> Send the email to the patient
     *          -> Mark the com as sent
     */

    const fnName = 'operations/scheduledComsRuling/sendPendingEventEmailCommunications';
    log.info(reqId, `${fnName} ### start`);

    const sql = `
        SELECT e.*, com.*, com.id AS comId
        FROM event_email_com AS com
        JOIN event AS e ON com.eventId = e.id
        WHERE com.wasSent = 0
        AND com.forWhom = 'patient'
        AND e.patientEmail IS NOT NULL
        AND e.patientEmail != ''
        AND e.status IN ("${Enums.EventStatuses.Confirmed}","${Enums.EventStatuses.Completed}")
        AND IF(com.whenToSend < 0, e.dateFrom - INTERVAL ABS(com.whenToSend) MINUTE, e.dateTo + INTERVAL ABS(com.whenToSend) MINUTE) < NOW()
    `

    log.info(reqId, `${fnName} ### [query] get pending event email communications`);
    // -> Query
    const [coms] = await con.query(sql);

    for(let i = 0; i < coms.length; i++) {
        const com = coms[i];

        // -> Build the Email Template
        const template = {
            id: com.templateId,
            name: com.templateName,
            subject: com.templateSubject,
            body: com.templateBody
        }

        const [attachments] = await con.query(`SELECT file FROM event_email_com_attachment WHERE comId = ?`, [com.comId]);
        if(attachments.length > 0) {
            template.attachments = attachments.map(a => a.file);
        }

        // -> Call the Operation
        com.id = com.eventId;
        SendPatientCom.sendEmail(log, reqId, template, com, null)
            .then(() => {
                // -> Set as Sent
                con.query(`UPDATE event_email_com SET wasSent = 1, sentOn = NOW() WHERE id = ?`, [com.comId]);
            })
            .catch(err => {
                log.warn(reqId, `${fnName} ### failed to send scheduled event email communication ### error: ${JSON.stringify(err)}`);
            })

    }

}

async function sendPendingEventEmailPhysicianReminders(log, reqId) {

    /**
     * Steps:
     *      -> Query event scheduled email communications on:
     *          _not sent yet
     *          _event's date crossed the date that the com should be sent on
     *      -> For each returned communication:
     *          -> Get its attachments
     *          -> Send the email to the physician
     *          -> Mark the com as sent
     */

    const fnName = 'operations/scheduledComsRuling/sendPendingEventEmailPhysicianReminders';
    log.info(reqId, `${fnName} ### start`);

    const sql = `
        SELECT e.*, com.*, com.id AS comId, phy.email AS physicianEmail
        FROM event_email_com AS com
        JOIN event AS e ON com.eventId = e.id
        JOIN user AS phy ON e.physicianId = phy.id
        WHERE com.wasSent = 0
        AND com.forWhom = 'physician'
        AND phy.email IS NOT NULL
        AND phy.email != ''
        AND e.status IN ("${Enums.EventStatuses.Confirmed}","${Enums.EventStatuses.Completed}")
        AND IF(com.whenToSend < 0, e.dateFrom - INTERVAL ABS(com.whenToSend) MINUTE, e.dateTo + INTERVAL ABS(com.whenToSend) MINUTE) < NOW()
    `

    log.info(reqId, `${fnName} ### [query] get pending event email communications`);
    // -> Query
    const [coms] = await con.query(sql);

    for(let i = 0; i < coms.length; i++) {
        const com = coms[i];

        // -> Build the Email Template
        const template = {
            id: com.templateId,
            name: com.templateName,
            subject: com.templateSubject,
            body: com.templateBody
        }

        const [attachments] = await con.query(`SELECT file FROM event_email_com_attachment WHERE comId = ?`, [com.comId]);
        if(attachments.length > 0) {
            template.attachments = attachments.map(a => a.file);
        }

        // -> Call the Operation
        com.id = com.eventId;
        SendPhysicianCom.sendEmail(log, reqId, template, com, com.physicianEmail)
            .then(() => {
                // -> Set as Sent
                con.query(`UPDATE event_email_com SET wasSent = 1, sentOn = NOW() WHERE id = ?`, [com.comId]);
            })
            .catch(err => {
                log.warn(reqId, `${fnName} ### failed to send scheduled event email communication ### error: ${JSON.stringify(err)}`);
            })

    }

}


async function forceSendEventEmailCommunication(log, reqId, comId, userId) {

    const fnName = 'operations/scheduledComsRuling/forceSendEventEmailCommunication';
    log.info(reqId, `${fnName} ### start`);

    try {

        const sql = `
            SELECT e.*, com.*, com.id AS comId
            FROM event_email_com AS com
            JOIN event AS e ON com.eventId = e.id
            WHERE com.id = ?
            AND com.forWhom = 'patient'
            AND e.patientEmail IS NOT NULL
            AND e.patientEmail != ''
            LIMIT 1
        `
    
        log.info(reqId, `${fnName} ### [query] get event email communication`);
        // -> Query
        const [coms] = await con.query(sql, [comId]);
        const com = coms[0];
    
        // Sanity Checks, throw error in the following cases
        // ? If com not found
        if(!com) throw new Errors.CommonErrors.SanityError('event scheduled email not found', failure.eventScheduledEmailNotFound())
    
        // -> Build the Email Template
        const template = {
            id: com.templateId,
            name: com.templateName,
            subject: com.templateSubject,
            body: com.templateBody
        }
    
        const [attachments] = await con.query(`SELECT file FROM event_email_com_attachment WHERE comId = ?`, [com.comId]);
        if(attachments.length > 0) {
            template.attachments = attachments.map(a => a.file);
        }
    
        log.info(reqId, `${fnName} ### calling (SendPatientCom)'sendEmail'`);
        // -> Call the Operation
        com.id = com.eventId;
        await SendPatientCom.sendEmail(log, reqId, template, com, userId);
    
        log.info(reqId, `${fnName} ### [query] set com as sent`);
        // -> Set as Sent
        await con.query(`UPDATE event_email_com SET wasSent = 1, sentOn = NOW() WHERE id = ?`, [com.comId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
            
    }
    catch(err) {
        log.warn(reqId, `${fnName} ### failed to send scheduled event email communication ### error: ${JSON.stringify(err)}`);
        throw err;
    }

}


/**
 * bulk update event scheduled email communications by 
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {[{ 
 *  id: number, 
 *  keep: boolean,
 *  templateSubject: string,
 *  templateBody: string,
 *  whenToSend: number,
 *  attachments: [{
 *      id: number,
 *      keep: boolean
 *  }]
 * }]} comStates update parameters
 */
async function bulkUpdateEventEmailCommunications(log, reqId, comStates) {

    const fnName = 'operations/scheduledComsRuling/bulkUpdateEventEmailCommunications';
    log.info(reqId, `${fnName} ### start ### comStates: ${JSON.stringify(comStates)}`);

    try {

        // Separate coms that are to be deleted
        const comsToDelete = comStates.filter(c => !c.keep);
        const comsToKeep = comStates.filter(c => c.keep);

        // Separate com files that are to be deleted
        const filesToDeleteFromComsToDelete = comsToDelete
            .reduce((prev, cur) => { 
                prev.push(...cur.attachments.map(a => a.id));
                return prev;
            }, []);
        const filesToDeleteFromComsToKeep = comsToKeep
            .reduce((prev, cur) => { 
                prev.push(...cur.attachments.filter(a => !a.keep).map(a => a.id));
                return prev;
            }, [])
        const filesToDelete = [...filesToDeleteFromComsToDelete, ...filesToDeleteFromComsToKeep];

        if(filesToDelete?.length > 0) {
            log.info(reqId, `${fnName} ### [query] delete attachments`);
            // -> Delete Files
            await con.query(`DELETE FROM event_email_com_attachment WHERE id IN (?)`, [filesToDelete]);
        }

        if(comsToDelete?.length > 0) {
            log.info(reqId, `${fnName} ### [query] delete scheduled email communications`);
            // -> Delete Coms
            await con.query(`DELETE FROM event_email_com WHERE id IN (?)`, [comsToDelete.map(c => c.id)]);
        }

        // -> Update Coms
        for(let i = 0; i < comsToKeep.length; i++) {

            const comToKeep = comsToKeep[i];

            const comToKeepUpdateParams = {
                templateSubject: comToKeep.templateSubject,
                templateBody: comToKeep.templateBody,
                whenToSend: comToKeep.whenToSend
            }

            log.info(reqId, `${fnName} ### [query] update scheduled email communications`);
            // -> Update Com
            await con.query(`UPDATE event_email_com SET ? WHERE id = ?`, [comToKeepUpdateParams, comToKeep.id]);
        }

        // End
        log.info(reqId, `${fnName} ### end [success]`);
            
    }
    catch(err) {
        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(err)}`);
        throw err;
    }

}



/**
 * --------------------------------------------------------------------------------
 * -------------------------------- SMS -------------------------------------------
 * --------------------------------------------------------------------------------
 */
async function addRuledInScheduledSmsCommunications(log, reqId, eventId) {

    /**
     * Steps:
     *      -> Get the event
     *      -> Get the event's:
     *          _procedures
     *          _blood thinners
     *          _scheduled sms communications
     *      -> Get all master data sms templates
     *      -> Get all master data scheduled sms communication rulings
     *      -> For each ruling, if it matches the event's procedures and blood thinners and diagnosis and facility and event type, add its template to the array
     *      !! A ruling that references an sms template id that does not exist in the sms template master data will not be included even if it matches
     *      -> For each sms template in the resulted array, if it is not included in the event's scheduled sms communications already, add it and use the ruling's "whenToSend"
     */

    const fnName = 'operations/scheduledComsRuling/addRuledInScheduledSmsCommunications';
    log.info(reqId, `${fnName} ### start`);

    log.info(reqId, `${fnName} ### calling (EventRepo)'findById'`);
    // -> Get Event
    const event = await EventRepo.findById(log, reqId, eventId);

    // Sanity Checks, throw error in the following cases
    // ? If event not found
    if(!event) throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());

    log.info(reqId, `${fnName} ### build the necessary data call promises array`);
    // -> Build the Data Promises Array
    const promises = [
        EventProcedureRepo.getForEvent(log, reqId, eventId),
        EventBloodThinnerRepo.getForEvent(log, reqId, eventId),
        EventScheduledSmsComRepo.getForEvent(log, reqId, eventId),
        SmsTemplateRepo.getAll(log, reqId),
        ScheduledComRepo.getAllSmsScheduledCommunications(log, reqId),
        EventDiagnosisRepo.getForEvent(log, reqId, eventId)
    ]

    log.info(reqId, `${fnName} ### run the promises`);
    // -> Run the Promises
    const results = await Promise.all(promises);

    log.info(reqId, `${fnName} ### extract the data`);
    // -> Extract the Data
    const eventProcedures = results[0];
    const eventBloodThinners = results[1];
    const eventScheduledSMSs = results[2];
    const smsTemplates = results[3];
    const smsScheduledComRulings = results[4];
    const eventDiagnosis = results[5];

    let smsComsToBeAdded = [];


    // Loop through rulings and build array of scheduled SMSs that should be present (with their template data)
    // Add them to the event scheduled SMSs if not present
    for(let i = 0; i < smsScheduledComRulings.length; i++) {

        
        const rulingRow = smsScheduledComRulings[i];
        const rule = rulingRow.rule;
        let shouldBeAdded = false;

        // if(rulingRow.forWhom != 'patient') continue;

        if(!rule) {
            shouldBeAdded = true
        }
        else {
            let doProceduresPass = false;
            let doBloodThinnersPass = false;
            let doesFacilityPass = false;
            let doesEventTypePass = false;
            let doDiagnosisPass = false;

            const ruleVars = ScheduledComLogic.formulaToRulingVariables(rule);

            // Check if procedure rule passes
            if(ruleVars.procedureIds?.length > 1) {
                if(ruleVars.procedureRulingType == 'and') doProceduresPass = (ruleVars.procedureIds.every(rp => eventProcedures.some(ep => ep.procedureId == rp)));
                else if(ruleVars.procedureRulingType == 'or') doProceduresPass = (ruleVars.procedureIds.some(rp => eventProcedures.some(ep => ep.procedureId == rp)));
            }
            else if(ruleVars.procedureIds?.length == 1) {
                doProceduresPass = (eventProcedures.some(ep => ep.procedureId == ruleVars.procedureIds[0]));
            }
            else {
                doProceduresPass = true;
            }

            // Check if blood thinner rule passes
            if(ruleVars.bloodThinnerIds?.length > 1) {
                if(ruleVars.bloodThinnerRulingType == 'and') doBloodThinnersPass = (ruleVars.bloodThinnerIds.every(rp => eventBloodThinners.some(ebt => ebt.bloodThinnerId == rp)));
                else if(ruleVars.bloodThinnerRulingType == 'or') doBloodThinnersPass = (ruleVars.bloodThinnerIds.some(rp => eventBloodThinners.some(ebt => ebt.bloodThinnerId == rp)));
            }
            else if(ruleVars.bloodThinnerIds?.length == 1) {
                doBloodThinnersPass = (eventBloodThinners.some(ebt => ebt.bloodThinnerId == ruleVars.bloodThinnerIds[0]));
            }
            else {
                doBloodThinnersPass = true;
            }

            // Check if diagnosis rule passes
            if(ruleVars.diagnosisIds?.length > 1) {
                if(ruleVars.diagnosisRulingType == 'and') doDiagnosisPass = (ruleVars.diagnosisIds.every(rp => eventDiagnosis.some(ed => ed.diagnosisId == rp)));
                else if(ruleVars.diagnosisRulingType == 'or') doDiagnosisPass = (ruleVars.diagnosisIds.some(rp => eventDiagnosis.some(ed => ed.diagnosisId == rp)));
            }
            else if(ruleVars.diagnosisIds?.length == 1) {
                doDiagnosisPass = (eventDiagnosis.some(ed => ed.diagnosisId == ruleVars.diagnosisIds[0]));
            }
            else {
                doDiagnosisPass = true;
            }

            // Check if facility passes
            if(ruleVars.facilityId && ruleVars.facilityId == event.facilityId) doesFacilityPass = true;
            else if(ruleVars.facilityId) doesFacilityPass = false;
            else doesFacilityPass = true;

            // Check if event type passes
            if(ruleVars.eventTypeId && ruleVars.eventTypeId == event.eventTypeId) doesEventTypePass = true;
            else if(ruleVars.eventTypeId) doesEventTypePass = false;
            else doesEventTypePass = true;

            shouldBeAdded = doProceduresPass && doBloodThinnersPass && doesFacilityPass && doesEventTypePass && doDiagnosisPass;

        }

        // If should be added, include in the toBeAdded array
        if(shouldBeAdded) {

            const smsTemplate = smsTemplates.find(t => t.id == rulingRow.templateId);
            if(!smsTemplate) continue;

            // If this template was already added from a previous ruling, skip
            if(smsComsToBeAdded.some(com => com.templateId == smsTemplate.id)) continue;

            smsComsToBeAdded.push({
                eventId: eventId,
                templateId: smsTemplate.id,
                templateName: smsTemplate.name,
                templateBody: smsTemplate.body,
                whenToSend: rulingRow.whenToSend,
                forWhom: rulingRow.forWhom
            })
    
        }
        
    }

    log.info(reqId, `${fnName} ### Filter out the coms that already are enabled in this event`);
    // -> Filter out the coms that already are enabled in this event
    smsComsToBeAdded = smsComsToBeAdded.filter(com => !eventScheduledSMSs.some(e => e.templateId == com.templateId));
    
    log.info(reqId, `${fnName} ### Add the Coms`);
    // -> Add the Coms
    await Promise.all(smsComsToBeAdded.map(com => EventScheduledSmsComRepo.add(log, reqId, com)));

    // End
    log.info(reqId, `${fnName} ### end [success] ### ${smsComsToBeAdded.length} scheduled sms communication(s) added`);

}


async function sendPendingEventSmsCommunications(log, reqId) {

    /**
     * Steps:
     *      -> Query event scheduled sms communications on:
     *          _not sent yet
     *          _event's date crossed the date that the com should be sent on
     *          _event has a patient phone number
     *      -> For each returned communication:
     *          -> Send the sms to the patient
     *          -> Mark the com as sent
     */

    const fnName = 'operations/scheduledComsRuling/sendPendingEventSmsCommunications';
    log.info(reqId, `${fnName} ### start`);

    const sql = `
        SELECT e.*, com.*, com.id AS comId
        FROM event_sms_com AS com
        JOIN event AS e ON com.eventId = e.id
        WHERE com.wasSent = 0
        AND com.forWhom = 'patient'
        AND e.patientPreferredPhone IS NOT NULL
        AND e.patientPreferredPhone != ''
        AND e.status IN ("${Enums.EventStatuses.Confirmed}","${Enums.EventStatuses.Completed}")
        AND IF(com.whenToSend < 0, e.dateFrom - INTERVAL ABS(com.whenToSend) MINUTE, e.dateTo + INTERVAL ABS(com.whenToSend) MINUTE) < NOW()
    `

    log.info(reqId, `${fnName} ### [query] get pending event sms communications`);
    // -> Query
    const [coms] = await con.query(sql);

    for(let i = 0; i < coms.length; i++) {
        const com = coms[i];

        // -> Build the Sms Template
        const template = {
            id: com.templateId,
            name: com.templateName,
            body: com.templateBody
        }

        // -> Call the Operation
        com.id = com.eventId;
        SendPatientCom.sendSms(log, reqId, template, com, null)
            .then(() => {
                // -> Set as Sent
                con.query(`UPDATE event_sms_com SET wasSent = 1, sentOn = NOW() WHERE id = ?`, [com.comId]);
            })
            .catch(err => {
                log.warn(reqId, `${fnName} ### failed to send scheduled event sms communication ### error: ${JSON.stringify(err)}`);
            })

    }

}

async function sendPendingEventSmsPhysicianReminders(log, reqId) {

    /**
     * Steps:
     *      -> Query event scheduled sms communications on:
     *          _not sent yet
     *          _event's date crossed the date that the com should be sent on
     *      -> For each returned communication:
     *          -> Send the sms to the patient
     *          -> Mark the com as sent
     */

    const fnName = 'operations/scheduledComsRuling/sendPendingEventSmsPhysicianReminders';
    log.info(reqId, `${fnName} ### start`);

    const sql = `
        SELECT e.*, com.*, com.id AS comId, phy.phone AS physicianMobile
        FROM event_sms_com AS com
        JOIN event AS e ON com.eventId = e.id
        JOIN user AS phy ON e.physicianId = phy.id
        WHERE com.wasSent = 0
        AND com.forWhom = 'physician'
        AND phy.phone IS NOT NULL
        AND phy.phone != ''
        AND e.status IN ("${Enums.EventStatuses.Confirmed}","${Enums.EventStatuses.Completed}")
        AND IF(com.whenToSend < 0, e.dateFrom - INTERVAL ABS(com.whenToSend) MINUTE, e.dateTo + INTERVAL ABS(com.whenToSend) MINUTE) < NOW()
    `

    log.info(reqId, `${fnName} ### [query] get pending event sms communications`);
    // -> Query
    const [coms] = await con.query(sql);

    for(let i = 0; i < coms.length; i++) {
        const com = coms[i];

        // -> Build the Sms Template
        const template = {
            id: com.templateId,
            name: com.templateName,
            body: com.templateBody
        }

        // -> Call the Operation
        com.id = com.eventId;
        SendPhysicianCom.sendSms(log, reqId, template, com, com.physicianMobile)
            .then(() => {
                // -> Set as Sent
                con.query(`UPDATE event_sms_com SET wasSent = 1, sentOn = NOW() WHERE id = ?`, [com.comId]);
            })
            .catch(err => {
                log.warn(reqId, `${fnName} ### failed to send scheduled event sms communication ### error: ${JSON.stringify(err)}`);
            })

    }

}


async function forceSendEventSmsCommunication(log, reqId, comId, userId) {

    const fnName = 'operations/scheduledComsRuling/forceSendEventSmsCommunication';
    log.info(reqId, `${fnName} ### start`);

    try {

        const sql = `
            SELECT e.*, com.*, com.id AS comId
            FROM event_sms_com AS com
            JOIN event AS e ON com.eventId = e.id
            WHERE com.id = ?
            AND com.forWhom = 'patient'
            AND e.patientPreferredPhone IS NOT NULL
            AND e.patientPreferredPhone != ''
            LIMIT 1
        `
    
        log.info(reqId, `${fnName} ### [query] get event sms communication`);
        // -> Query
        const [coms] = await con.query(sql, [comId]);
        const com = coms[0];
    
        // Sanity Checks, throw error in the following cases
        // ? If com not found
        if(!com) throw new Errors.CommonErrors.SanityError('event scheduled sms not found', failure.eventScheduledSmsNotFound())
    
        // -> Build the Sms Template
        const template = {
            id: com.templateId,
            name: com.templateName,
            body: com.templateBody
        }
    
        log.info(reqId, `${fnName} ### calling (SendPatientCom)'sendSms'`);
        // -> Call the Operation
        com.id = com.eventId;
        await SendPatientCom.sendSms(log, reqId, template, com, userId);
    
        log.info(reqId, `${fnName} ### [query] set com as sent`);
        // -> Set as Sent
        await con.query(`UPDATE event_sms_com SET wasSent = 1, sentOn = NOW() WHERE id = ?`, [com.comId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
            
    }
    catch(err) {
        log.warn(reqId, `${fnName} ### failed to send scheduled event sms communication ### error: ${JSON.stringify(err)}`);
        throw err;
    }

}


/**
 * bulk update event scheduled sms communications by 
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {[{ 
 *  id: number, 
 *  keep: boolean,
 *  templateBody: string,
 *  whenToSend: number
 * }]} comStates update parameters
 */
async function bulkUpdateEventSmsCommunications(log, reqId, comStates) {

    const fnName = 'operations/scheduledComsRuling/bulkUpdateEventSmsCommunications';
    log.info(reqId, `${fnName} ### start ### comStates: ${JSON.stringify(comStates)}`);

    try {

        // Separate coms that are to be deleted
        const comsToDelete = comStates.filter(c => !c.keep);
        const comsToKeep = comStates.filter(c => c.keep);

        if(comsToDelete?.length > 0) {
            log.info(reqId, `${fnName} ### [query] delete scheduled sms communications`);
            // -> Delete Coms
            await con.query(`DELETE FROM event_sms_com WHERE id IN (?)`, [comsToDelete.map(c => c.id)]);
        }

        // -> Update Coms
        for(let i = 0; i < comsToKeep.length; i++) {

            const comToKeep = comsToKeep[i];

            const comToKeepUpdateParams = {
                templateBody: comToKeep.templateBody,
                whenToSend: comToKeep.whenToSend
            }

            log.info(reqId, `${fnName} ### [query] update scheduled sms communications`);
            // -> Update Com
            await con.query(`UPDATE event_sms_com SET ? WHERE id = ?`, [comToKeepUpdateParams, comToKeep.id]);
        }

        // End
        log.info(reqId, `${fnName} ### end [success]`);
            
    }
    catch(err) {
        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(err)}`);
        throw err;
    }

}


/**
 * Exports
 */
module.exports = {
    addRuledInScheduledEmailCommunications,
    sendPendingEventEmailCommunications,
    sendPendingEventEmailPhysicianReminders,
    forceSendEventEmailCommunication,
    bulkUpdateEventEmailCommunications,
    addRuledInScheduledSmsCommunications,
    sendPendingEventSmsCommunications,
    sendPendingEventSmsPhysicianReminders,
    forceSendEventSmsCommunication,
    bulkUpdateEventSmsCommunications,
}