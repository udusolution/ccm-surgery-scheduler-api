/**
 * @description business logic for if a user should be allowed to edit a specific event or not
 */


const Enums = require('../common/enums');
const EventRepo = require('../repos/event.repo');
const UserRepo = require('../repos/user.repo');
const Errors = require('../common/errors/_CustomErrors');
const M = require('../resources/languages/messages');


/**
 * check if a user should be able to edit a specific event or not
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {*} user user info that is trying to edit an event
 * @param {number} eventId ID of the event being edited
 * @returns {Promise<Boolean>} whether this user should be allowed to edit this event or not
 * 
 * User can edit if:
 * - Is Admin
 * - Or is this event's coordinator
 * - Or is another coordinator and event's coordinator has edit settings as "allow all"
 * - Or is another coordinator and event's coordinator has edit settings as "upon request" and this coordinator is in the whitelist
 * - Or is another coordinator and event's coordinator has edit settings as "upon request" and this coordinator was granted permission by the event's coordinator
 */
async function isAllowed(log, reqId, user, eventId) {

    const fnName = "operations/eventEditPerm/isAllowed";
    log.info(reqId, `${fnName} ### start`);

    log.info(reqId, `${fnName} ### calling (EventRepo)'findById'`);
    // -> Get Event
    const event = await EventRepo.findById(log, reqId, eventId);

    // Sanity Checks, throw error in the following cases
    // ? If event not found
    if(!event) throw new Errors.CommonErrors.SanityError('event not found', M.failure.eventNotFound());

    // Is admin? Return true
    if(user.roles?.includes(Enums.UserRoles.Admin)) return true;

    // Is coordinator?
    if(user.roles?.includes(Enums.UserRoles.Coordinator)) {

        // Is this event's coordinator? Return true
        if(event.coordinatorId == user.id) return true;

        log.info(reqId, `${fnName} ### calling (UserRepo)'getWithWhitelist'`);
        // -> Get this event's coordinator with his whitelist
        const coordinator = await UserRepo.getWithWhitelist(log, reqId, event.coordinatorId);

        // Sanity Checks, throw error in the following cases
        // ? If coordinator not found
        if(!coordinator) throw new Errors.CommonErrors.SanityError(`event's coordinator not found`, M.failure.eventCoordinatorNotFound())

        // Does this coordinator allow everyone to edit his events? Return true
        if(coordinator.eventEditPerm == Enums.UserEventEditPermissions.AllowAll) return true;
        // ! But does he allow none? Return false
        else if(coordinator.eventEditPerm == Enums.UserEventEditPermissions.AllowNone) return false;

        // Is this user in this coordinator's whitelist? Return true
        if(coordinator.whitelist?.some(w => w == user.id)) return true;

        log.info(reqId, `${fnName} ### calling (EventRepo)'userWasGrantedEditPerm'`);
        // -> Check if this user was granted edit permission for this event
        const wasGrantedEditPerm = await EventRepo.userWasGrantedEditPerm(log, reqId, eventId, user.id);

        // Was this user granted edit permission for this event? Return true
        if(wasGrantedEditPerm) return true;

    }

    // No rule Passed, this user is not allowed to edit this event
    return false;

}


/**
 * Exports
 */
module.exports = {
    isAllowed
}