/**
 * @description helper module that sends patient communications
 * @note created to avoid duplicating this code in various endpoints
 */


const config = require('../config/config');
const Enums = require('../common/enums');
const mail = require('../common/services/mail');
const sms = require('../common/services/sms');
const EmailTemplates = require('../resources/templates/mail/emailTemplates');
const SmsTemplates = require('../resources/templates/sms/smsTemplates');
const momentTZ = require('moment-timezone');
const path = require('path');
const PatientComRepo = require('../repos/patientCom.repo');


/**
 * send patient communication email
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {*} eventDetails event details
 */
async function sendEmail(log, reqId, template, eventDetails, userId) {

    const fnName = 'sendEmail';
    log.info(reqId, `${fnName} ### start`);

    log.info(reqId, `${fnName} ### calling (EmailTemplates)'patientCom' ### template: ${JSON.stringify(template)}`);
    // Get email content
    const comsEmailHtml = EmailTemplates.patientComGeneral(template.body, eventDetails);

    // Format Attachments
    const attachments = template.attachments?.map(f => {
        return {
            filename: path.basename(f).replace(/\d+-/, ''),
            path: path.join(global.appRoot, 'public', f)
        }
    })

    log.info(reqId, `${fnName} ### calling (mail)'send'`);
    // -> Send Patient Com Email
    await mail.send(log, reqId, eventDetails.patientEmail, template.subject, comsEmailHtml, attachments);

    // Add Record Params
    const addRecordParams = {
        userId, 
        eventId: eventDetails.id, 
        patientEmail: eventDetails.patientEmail,
        emailSubject: template.subject, 
        emailBody: comsEmailHtml, 
        emailAttachments: template.attachments?.join(':::')
    }

    log.info(reqId, `${fnName} ### calling (PatientComRepo)'addRecord'`);
    // -> Add Patient Com Record
    await PatientComRepo.addRecord(log, reqId, addRecordParams);

    // End
    log.info(reqId, `${fnName} ### end [success]`);

}


/**
 * send patient communication sms
 * @param {*} log 
 * @param {*} reqId 
 * @param {*} template 
 * @param {*} eventDetails 
 * @param {*} userId 
 */
async function sendSms(log, reqId, template, eventDetails, userId) {

    const fnName = 'sendSms';
    log.info(reqId, `${fnName} ### start`);

    log.info(reqId, `calling (SmsTemplates)'patientCom'`);
    // Get sms content
    const comsSmsBody = SmsTemplates.patientComGeneral(template.body, eventDetails);

    log.info(reqId, `calling (sms)'send'`);
    // -> Send Patient Com Sms
    await sms.send(log, reqId, eventDetails.patientPreferredPhone, comsSmsBody);

    // Add Record Params
    const addRecordParams = {
        userId, 
        eventId: eventDetails.id, 
        patientPhone: eventDetails.patientPreferredPhone,
        smsBody: comsSmsBody,
    }

    log.info(reqId, `calling (PatientComRepo)'addRecord'`);
    // -> Add Patient Com Record
    await PatientComRepo.addRecord(log, reqId, addRecordParams);

    // End
    log.info(reqId, `${fnName} ### end [success]`);
}


/**
 * Exports
 */
module.exports = {
    sendEmail,
    sendSms
}