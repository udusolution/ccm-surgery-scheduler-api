/**
 * @description helper module that sends physician communications
 * @note created to avoid duplicating this code in various endpoints
 */


const config = require('../config/config');
const Enums = require('../common/enums');
const mail = require('../common/services/mail');
const sms = require('../common/services/sms');
const EmailTemplates = require('../resources/templates/mail/emailTemplates');
const SmsTemplates = require('../resources/templates/sms/smsTemplates');
const momentTZ = require('moment-timezone');
const path = require('path');
const PatientComRepo = require('../repos/patientCom.repo');


/**
 * send physician communication email
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {*} eventDetails event details
 */
async function sendEmail(log, reqId, template, eventDetails, physicianEmail) {

    const fnName = 'sendEmail';
    log.info(reqId, `${fnName} ### start`);

    log.info(reqId, `${fnName} ### calling (EmailTemplates)'physicianComGeneral' ### template: ${JSON.stringify(template)}`);
    // Get email content
    const comsEmailHtml = EmailTemplates.physicianComGeneral(template.body, eventDetails);

    // Format Attachments
    const attachments = template.attachments?.map(f => {
        return {
            filename: path.basename(f).replace(/\d+-/, ''),
            path: path.join(global.appRoot, 'public', f)
        }
    })

    log.info(reqId, `${fnName} ### calling (mail)'send'`);
    // -> Send Physician Com Email
    await mail.send(log, reqId, physicianEmail, template.subject, comsEmailHtml, attachments);

    // End
    log.info(reqId, `${fnName} ### end [success]`);

}


/**
 * send physician communication sms
 * @param {*} log 
 * @param {*} reqId 
 * @param {*} template 
 * @param {*} eventDetails 
 * @param {*} userId 
 */
async function sendSms(log, reqId, template, eventDetails, physicianPhone) {

    const fnName = 'sendSms';
    log.info(reqId, `${fnName} ### start`);

    log.info(reqId, `calling (SmsTemplates)'physicianComGeneral'`);
    // Get sms content
    const comsSmsBody = SmsTemplates.physicianComGeneral(template.body, eventDetails);

    log.info(reqId, `calling (sms)'send'`);
    // -> Send Patient Com Sms
    await sms.send(log, reqId, physicianPhone, comsSmsBody);

    // End
    log.info(reqId, `${fnName} ### end [success]`);
}


/**
 * Exports
 */
module.exports = {
    sendEmail,
    sendSms
}