/**
 * @description operations for syncing the local database with Advanced MD patient database
 */


const con = require('../config/db/db');
const MD = require('../common/services/md');
const { formatSSN } = require('../common/utils/string.utils');
const { DbErrors, MdErrors } = require('../common/errors/_CustomErrors');
const { default: axios } = require('axios');


async function syncMdData(log, reqId) {


    const fnName = "syncWithMd/syncMdData";
    log.info(reqId, `${fnName} ### start`);

    try {

        log.info(reqId, `${fnName} ### calling (MD)'login'`);
        // -> Login to AdvancedMD API
        const loginRes = await MD.login(log, reqId);
        
        // Request Variables
        const apiToken = loginRes.token;
        const apiUrl = loginRes.url;

        log.info(reqId, `${fnName} ### [query] get latest sync date`);
        // -> Get Last Sync Date
        const [appData] = await con.query(`SELECT * FROM app_data WHERE id = 1`);
        const lastSyncDate = appData[0] && appData[0].mdLastSyncDate ? appData[0].mdLastSyncDate : `1900-01-01T12:00:00.510`;

        log.info(reqId, `${fnName} ### calling (MD)'getUpdatedPatients'`);
        // -> Get Patient Updates Since Last Sync Date
        const patientChanges = await MD.getUpdatedPatients(log, reqId, apiToken, apiUrl, lastSyncDate);

        let changes = patientChanges.updatedPatientData?.patient ?? [];
        if(!Array.isArray(changes)) changes = [changes];

        // If more than 1,000 updates and response had only patient IDs,
        // Chunk them into batches of 500 patients each and get updates
        if(patientChanges.isIdsOnly) {

            log.info(reqId, `${fnName} ### ${patientChanges.patientsCount} patient changes, must be chunked`);

            // Batch up the returned patient ID objects
            const batches = changes.reduce((resultArray, item, index) => { 
                const chunkIndex = Math.floor(index/500)
                if(!resultArray[chunkIndex]) {
                  resultArray[chunkIndex] = [] // start a new chunk
                }
                resultArray[chunkIndex].push(item)
                return resultArray
            }, [])

            // This array holds the full patient changes
            let subChanges = [];

            const asyncOperations = [];
            // Fetch patient changes for each batch and append to the subChanges array
            for(let i = 0; i < batches.length; i++) {
                asyncOperations.push(async () => {
                    const batch = batches[i];
                    log.info(reqId, `${fnName} ### Calling 'MD'(getUpdatedPatientsUsingIds) for batch #${i + 1}`);
                    let subChangeRes = await MD.getUpdatedPatientsUsingIds(log, reqId, apiToken, apiUrl, lastSyncDate, batch);
                    let subChange = subChangeRes.updatedPatientData?.patient ?? [];
                    if(!Array.isArray(subChange)) subChange = [subChange];
                    subChanges = [...subChanges, ...subChange];
                })
            }

            await Promise.all(asyncOperations);

            log.info(reqId, `${fnName} ### updating the changes array to become the patient updates from all batches`);
            // Make the changes array be the subChanges array
            changes = subChanges;
        }

        // Get Server Sync Date
        const currentSyncDate = patientChanges.currentServerTime || null;

        // -> Loop Through Changes And Sync
        for(let i = 0; i < changes.length; i++) {

            if(i % 10 === 0) log.info(reqId, `${fnName} ### applying update of patient in position ${i}`);

            const change = changes[i];

            const changeType = change['@updatestatus'];

            // If New patient
            if(changeType === 'N') {
                await addPatient(change).catch(err => log.warn(reqId, `${fnName} ### Add failed ### change: ${JSON.stringify(change)} ### error: ${JSON.stringify(err)}`));
            }

            // If Update patient
            if(changeType === 'C') {
                await updatePatient(change).catch(err => log.warn(reqId, `${fnName} ### Update failed ### change: ${JSON.stringify(change)} ### error: ${JSON.stringify(err)}`));
            }

            // If Delete patient
            if(changeType === 'D') {
                await deletePatient(change).catch(err => log.warn(reqId, `${fnName} ### Delete failed ### change: ${JSON.stringify(change)} ### error: ${JSON.stringify(err)}`));
            }
        }

        log.info(reqId, `${fnName} ### [query] update latest sync date`);
        // -> Update Server Sync Date
        if(currentSyncDate) await con.query(`UPDATE app_data SET mdLastSyncDate = ? WHERE id = 1`, [currentSyncDate]);

        // End
        log.info(reqId, `end [success] ### synced ${changes.length} change(s)`);

            
    } catch(e) {
        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        // () If Too Many Requests Error
        if(axios.isAxiosError(e) && e.response.status === 429) {
            throw new MdErrors.TooManyRequestsMdError(e.message, e);
        }
        else {
            throw new MdErrors.MdError(e.message, e);
        }
    }
}

async function addPatient(change) {

    const patientId = change['@id'];
    const [existenceCheck] = await con.query(`SELECT * FROM patient WHERE apiId = ? LIMIT 1`, [patientId]);
    
    if(existenceCheck?.length > 0) {
        // Update Patient
        updatePatient(change);
    }
    else {
        let insurances = change.insurancelist?.insurance;
        if(!insurances) insurances = [];
        else if(insurances && !Array.isArray(insurances)) insurances = [insurances];
        const primaryInsurance = insurances[0];
        const secondaryInsurance = insurances[1];
        let primaryInsuranceId = null;
        let secondaryInsuranceId = null;

        try {
            if(primaryInsurance && primaryInsurance['@carcode']?.trim() && primaryInsurance['@carname']?.trim()) primaryInsuranceId = (await  getOrCreateCarrier(primaryInsurance['@carcode'].trim(), primaryInsurance['@carname'].trim())).id;
            if(secondaryInsurance && secondaryInsurance['@carcode']?.trim() && secondaryInsurance['@carname']?.trim()) secondaryInsuranceId = (await  getOrCreateCarrier(secondaryInsurance['@carcode'].trim(), secondaryInsurance['@carname'].trim())).id;
        }
        catch(e) {
            // Swallow
        }

        // Add Patient
        const addParams = {
            apiId: patientId,
            name: change['@name']?.trim() || null,
            firstName: change['@firstname']?.trim() || null,
            middleName: change['@middlename']?.trim() || null,
            lastName: change['@lastname']?.trim() || null,
            address1: change['@address1']?.trim() || null,
            address2: change['@address2']?.trim() || null,
            city: change['@city']?.trim() || null,
            state: change['@state']?.trim() || null,
            zipcode: change['@zip']?.trim() || null,
            dateofbirth: change['@dob']?.trim() || null,
            email: change['@email']?.trim() || null,
            phone: change['@homephone']?.trim() || change['@otherphone']?.trim() || null,
            ssn: formatSSN(change['@ssn']?.trim()) || null,
            sex: change['@sex']?.trim() == 'M' ? 'male' : 'female',
            carrierId: primaryInsuranceId ? primaryInsuranceId : null,
            carrierCode: primaryInsurance ? (primaryInsurance['@carcode']?.trim() || null) : null,
            carrierName: primaryInsurance ? (primaryInsurance['@carname']?.trim() || null) : null,
            subscriberId: primaryInsurance ? (primaryInsurance['@subidnumber']?.trim() || null) : null,
            groupName: primaryInsurance ? (primaryInsurance['@groupname']?.trim() || null) : null,
            groupNumber: primaryInsurance ? (primaryInsurance['@groupnumber']?.trim() || null) : null,
            carrierId2: secondaryInsuranceId ? secondaryInsuranceId : null,
            carrierCode2: secondaryInsurance ? (secondaryInsurance['@carcode']?.trim() || null) : null,
            carrierName2: secondaryInsurance ? (secondaryInsurance['@carname']?.trim() || null) : null,
            subscriberId2: secondaryInsurance ? (secondaryInsurance['@subidnumber']?.trim() || null) : null,
            groupName2: secondaryInsurance ? (secondaryInsurance['@groupname']?.trim() || null) : null,
            groupNumber2: secondaryInsurance ? (secondaryInsurance['@groupnumber']?.trim() || null) : null,
        }

        // Add patient
        await con.query(`INSERT INTO patient SET ?`, [addParams]);
    }
}

async function updatePatient(change) {

    const patientId = change['@id'];

    // Update Patient
    let insurances = change.insurancelist?.insurance;
    if(!insurances) insurances = [];
    else if(insurances && !Array.isArray(insurances)) insurances = [insurances];
    const primaryInsurance = insurances[0];
    const secondaryInsurance = insurances[1];
    let primaryInsuranceId = null;
    let secondaryInsuranceId = null;
    try {
        if(primaryInsurance && primaryInsurance['@carcode']?.trim() && primaryInsurance['@carname']?.trim()) primaryInsuranceId = (await  getOrCreateCarrier(primaryInsurance['@carcode'].trim(), primaryInsurance['@carname'].trim())).id;
        if(secondaryInsurance && secondaryInsurance['@carcode']?.trim() && secondaryInsurance['@carname']?.trim()) secondaryInsuranceId = (await  getOrCreateCarrier(secondaryInsurance['@carcode'].trim(), secondaryInsurance['@carname'].trim())).id;
    }
    catch(e) {
        // Swallow
    }

    // Params
    const updateParams = {};
    if(change['@name'] !== undefined) updateParams.name = change['@name']?.trim() || null;
    if(change['@firstname'] !== undefined) updateParams.firstName = change['@firstname']?.trim() || null;
    if(change['@middlename'] !== undefined) updateParams.middleName = change['@middlename']?.trim() || null;
    if(change['@lastname'] !== undefined) updateParams.lastName = change['@lastname']?.trim() || null;
    if(change['@address1'] !== undefined) updateParams.address1 = change['@address1']?.trim() || null;
    if(change['@address2'] !== undefined) updateParams.address2 = change['@address2']?.trim() || null;
    if(change['@city'] !== undefined) updateParams.city = change['@city']?.trim() || null;
    if(change['@state'] !== undefined) updateParams.state = change['@state']?.trim() || null;
    if(change['@zip'] !== undefined) updateParams.zipcode = change['@zip']?.trim() || null;
    if(change['@dob'] !== undefined) updateParams.dateofbirth = change['@dob']?.trim() || null;
    if(change['@email'] !== undefined) updateParams.email = change['@email']?.trim() || null;
    if(change['@homephone']?.trim()) updateParams.phone = change['@homephone']?.trim() || null;
    else if(change['@otherphone']?.trim()) updateParams.phone = change['@otherphone']?.trim() || null;
    if(change['@ssn'] !== undefined) updateParams.ssn = formatSSN(change['@ssn']?.trim()) || null;
    if(change['@sex'] !== undefined) updateParams.sex = change['@sex']?.trim() == 'M' ? 'male' : 'female';

    if(primaryInsuranceId) updateParams.carrierId = primaryInsuranceId;
    if(primaryInsurance && primaryInsurance['@carcode'] !== undefined) updateParams.carrierCode = primaryInsurance['@carcode']?.trim() || null;
    if(primaryInsurance && primaryInsurance['@carname'] !== undefined) updateParams.carrierName = primaryInsurance['@carname']?.trim() || null;
    if(primaryInsurance && primaryInsurance['@subidnumber'] !== undefined) updateParams.subscriberId = primaryInsurance['@subidnumber']?.trim() || null;
    if(primaryInsurance && primaryInsurance['@groupname'] !== undefined) updateParams.groupName = primaryInsurance['@groupname']?.trim() || null;
    if(primaryInsurance && primaryInsurance['@groupnumber'] !== undefined) updateParams.groupNumber = primaryInsurance['@groupnumber']?.trim() || null;
    if(secondaryInsuranceId) updateParams.carrierId2 = secondaryInsuranceId;
    if(secondaryInsurance && secondaryInsurance['@carcode'] !== undefined) updateParams.carrierCode2 = secondaryInsurance['@carcode']?.trim() || null;
    if(secondaryInsurance && secondaryInsurance['@carname'] !== undefined) updateParams.carrierName2 = secondaryInsurance['@carname']?.trim() || null;
    if(secondaryInsurance && secondaryInsurance['@subidnumber'] !== undefined) updateParams.subscriberId2 = secondaryInsurance['@subidnumber']?.trim() || null;
    if(secondaryInsurance && secondaryInsurance['@groupname'] !== undefined) updateParams.groupName2 = secondaryInsurance['@groupname']?.trim() || null;
    if(secondaryInsurance && secondaryInsurance['@groupnumber'] !== undefined) updateParams.groupNumber2 = secondaryInsurance['@groupnumber']?.trim() || null;
    
    // Update
    await con.query(`UPDATE patient SET ? WHERE apiId = ?`, [updateParams, patientId]);
}

async function deletePatient(change) {

    const patientId = change['@id'];

    // Update
    await con.query(`DELETE FROM patient WHERE apiId = ?`, [patientId]);
}

async function getOrCreateCarrier(carrierCode, carrierName) {

    // -> Fetch Carrier From DB Using Carrier Code
    const [selectResults] = await con.query(`SELECT * FROM carrier WHERE code = ?`, [carrierCode]);

    const foundCarrier = selectResults[0];

    // -> If Found, return
    if(foundCarrier) {
        return foundCarrier;
    }
    // -> Else If Not Found, Create And Return
    else {

        // Insert Params
        const insertParams = {
            code: carrierCode,
            name: carrierName
        }

        // -> Insert New Carrier
        const [insertResult] = await con.query(`INSERT INTO carrier SET ?`, [insertParams]);

        // New Carrier
        const newCarrier = { ...insertParams, id: insertResult.insertId };

        return newCarrier;

    }
}

async function getLastMdSync(log, reqId) {

    const fnName = 'operations/syncWithMd/getLastMdSync';
    log.info(reqId, `${fnName} ### start`);

    try {

        log.info(reqId, `${fnName} ### [query] get latest MD sync date`);
        // -> Get Latest Sync Date
        const [results] = await con.query(`SELECT mdLastSyncDate FROM app_data WHERE id = 1`);

        // End
        const latestSyncDate = results[0]?.mdLastSyncDate || null;
        log.info(reqId, `${fnName} ### end [success] ### latest sync date: ${latestSyncDate}`);
        return latestSyncDate;
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new DbErrors.DbError(e.message, e);
    }

}

/**
 * Exports
 */
module.exports = {
    syncMdData,
    getLastMdSync
}