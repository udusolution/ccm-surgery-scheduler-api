/**
 * @description helper module that sends patient communications
 * @note created to avoid duplicating this code in various endpoints
 */


const config = require('../config/config');
const Enums = require('../common/enums');
const mail = require('../common/services/mail');
const sms = require('../common/services/sms');
const EmailTemplates = require('../resources/templates/mail/emailTemplates');
const SmsTemplates = require('../resources/templates/sms/smsTemplates');
const moment = require('moment');
const momentTZ = require('moment-timezone');
const path = require('path');


async function sendEmailAndSms(log, reqId, { patientName, patientEmail, facilityName, eventTypeName, dateFrom, coms }) {

    const fnName = 'sendEmailAndSms';
    log.info(reqId, `${fnName} ### start`);

    log.info(reqId, `${fnName} ### start`);
    // -> Call sendEmail & sendSms
    await Promise.all([
        sendEmail(log, reqId, { patientName, patientEmail, facilityName, eventTypeName, dateFrom, coms }),
        sendSms(log, reqId, { patientName, patientEmail, facilityName, eventTypeName, dateFrom, coms }),
    ])

    // End
    log.info(reqId, `${fnName} ### end [success]`);
}

/**
 * send patient communication email
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {{
 *  patientName: string,
 *  patientEmail: string,
 *  facilityName: string,
 *  eventTypeName: string,
 *  dateFrom: string,
 *  coms: [{ title: string, description?: string, file?: string }]
 * }} content 
 */
async function sendEmail(log, reqId, template, { patientName, patientEmail, facilityName, eventTypeName, dateFrom, coms }) {

    const fnName = 'sendEmail';
    log.info(reqId, `${fnName} ### start`);

    log.info(reqId, `calling (EmailTemplates)'patientCom'`);
    // Get email content
    const subject = `${config.identity.name} - Event Scheduled`
    const comsEmailHtml = EmailTemplates.patientCom(template, { 
        patient: patientName,
        facility: facilityName,
        department: eventTypeName,
        dateFrom: momentTZ(dateFrom).tz(Enums.ZoneFormats.MichiganDetroit).format(Enums.DateFormats.dateTimeZ),
        coms
    });
    const attachments = coms.filter(c => c.file).map(c => {
        return {
            filename: path.basename(c.file),
            path: path.join(global.appRoot, 'public', c.file)
        }
    })

    log.info(reqId, `calling (mail)'send'`);
    // -> Send Patient Com Email
    await mail.send(log, reqId, patientEmail, subject, comsEmailHtml, attachments)
        .then(() => {});

    // End
    log.info(reqId, `${fnName} ### end [success]`);

}

async function sendSms(log, reqId, template, { patientName, patientPreferredPhone, facilityName, eventTypeName, dateFrom, coms }) {

    const fnName = 'sendSms';
    log.info(reqId, `${fnName} ### start`);

    log.info(reqId, `calling (SmsTemplates)'patientCom'`);
    // Get sms content
    const comsSmsBody = SmsTemplates.patientCom(template, { 
        patient: patientName,
        facility: facilityName,
        department: eventTypeName,
        dateFrom: momentTZ(dateFrom).tz(Enums.ZoneFormats.MichiganDetroit).format(Enums.DateFormats.dateTimeZ),
        coms
    });

    log.info(reqId, `calling (sms)'send'`);
    // -> Send Patient Com Sms
    await sms.send(log, reqId, patientPreferredPhone, comsSmsBody)
        .then(() => {});

    // End
    log.info(reqId, `${fnName} ### end [success]`);
}


/**
 * Exports
 */
module.exports = {
    sendEmailAndSms,
    sendEmail,
    sendSms
}