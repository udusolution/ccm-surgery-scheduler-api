DROP TABLE `announcement`;
CREATE TABLE `announcement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dateFrom` datetime NOT NULL,
  `dateTo` datetime NOT NULL,
  `title` varchar(512) NOT NULL,
  `message` text NOT NULL,
  `showTo` enum('all','admins','coordinators','physicians') NOT NULL DEFAULT 'all',
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedOn` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;