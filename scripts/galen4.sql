-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 09, 2021 at 03:31 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `galen`
--

-- --------------------------------------------------------

--
-- Table structure for table `blood_thinner`
--

CREATE TABLE `blood_thinner` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blood_thinner`
--

INSERT INTO `blood_thinner` (`id`, `code`, `name`, `createdOn`) VALUES
(3, 'BT123', 'Blood Thinner', '2021-05-31 08:57:21');

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis`
--

CREATE TABLE `diagnosis` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `diagnosis`
--

INSERT INTO `diagnosis` (`id`, `code`, `name`, `createdOn`) VALUES
(1, 'K29.70', 'Gastritis', '2021-05-30 15:24:45');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `dateFrom` datetime NOT NULL,
  `dateTo` datetime NOT NULL,
  `isMoveUpRequested` tinyint(1) NOT NULL DEFAULT 0,
  `eventTypeId` int(11) NOT NULL,
  `eventTypeCode` varchar(256) NOT NULL,
  `eventTypeName` varchar(256) NOT NULL,
  `priorityId` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `priorityText` varchar(256) NOT NULL,
  `facilityId` int(11) NOT NULL,
  `facilityCode` varchar(256) NOT NULL,
  `facilityName` varchar(256) NOT NULL,
  `facilityAddress` varchar(512) NOT NULL,
  `roomId` int(11) NOT NULL,
  `roomCode` varchar(128) NOT NULL,
  `roomName` varchar(256) NOT NULL,
  `physicianId` int(11) NOT NULL,
  `physicianName` varchar(256) NOT NULL,
  `coordinatorId` int(11) NOT NULL,
  `coordinatorName` varchar(256) NOT NULL,
  `patientId` int(11) NOT NULL,
  `patientName` varchar(128) DEFAULT NULL,
  `patientLastName` varchar(128) DEFAULT NULL,
  `patientDoB` datetime DEFAULT NULL,
  `patientGender` varchar(64) DEFAULT NULL,
  `patientAddress` varchar(512) DEFAULT NULL,
  `patientPreferredPhone` varchar(256) DEFAULT NULL,
  `patientSSN` varchar(128) DEFAULT NULL,
  `insuranceId` int(11) DEFAULT NULL,
  `insuranceName` varchar(256) DEFAULT NULL,
  `insuranceInfo` text DEFAULT NULL,
  `authorizationRequired` varchar(64) DEFAULT NULL,
  `diagnosisId` int(11) DEFAULT NULL,
  `diagnosisCode` varchar(64) DEFAULT NULL,
  `diagnosisDescription` varchar(512) DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `isMedicalClearanceNeeded` tinyint(1) DEFAULT NULL,
  `isCardiacClearanceNeeded` tinyint(1) DEFAULT NULL,
  `otherClearance` text DEFAULT NULL,
  `bloodThinnerId` int(11) DEFAULT NULL,
  `bloodThinnerCode` varchar(128) DEFAULT NULL,
  `bloodThinnerDescription` varchar(512) DEFAULT NULL,
  `status` varchar(64) NOT NULL DEFAULT 'pending',
  `isEmergency` tinyint(4) NOT NULL DEFAULT 0,
  `isDraft` tinyint(4) NOT NULL DEFAULT 0,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `dateFrom`, `dateTo`, `isMoveUpRequested`, `eventTypeId`, `eventTypeCode`, `eventTypeName`, `priorityId`, `priority`, `priorityText`, `facilityId`, `facilityCode`, `facilityName`, `facilityAddress`, `roomId`, `roomCode`, `roomName`, `physicianId`, `physicianName`, `coordinatorId`, `coordinatorName`, `patientId`, `patientName`, `patientLastName`, `patientDoB`, `patientGender`, `patientAddress`, `patientPreferredPhone`, `patientSSN`, `insuranceId`, `insuranceName`, `insuranceInfo`, `authorizationRequired`, `diagnosisId`, `diagnosisCode`, `diagnosisDescription`, `comment`, `isMedicalClearanceNeeded`, `isCardiacClearanceNeeded`, `otherClearance`, `bloodThinnerId`, `bloodThinnerCode`, `bloodThinnerDescription`, `status`, `isEmergency`, `isDraft`, `createdOn`, `updatedOn`) VALUES
(3, '2021-06-17 18:51:00', '2021-06-17 21:06:00', 0, 1, 'EV123', 'Surgery', 1, 1, 'High', 1, 'HSP1', 'Saint Terez', 'Hadat Beirut', 5, 'RM2', 'A2', 6, 'user9@mail.com', 3, 'user3@mail.com', 1, 'Amelia Watson', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', 0, 0, '2021-06-03 15:56:00', '2021-06-03 15:56:00'),
(4, '2021-06-14 16:10:00', '2021-06-14 17:10:00', 0, 2, 'EV321', 'Endoscopy', 1, 1, 'High', 1, 'HSP1', 'Saint Terez', 'Hadat Beirut', 1, 'RM1', 'A1', 1, 'user1@mail.com', 3, 'user3@mail.com', 1, 'Amelia Watson', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', 0, 0, '2021-06-03 16:10:58', '2021-06-03 16:10:58'),
(5, '2021-06-17 16:10:00', '2021-06-17 17:10:00', 0, 1, 'EV123', 'Surgery', 3, 3, 'Moderate', 1, 'HSP1', 'Saint Terez', 'Hadat Beirut', 6, 'RM3', 'B1', 1, 'user1@mail.com', 3, 'user3@mail.com', 1, 'Amelia Watson', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', 1, 0, '2021-06-03 16:14:11', '2021-06-03 16:14:11'),
(6, '2021-06-25 16:23:00', '2021-06-25 17:23:00', 0, 2, 'EV321', 'Endoscopy', 1, 1, 'High', 1, 'HSP1', 'Saint Terez', 'Hadat Beirut', 6, 'RM3', 'B1', 4, 'user4@mail.com', 3, 'user3@mail.com', 1, 'Amelia Watson', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', 0, 0, '2021-06-03 16:23:53', '2021-06-03 16:23:53'),
(7, '2021-06-16 09:43:00', '2021-06-16 11:13:00', 0, 1, 'EV123', 'Surgery', 3, 3, 'Moderate', 1, 'HSP1', 'Saint Terez', 'Hadat Beirut', 5, 'RM2', 'A2', 4, 'user4@mail.com', 3, 'user3@mail.com', 1, 'Amelia Watson', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', 0, 0, '2021-06-04 09:43:13', '2021-06-04 09:43:13'),
(8, '2021-06-17 07:40:00', '2021-06-17 08:40:00', 0, 1, 'EV123', 'Surgery', 3, 3, 'Moderate', 1, 'HSP1', 'Saint Terez', 'Hadat Beirut', 5, 'RM2', 'A2', 6, 'user9@mail.com', 3, 'user3@mail.com', 1, 'Amelia Watson', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', 0, 0, '2021-06-04 12:40:33', '2021-06-04 12:40:33'),
(9, '2021-06-17 19:30:00', '2021-06-17 20:30:00', 0, 1, 'EV123', 'Surgery', 1, 1, 'High', 1, 'HSP1', 'Saint Terez', 'Hadat Beirut', 1, 'RM1', 'A1', 6, 'user9@mail.com', 3, 'user3@mail.com', 1, 'Amelia Watson', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', 0, 0, '2021-06-04 12:42:28', '2021-06-04 12:42:28'),
(10, '2021-06-17 18:42:00', '2021-06-17 19:42:00', 0, 1, 'EV123', 'Surgery', 3, 3, 'Moderate', 1, 'HSP1', 'Saint Terez', 'Hadat Beirut', 1, 'RM1', 'A1', 4, 'user4@mail.com', 3, 'user3@mail.com', 1, 'Amelia Watson', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', 1, 0, '2021-06-04 13:46:49', '2021-06-04 13:46:49'),
(11, '2021-06-23 13:11:00', '2021-06-23 14:11:00', 0, 1, 'EV123', 'Surgery', 1, 1, 'High', 1, 'HSP1', 'Saint Terez', 'Hadat Beirut', 1, 'RM1', 'A1', 6, 'user9@mail.com', 3, 'user3@mail.com', 1, 'Amelia Watson', NULL, '1995-05-24 00:00:00', NULL, 'Main Street 123', '1117685946', '123456', NULL, NULL, 'asfasf', NULL, NULL, NULL, NULL, NULL, 0, 1, 'This is other clearance', NULL, NULL, NULL, 'pending', 0, 0, '2021-06-08 13:11:24', '2021-06-08 13:11:24');

-- --------------------------------------------------------

--
-- Table structure for table `event_procedure`
--

CREATE TABLE `event_procedure` (
  `id` int(11) NOT NULL,
  `eventId` int(11) NOT NULL,
  `procedureId` int(11) NOT NULL,
  `procedureCode` varchar(256) NOT NULL,
  `procedureName` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event_procedure`
--

INSERT INTO `event_procedure` (`id`, `eventId`, `procedureId`, `procedureCode`, `procedureName`, `createdOn`) VALUES
(9, 11, 1, '45378', 'Colonoscopy', '2021-06-09 15:05:24'),
(10, 11, 4, '11111', 'Procedure 1', '2021-06-09 15:06:40');

-- --------------------------------------------------------

--
-- Table structure for table `event_type`
--

CREATE TABLE `event_type` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event_type`
--

INSERT INTO `event_type` (`id`, `code`, `name`, `createdOn`) VALUES
(1, 'EV123', 'Surgery', '2021-05-30 15:04:18'),
(2, 'EV321', 'Endoscopy', '2021-05-30 15:04:30');

-- --------------------------------------------------------

--
-- Table structure for table `facility`
--

CREATE TABLE `facility` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `address` varchar(256) NOT NULL,
  `schedulingOfficePhone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `facility`
--

INSERT INTO `facility` (`id`, `code`, `name`, `address`, `schedulingOfficePhone`, `fax`, `createdOn`) VALUES
(1, 'HSP1', 'Saint Terez', 'Hadat Beirut', '1234567890', '0987654321', '2021-05-30 13:34:56'),
(4, 'HSP2', 'Hotel Duo', 'Beirut', '1113452456', '1117856374', '2021-06-01 13:45:57');

-- --------------------------------------------------------

--
-- Table structure for table `facility_room`
--

CREATE TABLE `facility_room` (
  `id` int(11) NOT NULL,
  `facilityId` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `facility_room`
--

INSERT INTO `facility_room` (`id`, `facilityId`, `code`, `name`, `createdOn`) VALUES
(1, 1, 'RM1', 'A1', '2021-06-01 10:18:35'),
(5, 1, 'RM2', 'A2', '2021-06-01 13:56:45'),
(6, 1, 'RM3', 'B1', '2021-06-01 13:56:51'),
(7, 4, 'RM44', 'Emergency Room 1', '2021-06-04 13:26:57');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `actionType` varchar(64) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `extra1` text DEFAULT NULL,
  `extra2` int(11) DEFAULT NULL,
  `extra3` int(11) DEFAULT NULL,
  `extra4` int(11) DEFAULT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `userId`, `actionType`, `description`, `extra1`, `extra2`, `extra3`, `extra4`, `createdOn`) VALUES
(1, 3, 'AddMasterData', 'Added master data entry \"Urgent\" to master data \"priority\"', NULL, NULL, NULL, NULL, '2021-06-02 10:34:20'),
(2, 3, 'AddMasterData', 'Added master data entry \"Diagnosis 2\" to \"diagnosis\"', NULL, NULL, NULL, NULL, '2021-06-02 10:37:18'),
(3, 3, 'AddMasterData', 'Added master data entry \"Blood Thinner New\" to \"blood_thinner\"', NULL, NULL, NULL, NULL, '2021-06-02 10:40:52'),
(4, 3, 'DeleteMasterData', 'Deleted master data entry from \"blood_thinner\"', NULL, NULL, NULL, NULL, '2021-06-02 10:40:54'),
(5, 3, 'AddEvent', 'Added event #6 for patient \"undefined\"', NULL, NULL, NULL, NULL, '2021-06-03 16:23:53'),
(6, 3, 'AddEvent', 'Added event #7 for patient #1', NULL, NULL, NULL, NULL, '2021-06-04 09:43:13'),
(7, 3, 'AddEvent', 'Added event #8 for patient #1', NULL, NULL, NULL, NULL, '2021-06-04 12:40:33'),
(8, 3, 'AddEvent', 'Added event #9 for patient #1', NULL, NULL, NULL, NULL, '2021-06-04 12:42:28'),
(9, 3, 'AddMasterData', 'Added master data entry \"Emergency Room 1\" to \"facility_room\"', NULL, NULL, NULL, NULL, '2021-06-04 13:26:57'),
(10, 3, 'AddEvent', 'Added event #10 for patient #1', NULL, NULL, NULL, NULL, '2021-06-04 13:46:49'),
(11, 3, 'AddEvent', 'Added event #11 for patient #1', NULL, NULL, NULL, NULL, '2021-06-08 13:11:24'),
(12, 3, 'AddMasterData', 'Added master data entry \"Procedure 1\" to \"procedures\"', NULL, NULL, NULL, NULL, '2021-06-09 10:46:32'),
(13, 3, 'AddMasterData', 'Added master data entry \"Procedure 2\" to \"procedures\"', NULL, NULL, NULL, NULL, '2021-06-09 10:46:38'),
(14, 3, 'AddMasterData', 'Added master data entry \"Procedure 3\" to \"procedures\"', NULL, NULL, NULL, NULL, '2021-06-09 10:46:44'),
(15, 3, 'AddMasterData', 'Added master data entry \"Procedure 4\" to \"procedures\"', NULL, NULL, NULL, NULL, '2021-06-09 10:46:50'),
(16, 3, 'AddMasterData', 'Added master data entry \"Procedure 5\" to \"procedures\"', NULL, NULL, NULL, NULL, '2021-06-09 10:46:55');

-- --------------------------------------------------------

--
-- Table structure for table `insurance`
--

CREATE TABLE `insurance` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `insurance`
--

INSERT INTO `insurance` (`id`, `code`, `name`, `createdOn`) VALUES
(1, 'INS124', 'Medicare', '2021-05-30 15:09:40');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `address1` varchar(256) NOT NULL,
  `address2` varchar(256) NOT NULL,
  `city` varchar(32) NOT NULL,
  `state` varchar(32) NOT NULL,
  `zipcode` varchar(32) NOT NULL,
  `dateofbirth` date NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `email` varchar(256) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `ssn` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `name`, `address1`, `address2`, `city`, `state`, `zipcode`, `dateofbirth`, `createdOn`, `email`, `phone`, `ssn`) VALUES
(1, 'Amelia Watson', 'Main Street 123', 'Bld 123', 'Detroit', 'Michigan', '12345678', '1995-05-24', '2021-05-28 16:12:35', 'ame.wat@mail.com', '1117685946', '123456'),
(7, 'John Doe', 'Add1', 'Add2', 'Boston', 'Washington', '12234556', '1976-06-25', '2021-06-08 12:55:24', 'john.doe@mail.com', '12345654321', '666777');

-- --------------------------------------------------------

--
-- Table structure for table `patient_allergy`
--

CREATE TABLE `patient_allergy` (
  `id` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `status` varchar(128) NOT NULL,
  `allergyDate` datetime NOT NULL,
  `reaction` varchar(512) NOT NULL,
  `rxNormId` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient_allergy`
--

INSERT INTO `patient_allergy` (`id`, `patientId`, `name`, `status`, `allergyDate`, `reaction`, `rxNormId`) VALUES
(1, 1, 'Peanut Allergy', 'active', '2020-02-04 00:00:00', 'Swelling', 'RX123'),
(2, 1, 'Milk Allergy', 'active', '2017-11-06 00:00:00', 'Fever', 'RX321'),
(3, 1, 'Adrenaline Allergy', 'serious', '2018-03-26 00:00:00', 'Low blood pressure, light headedness', 'RX456');

-- --------------------------------------------------------

--
-- Table structure for table `priority`
--

CREATE TABLE `priority` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `priority` int(11) NOT NULL DEFAULT 3
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `priority`
--

INSERT INTO `priority` (`id`, `code`, `name`, `createdOn`, `priority`) VALUES
(1, 'PR123', 'High', '2021-05-30 15:33:24', 1),
(3, 'PR124', 'Moderate', '2021-06-01 09:03:20', 3),
(4, 'PR125', 'Low', '2021-06-01 09:03:36', 5);

-- --------------------------------------------------------

--
-- Table structure for table `procedures`
--

CREATE TABLE `procedures` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `procedures`
--

INSERT INTO `procedures` (`id`, `code`, `name`, `createdOn`) VALUES
(1, '45378', 'Colonoscopy', '2021-05-30 15:15:19'),
(4, '11111', 'Procedure 1', '2021-06-09 10:46:32'),
(5, '22222', 'Procedure 2', '2021-06-09 10:46:38'),
(6, '33333', 'Procedure 3', '2021-06-09 10:46:44'),
(7, '44444', 'Procedure 4', '2021-06-09 10:46:50'),
(8, '55555', 'Procedure 5', '2021-06-09 10:46:55');

-- --------------------------------------------------------

--
-- Table structure for table `refresh`
--

CREATE TABLE `refresh` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `token` varchar(512) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `refresh`
--

INSERT INTO `refresh` (`id`, `userId`, `token`, `createdOn`) VALUES
(26, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjA5NjQyNCwiZXhwIjoxNjI0Njg4NDI0fQ.6K-UGRqUqDBx0U3fGCnHFJ5bOiSUalhX_UoCTyuwP1c', '2021-05-27 09:20:24'),
(27, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjA5NjU4MiwiZXhwIjoxNjI0Njg4NTgyfQ.YFNaRs793Rla-5sdilcVjxs2pj-0gOalkK7TLOZX_pQ', '2021-05-27 09:23:02'),
(28, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjA5Njk5MywiZXhwIjoxNjI0Njg4OTkzfQ.hOoZJ8vmOYOidl3bb1H16bi0W4JXLEyjP8-11v6HLJo', '2021-05-27 09:29:53'),
(30, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjA5OTg2MCwiZXhwIjoxNjI0NjkxODYwfQ.vnFOY9yt-SlYsblidhdcv1wpDz0FHrF17m_d9j8d-j8', '2021-05-27 10:17:40'),
(31, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDA1NCwiZXhwIjoxNjI0NjkyMDU0fQ.Cu4m5oL1t4hj7L0NCNgfUUgL3njbe_ZjYStWOWIqDqo', '2021-05-27 10:20:54'),
(32, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDA1NywiZXhwIjoxNjI0NjkyMDU3fQ.HYsO7kVqzsDiJCXroqUS7z76rp_aRjDDHZcLVWBaNNs', '2021-05-27 10:20:57'),
(33, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDA5OSwiZXhwIjoxNjI0NjkyMDk5fQ.fyk9s8Z-h9We7-MvQ5uu-JnKNqynWKA7BWRQiOFJAnU', '2021-05-27 10:21:39'),
(34, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE0NiwiZXhwIjoxNjI0NjkyMTQ2fQ.LN0QiI8R_l6zxR1gZKvEIwjcZsfDu5UlxcsQ_le2tXI', '2021-05-27 10:22:26'),
(35, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE1MywiZXhwIjoxNjI0NjkyMTUzfQ.ZSAnN2ryX_TeI1z4JIKPiIJ1-_zn01n_EpI5pF7EVDk', '2021-05-27 10:22:33'),
(36, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE2MywiZXhwIjoxNjI0NjkyMTYzfQ.i3SUdlUBBMAVLWD-gAuLFvRh7DpXkMq_OyY7ezlcm3s', '2021-05-27 10:22:43'),
(37, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE3MiwiZXhwIjoxNjI0NjkyMTcyfQ.ySVBEbmZ3eF8OTuQaghVRc8DUY6p3j19vv3JE6bZb7A', '2021-05-27 10:22:52'),
(38, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE5NywiZXhwIjoxNjI0NjkyMTk3fQ.u6uE22cxGuSWbQS-yPQUcbk1MWxU9oitgvmDHLH47Nw', '2021-05-27 10:23:17'),
(39, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDIzMiwiZXhwIjoxNjI0NjkyMjMyfQ.utGNd9y7tPLwL4YhIlwZ7ky6h4y63dmEuyNC8tJx18o', '2021-05-27 10:23:52'),
(40, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDI2MiwiZXhwIjoxNjI0NjkyMjYyfQ.A7mqDlOS5wu67QRPhSkVCVL8tl4n4tOnF39FKrQTDuc', '2021-05-27 10:24:22'),
(41, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDQyOCwiZXhwIjoxNjI0NjkyNDI4fQ.PFKbOpoqWJZ_z1QvrZhfbV2KkPxVS3nC7beEiy0MyOc', '2021-05-27 10:27:08'),
(42, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDQ2NCwiZXhwIjoxNjI0NjkyNDY0fQ.13V9fVCeZWiEqI6oJIR71Mr3naNc0EWBf1HrBrQc9Vw', '2021-05-27 10:27:44'),
(43, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDU4MywiZXhwIjoxNjI0NjkyNTgzfQ.CJfOyeK2zufVl8EZoS1x8XiLeXHmaEFWjhQytRDKmBY', '2021-05-27 10:29:43'),
(44, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDY4MywiZXhwIjoxNjI0NjkyNjgzfQ.o3S7gTjdWz-gJ0tk63NlwZpVr7e2nxuaosfO7gHkFIk', '2021-05-27 10:31:23'),
(45, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDY5MSwiZXhwIjoxNjI0NjkyNjkxfQ.uNw_JkfyP0siAJ3os1iW8-Tcwu0-D91jX3CAkFG5PSI', '2021-05-27 10:31:31'),
(46, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDc4NywiZXhwIjoxNjI0NjkyNzg3fQ.o0nynrypSLFMYlcL7fw5lPRJ3MaLHtqxW2IbfxwZafk', '2021-05-27 10:33:07'),
(47, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDkwNywiZXhwIjoxNjI0NjkyOTA3fQ.geqnrMMYSrBvwOuEhYQ85pojgGBm9Lh7NnSwmEfcNp8', '2021-05-27 10:35:07'),
(48, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMjA4OSwiZXhwIjoxNjI0Njk0MDg5fQ.Q_k9G9VL3v1EcWLopoWEcOCnrth_3OIM1XVM9hzllac', '2021-05-27 10:54:49'),
(100, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMzE0Mjg4NSwiZXhwIjoxNjIzMjI5Mjg1fQ.LnfRLS1a6QPkqX-4RvWkgV_YwVCYyE5bCV-dTeOll_Y', '2021-06-08 12:01:25'),
(101, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMzIzMTA5OCwiZXhwIjoxNjIzMzE3NDk4fQ.Qcq51mQQpslVSFupekNn66NZ8c_KCZklVFy_lhmv8iw', '2021-06-09 12:31:38');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `password` varchar(256) NOT NULL,
  `avatar` varchar(128) DEFAULT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `phone`, `password`, `avatar`, `createdOn`) VALUES
(1, 'user1@mail.com', '1234567890', '$2b$10$yFc7Sg7WNxrC5t5cUAs5JupzNdWj/MrYHz8L1/MFX/zBncdsfwiga', NULL, '2021-05-27 12:47:25'),
(2, 'user2@mail.com', '1234567891', '$2b$10$8MtPf2IXeFXOcHbQRcQyUumOjfZ3n/ugtrID6SWjlZwmTNqCZqcN.', NULL, '2021-06-01 12:47:25'),
(3, 'user3@mail.com', '1234567897', '$2b$10$VeFW/IJe8LxoiRPDXVzWMOwdYqUx0.Tsq0Tkxkf8kJKv/INue7eoa', NULL, '2021-06-01 12:47:25'),
(4, 'user4@mail.com', '1234567894', '$2b$10$btFaK1cJrdN9.cI3clA9W.MKg1UGPrzIsFkjzFB6p1fPGHxs1FSLC', NULL, '2021-05-28 12:47:25'),
(5, 'user8@mail.com', '70123456', '$2b$10$gMwjr2X1hVxwb5HEpK.nGONilhP6tLv7eoYQsPf3w134P5AjlIiV6', NULL, '2021-06-01 12:47:25'),
(6, 'user9@mail.com', '70123457', '$2b$10$NVfMcQIHto.ScESG.z4BFuQtzs91i.gTKI0LchTnp/6/bbnuA1o3W', NULL, '2021-06-01 12:47:25'),
(7, 'user10@mail.com', '1234567899', '$2b$10$GPY8E9ngLoIUK5n1HQEBfutnFah0ko5s.hK3oYjReobFiNaezwHhW', NULL, '2021-06-01 12:47:25'),
(8, 'user11@mail.com', '70123452', '$2b$10$K7fpE1Bz/rvh6hlxE5SegO74Er.liaTwGLcXo3d0oObmCv8aYsk2a', NULL, '2021-06-01 12:47:25');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `role` varchar(32) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `userId`, `role`, `createdOn`) VALUES
(24, 3, 'coordinator', '2021-05-31 15:37:36'),
(25, 3, 'admin', '2021-05-31 15:37:36'),
(26, 7, 'coordinator', '2021-05-31 16:29:59'),
(27, 6, 'physician', '2021-06-01 08:53:10'),
(28, 4, 'physician', '2021-06-01 08:53:16'),
(29, 1, 'physician', '2021-06-01 08:54:46');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blood_thinner`
--
ALTER TABLE `blood_thinner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diagnosis`
--
ALTER TABLE `diagnosis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eventTypeId` (`eventTypeId`,`priorityId`,`facilityId`,`physicianId`,`coordinatorId`,`patientId`,`insuranceId`,`diagnosisId`,`bloodThinnerId`),
  ADD KEY `facilityId` (`facilityId`),
  ADD KEY `patientId` (`patientId`),
  ADD KEY `coordinatorId` (`coordinatorId`),
  ADD KEY `physicianId` (`physicianId`),
  ADD KEY `insuranceId` (`insuranceId`),
  ADD KEY `priorityId` (`priorityId`),
  ADD KEY `diagnosisId` (`diagnosisId`),
  ADD KEY `bloodThinnerId` (`bloodThinnerId`),
  ADD KEY `roomId` (`roomId`);

--
-- Indexes for table `event_procedure`
--
ALTER TABLE `event_procedure`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eventId` (`eventId`),
  ADD KEY `procedureId` (`procedureId`);

--
-- Indexes for table `event_type`
--
ALTER TABLE `event_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility`
--
ALTER TABLE `facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility_room`
--
ALTER TABLE `facility_room`
  ADD PRIMARY KEY (`id`),
  ADD KEY `facilityId` (`facilityId`),
  ADD KEY `facilityId_2` (`facilityId`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance`
--
ALTER TABLE `insurance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_allergy`
--
ALTER TABLE `patient_allergy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patientId` (`patientId`);

--
-- Indexes for table `priority`
--
ALTER TABLE `priority`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procedures`
--
ALTER TABLE `procedures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refresh`
--
ALTER TABLE `refresh`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`),
  ADD KEY `token` (`token`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blood_thinner`
--
ALTER TABLE `blood_thinner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `diagnosis`
--
ALTER TABLE `diagnosis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `event_procedure`
--
ALTER TABLE `event_procedure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `event_type`
--
ALTER TABLE `event_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `facility`
--
ALTER TABLE `facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `facility_room`
--
ALTER TABLE `facility_room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `insurance`
--
ALTER TABLE `insurance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `patient_allergy`
--
ALTER TABLE `patient_allergy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `priority`
--
ALTER TABLE `priority`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `procedures`
--
ALTER TABLE `procedures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `refresh`
--
ALTER TABLE `refresh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=102;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `event_ibfk_1` FOREIGN KEY (`facilityId`) REFERENCES `facility` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_10` FOREIGN KEY (`roomId`) REFERENCES `facility_room` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_2` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_3` FOREIGN KEY (`coordinatorId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_4` FOREIGN KEY (`physicianId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_5` FOREIGN KEY (`insuranceId`) REFERENCES `insurance` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_6` FOREIGN KEY (`priorityId`) REFERENCES `priority` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_7` FOREIGN KEY (`diagnosisId`) REFERENCES `diagnosis` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_8` FOREIGN KEY (`eventTypeId`) REFERENCES `event_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_9` FOREIGN KEY (`bloodThinnerId`) REFERENCES `blood_thinner` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `event_procedure`
--
ALTER TABLE `event_procedure`
  ADD CONSTRAINT `event_procedure_ibfk_1` FOREIGN KEY (`eventId`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_procedure_ibfk_2` FOREIGN KEY (`procedureId`) REFERENCES `procedures` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `facility_room`
--
ALTER TABLE `facility_room`
  ADD CONSTRAINT `facility_room_ibfk_1` FOREIGN KEY (`facilityId`) REFERENCES `facility` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `patient_allergy`
--
ALTER TABLE `patient_allergy`
  ADD CONSTRAINT `patient_allergy_ibfk_1` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
