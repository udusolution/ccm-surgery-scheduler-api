-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2021 at 03:47 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `galen2`
--

-- --------------------------------------------------------

--
-- Table structure for table `blood_thinner`
--

CREATE TABLE `blood_thinner` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` text DEFAULT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blood_thinner`
--

INSERT INTO `blood_thinner` (`id`, `code`, `name`, `description`, `createdOn`) VALUES
(26, 'Plavix', 'Plavix 75mg', 'Patient needs to stop taking Plavix 75mg 7 days before surgery.', '2021-06-17 12:17:33'),
(27, 'ASA325', 'Aspirin 325mg', 'Patient needs to stop taking Aspirin 325mg 7 days before surgery.', '2021-06-17 12:17:33'),
(28, 'ASA81', 'Aspirin 81mg', 'No need to stop aspirin 81mg. Patient to continue taking normally.', '2021-06-17 12:17:33'),
(29, 'Eliquis', 'Eliquis', 'Patient needs to stop taking Eliquis 3 days before surgery.', '2021-06-17 12:17:33'),
(30, 'Coumadin', 'Coumadin', 'Patient needs to stop taking Coumadin 4 days before surgery.', '2021-06-17 12:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `carrier`
--

CREATE TABLE `carrier` (
  `id` int(11) NOT NULL,
  `code` varchar(256) NOT NULL,
  `name` varchar(512) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `carrier`
--

INSERT INTO `carrier` (`id`, `code`, `name`, `createdOn`) VALUES
(633, 'AAA', 'AAA OF MICHIGAN', '2021-06-17 12:17:33'),
(634, 'AAR01', 'AARP', '2021-06-17 12:17:33'),
(635, 'ABS', 'ABS', '2021-06-17 12:17:33'),
(636, 'ABSO', 'ABSOLUTE TOTAL CARE', '2021-06-17 12:17:33'),
(637, 'ACI', 'ACI', '2021-06-17 12:17:33'),
(638, 'AET05', 'AETNA BETTER HEALTH OF MICHIGAN', '2021-06-17 12:17:33'),
(639, 'AET07C1', 'AETNA', '2021-06-17 12:17:33'),
(640, 'AETSS', 'AETNA SENIOR SUP INS', '2021-06-17 12:17:33'),
(641, 'AIG', 'AIG', '2021-06-17 12:17:33'),
(642, 'ALL01', 'ALLIED BENEFIT SYSTEM', '2021-06-17 12:17:33'),
(643, 'ALL08', 'ALL SAVERS INSURANCE', '2021-06-17 12:17:33'),
(644, 'AME14', 'AMERIHEALTH ADMINISTRATORS', '2021-06-17 12:17:33'),
(645, 'AME35', 'AMERICAS CHOICE HEALTHPLAN', '2021-06-17 12:17:33'),
(646, 'AMHLT', 'AMERIHEALTH VIP', '2021-06-17 12:17:33'),
(647, 'APL', 'AMERICAN PUBLIC LIFE', '2021-06-17 12:17:33'),
(648, 'APOS', 'APOSTROPHE', '2021-06-17 12:17:33'),
(649, 'APWU', 'APWU', '2021-06-17 12:17:33'),
(650, 'ASR01', 'ASR PHYSICIANS CARE', '2021-06-17 12:17:33'),
(651, 'ASRM', 'ASRM', '2021-06-17 12:17:33'),
(652, 'BANFID', 'BANKERS FIDELITY', '2021-06-17 12:17:33'),
(653, 'BANKERS', 'BANKERS LIFE & CASUALTY', '2021-06-17 12:17:33'),
(654, 'BCC', 'BLUE CROSS COMPLETE', '2021-06-17 12:17:33'),
(655, 'BCN', 'BLUE CARE NETWORK HMO', '2021-06-17 12:17:33'),
(656, 'BNKFID', 'BANKERS FIDELITY', '2021-06-17 12:17:33'),
(657, 'CALP', 'CALPERS', '2021-06-17 12:17:33'),
(658, 'CDB', 'CUSTOM DESIGN BENEFITS', '2021-06-17 12:17:33'),
(659, 'CHA03', 'CHAMPVA', '2021-06-17 12:17:33'),
(660, 'CHEST', 'CHESTERFIELD RESOURCES INC', '2021-06-17 12:17:33'),
(661, 'CIG02', 'CIGNA', '2021-06-17 12:17:33'),
(662, 'CIG08', 'CIGNA HEALTH PLANS', '2021-06-17 12:17:33'),
(663, 'CITZ', 'CITIZENS', '2021-06-17 12:17:33'),
(664, 'CLCH', 'CLAIM CHOICE', '2021-06-17 12:17:33'),
(665, 'COL', 'COLONIAL PENN', '2021-06-17 12:17:33'),
(666, 'COLHLTH', 'COLONIAL HEALTH', '2021-06-17 12:17:33'),
(667, 'COM28', 'COMMUNITY CARE ASSOCIATES', '2021-06-17 12:17:33'),
(668, 'COMP', 'COMP RISK SERV - REVIEW WORKS', '2021-06-17 12:17:33'),
(669, 'CONSUMER', 'CONSUMERS MUTUAL INS CO', '2021-06-17 12:17:33'),
(670, 'CONWE', 'CONTINENTAL WESTERN', '2021-06-17 12:17:33'),
(671, 'COR', 'CORE SOURCE', '2021-06-17 12:17:33'),
(672, 'CORP', 'CORPORATE BENEFIT SERVICES OF AMERI', '2021-06-17 12:17:33'),
(673, 'COU03V1', 'COVENTRY HEALTH CARE / FIRST HEALTH', '2021-06-17 12:17:33'),
(674, 'CSHL', 'CENTRAL STATES HEALTH AND LIFE', '2021-06-17 12:17:33'),
(675, 'EGP', 'EGP ENTERPRISE GROUP', '2021-06-17 12:17:33'),
(676, 'EHIM', 'EHIM', '2021-06-17 12:17:33'),
(677, 'EMPLOY', 'EMPLOYEE BENEFIT LOGISTIC', '2021-06-17 12:17:33'),
(678, 'EQUIP', 'EQUIPOINT', '2021-06-17 12:17:33'),
(679, 'FARM', 'FARM BUREAU', '2021-06-17 12:17:33'),
(680, 'FCPC', 'FCPC', '2021-06-17 12:17:33'),
(681, 'FID03', 'FIDELIS SECURE CARE', '2021-06-17 12:17:33'),
(682, 'FREE', 'FREEDOM LIFE', '2021-06-17 12:17:33'),
(683, 'GBI', 'GALLAGHER BASSETT', '2021-06-17 12:17:33'),
(684, 'GEHA', 'GEHA', '2021-06-17 12:17:33'),
(685, 'GIL01', 'GILSBAR', '2021-06-17 12:17:33'),
(686, 'GLO01', 'GLOBAL CARE', '2021-06-17 12:17:33'),
(687, 'GOLD2', 'GOLDEN RULE UHC', '2021-06-17 12:17:33'),
(688, 'GRI', 'GRAND RIVER INSURANCE', '2021-06-17 12:17:33'),
(689, 'GRPRES', 'GROUP RESOURCES', '2021-06-17 12:17:33'),
(690, 'HEA41D', 'HEALTH NET', '2021-06-17 12:17:33'),
(691, 'HEA45C1', 'HEALTH PLAN OF MICHIGAN', '2021-06-17 12:17:33'),
(692, 'HEALT', 'HEALTHSCOPE ADMINISTRATORS', '2021-06-17 12:17:33'),
(693, 'HLA', 'HARTFORD LIFE/MERCER HEALTH', '2021-06-17 12:17:33'),
(694, 'HPN', 'HEALTH PLAN OF NEVADA', '2021-06-17 12:17:33'),
(695, 'HRTFD', 'THE HARTFORD', '2021-06-17 12:17:33'),
(696, 'HSPR', 'HEALTH SPRING', '2021-06-17 12:17:33'),
(697, 'HUM01C1', 'HUMANA COMMERCIAL', '2021-06-17 12:17:33'),
(698, 'HUMA1', 'HUMANA MEDICARE', '2021-06-17 12:17:33'),
(699, 'IHCSOL', 'IHC SOLUTIONS', '2021-06-17 12:17:33'),
(700, 'JPFAR', 'JP FARLEY CORP', '2021-06-17 12:17:33'),
(701, 'KEY', 'KEY BENEFIT ADMINISTRATORS', '2021-06-17 12:17:33'),
(702, 'L', 'LOOMIS', '2021-06-17 12:17:33'),
(703, 'LEG', 'LEGION LIMIT MEDICAL', '2021-06-17 12:17:33'),
(704, 'LIB', 'LIBERTY MUTUAL', '2021-06-17 12:17:33'),
(705, 'LIBUN', 'LIBERTY  UNION LIFE', '2021-06-17 12:17:33'),
(706, 'LUM', 'LUMICO', '2021-06-17 12:17:33'),
(707, 'MAN', 'MANHATTAN LIFE', '2021-06-17 12:17:33'),
(708, 'MCL01', 'MCLAREN HEALTH PLANS', '2021-06-17 12:17:33'),
(709, 'MCL02', 'MCLAREN MEDICAID', '2021-06-17 12:17:33'),
(710, 'MCRADV', 'DO NOT USE', '2021-06-17 12:17:33'),
(711, 'MCWF', 'MOTOR CITY WELFARE FUND', '2021-06-17 12:17:33'),
(712, 'MED', 'LIFE MEDICO CORP LIFE INS', '2021-06-17 12:17:33'),
(713, 'MED13', 'MEDICARE PLUS BLUE', '2021-06-17 12:17:33'),
(714, 'MEDI', 'MEDISHARE', '2021-06-17 12:17:33'),
(715, 'MEDICO', 'MEDICO CORP LIFE INS', '2021-06-17 12:17:33'),
(716, 'MEDMUT', 'MEDICAL MUTUAL', '2021-06-17 12:17:33'),
(717, 'MEEMIC', 'MEEMIC AUTO', '2021-06-17 12:17:33'),
(718, 'MER02', 'MERITAIN HEALTH', '2021-06-17 12:17:33'),
(719, 'MERID1', 'MERIDIAN HEALTH PLAN', '2021-06-17 12:17:33'),
(720, 'MIC02C1', 'BLUE CROSS BLUE SHIELD', '2021-06-17 12:17:33'),
(721, 'MIC03', 'DO NOT USE', '2021-06-17 12:17:33'),
(722, 'MIC04C1', 'MICHIGAN MEDICARE', '2021-06-17 12:17:33'),
(723, 'MIC07C', 'MEDICAID', '2021-06-17 12:17:33'),
(724, 'MICH', 'MICHIGAN COMPLETE HEALTH', '2021-06-17 12:17:33'),
(725, 'MID06', 'MIDWEST HAP EMPOWERED', '2021-06-17 12:17:33'),
(726, 'MOL12', 'MOLINA', '2021-06-17 12:17:33'),
(727, 'MULT1', 'MULTIPLAN', '2021-06-17 12:17:33'),
(728, 'MUT01', 'MUTUAL OF OMAHA', '2021-06-17 12:17:33'),
(729, 'MWV', 'MEDICAID WEST VIRGINIA', '2021-06-17 12:17:33'),
(730, 'NASI', 'NATIONAL AUTOMATIC SPRINKLER IND', '2021-06-17 12:17:33'),
(731, 'NAT11', 'NATIONAL ASSOCIATION OF LETTER', '2021-06-17 12:17:33'),
(732, 'NEB', 'NEBRASKA MEDICAID', '2021-06-17 12:17:33'),
(733, 'NEIHBP', 'NATIONAL ELEVATOR INDUSTRY', '2021-06-17 12:17:33'),
(734, 'NIPPON', 'NIPPON LIFE INS', '2021-06-17 12:17:33'),
(735, 'OPTIMED', 'OPTIMED', '2021-06-17 12:17:33'),
(736, 'OXF', 'OXFORD UHC', '2021-06-17 12:17:33'),
(737, 'PACE', 'PACE SOUTHEASTERN MICHIGAN', '2021-06-17 12:17:33'),
(738, 'PACIF', 'PACIFICARE', '2021-06-17 12:17:33'),
(739, 'PAI01', 'PAI', '2021-06-17 12:17:33'),
(740, 'PAL', 'PAN AMERICAN LIFE', '2021-06-17 12:17:33'),
(741, 'PARA', 'PARADIGM MGNT SERVICE LLC', '2021-06-17 12:17:33'),
(742, 'PARAM', 'PARAMOUNT INSURANCE', '2021-06-17 12:17:33'),
(743, 'PHCS', 'PHCS', '2021-06-17 12:17:33'),
(744, 'PHIL', 'PHILADELPHIA AMERICAN LIFE', '2021-06-17 12:17:33'),
(745, 'PIF', 'PAINTERS INSURANCE FUND', '2021-06-17 12:17:33'),
(746, 'PRE18', 'PREFERRED PROVIDER', '2021-06-17 12:17:33'),
(747, 'PREM', 'PREMIER ACCESS', '2021-06-17 12:17:33'),
(748, 'PRI10V1', 'PRIORITY HEALTH', '2021-06-17 12:17:33'),
(749, 'PROGR', 'PROGRESSIVE AUTO INSURANCE', '2021-06-17 12:17:33'),
(750, 'RAIL', 'RAILROAD MEDICARE', '2021-06-17 12:17:33'),
(751, 'RELI', 'RELIANCE STANDARD LIFE', '2021-06-17 12:17:33'),
(752, 'RMA', 'RELIANCE MEDICARE ADV', '2021-06-17 12:17:33'),
(753, 'SHEET', 'SHEET METAL WORKERS', '2021-06-17 12:17:33'),
(754, 'SHP', 'SECURITY HEALTH PLAN', '2021-06-17 12:17:33'),
(755, 'SOMPO', 'SOMPO GLOBAL RISK', '2021-06-17 12:17:33'),
(756, 'STATE', 'STATE FARM GEORGIA', '2021-06-17 12:17:33'),
(757, 'STAWEL', 'STAYWELL HEALTH PLAN', '2021-06-17 12:17:33'),
(758, 'STFR', 'STATE FARM', '2021-06-17 12:17:33'),
(759, 'STFRM', 'STATE FARM DETROIT', '2021-06-17 12:17:33'),
(760, 'STONE', 'STONEBRIDGE LIFE', '2021-06-17 12:17:33'),
(761, 'STUDENT', 'STUDENT RESOURCES', '2021-06-17 12:17:33'),
(762, 'SUP', 'SUPERIOR HEALTH PLAN', '2021-06-17 12:17:33'),
(763, 'TOT03C', 'TOTAL HEALTH CARE', '2021-06-17 12:17:33'),
(764, 'TRAN', 'TRANS AMERICA', '2021-06-17 12:17:33'),
(765, 'TRANSAM', 'TRANSAMERICA FINANCIAL', '2021-06-17 12:17:33'),
(766, 'TRI05', 'TRICARE FOR LIFE', '2021-06-17 12:17:33'),
(767, 'TRIEAST', 'TRICARE EAST', '2021-06-17 12:17:33'),
(768, 'TRMARK', 'TRUSTMARK HEALTH BENEFITS', '2021-06-17 12:17:33'),
(769, 'UMR', 'UMR BEAUMONT', '2021-06-17 12:17:33'),
(770, 'UNI20V1', 'UNITED HEALTHCARE', '2021-06-17 12:17:33'),
(771, 'UNIF', 'UNIFIED LIFE INS COMP', '2021-06-17 12:17:33'),
(772, 'UNITAM', 'UNITED AMERICAN', '2021-06-17 12:17:33'),
(773, 'UPMC', 'UPMC INS SERVICES', '2021-06-17 12:17:33'),
(774, 'USAA', 'USAA LIFE INS COMP', '2021-06-17 12:17:33'),
(775, 'USHL', 'USHL FOR SMARTHEALTH', '2021-06-17 12:17:33'),
(776, 'UWL', 'UNITED WORLD LIFE', '2021-06-17 12:17:33'),
(777, 'VACCN', 'VA CCN OPTUM', '2021-06-17 12:17:33'),
(778, 'VADET', 'VETERANS - FEE FOR SERVICE', '2021-06-17 12:17:33'),
(779, 'VANI', 'VA NORTHERN INDIANA HEALTHCARE SYST', '2021-06-17 12:17:33'),
(780, 'VARIP', 'VARIPRO', '2021-06-17 12:17:33'),
(781, 'VAWPS', 'WPS MVH-CCN', '2021-06-17 12:17:33'),
(782, 'VCH', 'VETERANS CHOICE VACAA', '2021-06-17 12:17:33'),
(783, 'VMC', 'VMC', '2021-06-17 12:17:33'),
(784, 'VOCC', 'VHA OFFICE OF COMMUNITY CARE', '2021-06-17 12:17:33'),
(785, 'WASH', 'WASHINGTON NATIONAL', '2021-06-17 12:17:33'),
(786, 'WEA', 'WEA TRUST', '2021-06-17 12:17:33'),
(787, 'WEL04', 'WELLCARE', '2021-06-17 12:17:33'),
(788, 'WEST', 'WESTERN UNITED LIFE', '2021-06-17 12:17:33'),
(789, 'WPSVAP', 'WPS VAPC3', '2021-06-17 12:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis`
--

CREATE TABLE `diagnosis` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `diagnosis`
--

INSERT INTO `diagnosis` (`id`, `code`, `name`, `createdOn`) VALUES
(16, 'K29.70', 'Gastritis', '2021-06-17 12:17:33'),
(17, 'K40.0', 'Inguinal Hernia – Bilateral', '2021-06-17 12:17:33'),
(18, 'K40.90', 'Inguinal Hernia – Unilateral', '2021-06-17 12:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `dateFrom` datetime NOT NULL,
  `dateTo` datetime NOT NULL,
  `isMoveUpRequested` tinyint(1) NOT NULL DEFAULT 0,
  `eventTypeId` int(11) NOT NULL,
  `eventTypeCode` varchar(256) NOT NULL,
  `eventTypeName` varchar(256) NOT NULL,
  `priorityId` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `priorityText` varchar(256) NOT NULL,
  `facilityId` int(11) NOT NULL,
  `facilityCode` varchar(256) NOT NULL,
  `facilityName` varchar(256) NOT NULL,
  `facilityAddress` varchar(512) NOT NULL,
  `physicianId` int(11) NOT NULL,
  `physicianName` varchar(256) NOT NULL,
  `physicianPhone` varchar(64) DEFAULT NULL,
  `physicianFax` varchar(64) DEFAULT NULL,
  `coordinatorId` int(11) NOT NULL,
  `coordinatorName` varchar(256) NOT NULL,
  `patientId` int(11) NOT NULL,
  `patientName` varchar(128) DEFAULT NULL,
  `patientLastName` varchar(128) DEFAULT NULL,
  `patientDoB` datetime DEFAULT NULL,
  `patientGender` varchar(64) DEFAULT NULL,
  `patientAddress` varchar(512) DEFAULT NULL,
  `patientPreferredPhone` varchar(256) DEFAULT NULL,
  `patientSSN` varchar(128) DEFAULT NULL,
  `insuranceInfo` text DEFAULT NULL,
  `authorizationRequired` varchar(64) DEFAULT NULL,
  `diagnosisId` int(11) DEFAULT NULL,
  `diagnosisCode` varchar(64) DEFAULT NULL,
  `diagnosisDescription` varchar(512) DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `medicalClearanceStatus` enum('not_required','pending','accepted','rejected') NOT NULL DEFAULT 'not_required',
  `cardiacClearanceStatus` enum('not_required','pending','accepted','rejected') DEFAULT 'not_required',
  `otherClearanceStatus` enum('not_required','pending','accepted','rejected') NOT NULL DEFAULT 'not_required',
  `otherClearance` text DEFAULT NULL,
  `medicalClearancePhysician` varchar(512) DEFAULT NULL,
  `cardiacClearancePhysician` varchar(512) DEFAULT NULL,
  `otherClearancePhysician` varchar(512) DEFAULT NULL,
  `bloodThinnerId` int(11) DEFAULT NULL,
  `bloodThinnerCode` varchar(128) DEFAULT NULL,
  `bloodThinnerName` varchar(256) DEFAULT NULL,
  `bloodThinnerDescription` varchar(512) DEFAULT NULL,
  `status` varchar(64) NOT NULL DEFAULT 'pending',
  `isEmergency` tinyint(4) NOT NULL DEFAULT 0,
  `isDraft` tinyint(4) NOT NULL DEFAULT 0,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedOn` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `bsgOn` datetime DEFAULT NULL,
  `wfcOn` datetime DEFAULT NULL,
  `confirmedOn` datetime DEFAULT NULL,
  `completedOn` datetime DEFAULT NULL,
  `reminders` int(11) NOT NULL DEFAULT 0,
  `latestReminderOn` datetime DEFAULT NULL,
  `carrierId` int(11) DEFAULT NULL,
  `carrierCode` varchar(256) DEFAULT NULL,
  `carrierName` varchar(512) DEFAULT NULL,
  `subscriberId` text DEFAULT NULL,
  `groupName` text DEFAULT NULL,
  `groupNumber` text DEFAULT NULL,
  `insuranceAuthorizationStatus` enum('not-required','tbd','approved','') NOT NULL DEFAULT 'not-required',
  `insuranceAuthorizationNumber` varchar(256) DEFAULT NULL,
  `cancelReason` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `dateFrom`, `dateTo`, `isMoveUpRequested`, `eventTypeId`, `eventTypeCode`, `eventTypeName`, `priorityId`, `priority`, `priorityText`, `facilityId`, `facilityCode`, `facilityName`, `facilityAddress`, `physicianId`, `physicianName`, `physicianPhone`, `physicianFax`, `coordinatorId`, `coordinatorName`, `patientId`, `patientName`, `patientLastName`, `patientDoB`, `patientGender`, `patientAddress`, `patientPreferredPhone`, `patientSSN`, `insuranceInfo`, `authorizationRequired`, `diagnosisId`, `diagnosisCode`, `diagnosisDescription`, `comment`, `medicalClearanceStatus`, `cardiacClearanceStatus`, `otherClearanceStatus`, `otherClearance`, `medicalClearancePhysician`, `cardiacClearancePhysician`, `otherClearancePhysician`, `bloodThinnerId`, `bloodThinnerCode`, `bloodThinnerName`, `bloodThinnerDescription`, `status`, `isEmergency`, `isDraft`, `createdOn`, `updatedOn`, `bsgOn`, `wfcOn`, `confirmedOn`, `completedOn`, `reminders`, `latestReminderOn`, `carrierId`, `carrierCode`, `carrierName`, `subscriberId`, `groupName`, `groupNumber`, `insuranceAuthorizationStatus`, `insuranceAuthorizationNumber`, `cancelReason`) VALUES
(33, '2021-06-24 12:23:00', '2021-06-24 14:38:00', 0, 14, 'ENDO', 'Endoscopy', 18, 1, 'Routine - Elective', 25, 'HSP1', 'Ascension - St. John', '22101 Moross Rd, Detroit, MI 48236', 20, 'Demo Physician One', '1116574897', '1113334456', 8, 'Louai', 20, 'John William Doe', NULL, '1995-07-24 00:00:00', 'male', 'Main Street 123/Detroit/Michigan/11223355', '1112223355', '111-22-3333', NULL, NULL, 17, 'K40.0', 'Inguinal Hernia – Bilateral', NULL, 'not_required', 'rejected', 'rejected', NULL, NULL, 'Example Cardiac Physician', 'Other Medical Physiciann', 26, 'Plavix', 'Plavix 75mg', 'Patient needs to stop taking Plavix 75mg 7 days before surgery.', 'completed', 0, 0, '2021-06-17 12:23:49', '2021-06-28 13:00:39', '2021-06-17 12:28:33', '2021-06-17 13:19:11', '2021-06-17 13:19:12', '2021-06-24 10:51:03', 0, NULL, 641, 'AIG', 'AIG', '111', 'Group One', 'G1', 'not-required', NULL, NULL),
(37, '2021-06-30 04:00:00', '2021-06-30 06:18:00', 0, 13, 'OR', 'Main OR', 19, 3, 'Elective w/ RTMU', 28, 'HSP4', 'Beaumont Hospital - Grosse Pointe', '468 Cadieux Rd, Grosse Pointe, MI 48230', 20, 'Demo Physician One', '1116574897', '1113334456', 8, 'Louai', 20, 'John William Doe', NULL, '1984-02-09 00:00:00', 'male', 'Main Street 123/Detroit/Michigan/11223343', '1112223388', '111-22-3333', NULL, NULL, 16, 'K29.70', 'Gastritis', NULL, 'pending', 'not_required', 'accepted', NULL, 'Med 1', NULL, 'Other Med 1', 27, 'ASA325', 'Aspirin 325mg', 'Patient needs to stop taking Aspirin 325mg 7 days before surgery.', 'confirmed', 0, 0, '2021-06-22 12:59:06', '2021-06-29 12:25:48', '2021-06-22 16:27:17', '2021-06-22 16:30:08', '2021-06-29 11:22:15', '2021-06-29 11:12:37', 0, NULL, 670, 'CONWE', 'CONTINENTAL WESTERN', '111', 'Group One', 'G111', 'approved', '5555', 'The entered information was incorrect'),
(39, '2021-06-27 13:49:00', '2021-06-27 14:49:00', 0, 13, 'OR', 'Main OR', 18, 1, 'Routine - Elective', 25, 'HSP1', 'Ascension - St. John', '22101 Moross Rd, Detroit, MI 48236', 20, 'Demo Physician One', '1116574897', '1113334456', 8, 'Louai', 20, 'John William Doe', NULL, '1984-02-09 00:00:00', 'male', 'Main Street 123/Detroit/Michigan/11223343', '1112223388', '111-22-3333', NULL, NULL, 17, 'K40.0', 'Inguinal Hernia – Bilateral', NULL, 'accepted', 'pending', 'rejected', NULL, 'Example Medical Physician', 'Car Phy 1', 'Other Medical Physician', 27, 'ASA325', 'Aspirin 325mg', 'Patient needs to stop taking Aspirin 325mg 7 days before surgery.', 'waiting_for_confirmation', 0, 0, '2021-06-23 16:13:42', '2021-06-28 13:00:39', '2021-06-25 16:27:19', '2021-06-25 16:28:30', '2021-06-25 16:27:26', '2021-06-25 16:16:53', 0, NULL, 670, 'CONWE', 'CONTINENTAL WESTERN', '111', 'Group One', 'G111', 'not-required', NULL, NULL),
(41, '2021-06-29 10:54:00', '2021-06-29 13:54:00', 0, 13, 'OR', 'Main OR', 20, 5, 'Emergency / STAT', 26, 'HSP2', 'Ascension - Macomb', '11800 Twelve Mile Rd, Warren, MI 48093', 20, 'Demo Physician One', '1116574897', '1113334456', 8, 'Louai', 22, 'Amelia Bob Watson', NULL, '1995-07-24 00:00:00', 'female', 'Main Street 123/Detroit/Michigan/11223344', '1112227777', '999-88-7777', NULL, NULL, 16, 'K29.70', 'Gastritis', NULL, 'not_required', 'not_required', 'not_required', NULL, NULL, NULL, NULL, 27, 'ASA325', 'Aspirin 325mg', 'Patient needs to stop taking Aspirin 325mg 7 days before surgery.', 'completed', 0, 0, '2021-06-24 10:55:05', '2021-06-29 11:09:33', '2021-06-24 11:00:42', '2021-06-24 11:01:06', '2021-06-29 11:09:22', '2021-06-29 11:09:33', 0, NULL, 777, 'VACCN', 'VA CCN OPTUM', '222', 'Group Two', 'G222', 'not-required', NULL, NULL),
(42, '2021-06-29 15:45:00', '2021-06-29 16:45:00', 0, 13, 'OR', 'Main OR', 18, 1, 'Routine - Elective', 26, 'HSP2', 'Ascension - Macomb', '11800 Twelve Mile Rd, Warren, MI 48093', 20, 'Demo Physician One', '1116574897', '1113334456', 8, 'Louai', 22, 'Amelia Bob Watson', NULL, '1995-07-24 00:00:00', 'female', 'Main Street 123/Detroit/Michigan/11223344', '1112227777', '999-88-7777', NULL, NULL, 17, 'K40.0', 'Inguinal Hernia – Bilateral', NULL, 'not_required', 'not_required', 'not_required', NULL, NULL, NULL, NULL, 27, 'ASA325', 'Aspirin 325mg', 'Patient needs to stop taking Aspirin 325mg 7 days before surgery.', 'waiting_for_confirmation', 0, 0, '2021-06-24 15:45:17', '2021-06-28 13:17:28', '2021-06-24 15:46:31', '2021-06-24 15:46:58', NULL, NULL, 0, NULL, 777, 'VACCN', 'VA CCN OPTUM', '222', 'Group Two', 'G222', 'approved', '8888', NULL),
(43, '2021-06-29 19:18:00', '2021-06-29 22:18:00', 0, 14, 'ENDO', 'Endoscopy', 19, 3, 'Elective w/ RTMU', 25, 'HSP1', 'Ascension - St. John', '22101 Moross Rd, Detroit, MI 48236', 20, 'Demo Physician One', '1116574897', '1113334456', 8, 'Louai', 22, 'Amelia Bob Watson', NULL, '1995-07-24 00:00:00', 'female', 'Main Street 123/Detroit/Michigan/11223344', '1112227777', '999-88-7778', 'Example', NULL, NULL, NULL, NULL, NULL, 'not_required', 'not_required', 'not_required', NULL, NULL, NULL, NULL, 27, 'ASA325', 'Aspirin 325mg', 'Patient needs to stop taking Aspirin 325mg 7 days before surgery.', 'cancelled', 0, 0, '2021-06-25 10:18:51', '2021-06-28 16:33:55', '2021-06-28 16:10:35', NULL, NULL, NULL, 0, NULL, 645, 'AME35', 'AMERICAS CHOICE HEALTHPLAN', '222', 'Group Two', 'G222', 'not-required', NULL, 'Incorrect details written'),
(44, '2021-06-30 19:18:00', '2021-06-30 20:18:00', 0, 13, 'OR', 'Main OR', 18, 1, 'Routine - Elective', 26, 'HSP2', 'Ascension - Macomb', '11800 Twelve Mile Rd, Warren, MI 48093', 20, 'Demo Physician One', '1116574897', '1113334456', 8, 'Louai', 22, 'Amelia Bob Watson', NULL, '1995-07-24 00:00:00', 'female', 'Main Street 123/Detroit/Michigan/11223344', '1112227777', '999-88-7778', NULL, NULL, 16, 'K29.70', 'Gastritis', NULL, 'not_required', 'not_required', 'not_required', NULL, NULL, NULL, NULL, 27, 'ASA325', 'Aspirin 325mg', 'Patient needs to stop taking Aspirin 325mg 7 days before surgery.', 'boarding_slip_generated', 0, 0, '2021-06-25 10:19:18', '2021-06-28 13:00:39', '2021-06-25 10:20:34', NULL, NULL, NULL, 0, NULL, 645, 'AME35', 'AMERICAS CHOICE HEALTHPLAN', '222', 'Group Two', 'G222', 'not-required', NULL, NULL),
(45, '2021-07-29 12:34:29', '2021-07-29 13:34:29', 0, 13, 'OR', 'Main OR', 18, 1, 'Routine - Elective', 27, 'HSP3', 'Ascension - Oakland', '27351 Dequindre Rd, Madison Heights, MI 48071', 20, 'Demo Physician One', '1116574897', '1113334456', 8, 'Louai', 22, 'Amelia Bob Watson', NULL, '1995-07-24 00:00:00', 'female', 'Main Street 123/Detroit/Michigan/11223344', '1112227777', '999-88-7778', NULL, NULL, NULL, NULL, NULL, NULL, 'accepted', 'not_required', 'not_required', NULL, 'Phy12', NULL, NULL, NULL, NULL, NULL, NULL, 'pending', 0, 0, '2021-06-25 12:35:05', '2021-06-29 13:43:02', NULL, NULL, NULL, NULL, 0, NULL, 645, 'AME35', 'AMERICAS CHOICE HEALTHPLAN', '222', 'Group Two', 'G222', 'approved', '1234', NULL),
(46, '2021-06-29 10:15:48', '2021-06-29 15:15:48', 0, 13, 'OR', 'Main OR', 20, 5, 'Emergency / STAT', 26, 'HSP2', 'Ascension - Macomb', '11800 Twelve Mile Rd, Warren, MI 48093', 20, 'Demo Physician One', '1116574897', '1113334456', 8, 'Louai', 22, 'Amelia Bob Watson', NULL, '1995-07-24 00:00:00', 'female', 'Main Street 123/Detroit/Michigan/11223344', '1112227777', '999-88-7778', NULL, NULL, NULL, NULL, NULL, NULL, 'accepted', 'not_required', 'not_required', NULL, 'Med Phy', NULL, NULL, NULL, NULL, NULL, NULL, 'pending', 1, 0, '2021-06-28 10:15:58', '2021-06-28 13:00:39', NULL, NULL, NULL, NULL, 0, NULL, 645, 'AME35', 'AMERICAS CHOICE HEALTHPLAN', '222', 'Group Two', 'G222', 'not-required', NULL, NULL),
(47, '2021-06-30 15:43:04', '2021-06-30 16:13:04', 0, 14, 'ENDO', 'Endoscopy', 18, 1, 'Routine - Elective', 29, 'HSP5', 'Beaumont Hospital - Dearborn', '18101 Oakwood Blvd, Dearborn, MI 48124', 20, 'Demo Physician One', '1116574897', '1113334456', 8, 'Louai', 22, 'Amelia Bob Watson', NULL, '1995-07-24 00:00:00', 'female', 'Main Street 123/Detroit/Michigan/11223344', '1112227777', '999-88-7778', NULL, NULL, NULL, NULL, NULL, NULL, 'not_required', 'not_required', 'not_required', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pending', 0, 0, '2021-06-28 15:43:15', '2021-06-28 15:43:15', NULL, NULL, NULL, NULL, 0, NULL, 728, 'MUT01', 'MUTUAL OF OMAHA', '222', 'Group Two', 'G222', 'not-required', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `event_checklist`
--

CREATE TABLE `event_checklist` (
  `id` int(11) NOT NULL,
  `eventId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `description` text NOT NULL,
  `step` int(11) NOT NULL DEFAULT 0,
  `groupName` varchar(512) DEFAULT NULL,
  `assigneeId` int(11) DEFAULT NULL,
  `dueDate` datetime NOT NULL DEFAULT current_timestamp(),
  `status` enum('tbd','done','na') NOT NULL DEFAULT 'tbd',
  `createdOn` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event_checklist`
--

INSERT INTO `event_checklist` (`id`, `eventId`, `userId`, `description`, `step`, `groupName`, `assigneeId`, `dueDate`, `status`, `createdOn`) VALUES
(21, 33, 8, 'Paperwork prepared for medical clearance', 0, 'Medical Clearance', 7, '2021-06-25 10:27:00', 'done', '2021-06-18 07:28:48'),
(22, 33, 8, 'Paperwork prepared for medical clearance', 0, 'Cardiac Clearance', 7, '2021-06-10 00:00:00', 'done', '2021-06-18 08:24:58'),
(23, 33, 8, 'Patient organized medical clearance appointment', 0, 'Medical Clearance', 7, '2021-06-26 00:00:00', 'tbd', '2021-06-18 08:33:23'),
(24, 33, 8, 'Medical clearance form filled out by PCP', 0, 'Medical Clearance', 7, '2021-06-30 00:00:00', 'tbd', '2021-06-18 10:25:42'),
(25, 33, 8, 'Medical clearance reviewed for abnormalities', 0, 'Cardiac Clearance', 1, '2021-06-24 00:00:00', 'tbd', '2021-06-18 11:00:16'),
(26, 37, 8, 'Paperwork prepared for medical clearance', 0, 'Medical Clearance', 8, '2021-06-30 00:00:00', 'tbd', '2021-06-22 13:26:58'),
(27, 39, 8, 'Paperwork prepared for medical clearance', 0, 'Medical Clearance', 8, '2021-06-29 00:00:00', 'done', '2021-06-23 13:16:08'),
(28, 41, 8, 'Paperwork prepared for medical clearance', 0, 'Medical Clearance', 8, '2021-06-26 00:00:00', 'tbd', '2021-06-24 08:00:27'),
(29, 37, 8, 'Medical clearance form filled out by PCP', 0, 'Medical Clearance', 8, '2022-06-06 00:00:00', 'done', '2021-06-24 11:24:55'),
(30, 37, 8, 'Paperwork prepared for medical clearance', 0, 'Cardiac Clearance', 1, '2022-02-02 00:00:00', 'tbd', '2021-06-24 11:25:59'),
(31, 42, 8, 'Paperwork prepared for medical clearance', 0, 'Medical Clearance', 8, '2021-06-30 00:00:00', 'tbd', '2021-06-24 12:45:45'),
(32, 44, 8, 'Paperwork prepared for medical clearance', 0, 'Medical Clearance', 8, '2021-06-27 00:00:00', 'tbd', '2021-06-25 07:20:28'),
(33, 43, 8, 'Paperwork prepared for medical clearance', 0, 'Medical Clearance', 8, '2021-06-26 00:00:00', 'tbd', '2021-06-28 13:10:31'),
(34, 45, 8, 'Paperwork prepared for medical clearance', 0, 'Medical Clearance', 8, '2021-07-26 00:00:00', 'tbd', '2021-06-29 10:26:10');

-- --------------------------------------------------------

--
-- Table structure for table `event_note`
--

CREATE TABLE `event_note` (
  `id` int(11) NOT NULL,
  `eventId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `note` text NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event_note`
--

INSERT INTO `event_note` (`id`, `eventId`, `userId`, `note`, `createdOn`) VALUES
(12, 33, 8, 'This is an example note', '2021-06-17 12:28:29');

-- --------------------------------------------------------

--
-- Table structure for table `event_procedure`
--

CREATE TABLE `event_procedure` (
  `id` int(11) NOT NULL,
  `eventId` int(11) NOT NULL,
  `procedureId` int(11) NOT NULL,
  `procedureCode` varchar(256) NOT NULL,
  `procedureName` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event_procedure`
--

INSERT INTO `event_procedure` (`id`, `eventId`, `procedureId`, `procedureCode`, `procedureName`, `createdOn`) VALUES
(31, 33, 21, '45378', 'Colonoscopy', '2021-06-21 11:10:18'),
(34, 39, 21, '45378', 'Colonoscopy', '2021-06-24 09:58:37'),
(37, 42, 22, '45380', 'Colonoscopy w/ Biopsies', '2021-06-24 15:45:23'),
(41, 44, 21, '45378', 'Colonoscopy', '2021-06-25 10:20:09'),
(42, 46, 21, '45378', 'Colonoscopy', '2021-06-28 10:16:39'),
(43, 43, 21, '45378', 'Colonoscopy', '2021-06-28 16:09:07'),
(57, 41, 23, '45990', 'Examination Under Anesthesia', '2021-06-29 11:09:33'),
(58, 37, 21, '45378', 'Colonoscopy', '2021-06-29 11:12:37'),
(59, 45, 21, '45378', 'Colonoscopy', '2021-06-29 13:24:27');

-- --------------------------------------------------------

--
-- Table structure for table `event_type`
--

CREATE TABLE `event_type` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `color` varchar(32) DEFAULT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event_type`
--

INSERT INTO `event_type` (`id`, `code`, `name`, `color`, `createdOn`) VALUES
(13, 'OR', 'Main OR', '#668822', '2021-06-17 12:17:33'),
(14, 'ENDO', 'Endoscopy', '#884466', '2021-06-17 12:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `facility`
--

CREATE TABLE `facility` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `address` varchar(256) NOT NULL,
  `schedulingOfficePhone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `facility`
--

INSERT INTO `facility` (`id`, `code`, `name`, `address`, `schedulingOfficePhone`, `fax`, `createdOn`) VALUES
(25, 'HSP1', 'Ascension - St. John', '22101 Moross Rd, Detroit, MI 48236', '3133433366', '3133433366', '2021-06-17 12:17:33'),
(26, 'HSP2', 'Ascension - Macomb', '11800 Twelve Mile Rd, Warren, MI 48093', '5865735000', '5865735000', '2021-06-17 12:17:33'),
(27, 'HSP3', 'Ascension - Oakland', '27351 Dequindre Rd, Madison Heights, MI 48071', '2489677000', '2489677000', '2021-06-17 12:17:33'),
(28, 'HSP4', 'Beaumont Hospital - Grosse Pointe', '468 Cadieux Rd, Grosse Pointe, MI 48230', '3134731000', '3134731000', '2021-06-17 12:17:33'),
(29, 'HSP5', 'Beaumont Hospital - Dearborn', '18101 Oakwood Blvd, Dearborn, MI 48124', '3135937000', '3135937000', '2021-06-17 12:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `facility_physician`
--

CREATE TABLE `facility_physician` (
  `id` int(11) NOT NULL,
  `facilityId` int(11) NOT NULL,
  `physicianId` int(11) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `facility_physician`
--

INSERT INTO `facility_physician` (`id`, `facilityId`, `physicianId`, `createdOn`) VALUES
(5, 25, 20, '2021-06-17 12:19:16'),
(6, 26, 20, '2021-06-17 12:19:20'),
(7, 27, 20, '2021-06-17 12:19:22'),
(8, 28, 20, '2021-06-17 12:19:24'),
(9, 29, 20, '2021-06-17 12:19:25');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `eventId` int(11) DEFAULT NULL,
  `patientId` int(11) DEFAULT NULL,
  `actionType` varchar(64) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `extra1` text DEFAULT NULL,
  `extra2` int(11) DEFAULT NULL,
  `extra3` int(11) DEFAULT NULL,
  `extra4` int(11) DEFAULT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `userId`, `eventId`, `patientId`, `actionType`, `description`, `extra1`, `extra2`, `extra3`, `extra4`, `createdOn`) VALUES
(1, 3, NULL, NULL, 'AddMasterData', 'Added master data entry \"Urgent\" to master data \"priority\"', NULL, NULL, NULL, NULL, '2021-06-02 10:34:20'),
(2, 3, NULL, NULL, 'AddMasterData', 'Added master data entry \"Diagnosis 2\" to \"diagnosis\"', NULL, NULL, NULL, NULL, '2021-06-02 10:37:18'),
(3, 3, NULL, NULL, 'AddMasterData', 'Added master data entry \"Blood Thinner New\" to \"blood_thinner\"', NULL, NULL, NULL, NULL, '2021-06-02 10:40:52'),
(4, 3, NULL, NULL, 'DeleteMasterData', 'Deleted master data entry from \"blood_thinner\"', NULL, NULL, NULL, NULL, '2021-06-02 10:40:54'),
(5, 3, NULL, NULL, 'AddEvent', 'Added event #6 for patient \"undefined\"', NULL, NULL, NULL, NULL, '2021-06-03 16:23:53'),
(6, 3, NULL, NULL, 'AddEvent', 'Added event #7 for patient #1', NULL, NULL, NULL, NULL, '2021-06-04 09:43:13'),
(7, 3, NULL, NULL, 'AddEvent', 'Added event #8 for patient #1', NULL, NULL, NULL, NULL, '2021-06-04 12:40:33'),
(8, 3, NULL, NULL, 'AddEvent', 'Added event #9 for patient #1', NULL, NULL, NULL, NULL, '2021-06-04 12:42:28'),
(9, 3, NULL, NULL, 'AddMasterData', 'Added master data entry \"Emergency Room 1\" to \"facility_room\"', NULL, NULL, NULL, NULL, '2021-06-04 13:26:57'),
(10, 3, NULL, NULL, 'AddEvent', 'Added event #10 for patient #1', NULL, NULL, NULL, NULL, '2021-06-04 13:46:49'),
(11, 3, NULL, NULL, 'AddEvent', 'Added event #11 for patient #1', NULL, NULL, NULL, NULL, '2021-06-08 13:11:24'),
(12, 3, NULL, NULL, 'AddMasterData', 'Added master data entry \"Procedure 1\" to \"procedures\"', NULL, NULL, NULL, NULL, '2021-06-09 10:46:32'),
(13, 3, NULL, NULL, 'AddMasterData', 'Added master data entry \"Procedure 2\" to \"procedures\"', NULL, NULL, NULL, NULL, '2021-06-09 10:46:38'),
(14, 3, NULL, NULL, 'AddMasterData', 'Added master data entry \"Procedure 3\" to \"procedures\"', NULL, NULL, NULL, NULL, '2021-06-09 10:46:44'),
(15, 3, NULL, NULL, 'AddMasterData', 'Added master data entry \"Procedure 4\" to \"procedures\"', NULL, NULL, NULL, NULL, '2021-06-09 10:46:50'),
(16, 3, NULL, NULL, 'AddMasterData', 'Added master data entry \"Procedure 5\" to \"procedures\"', NULL, NULL, NULL, NULL, '2021-06-09 10:46:55'),
(17, 3, NULL, NULL, 'AddEvent', 'Added event #12 for patient #1', NULL, NULL, NULL, NULL, '2021-06-10 10:23:55'),
(18, 3, NULL, NULL, 'AddMasterData', 'Added master data entry \"Aspirin\" to \"blood_thinner\"', NULL, NULL, NULL, NULL, '2021-06-10 11:04:31'),
(19, 3, NULL, NULL, 'AddEvent', 'Added event #13 for patient #1', NULL, NULL, NULL, NULL, '2021-06-11 13:32:04'),
(20, 18, NULL, NULL, 'AddEvent', 'Added event #14 for patient #10', NULL, NULL, NULL, NULL, '2021-06-11 16:23:27'),
(21, 18, NULL, NULL, 'AddEvent', 'Added event #15 for patient #9', NULL, NULL, NULL, NULL, '2021-06-11 16:25:27'),
(22, 18, NULL, NULL, 'AddEvent', 'Added event #16 for patient #9', NULL, NULL, NULL, NULL, '2021-06-14 12:12:15'),
(23, 18, NULL, NULL, 'AddEvent', 'Added event #17 for patient #9', NULL, NULL, NULL, NULL, '2021-06-14 12:13:54'),
(24, 1, NULL, NULL, 'AddEvent', 'Added event #18 for patient #10', NULL, NULL, NULL, NULL, '2021-06-14 13:57:50'),
(25, 1, NULL, NULL, 'AddEvent', 'Added event #19 for patient #10', NULL, NULL, NULL, NULL, '2021-06-14 13:58:27'),
(26, 1, NULL, NULL, 'AddEvent', 'Added event #20 for patient #10', NULL, NULL, NULL, NULL, '2021-06-15 11:25:29'),
(27, 1, NULL, NULL, 'AddEvent', 'Added event #21 for patient #10', NULL, NULL, NULL, NULL, '2021-06-15 11:27:11'),
(28, 1, NULL, NULL, 'AddEvent', 'Added event #22 for patient #16', NULL, NULL, NULL, NULL, '2021-06-15 12:08:29'),
(29, 1, NULL, NULL, 'AddEvent', 'Added event #24 for patient #10', NULL, NULL, NULL, NULL, '2021-06-16 08:43:19'),
(30, 1, NULL, NULL, 'AddEvent', 'Added event #25 for patient #10', NULL, NULL, NULL, NULL, '2021-06-16 08:44:07'),
(31, 1, NULL, NULL, 'AddEvent', 'Added event #26 for patient #10', NULL, NULL, NULL, NULL, '2021-06-16 11:44:30'),
(32, 1, NULL, NULL, 'AddEvent', 'Added event #27 for patient #10', NULL, NULL, NULL, NULL, '2021-06-16 11:44:58'),
(33, 1, NULL, NULL, 'AddEvent', 'Added event #28 for patient #10', NULL, NULL, NULL, NULL, '2021-06-16 11:46:14'),
(34, 1, NULL, NULL, 'AddMasterData', 'Added master data entry \"Carrier 1\" to \"carrier\"', NULL, NULL, NULL, NULL, '2021-06-16 12:16:58'),
(35, 1, NULL, NULL, 'AddMasterData', 'Added master data entry \"asfasf\" to \"carrier\"', NULL, NULL, NULL, NULL, '2021-06-16 12:17:12'),
(36, 1, NULL, NULL, 'DeleteMasterData', 'Deleted master data entry from \"carrier\"', NULL, NULL, NULL, NULL, '2021-06-16 12:17:16'),
(37, 1, NULL, NULL, 'AddMasterData', 'Added master data entry \"Carrier 2\" to \"carrier\"', NULL, NULL, NULL, NULL, '2021-06-16 12:17:26'),
(38, 1, NULL, NULL, 'AddMasterData', 'Added master data entry \"Carrier 3\" to \"carrier\"', NULL, NULL, NULL, NULL, '2021-06-16 12:17:34'),
(39, 1, NULL, NULL, 'AddEvent', 'Added event #29 for patient #17', NULL, NULL, NULL, NULL, '2021-06-16 12:40:18'),
(40, 1, NULL, NULL, 'AddEvent', 'Added event #30 for patient #19', NULL, NULL, NULL, NULL, '2021-06-16 13:40:34'),
(41, 1, NULL, NULL, 'AddEvent', 'Added event #31 for patient #19', NULL, NULL, NULL, NULL, '2021-06-16 13:42:12'),
(42, 1, NULL, NULL, 'AddMasterData', 'Added master data entry \"safasfasf\" to \"event_type\"', NULL, NULL, NULL, NULL, '2021-06-16 15:14:15'),
(43, 1, NULL, NULL, 'DeleteMasterData', 'Deleted master data entry from \"event_type\"', NULL, NULL, NULL, NULL, '2021-06-16 15:14:19'),
(44, 1, NULL, NULL, 'AddEvent', 'Added event #32 for patient #10', NULL, NULL, NULL, NULL, '2021-06-17 08:29:05'),
(45, 8, NULL, NULL, 'AddEvent', 'Added event #33 for patient #20', NULL, NULL, NULL, NULL, '2021-06-17 12:23:49'),
(46, 8, NULL, NULL, 'AddEvent', 'Added event #34 for patient #20', NULL, NULL, NULL, NULL, '2021-06-21 16:41:58'),
(47, 8, NULL, NULL, 'AddEvent', 'Added event #35 for patient #20', NULL, NULL, NULL, NULL, '2021-06-22 08:40:32'),
(48, 8, NULL, NULL, 'AddEvent', 'Added event #36 for patient #20', NULL, NULL, NULL, NULL, '2021-06-22 08:55:54'),
(49, 8, NULL, NULL, 'UpdatePatient', 'Updated patient #20', '{\"patientId\":20,\"firstName\":\"John\",\"middleName\":\"William\",\"lastName\":\"Doe\",\"address1\":\"Main Street 123\",\"address2\":\"Bld 3\",\"state\":\"Michigan\",\"city\":\"Detroit\",\"zipcode\":\"11223343\",\"dateofbirth\":\"1984-02-09\",\"email\":\"john.doe@mail.com\",\"phone\":\"1112223366\",\"ssn\":\"111-22-3333\",\"sex\":\"male\"}', NULL, NULL, NULL, '2021-06-22 09:07:51'),
(50, 20, NULL, NULL, 'UpdatePatient', 'Updated patient #20', '{\"patientId\":20,\"firstName\":\"John\",\"middleName\":\"William\",\"lastName\":\"Doe\",\"address1\":\"Main Street 123\",\"address2\":\"Bld 3\",\"state\":\"Michigan\",\"city\":\"Detroit\",\"zipcode\":\"11223343\",\"dateofbirth\":\"1984-02-09\",\"email\":\"john.doe@mail.com\",\"phone\":\"1112223377\",\"ssn\":\"111-22-3333\",\"sex\":\"male\"}', NULL, NULL, NULL, '2021-06-22 09:08:08'),
(51, 20, NULL, NULL, 'UpdatePatient', 'Updated patient #20', '{\"patientId\":20,\"firstName\":\"John\",\"middleName\":\"William\",\"lastName\":\"Doe\",\"address1\":\"Main Street 123\",\"address2\":\"Bld 3\",\"state\":\"Michigan\",\"city\":\"Detroit\",\"zipcode\":\"11223343\",\"dateofbirth\":\"1984-02-09\",\"email\":\"john.doe@mail.com\",\"phone\":\"1112223388\",\"ssn\":\"111-22-3333\",\"sex\":\"male\"}', NULL, NULL, NULL, '2021-06-22 09:08:29'),
(52, 8, 33, NULL, 'UpdateEvent', 'Updated event #33', '{\"cardiacClearanceStatus\":\"rejected\"}', NULL, NULL, NULL, '2021-06-22 11:08:30'),
(53, 8, 33, NULL, 'UpdateEvent', 'Updated event #33', '{\"cardiacClearanceStatus\":\"accepted\"}', NULL, NULL, NULL, '2021-06-22 11:08:36'),
(54, 8, 33, NULL, 'UpdateEvent', 'Updated event #33', '{\"cardiacClearanceStatus\":\"pending\"}', NULL, NULL, NULL, '2021-06-22 11:17:34'),
(55, 8, 33, NULL, 'UpdateEventClearance', 'Updated event #33 cardiac clearance from accepted to pending', NULL, NULL, NULL, NULL, '2021-06-22 11:17:34'),
(56, 8, 33, NULL, 'UpdateEvent', 'Updated event #33', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223355\",\"patientPreferredPhone\":\"1112223355\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"male\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":17,\"diagnosisCode\":\"K40.0\",\"diagnosisDescription\":\"Inguinal Hernia – Bilateral\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"rejected\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Example Cardiac Physician\",\"medicalClearancePhysician\":null,\"otherClearancePhysician\":\"Other Medical Physiciann\"}', NULL, NULL, NULL, '2021-06-22 11:25:51'),
(57, 8, 33, NULL, 'UpdateEventClearance', 'Updated event #33 cardiac clearance from pending to rejected', NULL, NULL, NULL, NULL, '2021-06-22 11:25:51'),
(58, 8, 33, NULL, 'UpdateEventClearance', 'Updated event #33 other clearance from accepted to rejected', NULL, NULL, NULL, NULL, '2021-06-22 11:25:51'),
(60, 8, NULL, NULL, 'DeleteEvent', 'Deleted event #36 for patient \"John William Doe\" with physician \"Demo Physician One\" in facility \"Ascension - Oakland\" planned to begin in \"Wed Jun 23 2021 01:55:00 GMT+0300 (Eastern European Summer Time)\"', NULL, NULL, NULL, NULL, '2021-06-22 12:16:03'),
(61, 8, 33, NULL, 'UpdateEvent', 'Updated event #33', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223355\",\"patientPreferredPhone\":\"1112223355\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"male\",\"bloodThinnerId\":26,\"bloodThinnerCode\":\"Plavix\",\"bloodThinnerName\":\"Plavix 75mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Plavix 75mg 7 days before surgery.\",\"diagnosisId\":17,\"diagnosisCode\":\"K40.0\",\"diagnosisDescription\":\"Inguinal Hernia – Bilateral\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"rejected\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Example Cardiac Physician\",\"medicalClearancePhysician\":null,\"otherClearancePhysician\":\"Other Medical Physiciann\"}', NULL, NULL, NULL, '2021-06-22 12:43:11'),
(62, 8, 37, NULL, 'AddEvent', 'Added event #37 for patient #20', NULL, NULL, NULL, NULL, '2021-06-22 12:59:06'),
(63, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"pending\",\"otherClearanceStatus\":\"pending\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-22 12:59:29'),
(64, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from not_required to pending', NULL, NULL, NULL, NULL, '2021-06-22 12:59:29'),
(65, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 cardiac clearance from not_required to pending', NULL, NULL, NULL, NULL, '2021-06-22 12:59:29'),
(66, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 other clearance from not_required to pending', NULL, NULL, NULL, NULL, '2021-06-22 12:59:29'),
(67, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"rejected\"}', NULL, NULL, NULL, '2021-06-22 13:02:50'),
(68, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from pending to rejected', NULL, NULL, NULL, NULL, '2021-06-22 13:02:50'),
(69, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"rejected\"}', NULL, NULL, NULL, '2021-06-22 13:03:06'),
(70, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"cardiacClearanceStatus\":\"accepted\"}', NULL, NULL, NULL, '2021-06-22 13:04:16'),
(71, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 cardiac clearance from pending to accepted', NULL, NULL, NULL, NULL, '2021-06-22 13:04:16'),
(72, 8, NULL, NULL, 'AddPatientData', 'Added patient data entry \"Test Allergy\" to \"patient_allergy\"', NULL, NULL, NULL, NULL, '2021-06-22 13:54:36'),
(73, 8, NULL, NULL, 'AddPatientData', 'Added patient data entry \"undefined\" to \"patient_smoking_status\"', NULL, NULL, NULL, NULL, '2021-06-22 16:03:20'),
(74, 8, NULL, NULL, 'AddPatientData', 'Added patient data entry \"undefined\" to \"patient_health_concern\"', NULL, NULL, NULL, NULL, '2021-06-22 16:13:27'),
(75, 8, NULL, NULL, 'AddPatientData', 'Added patient data entry #\"2\" to \"patient_health_concern\"', NULL, NULL, NULL, NULL, '2021-06-22 16:15:09'),
(76, 8, NULL, NULL, 'UpdatePatientData', 'Updated patient data \"patient_smoking_status\" entry with id #1, property \"description\" into new value \"This is an example smoking status description 2\"', NULL, NULL, NULL, NULL, '2021-06-22 16:22:16'),
(77, 8, NULL, NULL, 'DeletePatientData', 'Deleted patient data entry from \"patient_health_concern\"', NULL, NULL, NULL, NULL, '2021-06-22 16:22:48'),
(78, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"accepted\",\"medicalClearanceStatus\":\"rejected\",\"otherClearanceStatus\":\"pending\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-22 16:26:46'),
(79, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"accepted\",\"medicalClearanceStatus\":\"rejected\",\"otherClearanceStatus\":\"pending\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-22 16:27:14'),
(80, 8, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Boarding Slip Generated\"', NULL, NULL, NULL, NULL, '2021-06-22 16:27:17'),
(81, 8, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Waiting For Confirmation\"', NULL, NULL, NULL, NULL, '2021-06-22 16:27:30'),
(82, 8, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Rejected\"', NULL, NULL, NULL, NULL, '2021-06-22 16:28:23'),
(83, 8, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Waiting For Confirmation\"', NULL, NULL, NULL, NULL, '2021-06-22 16:28:28'),
(84, 8, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Rejected\"', NULL, NULL, NULL, NULL, '2021-06-22 16:30:02'),
(85, 8, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Waiting For Confirmation\"', NULL, NULL, NULL, NULL, '2021-06-22 16:30:08'),
(86, 8, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-22 16:30:13'),
(87, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"otherClearanceStatus\":\"rejected\"}', NULL, NULL, NULL, '2021-06-22 16:32:41'),
(88, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 other clearance from pending to rejected', NULL, NULL, NULL, NULL, '2021-06-22 16:32:41'),
(89, 8, NULL, NULL, 'AddPatientData', 'Added patient data entry with id #1 to \"patient_immunization\"', NULL, NULL, NULL, NULL, '2021-06-23 08:38:07'),
(90, 8, NULL, NULL, 'AddPatientData', 'Added patient data entry with id #2 to \"patient_immunization\"', NULL, NULL, NULL, NULL, '2021-06-23 08:42:51'),
(91, 8, NULL, NULL, 'UpdatePatientData', 'Updated patient data \"patient_immunization\" entry with id #2, property \"quantity\" into new value \"51\"', NULL, NULL, NULL, NULL, '2021-06-23 08:42:55'),
(92, 8, NULL, NULL, 'AddPatientData', 'Added patient data entry with id #1 to \"patient_goal\"', NULL, NULL, NULL, NULL, '2021-06-23 08:51:17'),
(93, 8, NULL, NULL, 'UpdatePatientData', 'Updated patient data \"patient_goal\" entry with id #1, property \"goalText\" into new value \"I want to be healthy now\"', NULL, NULL, NULL, NULL, '2021-06-23 08:51:32'),
(94, 8, NULL, NULL, 'UpdatePatientData', 'Updated patient data \"patient_goal\" entry with id #1, property \"goalText\" into new value \"I want to be healthy\"', NULL, NULL, NULL, NULL, '2021-06-23 08:51:36'),
(95, 8, NULL, NULL, 'AddPatientData', 'Added patient data entry with id #1 to \"patient_assessment\"', NULL, NULL, NULL, NULL, '2021-06-23 08:59:44'),
(96, 8, NULL, NULL, 'UpdatePatientData', 'Updated patient data \"patient_assessment\" entry with id #1, property \"assessment\" into new value \"Example assessment 1\"', NULL, NULL, NULL, NULL, '2021-06-23 08:59:47'),
(97, 8, NULL, NULL, 'AddPatientData', 'Added patient data entry with id #1 to \"patient_implanted_device\"', NULL, NULL, NULL, NULL, '2021-06-23 09:20:05'),
(98, 8, NULL, NULL, 'UpdatePatientData', 'Updated patient data \"patient_implanted_device\" entry with id #1, property \"siteCode\" into new value \"E1234\"', NULL, NULL, NULL, NULL, '2021-06-23 09:20:54'),
(99, 8, NULL, NULL, 'AddPatientData', 'Added patient data entry with id #1 to \"patient_medication\"', NULL, NULL, NULL, NULL, '2021-06-23 09:32:54'),
(100, 8, NULL, NULL, 'UpdatePatientData', 'Updated patient data \"patient_medication\" entry with id #1, property \"name\" into new value \"Xanaxx\"', NULL, NULL, NULL, NULL, '2021-06-23 09:32:59'),
(101, 8, NULL, NULL, 'UpdatePatientData', 'Updated patient data \"patient_medication\" entry with id #1, property \"name\" into new value \"Xanax\"', NULL, NULL, NULL, NULL, '2021-06-23 09:33:02'),
(102, 8, NULL, NULL, 'AddPatient', 'Added new patient \"Amelia Bob Watson\" with id #22', NULL, NULL, NULL, NULL, '2021-06-23 09:44:07'),
(103, 8, NULL, NULL, 'AddMasterData', 'Added master data entry \"Test Dep\" to \"event_type\"', NULL, NULL, NULL, NULL, '2021-06-23 13:49:06'),
(104, 8, NULL, NULL, 'AddEvent', 'Added event #38 for patient #22', NULL, NULL, NULL, NULL, '2021-06-23 13:49:33'),
(105, 8, NULL, NULL, 'DeleteMasterData', 'Deleted master data entry from \"event_type\"', NULL, NULL, NULL, NULL, '2021-06-23 13:52:08'),
(106, 8, NULL, NULL, 'DeleteEvent', 'Deleted event #38 for patient \"Amelia Bob Watson\" with physician \"Demo Physician One\" in facility \"Ascension - St. John\" planned to begin in \"Sat Jun 26 2021 13:49:00 GMT+0300 (Eastern European Summer Time)\"', NULL, NULL, NULL, NULL, '2021-06-23 13:52:19'),
(107, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"cardiacClearanceStatus\":\"pending\"}', NULL, NULL, NULL, '2021-06-23 16:12:01'),
(108, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 cardiac clearance from accepted to pending', NULL, NULL, NULL, NULL, '2021-06-23 16:12:01'),
(109, 8, 39, NULL, 'AddEvent', 'Added event #39 for patient #20', NULL, NULL, NULL, NULL, '2021-06-23 16:13:42'),
(110, 8, 39, NULL, 'UpdateEvent', 'Updated event #39', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-23 16:13:57'),
(111, 8, 39, NULL, 'UpdateEvent', 'Updated event #39', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"pending\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-23 16:14:19'),
(112, 8, 39, NULL, 'UpdateEventClearance', 'Updated event #39 medical clearance from not_required to pending', NULL, NULL, NULL, NULL, '2021-06-23 16:14:19'),
(113, 8, 39, NULL, 'UpdateEvent', 'Updated event #39', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"pending\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-23 16:15:33'),
(114, 8, 39, NULL, 'UpdateEvent', 'Updated event #39', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":17,\"diagnosisCode\":\"K40.0\",\"diagnosisDescription\":\"Inguinal Hernia – Bilateral\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"pending\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-23 16:17:03'),
(115, 8, 39, NULL, 'UpdateEvent', 'Updated event #39', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":17,\"diagnosisCode\":\"K40.0\",\"diagnosisDescription\":\"Inguinal Hernia – Bilateral\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"pending\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-23 16:17:30'),
(116, 8, 39, NULL, 'UpdateEvent', 'Updated status of event #39 to \"Boarding Slip Generated\"', NULL, NULL, NULL, NULL, '2021-06-23 16:17:58'),
(117, 8, 39, NULL, 'UpdateEvent', 'Updated event #39', '{\"medicalClearanceStatus\":\"accepted\"}', NULL, NULL, NULL, '2021-06-23 16:20:23'),
(118, 8, 39, NULL, 'UpdateEventClearance', 'Updated event #39 medical clearance from pending to accepted', NULL, NULL, NULL, NULL, '2021-06-23 16:20:23'),
(119, 8, NULL, NULL, 'AddEvent', 'Added event #40 for patient #22', NULL, NULL, NULL, NULL, '2021-06-23 16:21:48'),
(120, 8, NULL, NULL, 'DeleteEvent', 'Deleted event #40 for patient \"Amelia Bob Watson\" with physician \"Demo Physician One\" in facility \"Ascension - Oakland\" planned to begin in \"Wed Jun 30 2021 13:49:00 GMT+0300 (Eastern European Summer Time)\"', NULL, NULL, NULL, NULL, '2021-06-23 16:21:58'),
(121, 8, 39, NULL, 'UpdateEvent', 'Updated status of event #39 to \"Waiting For Confirmation\"', NULL, NULL, NULL, NULL, '2021-06-23 16:22:25'),
(122, 8, 39, NULL, 'UpdateEvent', 'Updated status of event #39 to \"Rejected\"', NULL, NULL, NULL, NULL, '2021-06-23 16:22:38'),
(123, 8, 39, NULL, 'UpdateEvent', 'Updated status of event #39 to \"Waiting For Confirmation\"', NULL, NULL, NULL, NULL, '2021-06-23 16:22:45'),
(124, 8, 39, NULL, 'UpdateEvent', 'Updated status of event #39 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-23 16:24:51'),
(125, 8, 39, NULL, 'UpdateEvent', 'Updated event #39', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":17,\"diagnosisCode\":\"K40.0\",\"diagnosisDescription\":\"Inguinal Hernia – Bilateral\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"accepted\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-24 10:14:15'),
(126, 8, 39, NULL, 'UpdateEventClearance', 'Updated event #39 cardiac clearance from not_required to pending', NULL, NULL, NULL, NULL, '2021-06-24 10:14:15'),
(127, 8, 39, NULL, 'UpdateEventClearance', 'Updated event #39 other clearance from not_required to rejected', NULL, NULL, NULL, NULL, '2021-06-24 10:14:15'),
(128, 8, 33, NULL, 'UpdateEvent', 'Updated status of event #33 to \"Completed\"', NULL, NULL, NULL, NULL, '2021-06-24 10:51:03'),
(129, 8, 41, NULL, 'AddEvent', 'Added event #41 for patient #22', NULL, NULL, NULL, NULL, '2021-06-24 10:55:05'),
(130, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-24 11:00:16'),
(131, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-24 11:00:37'),
(132, 8, 41, NULL, 'UpdateEvent', 'Updated status of event #41 to \"Boarding Slip Generated\"', NULL, NULL, NULL, NULL, '2021-06-24 11:00:42'),
(133, 8, 41, NULL, 'UpdateEvent', 'Updated status of event #41 to \"Waiting For Confirmation\"', NULL, NULL, NULL, NULL, '2021-06-24 11:00:59'),
(134, 8, 41, NULL, 'UpdateEvent', 'Updated status of event #41 to \"Rejected\"', NULL, NULL, NULL, NULL, '2021-06-24 11:01:04'),
(135, 8, 41, NULL, 'UpdateEvent', 'Updated status of event #41 to \"Waiting For Confirmation\"', NULL, NULL, NULL, NULL, '2021-06-24 11:01:06'),
(136, 8, 41, NULL, 'UpdateEvent', 'Updated status of event #41 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-24 11:01:10'),
(137, 8, 41, NULL, 'UpdateEvent', 'Updated status of event #41 to \"Completed\"', NULL, NULL, NULL, NULL, '2021-06-24 11:01:13'),
(138, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"rejected\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-24 12:19:50'),
(139, 8, 39, NULL, 'UpdateEvent', 'Updated event #39', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":17,\"diagnosisCode\":\"K40.0\",\"diagnosisDescription\":\"Inguinal Hernia – Bilateral\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"accepted\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-24 12:24:47'),
(140, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"rejected\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-24 14:19:50'),
(141, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"rejected\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-24 14:24:09'),
(142, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"rejected\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-24 14:25:46'),
(143, 8, 33, NULL, 'UpdateEvent', 'Updated event #33', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223355\",\"patientPreferredPhone\":\"1112223355\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"male\",\"bloodThinnerId\":26,\"bloodThinnerCode\":\"Plavix\",\"bloodThinnerName\":\"Plavix 75mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Plavix 75mg 7 days before surgery.\",\"diagnosisId\":17,\"diagnosisCode\":\"K40.0\",\"diagnosisDescription\":\"Inguinal Hernia – Bilateral\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"rejected\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Example Cardiac Physician\",\"medicalClearancePhysician\":null,\"otherClearancePhysician\":\"Other Medical Physiciann\"}', NULL, NULL, NULL, '2021-06-24 15:42:24'),
(144, 8, 33, NULL, 'UpdateEvent', 'Updated event #33', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223355\",\"patientPreferredPhone\":\"1112223355\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"male\",\"bloodThinnerId\":26,\"bloodThinnerCode\":\"Plavix\",\"bloodThinnerName\":\"Plavix 75mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Plavix 75mg 7 days before surgery.\",\"diagnosisId\":17,\"diagnosisCode\":\"K40.0\",\"diagnosisDescription\":\"Inguinal Hernia – Bilateral\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"rejected\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Example Cardiac Physician\",\"medicalClearancePhysician\":null,\"otherClearancePhysician\":\"Other Medical Physiciann\"}', NULL, NULL, NULL, '2021-06-24 15:42:56'),
(145, 8, 33, NULL, 'UpdateEvent', 'Updated event #33', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223355\",\"patientPreferredPhone\":\"1112223355\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"male\",\"bloodThinnerId\":26,\"bloodThinnerCode\":\"Plavix\",\"bloodThinnerName\":\"Plavix 75mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Plavix 75mg 7 days before surgery.\",\"diagnosisId\":17,\"diagnosisCode\":\"K40.0\",\"diagnosisDescription\":\"Inguinal Hernia – Bilateral\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"rejected\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Example Cardiac Physician\",\"medicalClearancePhysician\":null,\"otherClearancePhysician\":\"Other Medical Physiciann\"}', NULL, NULL, NULL, '2021-06-24 15:43:02'),
(146, 8, 33, NULL, 'UpdateEvent', 'Updated event #33', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223355\",\"patientPreferredPhone\":\"1112223355\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"male\",\"bloodThinnerId\":26,\"bloodThinnerCode\":\"Plavix\",\"bloodThinnerName\":\"Plavix 75mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Plavix 75mg 7 days before surgery.\",\"diagnosisId\":17,\"diagnosisCode\":\"K40.0\",\"diagnosisDescription\":\"Inguinal Hernia – Bilateral\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"rejected\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Example Cardiac Physician\",\"medicalClearancePhysician\":null,\"otherClearancePhysician\":\"Other Medical Physiciann\"}', NULL, NULL, NULL, '2021-06-24 15:43:04'),
(147, 8, 33, NULL, 'UpdateEvent', 'Updated event #33', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223355\",\"patientPreferredPhone\":\"1112223355\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"male\",\"bloodThinnerId\":26,\"bloodThinnerCode\":\"Plavix\",\"bloodThinnerName\":\"Plavix 75mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Plavix 75mg 7 days before surgery.\",\"diagnosisId\":17,\"diagnosisCode\":\"K40.0\",\"diagnosisDescription\":\"Inguinal Hernia – Bilateral\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"rejected\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Example Cardiac Physician\",\"medicalClearancePhysician\":null,\"otherClearancePhysician\":\"Other Medical Physiciann\"}', NULL, NULL, NULL, '2021-06-24 15:43:11'),
(148, 8, 42, NULL, 'AddEvent', 'Added event #42 for patient #22', NULL, NULL, NULL, NULL, '2021-06-24 15:45:17'),
(149, 8, 42, NULL, 'UpdateEvent', 'Updated event #42', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-24 15:45:23'),
(150, 8, 42, NULL, 'UpdateEvent', 'Updated event #42', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":17,\"diagnosisCode\":\"K40.0\",\"diagnosisDescription\":\"Inguinal Hernia – Bilateral\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-24 15:45:34'),
(151, 8, 42, NULL, 'UpdateEvent', 'Updated event #42', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":17,\"diagnosisCode\":\"K40.0\",\"diagnosisDescription\":\"Inguinal Hernia – Bilateral\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-24 15:45:35'),
(152, 8, 42, NULL, 'UpdateEvent', 'Updated status of event #42 to \"Boarding Slip Generated\"', NULL, NULL, NULL, NULL, '2021-06-24 15:46:31'),
(153, 8, 42, NULL, 'UpdateEvent', 'Updated status of event #42 to \"Waiting For Confirmation\"', NULL, NULL, NULL, NULL, '2021-06-24 15:46:58'),
(154, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"rejected\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-24 16:17:16'),
(155, 8, NULL, NULL, 'UpdatePatient', 'Updated patient #22', '{\"patientId\":22,\"firstName\":\"Amelia\",\"middleName\":\"Bob\",\"lastName\":\"Watson\",\"address1\":\"Main Street 123\",\"address2\":\"Bld 3\",\"state\":\"Michigan\",\"city\":\"Detroit\",\"zipcode\":\"11223344\",\"dateofbirth\":\"1995-07-24\",\"email\":\"ame.wat@mail.com\",\"phone\":\"1112227777\",\"ssn\":\"999-88-7778\",\"sex\":\"female\",\"carrierId\":\"633\",\"subscriberId\":\"222\",\"groupName\":\"Group Twooo\",\"groupNumber\":\"G222\"}', NULL, NULL, NULL, '2021-06-25 08:42:34'),
(156, 8, NULL, NULL, 'UpdatePatient', 'Updated patient #22', '{\"patientId\":22,\"firstName\":\"Amelia\",\"middleName\":\"Bob\",\"lastName\":\"Watson\",\"address1\":\"Main Street 123\",\"address2\":\"Bld 3\",\"state\":\"Michigan\",\"city\":\"Detroit\",\"zipcode\":\"11223344\",\"dateofbirth\":\"1995-07-24\",\"email\":\"ame.wat@mail.com\",\"phone\":\"1112227777\",\"ssn\":\"999-88-7778\",\"sex\":\"female\",\"carrierId\":\"638\",\"subscriberId\":\"222\",\"groupName\":\"Group Twooo\",\"groupNumber\":\"G222\"}', NULL, NULL, NULL, '2021-06-25 08:42:39'),
(157, 8, NULL, NULL, 'UpdatePatient', 'Updated patient #22', '{\"patientId\":22,\"firstName\":\"Amelia\",\"middleName\":\"Bob\",\"lastName\":\"Watson\",\"address1\":\"Main Street 123\",\"address2\":\"Bld 3\",\"state\":\"Michigan\",\"city\":\"Detroit\",\"zipcode\":\"11223344\",\"dateofbirth\":\"1995-07-24\",\"email\":\"ame.wat@mail.com\",\"phone\":\"1112227777\",\"ssn\":\"999-88-7778\",\"sex\":\"female\",\"carrierId\":\"637\",\"subscriberId\":\"222\",\"groupName\":\"Group Twooo\",\"groupNumber\":\"G222\"}', NULL, NULL, NULL, '2021-06-25 08:44:00'),
(158, 8, NULL, NULL, 'UpdatePatient', 'Updated patient #22', '{\"patientId\":22,\"firstName\":\"Amelia\",\"middleName\":\"Bob\",\"lastName\":\"Watson\",\"address1\":\"Main Street 123\",\"address2\":\"Bld 3\",\"state\":\"Michigan\",\"city\":\"Detroit\",\"zipcode\":\"11223344\",\"dateofbirth\":\"1995-07-24\",\"email\":\"ame.wat@mail.com\",\"phone\":\"1112227777\",\"ssn\":\"999-88-7778\",\"sex\":\"female\",\"carrierId\":\"637\",\"subscriberId\":\"222\",\"groupName\":\"Group Twooo\",\"groupNumber\":\"G222\"}', NULL, NULL, NULL, '2021-06-25 08:44:25'),
(159, 8, NULL, NULL, 'UpdatePatient', 'Updated patient #22', '{\"patientId\":22,\"firstName\":\"Amelia\",\"middleName\":\"Bob\",\"lastName\":\"Watson\",\"address1\":\"Main Street 123\",\"address2\":\"Bld 3\",\"state\":\"Michigan\",\"city\":\"Detroit\",\"zipcode\":\"11223344\",\"dateofbirth\":\"1995-07-24\",\"email\":\"ame.wat@mail.com\",\"phone\":\"1112227777\",\"ssn\":\"999-88-7778\",\"sex\":\"female\",\"carrierId\":\"638\",\"carrierCode\":\"AET05\",\"carrierName\":\"AETNA BETTER HEALTH OF MICHIGAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Twooo\",\"groupNumber\":\"G222\"}', NULL, NULL, NULL, '2021-06-25 08:46:02'),
(160, 8, NULL, NULL, 'UpdatePatient', 'Updated patient #22', '{\"patientId\":22,\"firstName\":\"Amelia\",\"middleName\":\"Bob\",\"lastName\":\"Watson\",\"address1\":\"Main Street 123\",\"address2\":\"Bld 3\",\"state\":\"Michigan\",\"city\":\"Detroit\",\"zipcode\":\"11223344\",\"dateofbirth\":\"1995-07-24\",\"email\":\"ame.wat@mail.com\",\"phone\":\"1112227777\",\"ssn\":\"999-88-7778\",\"sex\":\"female\",\"carrierId\":\"645\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Twooo\",\"groupNumber\":\"G222\"}', NULL, NULL, NULL, '2021-06-25 08:46:07'),
(161, 8, NULL, NULL, 'UpdatePatient', 'Updated patient #22', '{\"patientId\":22,\"firstName\":\"Amelia\",\"middleName\":\"Bob\",\"lastName\":\"Watson\",\"address1\":\"Main Street 123\",\"address2\":\"Bld 3\",\"state\":\"Michigan\",\"city\":\"Detroit\",\"zipcode\":\"11223344\",\"dateofbirth\":\"1995-07-24\",\"email\":\"ame.wat@mail.com\",\"phone\":\"1112227777\",\"ssn\":\"999-88-7778\",\"sex\":\"female\",\"carrierId\":\"645\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\"}', NULL, NULL, NULL, '2021-06-25 08:46:28'),
(162, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"carrierCode\":\"CONWE\",\"carrierName\":\"CONTINENTAL WESTERN\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"rejected\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-25 08:58:33'),
(163, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"carrierCode\":\"CONWE\",\"carrierName\":\"CONTINENTAL WESTERN\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"rejected\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-25 08:58:41'),
(164, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"carrierCode\":\"CIG08\",\"carrierName\":\"CIGNA HEALTH PLANS\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"rejected\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-25 08:58:53'),
(165, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"carrierCode\":\"CIG08\",\"carrierName\":\"CIGNA HEALTH PLANS\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"rejected\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-25 08:59:00');
INSERT INTO `history` (`id`, `userId`, `eventId`, `patientId`, `actionType`, `description`, `extra1`, `extra2`, `extra3`, `extra4`, `createdOn`) VALUES
(166, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"carrierCode\":\"RMA\",\"carrierName\":\"RELIANCE MEDICARE ADV\",\"subscriberId\":\"1111\",\"groupName\":\"Group One1\",\"groupNumber\":\"G1111\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"rejected\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-25 09:00:46'),
(167, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"carrierCode\":\"RMA\",\"carrierName\":\"RELIANCE MEDICARE ADV\",\"subscriberId\":\"111\",\"groupName\":\"Group One\",\"groupNumber\":\"G111\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"rejected\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Example Medical Physician\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-25 09:00:55'),
(168, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:24:01'),
(169, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:25:34'),
(170, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:26:52'),
(171, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:27:10'),
(172, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:27:26'),
(173, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:28:07'),
(174, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:30:18'),
(175, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:30:33'),
(176, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:30:36'),
(177, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:30:42'),
(178, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:30:48'),
(179, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:31:07'),
(180, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:31:09'),
(181, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:31:11'),
(182, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:31:13'),
(183, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:32:04'),
(184, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:32:58'),
(185, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:34:57'),
(186, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:47:26'),
(187, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:47:30'),
(188, 8, 41, NULL, 'UpdateEvent', 'Updated event #41', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 09:47:41'),
(189, 8, 43, NULL, 'AddEvent', 'Added event #43 for patient #22', NULL, NULL, NULL, NULL, '2021-06-25 10:18:51'),
(190, 8, 44, NULL, 'AddEvent', 'Added event #44 for patient #22', NULL, NULL, NULL, NULL, '2021-06-25 10:19:18'),
(191, 8, 44, NULL, 'UpdateEvent', 'Updated event #44', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 10:20:09'),
(192, 8, 44, NULL, 'UpdateEvent', 'Updated event #44', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 10:20:22'),
(193, 8, 44, NULL, 'UpdateEvent', 'Updated event #44', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"not_required\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 10:20:24'),
(194, 8, 44, NULL, 'UpdateEvent', 'Updated status of event #44 to \"Boarding Slip Generated\"', NULL, NULL, NULL, NULL, '2021-06-25 10:20:34'),
(195, 8, NULL, NULL, 'AddPatient', 'Added new patient \"John Bobzy Doezzz\" with id #23', NULL, NULL, NULL, NULL, '2021-06-25 11:03:26'),
(196, 8, NULL, NULL, 'AddPatient', 'Added new patient \"John Patient Doe\" with id #24', NULL, NULL, NULL, NULL, '2021-06-25 11:15:02'),
(197, 8, NULL, NULL, 'UpdatePatient', 'Updated patient #20', '{\"patientId\":20,\"firstName\":\"John\",\"middleName\":\"William\",\"lastName\":\"Doe\",\"address1\":\"Main Street 123\",\"address2\":\"Bld 3\",\"state\":\"Michigan\",\"city\":\"Detroit\",\"zipcode\":\"11223343\",\"dateofbirth\":\"1984-02-09\",\"email\":\"john.doe@mail.com\",\"phone\":\"1112223399\",\"ssn\":\"111-22-3333\",\"sex\":\"male\",\"carrierId\":\"670\",\"carrierCode\":\"CONWE\",\"carrierName\":\"CONTINENTAL WESTERN\",\"subscriberId\":\"111\",\"groupName\":\"Group One\",\"groupNumber\":\"G111\"}', NULL, NULL, NULL, '2021-06-25 11:49:41'),
(198, 8, NULL, NULL, 'AddPatient', 'Added new patient \"John Patient Doe\" with id #25', NULL, NULL, NULL, NULL, '2021-06-25 12:19:22'),
(199, 8, 45, NULL, 'AddEvent', 'Added event #45 for patient #22', NULL, NULL, NULL, NULL, '2021-06-25 12:35:05'),
(200, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"accepted\"}', NULL, NULL, NULL, '2021-06-25 15:14:11'),
(201, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from rejected to accepted', NULL, NULL, NULL, NULL, '2021-06-25 15:14:11'),
(202, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"accepted\"}', NULL, NULL, NULL, '2021-06-25 15:16:38'),
(203, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"accepted\"}', NULL, NULL, NULL, '2021-06-25 15:16:40'),
(204, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"pending\"}', NULL, NULL, NULL, '2021-06-25 15:19:39'),
(205, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from accepted to pending', NULL, NULL, NULL, NULL, '2021-06-25 15:19:39'),
(206, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"not_required\"}', NULL, NULL, NULL, '2021-06-25 15:19:40'),
(207, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from pending to not_required', NULL, NULL, NULL, NULL, '2021-06-25 15:19:40'),
(208, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"accepted\"}', NULL, NULL, NULL, '2021-06-25 15:19:46'),
(209, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from not_required to accepted', NULL, NULL, NULL, NULL, '2021-06-25 15:19:46'),
(210, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"not_required\"}', NULL, NULL, NULL, '2021-06-25 15:20:10'),
(211, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from accepted to not_required', NULL, NULL, NULL, NULL, '2021-06-25 15:20:10'),
(212, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":{\"medicalClearanceStatus\":\"pending\",\"medicalClearancePhysician\":\"Clearance Phy 1\"}}', NULL, NULL, NULL, '2021-06-25 15:22:59'),
(213, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from not_required to [object Object]', NULL, NULL, NULL, NULL, '2021-06-25 15:22:59'),
(214, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"accepted\"}', NULL, NULL, NULL, '2021-06-25 15:24:45'),
(215, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from pending to accepted', NULL, NULL, NULL, NULL, '2021-06-25 15:24:45'),
(216, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"rejected\"}', NULL, NULL, NULL, '2021-06-25 15:24:47'),
(217, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from accepted to rejected', NULL, NULL, NULL, NULL, '2021-06-25 15:24:47'),
(218, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"pending\"}', NULL, NULL, NULL, '2021-06-25 15:24:48'),
(219, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from rejected to pending', NULL, NULL, NULL, NULL, '2021-06-25 15:24:48'),
(220, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"not_required\",\"medicalClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 15:24:49'),
(221, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from pending to not_required', NULL, NULL, NULL, NULL, '2021-06-25 15:24:49'),
(222, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"pending\",\"medicalClearancePhysician\":\"AKHFJSAf\"}', NULL, NULL, NULL, '2021-06-25 15:26:26'),
(223, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from not_required to pending', NULL, NULL, NULL, NULL, '2021-06-25 15:26:26'),
(224, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"accepted\"}', NULL, NULL, NULL, '2021-06-25 15:26:27'),
(225, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from pending to accepted', NULL, NULL, NULL, NULL, '2021-06-25 15:26:27'),
(226, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"rejected\"}', NULL, NULL, NULL, '2021-06-25 15:26:28'),
(227, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from accepted to rejected', NULL, NULL, NULL, NULL, '2021-06-25 15:26:28'),
(228, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"pending\"}', NULL, NULL, NULL, '2021-06-25 15:26:29'),
(229, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from rejected to pending', NULL, NULL, NULL, NULL, '2021-06-25 15:26:29'),
(230, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"not_required\",\"medicalClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 15:26:50'),
(231, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from pending to not_required', NULL, NULL, NULL, NULL, '2021-06-25 15:26:50'),
(232, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"pending\",\"medicalClearancePhysician\":\"Med 1\"}', NULL, NULL, NULL, '2021-06-25 15:29:44'),
(233, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from not_required to pending', NULL, NULL, NULL, NULL, '2021-06-25 15:29:44'),
(234, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"carrierCode\":\"CONWE\",\"carrierName\":\"CONTINENTAL WESTERN\",\"subscriberId\":\"111\",\"groupName\":\"Group One\",\"groupNumber\":\"G111\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":null,\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-25 15:29:49'),
(235, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from pending to not_required', NULL, NULL, NULL, NULL, '2021-06-25 15:29:49'),
(236, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"pending\",\"medicalClearancePhysician\":\"Med 1\"}', NULL, NULL, NULL, '2021-06-25 15:31:15'),
(237, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from not_required to pending', NULL, NULL, NULL, NULL, '2021-06-25 15:31:15'),
(238, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"carrierCode\":\"CONWE\",\"carrierName\":\"CONTINENTAL WESTERN\",\"subscriberId\":\"111\",\"groupName\":\"Group One\",\"groupNumber\":\"G111\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Med 1\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-25 15:31:21'),
(239, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from pending to not_required', NULL, NULL, NULL, NULL, '2021-06-25 15:31:21'),
(240, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"carrierCode\":\"CONWE\",\"carrierName\":\"CONTINENTAL WESTERN\",\"subscriberId\":\"111\",\"groupName\":\"Group One\",\"groupNumber\":\"G111\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":null,\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-25 15:31:26'),
(241, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"accepted\",\"medicalClearancePhysician\":\"Med 1\"}', NULL, NULL, NULL, '2021-06-25 15:31:30'),
(242, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from not_required to accepted', NULL, NULL, NULL, NULL, '2021-06-25 15:31:30'),
(243, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"carrierCode\":\"CONWE\",\"carrierName\":\"CONTINENTAL WESTERN\",\"subscriberId\":\"111\",\"groupName\":\"Group One\",\"groupNumber\":\"G111\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Med 1\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-25 15:31:35'),
(244, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from accepted to not_required', NULL, NULL, NULL, NULL, '2021-06-25 15:31:35'),
(245, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"pending\",\"medicalClearancePhysician\":\"Med 1\"}', NULL, NULL, NULL, '2021-06-25 15:32:47'),
(246, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from not_required to pending', NULL, NULL, NULL, NULL, '2021-06-25 15:32:47'),
(247, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"carrierCode\":\"CONWE\",\"carrierName\":\"CONTINENTAL WESTERN\",\"subscriberId\":\"111\",\"groupName\":\"Group One\",\"groupNumber\":\"G111\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearanceStatus\":\"pending\",\"medicalClearanceStatus\":\"not_required\",\"otherClearanceStatus\":\"rejected\",\"cardiacClearancePhysician\":\"Car Phy 1\",\"medicalClearancePhysician\":\"Med 2\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-25 15:32:52'),
(248, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from pending to not_required', NULL, NULL, NULL, NULL, '2021-06-25 15:32:52'),
(249, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"accepted\",\"medicalClearancePhysician\":\"Med 1\"}', NULL, NULL, NULL, '2021-06-25 15:33:38'),
(250, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from not_required to accepted', NULL, NULL, NULL, NULL, '2021-06-25 15:33:38'),
(251, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"carrierCode\":\"CONWE\",\"carrierName\":\"CONTINENTAL WESTERN\",\"subscriberId\":\"111\",\"groupName\":\"Group One\",\"groupNumber\":\"G111\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"medicalClearancePhysician\":\"Med 2\"}', NULL, NULL, NULL, '2021-06-25 15:33:42'),
(252, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"carrierCode\":\"CONWE\",\"carrierName\":\"CONTINENTAL WESTERN\",\"subscriberId\":\"111\",\"groupName\":\"Group One\",\"groupNumber\":\"G111\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"medicalClearancePhysician\":\"Med 1\"}', NULL, NULL, NULL, '2021-06-25 15:33:51'),
(253, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"pending\"}', NULL, NULL, NULL, '2021-06-25 15:33:52'),
(254, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from accepted to pending', NULL, NULL, NULL, NULL, '2021-06-25 15:33:52'),
(255, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"rejected\"}', NULL, NULL, NULL, '2021-06-25 15:33:53'),
(256, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from pending to rejected', NULL, NULL, NULL, NULL, '2021-06-25 15:33:53'),
(257, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"accepted\"}', NULL, NULL, NULL, '2021-06-25 15:33:55'),
(258, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from rejected to accepted', NULL, NULL, NULL, NULL, '2021-06-25 15:33:55'),
(259, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"not_required\",\"medicalClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 15:33:56'),
(260, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from accepted to not_required', NULL, NULL, NULL, NULL, '2021-06-25 15:33:56'),
(261, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"pending\",\"medicalClearancePhysician\":\"Medical Phy 1\"}', NULL, NULL, NULL, '2021-06-25 15:34:03'),
(262, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from not_required to pending', NULL, NULL, NULL, NULL, '2021-06-25 15:34:03'),
(263, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"cardiacClearanceStatus\":\"accepted\"}', NULL, NULL, NULL, '2021-06-25 15:36:39'),
(264, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 cardiac clearance from pending to accepted', NULL, NULL, NULL, NULL, '2021-06-25 15:36:39'),
(265, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"cardiacClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 15:36:40'),
(266, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 cardiac clearance from accepted to not_required', NULL, NULL, NULL, NULL, '2021-06-25 15:36:40'),
(267, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"cardiacClearanceStatus\":\"accepted\",\"cardiacClearancePhysician\":\"Card Phy 1\"}', NULL, NULL, NULL, '2021-06-25 15:36:46'),
(268, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 cardiac clearance from not_required to accepted', NULL, NULL, NULL, NULL, '2021-06-25 15:36:46'),
(269, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"otherClearanceStatus\":\"accepted\"}', NULL, NULL, NULL, '2021-06-25 15:38:51'),
(270, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 other clearance from rejected to accepted', NULL, NULL, NULL, NULL, '2021-06-25 15:38:51'),
(271, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"otherClearanceStatus\":\"not_required\",\"otherClearancePhysician\":null,\"otherClearance\":null}', NULL, NULL, NULL, '2021-06-25 15:38:52'),
(272, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 other clearance from accepted to not_required', NULL, NULL, NULL, NULL, '2021-06-25 15:38:52'),
(273, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"otherClearanceStatus\":\"accepted\",\"otherClearancePhysician\":\"Other phy 1\"}', NULL, NULL, NULL, '2021-06-25 15:38:56'),
(274, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 other clearance from not_required to accepted', NULL, NULL, NULL, NULL, '2021-06-25 15:38:56'),
(275, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"cardiacClearanceStatus\":\"pending\"}', NULL, NULL, NULL, '2021-06-25 15:42:13'),
(276, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 cardiac clearance from accepted to pending', NULL, NULL, NULL, NULL, '2021-06-25 15:42:13'),
(277, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"cardiacClearanceStatus\":\"not_required\",\"cardiacClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 15:42:15'),
(278, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 cardiac clearance from pending to not_required', NULL, NULL, NULL, NULL, '2021-06-25 15:42:15'),
(279, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"not_required\",\"medicalClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-25 15:42:16'),
(280, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from pending to not_required', NULL, NULL, NULL, NULL, '2021-06-25 15:42:16'),
(281, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"otherClearanceStatus\":\"not_required\",\"otherClearancePhysician\":null,\"otherClearance\":null}', NULL, NULL, NULL, '2021-06-25 15:42:18'),
(282, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 other clearance from accepted to not_required', NULL, NULL, NULL, NULL, '2021-06-25 15:42:18'),
(283, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"accepted\",\"medicalClearancePhysician\":\"Med Phy 1\"}', NULL, NULL, NULL, '2021-06-25 15:42:30'),
(284, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from not_required to accepted', NULL, NULL, NULL, NULL, '2021-06-25 15:42:30'),
(285, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"medicalClearanceStatus\":\"pending\"}', NULL, NULL, NULL, '2021-06-25 15:47:48'),
(286, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 medical clearance from accepted to pending', NULL, NULL, NULL, NULL, '2021-06-25 15:47:48'),
(287, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"otherClearanceStatus\":\"accepted\",\"otherClearancePhysician\":\"Other Clr 1\"}', NULL, NULL, NULL, '2021-06-25 15:49:21'),
(288, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 other clearance from not_required to accepted', NULL, NULL, NULL, NULL, '2021-06-25 15:49:21'),
(289, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"carrierCode\":\"CONWE\",\"carrierName\":\"CONTINENTAL WESTERN\",\"subscriberId\":\"111\",\"groupName\":\"Group One\",\"groupNumber\":\"G111\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Med 1\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-25 15:49:31'),
(290, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"carrierCode\":\"CONWE\",\"carrierName\":\"CONTINENTAL WESTERN\",\"subscriberId\":\"111\",\"groupName\":\"Group One\",\"groupNumber\":\"G111\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":\"Sample Info\",\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Med 1\",\"otherClearancePhysician\":\"Other Medical Physician\"}', NULL, NULL, NULL, '2021-06-25 15:49:51'),
(291, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"otherClearanceStatus\":\"not_required\",\"otherClearancePhysician\":null,\"otherClearance\":null}', NULL, NULL, NULL, '2021-06-25 15:49:53'),
(292, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 other clearance from accepted to not_required', NULL, NULL, NULL, NULL, '2021-06-25 15:49:53'),
(293, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"otherClearanceStatus\":\"accepted\",\"otherClearancePhysician\":\"Other Med 1\"}', NULL, NULL, NULL, '2021-06-25 15:50:01'),
(294, 8, 37, NULL, 'UpdateEventClearance', 'Updated event #37 other clearance from not_required to accepted', NULL, NULL, NULL, NULL, '2021-06-25 15:50:01'),
(295, 8, NULL, NULL, 'DeletePatient', 'Deleted patient #23', NULL, NULL, NULL, NULL, '2021-06-25 15:53:30'),
(296, 8, NULL, NULL, 'DeletePatient', 'Deleted patient #24', NULL, NULL, NULL, NULL, '2021-06-25 15:53:34'),
(297, 8, NULL, NULL, 'DeletePatient', 'Deleted patient #25', NULL, NULL, NULL, NULL, '2021-06-25 15:53:37'),
(298, 8, 39, NULL, 'UpdateEvent', 'Updated status of event #39 to \"Completed\"', NULL, NULL, NULL, NULL, '2021-06-25 16:16:53'),
(299, 8, 39, NULL, 'UpdateEvent', 'Updated status of event #39 to \"Waiting For Confirmation\"', NULL, NULL, NULL, NULL, '2021-06-25 16:27:15'),
(300, 8, 39, NULL, 'UpdateEvent', 'Updated status of event #39 to \"Boarding Slip Generated\"', NULL, NULL, NULL, NULL, '2021-06-25 16:27:19'),
(301, 8, 39, NULL, 'UpdateEvent', 'Updated status of event #39 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-25 16:27:26'),
(302, 8, 39, NULL, 'UpdateEvent', 'Updated status of event #39 to \"Waiting For Confirmation\"', NULL, NULL, NULL, NULL, '2021-06-25 16:28:30'),
(303, 8, NULL, NULL, 'AddPatient', 'Added new patient \"Zahrah Bobzy Blevins\" with id #26', NULL, NULL, NULL, NULL, '2021-06-28 09:56:43'),
(304, 8, NULL, NULL, 'AddPatient', 'Added new patient \"Fletcher Bob Snider\" with id #27', NULL, NULL, NULL, NULL, '2021-06-28 09:57:32');
INSERT INTO `history` (`id`, `userId`, `eventId`, `patientId`, `actionType`, `description`, `extra1`, `extra2`, `extra3`, `extra4`, `createdOn`) VALUES
(305, 8, NULL, NULL, 'AddPatient', 'Added new patient \"Elaina Bob Pratt\" with id #28', NULL, NULL, NULL, NULL, '2021-06-28 09:58:44'),
(306, 8, NULL, NULL, 'AddPatient', 'Added new patient \"Jayden-James Bob Kerr\" with id #29', NULL, NULL, NULL, NULL, '2021-06-28 09:59:45'),
(307, 8, 46, NULL, 'AddEvent', 'Added event #46 for patient #22', NULL, NULL, NULL, NULL, '2021-06-28 10:15:58'),
(308, 8, 46, NULL, 'UpdateEvent', 'Updated event #46', '{\"medicalClearanceStatus\":\"accepted\",\"medicalClearancePhysician\":\"Med Phy\"}', NULL, NULL, NULL, '2021-06-28 10:16:23'),
(309, 8, 46, NULL, 'UpdateEventClearance', 'Updated event #46 medical clearance from not_required to accepted', NULL, NULL, NULL, NULL, '2021-06-28 10:16:23'),
(310, 8, 46, NULL, 'UpdateEvent', 'Updated event #46', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Med Phy\",\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-28 10:16:44'),
(311, 8, NULL, NULL, 'AddPatient', 'Added new patient \"Aniya John Gardiner\" with id #30', NULL, NULL, NULL, NULL, '2021-06-28 12:36:46'),
(312, 8, NULL, NULL, 'UpdatePatient', 'Updated patient #30', '{\"patientId\":30,\"firstName\":\"Aniya\",\"middleName\":\"John\",\"lastName\":\"Gardiner\",\"address1\":\"Add1\",\"address2\":\"Add2\",\"state\":\"Washington\",\"city\":\"Boston\",\"zipcode\":\"12234556\",\"dateofbirth\":\"1983-06-06\",\"email\":\"aniya@mail.com\",\"phone\":\"1112223333\",\"ssn\":\"777-66-5555\",\"sex\":\"female\",\"carrierId\":\"633\",\"carrierCode\":\"AAA\",\"carrierName\":\"AAA OF MICHIGAN\",\"subscriberId\":\"333\",\"groupName\":\"333\",\"groupNumber\":\"444\"}', NULL, NULL, NULL, '2021-06-28 12:38:52'),
(313, 8, NULL, NULL, 'UpdatePatient', 'Updated patient #30', '{\"patientId\":30,\"firstName\":\"Aniya\",\"middleName\":\"John\",\"lastName\":\"Gardiner\",\"address1\":\"Add1\",\"address2\":\"Add2\",\"state\":\"Washington\",\"city\":\"Boston\",\"zipcode\":\"12234556\",\"dateofbirth\":\"1983-06-06\",\"email\":\"aniya@mail.com\",\"phone\":\"1112223333\",\"ssn\":\"777-66-4444\",\"sex\":\"female\",\"carrierId\":\"633\",\"carrierCode\":\"AAA\",\"carrierName\":\"AAA OF MICHIGAN\",\"subscriberId\":\"333\",\"groupName\":\"333\",\"groupNumber\":\"444\"}', NULL, NULL, NULL, '2021-06-28 12:41:49'),
(314, 8, 42, NULL, 'UpdateEvent', 'Updated event #42', '{\"insuranceAuthorizationStatus\":\"tbd\",\"insuranceAuthorizationNumber\":\"5555\"}', NULL, NULL, NULL, '2021-06-28 13:16:19'),
(315, 8, 42, NULL, 'UpdateEventInsuranceAuthorization', 'Updated event #42 insurance authorization status from not-required to tbd with authorization number \"5555\"', NULL, NULL, NULL, NULL, '2021-06-28 13:16:19'),
(316, 8, 42, NULL, 'UpdateEvent', 'Updated event #42', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":17,\"diagnosisCode\":\"K40.0\",\"diagnosisDescription\":\"Inguinal Hernia – Bilateral\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-28 13:16:26'),
(317, 8, 42, NULL, 'UpdateEvent', 'Updated event #42', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":17,\"diagnosisCode\":\"K40.0\",\"diagnosisDescription\":\"Inguinal Hernia – Bilateral\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null,\"insuranceAuthorizationNumber\":\"7777\"}', NULL, NULL, NULL, '2021-06-28 13:17:17'),
(318, 8, 42, NULL, 'UpdateEvent', 'Updated event #42', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"VACCN\",\"carrierName\":\"VA CCN OPTUM\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":17,\"diagnosisCode\":\"K40.0\",\"diagnosisDescription\":\"Inguinal Hernia – Bilateral\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null,\"insuranceAuthorizationNumber\":\"8888\"}', NULL, NULL, NULL, '2021-06-28 13:17:26'),
(319, 8, 42, NULL, 'UpdateEvent', 'Updated event #42', '{\"insuranceAuthorizationStatus\":\"approved\"}', NULL, NULL, NULL, '2021-06-28 13:17:28'),
(320, 8, 42, NULL, 'UpdateEventInsuranceAuthorization', 'Updated event #42 insurance authorization status from tbd to approved', NULL, NULL, NULL, NULL, '2021-06-28 13:17:28'),
(321, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"insuranceAuthorizationStatus\":\"tbd\",\"insuranceAuthorizationNumber\":\"444\"}', NULL, NULL, NULL, '2021-06-28 13:23:26'),
(322, 8, 37, NULL, 'UpdateEventInsuranceAuthorization', 'Updated event #37 insurance authorization status from not-required to tbd with authorization number \"444\"', NULL, NULL, NULL, NULL, '2021-06-28 13:23:26'),
(323, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223343\",\"patientPreferredPhone\":\"1112223388\",\"patientDoB\":\"1984-02-09\",\"patientGender\":\"male\",\"carrierCode\":\"CONWE\",\"carrierName\":\"CONTINENTAL WESTERN\",\"subscriberId\":\"111\",\"groupName\":\"Group One\",\"groupNumber\":\"G111\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":16,\"diagnosisCode\":\"K29.70\",\"diagnosisDescription\":\"Gastritis\",\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Med 1\",\"otherClearancePhysician\":\"Other Med 1\",\"insuranceAuthorizationNumber\":\"333\"}', NULL, NULL, NULL, '2021-06-28 13:23:31'),
(324, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"insuranceAuthorizationStatus\":\"not-required\",\"insuranceAuthorizationNumber\":null}', NULL, NULL, NULL, '2021-06-28 13:23:48'),
(325, 8, 37, NULL, 'UpdateEventInsuranceAuthorization', 'Updated event #37 insurance authorization status from tbd to not-required', NULL, NULL, NULL, NULL, '2021-06-28 13:23:48'),
(326, 8, 37, NULL, 'UpdateEvent', 'Updated event #37', '{\"insuranceAuthorizationStatus\":\"approved\",\"insuranceAuthorizationNumber\":\"5555\"}', NULL, NULL, NULL, '2021-06-28 13:27:59'),
(327, 8, 37, NULL, 'UpdateEventInsuranceAuthorization', 'Updated event #37 insurance authorization status from not-required to approved with authorization number \"5555\"', NULL, NULL, NULL, NULL, '2021-06-28 13:27:59'),
(328, 8, NULL, NULL, 'UpdatePatient', 'Updated patient #29', '{\"patientId\":29,\"firstName\":\"Jayden-James\",\"middleName\":\"Bob\",\"lastName\":\"Kerr\",\"address1\":\"Street 2\",\"address2\":\"Bld 2\",\"state\":\"Michigan\",\"city\":\"Detroit\",\"zipcode\":\"11223344\",\"dateofbirth\":\"1999-02-02\",\"email\":\"jayjay@mail.com@mail.com\",\"phone\":\"1112226666\",\"ssn\":\"33-55-6666\",\"sex\":\"male\",\"carrierId\":\"633\",\"carrierCode\":\"AAA\",\"carrierName\":\"AAA OF MICHIGAN\",\"subscriberId\":\"555\",\"groupName\":\"555\",\"groupNumber\":\"555\"}', NULL, NULL, NULL, '2021-06-28 13:33:34'),
(329, 8, NULL, NULL, 'UpdatePatient', 'Updated patient #22', '{\"patientId\":22,\"firstName\":\"Amelia\",\"middleName\":\"Bob\",\"lastName\":\"Watson\",\"address1\":\"Main Street 123\",\"address2\":\"Bld 3\",\"state\":\"Michigan\",\"city\":\"Detroit\",\"zipcode\":\"11223344\",\"dateofbirth\":\"1995-07-24\",\"email\":\"ame.wat@mail.com\",\"phone\":\"11122277\",\"ssn\":\"999-88-7778\",\"sex\":\"female\",\"carrierId\":\"633\",\"carrierCode\":\"AAA\",\"carrierName\":\"AAA OF MICHIGAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\"}', NULL, NULL, NULL, '2021-06-28 13:57:56'),
(330, 8, NULL, NULL, 'UpdatePatient', 'Updated patient #22', '{\"patientId\":22,\"firstName\":\"Amelia\",\"middleName\":\"Bob\",\"lastName\":\"Watson\",\"address1\":\"Main Street 123\",\"address2\":\"Bld 3\",\"state\":\"Michigan\",\"city\":\"Detroit\",\"zipcode\":\"11223344\",\"dateofbirth\":\"1995-07-24\",\"email\":\"ame.wat@mail.com\",\"phone\":\"11122277\",\"ssn\":\"999-88-7778\",\"sex\":\"female\",\"carrierId\":\"633\",\"carrierCode\":\"AAA\",\"carrierName\":\"AAA OF MICHIGAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\"}', NULL, NULL, NULL, '2021-06-28 13:58:38'),
(331, 8, NULL, NULL, 'UpdatePatient', 'Updated patient #22', '{\"patientId\":22,\"firstName\":\"Amelia\",\"middleName\":\"Bob\",\"lastName\":\"Watson\",\"address1\":\"Main Street 123\",\"address2\":\"Bld 3\",\"state\":\"Michigan\",\"city\":\"Detroit\",\"zipcode\":\"11223344\",\"dateofbirth\":\"1995-07-24\",\"email\":\"ame.wat@mail.com\",\"phone\":\"11122277\",\"ssn\":\"999-88-7778\",\"sex\":\"female\",\"carrierId\":\"633\",\"carrierCode\":\"AAA\",\"carrierName\":\"AAA OF MICHIGAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\"}', NULL, NULL, NULL, '2021-06-28 13:58:55'),
(332, 8, NULL, NULL, 'UpdatePatient', 'Updated patient #22', '{\"patientId\":22,\"firstName\":\"Amelia\",\"middleName\":\"Bob\",\"lastName\":\"Watson\",\"address1\":\"Main Street 123\",\"address2\":\"Bld 3\",\"state\":\"Michigan\",\"city\":\"Detroit\",\"zipcode\":\"11223344\",\"dateofbirth\":\"1995-07-24\",\"email\":\"ame.wat@mail.com\",\"phone\":\"1112227777\",\"ssn\":\"999-88-7778\",\"sex\":\"female\",\"carrierId\":\"633\",\"carrierCode\":\"AAA\",\"carrierName\":\"AAA OF MICHIGAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\"}', NULL, NULL, NULL, '2021-06-28 13:59:20'),
(333, 8, NULL, 22, 'UpdatePatient', 'Updated patient #22', '{\"patientId\":22,\"firstName\":\"Amelia\",\"middleName\":\"Bob\",\"lastName\":\"Watson\",\"address1\":\"Main Street 123\",\"address2\":\"Bld 3\",\"state\":\"Michigan\",\"city\":\"Detroit\",\"zipcode\":\"11223344\",\"dateofbirth\":\"1995-07-24\",\"email\":\"ame.watson@mail.com\",\"phone\":\"1112227777\",\"ssn\":\"999-88-7778\",\"sex\":\"female\",\"carrierId\":\"728\",\"carrierCode\":\"MUT01\",\"carrierName\":\"MUTUAL OF OMAHA\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\"}', NULL, NULL, NULL, '2021-06-28 15:42:48'),
(334, 8, 47, 22, 'AddEvent', 'Added event #47 for patient #22', '{\"id\":47,\"dateFrom\":\"2021-06-30 15:43:04\",\"dateTo\":\"2021-06-30 16:13:04\",\"eventTypeId\":\"14\",\"eventTypeCode\":\"ENDO\",\"eventTypeName\":\"Endoscopy\",\"priorityId\":18,\"priority\":1,\"priorityText\":\"Routine - Elective\",\"facilityId\":\"29\",\"facilityCode\":\"HSP5\",\"facilityName\":\"Beaumont Hospital - Dearborn\",\"facilityAddress\":\"18101 Oakwood Blvd, Dearborn, MI 48124\",\"physicianId\":\"20\",\"physicianName\":\"Demo Physician One\",\"physicianPhone\":\"1116574897\",\"physicianFax\":\"1113334456\",\"coordinatorId\":8,\"coordinatorName\":\"Louai\",\"patientId\":22,\"patientName\":\"Amelia Bob Watson\",\"patientDoB\":\"1995-07-23T21:00:00.000Z\",\"patientGender\":\"female\",\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientSSN\":\"999-88-7778\",\"carrierId\":728,\"carrierCode\":\"MUT01\",\"carrierName\":\"MUTUAL OF OMAHA\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\"}', NULL, NULL, NULL, '2021-06-28 15:43:15'),
(335, 8, 43, NULL, 'UpdateEvent', 'Updated event #43', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-28 16:09:07'),
(336, 8, 43, NULL, 'UpdateEvent', 'Updated event #43', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-28 16:09:19'),
(337, 8, 43, NULL, 'UpdateEvent', 'Updated event #43', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":\"Example\",\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-28 16:09:29'),
(338, 8, 43, NULL, 'UpdateEvent', 'Updated event #43', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":27,\"bloodThinnerCode\":\"ASA325\",\"bloodThinnerName\":\"Aspirin 325mg\",\"bloodThinnerDescription\":\"Patient needs to stop taking Aspirin 325mg 7 days before surgery.\",\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":\"Example\",\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-28 16:10:26'),
(339, 8, 43, NULL, 'UpdateEvent', 'Updated status of event #43 to \"Boarding Slip Generated\"', NULL, NULL, NULL, NULL, '2021-06-28 16:10:35'),
(340, 8, 43, NULL, 'CancelEvent', 'Cancelled event #43 with reason \"Incorrect details written\"', NULL, NULL, NULL, NULL, '2021-06-28 16:33:55'),
(341, 20, 37, NULL, 'CodeEvent', 'Coded event #37 with procedures [[object Object],[object Object]]', NULL, NULL, NULL, NULL, '2021-06-29 09:45:05'),
(342, 20, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-29 09:45:56'),
(343, 20, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Completed\"', NULL, NULL, NULL, NULL, '2021-06-29 09:50:02'),
(344, 20, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-29 09:50:19'),
(345, 20, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Completed\"', NULL, NULL, NULL, NULL, '2021-06-29 09:50:22'),
(346, 20, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-29 09:50:32'),
(347, 20, 37, NULL, 'CancelEvent', 'Cancelled event #37 with reason \"The entered information was incorrect\"', NULL, NULL, NULL, NULL, '2021-06-29 09:50:51'),
(348, 20, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-29 09:58:52'),
(349, 20, 37, NULL, 'CodeEvent', 'Coded event #37 with procedures [[object Object]]', NULL, NULL, NULL, NULL, '2021-06-29 09:59:43'),
(350, 8, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-29 10:04:29'),
(351, 20, 37, NULL, 'CodeEvent', 'Completed event #37 with procedures [[object Object]]', NULL, NULL, NULL, NULL, '2021-06-29 10:04:52'),
(352, 20, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-29 10:05:41'),
(353, 20, 37, NULL, 'CodeEvent', 'Completed event #37 with procedures [45380,45990]', NULL, NULL, NULL, NULL, '2021-06-29 10:07:16'),
(354, 8, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-29 10:18:28'),
(355, 20, 37, NULL, 'CodeEvent', 'Completed event #37 with procedures [45990]', NULL, NULL, NULL, NULL, '2021-06-29 10:18:46'),
(356, 20, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-29 10:18:54'),
(357, 20, 37, NULL, 'CodeEvent', 'Completed event #37 with procedures [45378]', NULL, NULL, NULL, NULL, '2021-06-29 10:19:10'),
(358, 20, 41, NULL, 'UpdateEvent', 'Updated status of event #41 to \"Completed\"', NULL, NULL, NULL, NULL, '2021-06-29 10:26:23'),
(359, 20, 41, NULL, 'UpdateEvent', 'Updated status of event #41 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-29 10:42:32'),
(360, 20, 41, NULL, 'CodeEvent', 'Completed event #41 with procedures [45990]', NULL, NULL, NULL, NULL, '2021-06-29 10:46:22'),
(361, 20, 41, NULL, 'CodeEvent', 'Completed event #41 with procedures [45990]', NULL, NULL, NULL, NULL, '2021-06-29 10:51:59'),
(362, 20, 41, NULL, 'CodeEvent', 'Completed event #41 with procedures [45990]', NULL, NULL, NULL, NULL, '2021-06-29 10:52:23'),
(363, 20, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-29 10:57:38'),
(364, 20, 37, NULL, 'CodeEvent', 'Completed event #37 with procedures [45378]', NULL, NULL, NULL, NULL, '2021-06-29 10:57:46'),
(365, 20, 41, NULL, 'UpdateEvent', 'Updated status of event #41 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-29 11:08:43'),
(366, 20, 41, NULL, 'CodeEvent', 'Completed event #41 with procedures [45990]', NULL, NULL, NULL, NULL, '2021-06-29 11:08:51'),
(367, 20, 41, NULL, 'UpdateEvent', 'Updated status of event #41 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-29 11:09:22'),
(368, 20, 41, NULL, 'CodeEvent', 'Completed event #41 with procedures [45990]', NULL, NULL, NULL, NULL, '2021-06-29 11:09:33'),
(369, 20, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-29 11:12:15'),
(370, 20, 37, NULL, 'CodeEvent', 'Completed event #37 with procedures [45378]', NULL, NULL, NULL, NULL, '2021-06-29 11:12:37'),
(371, 8, 37, NULL, 'UpdateEvent', 'Updated status of event #37 to \"Confirmed\"', NULL, NULL, NULL, NULL, '2021-06-29 11:22:15'),
(372, 8, 37, 20, 'RescheduleEvent', 'Rescheduled event #37 to date-time \"2021-07-07 14:05:00\"', NULL, NULL, NULL, NULL, '2021-06-29 12:18:11'),
(373, 8, 37, 20, 'RescheduleEvent', 'Rescheduled event #37 to date-time \"2021-07-07 02:05:00\"', NULL, NULL, NULL, NULL, '2021-06-29 12:19:40'),
(374, 8, 37, 20, 'RescheduleEvent', 'Rescheduled event #37 to date-time \"2021-07-30 16:00:00\"', NULL, NULL, NULL, NULL, '2021-06-29 12:21:37'),
(375, 8, 37, 20, 'RescheduleEvent', 'Rescheduled event #37 to date-time \"2021-06-30 04:00:00\"', NULL, NULL, NULL, NULL, '2021-06-29 12:21:51'),
(376, 8, 37, 20, 'RescheduleEvent', 'Rescheduled event #37 to date-time \"2021-06-30 04:00:00\"', NULL, NULL, NULL, NULL, '2021-06-29 12:25:23'),
(377, 8, 37, 20, 'RescheduleEvent', 'Rescheduled event #37 to date-time \"2021-06-30 04:00:00\"', NULL, NULL, NULL, NULL, '2021-06-29 12:25:48'),
(378, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":null,\"otherClearancePhysician\":null}', NULL, NULL, NULL, '2021-06-29 13:24:27'),
(379, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"insuranceAuthorizationStatus\":\"tbd\",\"insuranceAuthorizationNumber\":\"123\"}', NULL, NULL, NULL, '2021-06-29 13:25:30'),
(380, 8, 45, NULL, 'UpdateEventInsuranceAuthorization', 'Updated event #45 insurance authorization status from not-required to tbd with authorization number \"123\"', NULL, NULL, NULL, NULL, '2021-06-29 13:25:30'),
(381, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"insuranceAuthorizationStatus\":\"approved\"}', NULL, NULL, NULL, '2021-06-29 13:25:32'),
(382, 8, 45, NULL, 'UpdateEventInsuranceAuthorization', 'Updated event #45 insurance authorization status from tbd to approved', NULL, NULL, NULL, NULL, '2021-06-29 13:25:32'),
(383, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"medicalClearanceStatus\":\"accepted\",\"medicalClearancePhysician\":\"Phy1\"}', NULL, NULL, NULL, '2021-06-29 13:25:44'),
(384, 8, 45, NULL, 'UpdateEventClearance', 'Updated event #45 medical clearance from not_required to accepted', NULL, NULL, NULL, NULL, '2021-06-29 13:25:44'),
(385, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"medicalClearanceStatus\":\"rejected\"}', NULL, NULL, NULL, '2021-06-29 13:25:45'),
(386, 8, 45, NULL, 'UpdateEventClearance', 'Updated event #45 medical clearance from accepted to rejected', NULL, NULL, NULL, NULL, '2021-06-29 13:25:45'),
(387, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"medicalClearanceStatus\":\"pending\"}', NULL, NULL, NULL, '2021-06-29 13:25:46'),
(388, 8, 45, NULL, 'UpdateEventClearance', 'Updated event #45 medical clearance from rejected to pending', NULL, NULL, NULL, NULL, '2021-06-29 13:25:46'),
(389, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"otherClearanceStatus\":\"pending\",\"otherClearancePhysician\":\"Other Phy\"}', NULL, NULL, NULL, '2021-06-29 13:25:53'),
(390, 8, 45, NULL, 'UpdateEventClearance', 'Updated event #45 other clearance from not_required to pending', NULL, NULL, NULL, NULL, '2021-06-29 13:25:53'),
(391, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"otherClearanceStatus\":\"accepted\"}', NULL, NULL, NULL, '2021-06-29 13:25:55'),
(392, 8, 45, NULL, 'UpdateEventClearance', 'Updated event #45 other clearance from pending to accepted', NULL, NULL, NULL, NULL, '2021-06-29 13:25:55'),
(393, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"otherClearanceStatus\":\"not_required\",\"otherClearancePhysician\":null,\"otherClearance\":null}', NULL, NULL, NULL, '2021-06-29 13:25:55'),
(394, 8, 45, NULL, 'UpdateEventClearance', 'Updated event #45 other clearance from accepted to not_required', NULL, NULL, NULL, NULL, '2021-06-29 13:25:55'),
(395, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Phy12\",\"otherClearancePhysician\":null,\"insuranceAuthorizationNumber\":\"123\"}', NULL, NULL, NULL, '2021-06-29 13:26:30'),
(396, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Phy12\",\"otherClearancePhysician\":null,\"insuranceAuthorizationNumber\":\"123\"}', NULL, NULL, NULL, '2021-06-29 13:34:24'),
(397, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Phy12\",\"otherClearancePhysician\":null,\"insuranceAuthorizationNumber\":\"123\"}', NULL, NULL, NULL, '2021-06-29 13:35:10'),
(398, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Phy12\",\"otherClearancePhysician\":null,\"insuranceAuthorizationNumber\":\"123\"}', NULL, NULL, NULL, '2021-06-29 13:35:31'),
(399, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Phy12\",\"otherClearancePhysician\":null,\"insuranceAuthorizationNumber\":\"1234\"}', NULL, NULL, NULL, '2021-06-29 13:37:17'),
(400, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"medicalClearanceStatus\":\"accepted\"}', NULL, NULL, NULL, '2021-06-29 13:43:02'),
(401, 8, 45, NULL, 'UpdateEventClearance', 'Updated event #45 medical clearance from pending to accepted', NULL, NULL, NULL, NULL, '2021-06-29 13:43:02'),
(402, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Phy12\",\"otherClearancePhysician\":null,\"insuranceAuthorizationNumber\":\"1234\"}', NULL, NULL, NULL, '2021-06-29 13:44:18'),
(403, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Phy12\",\"otherClearancePhysician\":null,\"insuranceAuthorizationNumber\":\"1234\"}', NULL, NULL, NULL, '2021-06-29 13:44:38'),
(404, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Phy12\",\"otherClearancePhysician\":null,\"insuranceAuthorizationNumber\":\"1234\"}', NULL, NULL, NULL, '2021-06-29 13:47:09'),
(405, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Phy12\",\"otherClearancePhysician\":null,\"insuranceAuthorizationNumber\":\"1234\"}', NULL, NULL, NULL, '2021-06-29 13:48:29'),
(406, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Phy12\",\"otherClearancePhysician\":null,\"insuranceAuthorizationNumber\":\"1234\"}', NULL, NULL, NULL, '2021-06-29 13:48:37'),
(407, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Phy12\",\"otherClearancePhysician\":null,\"insuranceAuthorizationNumber\":\"1234\"}', NULL, NULL, NULL, '2021-06-29 13:57:44'),
(408, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Phy12\",\"otherClearancePhysician\":null,\"insuranceAuthorizationNumber\":\"1234\"}', NULL, NULL, NULL, '2021-06-29 13:58:19'),
(409, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Phy12\",\"otherClearancePhysician\":null,\"insuranceAuthorizationNumber\":\"1234\"}', NULL, NULL, NULL, '2021-06-29 13:59:01'),
(410, 8, 45, NULL, 'UpdateEvent', 'Updated event #45', '{\"patientAddress\":\"Main Street 123/Detroit/Michigan/11223344\",\"patientPreferredPhone\":\"1112227777\",\"patientDoB\":\"1995-07-24\",\"patientGender\":\"female\",\"carrierCode\":\"AME35\",\"carrierName\":\"AMERICAS CHOICE HEALTHPLAN\",\"subscriberId\":\"222\",\"groupName\":\"Group Two\",\"groupNumber\":\"G222\",\"bloodThinnerId\":null,\"bloodThinnerCode\":null,\"bloodThinnerDescription\":null,\"diagnosisId\":null,\"diagnosisCode\":null,\"diagnosisDescription\":null,\"insuranceInfo\":null,\"otherClearance\":null,\"cardiacClearancePhysician\":null,\"medicalClearancePhysician\":\"Phy12\",\"otherClearancePhysician\":null,\"insuranceAuthorizationNumber\":\"1234\"}', NULL, NULL, NULL, '2021-06-29 13:59:48');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `firstName` varchar(256) DEFAULT NULL,
  `middleName` varchar(256) DEFAULT NULL,
  `lastName` varchar(256) DEFAULT NULL,
  `address1` varchar(256) NOT NULL,
  `address2` varchar(256) NOT NULL,
  `city` varchar(32) NOT NULL,
  `state` varchar(32) NOT NULL,
  `zipcode` varchar(32) NOT NULL,
  `dateofbirth` date NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `email` varchar(256) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `ssn` varchar(64) DEFAULT NULL,
  `sex` varchar(64) DEFAULT NULL,
  `carrierId` int(11) DEFAULT NULL,
  `carrierCode` varchar(256) DEFAULT NULL,
  `carrierName` varchar(512) DEFAULT NULL,
  `subscriberId` text DEFAULT NULL,
  `groupName` text DEFAULT NULL,
  `groupNumber` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `name`, `firstName`, `middleName`, `lastName`, `address1`, `address2`, `city`, `state`, `zipcode`, `dateofbirth`, `createdOn`, `email`, `phone`, `ssn`, `sex`, `carrierId`, `carrierCode`, `carrierName`, `subscriberId`, `groupName`, `groupNumber`) VALUES
(20, 'John William Doe', 'John', 'William', 'Doe', 'Main Street 123', 'Bld 3', 'Detroit', 'Michigan', '11223343', '1984-02-09', '2021-06-17 12:23:24', 'john.doe@mail.com', '1112223399', '111-22-3333', 'male', 670, 'CONWE', 'CONTINENTAL WESTERN', '111', 'Group One', 'G111'),
(22, 'Amelia Bob Watson', 'Amelia', 'Bob', 'Watson', 'Main Street 123', 'Bld 3', 'Detroit', 'Michigan', '11223344', '1995-07-24', '2021-06-23 09:44:07', 'ame.watson@mail.com', '1112227777', '999-88-7778', 'female', 728, 'MUT01', 'MUTUAL OF OMAHA', '222', 'Group Two', 'G222'),
(26, 'Zahrah Bobzy Blevins', 'Zahrah', 'Bobzy', 'Blevins', 'Add1', 'Add2', 'Boston', 'Washington', '12234556', '2021-06-10', '2021-06-28 09:56:43', 'john.doe@mail.com', '1112223333', '999-88-7778', 'male', 633, 'AAA', 'AAA OF MICHIGAN', '111', '111', '111'),
(27, 'Fletcher Bob Snider', 'Fletcher', 'Bob', 'Snider', 'Add1', '', 'Boston', 'Washington', '12234556', '2021-05-31', '2021-06-28 09:57:32', 'john.doe@mail.com', '1112224444', '999-88-8888', 'male', 648, 'APOS', 'APOSTROPHE', '333', '333', '333'),
(28, 'Elaina Bob Pratt', 'Elaina', 'Bob', 'Pratt', 'Street 4', 'Bld 5', 'Boston', 'Washington', '12234556', '2021-06-04', '2021-06-28 09:58:44', 'al.pra@mail.com', '1112225555', '111-22-3333', 'male', 633, 'AAA', 'AAA OF MICHIGAN', '333', '444', '4444'),
(29, 'Jayden-James Bob Kerr', 'Jayden-James', 'Bob', 'Kerr', 'Street 2', 'Bld 2', 'Detroit', 'Michigan', '11223344', '1999-02-02', '2021-06-28 09:59:45', 'jayjay@mail.com@mail.com', '1112226666', '33-55-6666', 'male', 633, 'AAA', 'AAA OF MICHIGAN', '555', '555', '555'),
(30, 'Aniya John Gardiner', 'Aniya', 'John', 'Gardiner', 'Add1', 'Add2', 'Boston', 'Washington', '12234556', '1983-06-06', '2021-06-28 12:36:46', 'aniya@mail.com', '1112223333', '777-66-4444', 'female', 633, 'AAA', 'AAA OF MICHIGAN', '333', '333', '444');

-- --------------------------------------------------------

--
-- Table structure for table `patient_allergy`
--

CREATE TABLE `patient_allergy` (
  `id` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `status` varchar(128) NOT NULL,
  `allergyDate` datetime NOT NULL,
  `reaction` varchar(512) NOT NULL,
  `rxNormId` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `patient_assessment`
--

CREATE TABLE `patient_assessment` (
  `id` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `assessment` text NOT NULL,
  `date` datetime NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedOn` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient_assessment`
--

INSERT INTO `patient_assessment` (`id`, `patientId`, `assessment`, `date`, `createdOn`, `updatedOn`) VALUES
(1, 20, 'Example assessment 1', '2021-06-11 00:00:00', '2021-06-23 08:59:44', '2021-06-23 08:59:47');

-- --------------------------------------------------------

--
-- Table structure for table `patient_goal`
--

CREATE TABLE `patient_goal` (
  `id` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `goalText` text NOT NULL,
  `date` datetime NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedOn` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient_goal`
--

INSERT INTO `patient_goal` (`id`, `patientId`, `goalText`, `date`, `createdOn`, `updatedOn`) VALUES
(1, 20, 'I want to be healthy', '2021-06-30 00:00:00', '2021-06-23 08:51:17', '2021-06-23 08:51:36');

-- --------------------------------------------------------

--
-- Table structure for table `patient_health_concern`
--

CREATE TABLE `patient_health_concern` (
  `id` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `concern` text NOT NULL,
  `date` datetime NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedOn` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient_health_concern`
--

INSERT INTO `patient_health_concern` (`id`, `patientId`, `concern`, `date`, `createdOn`, `updatedOn`) VALUES
(1, 20, 'Example health concern', '2021-06-13 00:00:00', '2021-06-22 16:13:27', '2021-06-22 16:13:33');

-- --------------------------------------------------------

--
-- Table structure for table `patient_immunization`
--

CREATE TABLE `patient_immunization` (
  `id` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `name` text NOT NULL,
  `date` datetime NOT NULL,
  `status` text NOT NULL,
  `quantity` text NOT NULL,
  `unit` text NOT NULL,
  `lotNumber` text NOT NULL,
  `cvxCode` text NOT NULL,
  `details` text NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedOn` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient_immunization`
--

INSERT INTO `patient_immunization` (`id`, `patientId`, `name`, `date`, `status`, `quantity`, `unit`, `lotNumber`, `cvxCode`, `details`, `createdOn`, `updatedOn`) VALUES
(2, 20, 'Covid-19 1st Dose', '2021-06-10 00:00:00', 'success', '51', 'ml', '123', '321', 'First covid vaccine dose, phyzer', '2021-06-23 08:42:51', '2021-06-23 08:42:55');

-- --------------------------------------------------------

--
-- Table structure for table `patient_implanted_device`
--

CREATE TABLE `patient_implanted_device` (
  `id` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `deviceId` text NOT NULL,
  `issuingAgency` text NOT NULL,
  `siteName` text NOT NULL,
  `siteCode` text NOT NULL,
  `deviceName` text NOT NULL,
  `deviceDescription` text NOT NULL,
  `manufacturer` text NOT NULL,
  `model` text NOT NULL,
  `serialNumber` text NOT NULL,
  `lot` text NOT NULL,
  `isActive` text NOT NULL,
  `date` datetime NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedOn` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient_implanted_device`
--

INSERT INTO `patient_implanted_device` (`id`, `patientId`, `deviceId`, `issuingAgency`, `siteName`, `siteCode`, `deviceName`, `deviceDescription`, `manufacturer`, `model`, `serialNumber`, `lot`, `isActive`, `date`, `createdOn`, `updatedOn`) VALUES
(1, 20, '123', 'Better Hearing Inc', 'Ear', 'E1234', 'Hearing Aid', 'In-ear hearing aid permanent', 'Samsung', 'EA123', '11223344', '321', 'active', '2021-01-22 00:00:00', '2021-06-23 09:20:05', '2021-06-23 09:20:54');

-- --------------------------------------------------------

--
-- Table structure for table `patient_medication`
--

CREATE TABLE `patient_medication` (
  `id` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `rxNormId` text NOT NULL,
  `name` text NOT NULL,
  `instructions` text NOT NULL,
  `quantity` text NOT NULL,
  `doseForm` text NOT NULL,
  `date` datetime NOT NULL,
  `expirationDate` datetime NOT NULL,
  `currentStatus` text NOT NULL,
  `type` text NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedOn` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient_medication`
--

INSERT INTO `patient_medication` (`id`, `patientId`, `rxNormId`, `name`, `instructions`, `quantity`, `doseForm`, `date`, `expirationDate`, `currentStatus`, `type`, `createdOn`, `updatedOn`) VALUES
(1, 20, 'RX123', 'Xanax', '1 pill per day', '20 pills', 'pill', '2021-06-10 00:00:00', '2024-11-28 00:00:00', 'taking', 'example type', '2021-06-23 09:32:54', '2021-06-23 09:33:02');

-- --------------------------------------------------------

--
-- Table structure for table `patient_smoking_status`
--

CREATE TABLE `patient_smoking_status` (
  `id` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `value` text NOT NULL,
  `description` text NOT NULL,
  `date` datetime NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedOn` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient_smoking_status`
--

INSERT INTO `patient_smoking_status` (`id`, `patientId`, `value`, `description`, `date`, `createdOn`, `updatedOn`) VALUES
(1, 20, 'Val1', 'This is an example smoking status description 2', '2021-03-19 00:00:00', '2021-06-22 16:03:20', '2021-06-22 16:22:16');

-- --------------------------------------------------------

--
-- Table structure for table `priority`
--

CREATE TABLE `priority` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `priority` int(11) NOT NULL DEFAULT 3
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `priority`
--

INSERT INTO `priority` (`id`, `code`, `name`, `createdOn`, `priority`) VALUES
(18, 'Elective', 'Routine - Elective', '2021-06-17 12:17:33', 1),
(19, 'PR124', 'Elective w/ RTMU', '2021-06-17 12:17:33', 3),
(20, 'Stat', 'Emergency / STAT', '2021-06-17 12:17:33', 5);

-- --------------------------------------------------------

--
-- Table structure for table `procedures`
--

CREATE TABLE `procedures` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `procedures`
--

INSERT INTO `procedures` (`id`, `code`, `name`, `createdOn`) VALUES
(21, '45378', 'Colonoscopy', '2021-06-17 12:17:33'),
(22, '45380', 'Colonoscopy w/ Biopsies', '2021-06-17 12:17:33'),
(23, '45990', 'Examination Under Anesthesia', '2021-06-17 12:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `refresh`
--

CREATE TABLE `refresh` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `token` varchar(512) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `refresh`
--

INSERT INTO `refresh` (`id`, `userId`, `token`, `createdOn`) VALUES
(26, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjA5NjQyNCwiZXhwIjoxNjI0Njg4NDI0fQ.6K-UGRqUqDBx0U3fGCnHFJ5bOiSUalhX_UoCTyuwP1c', '2021-05-27 09:20:24'),
(27, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjA5NjU4MiwiZXhwIjoxNjI0Njg4NTgyfQ.YFNaRs793Rla-5sdilcVjxs2pj-0gOalkK7TLOZX_pQ', '2021-05-27 09:23:02'),
(28, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjA5Njk5MywiZXhwIjoxNjI0Njg4OTkzfQ.hOoZJ8vmOYOidl3bb1H16bi0W4JXLEyjP8-11v6HLJo', '2021-05-27 09:29:53'),
(30, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjA5OTg2MCwiZXhwIjoxNjI0NjkxODYwfQ.vnFOY9yt-SlYsblidhdcv1wpDz0FHrF17m_d9j8d-j8', '2021-05-27 10:17:40'),
(31, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDA1NCwiZXhwIjoxNjI0NjkyMDU0fQ.Cu4m5oL1t4hj7L0NCNgfUUgL3njbe_ZjYStWOWIqDqo', '2021-05-27 10:20:54'),
(32, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDA1NywiZXhwIjoxNjI0NjkyMDU3fQ.HYsO7kVqzsDiJCXroqUS7z76rp_aRjDDHZcLVWBaNNs', '2021-05-27 10:20:57'),
(33, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDA5OSwiZXhwIjoxNjI0NjkyMDk5fQ.fyk9s8Z-h9We7-MvQ5uu-JnKNqynWKA7BWRQiOFJAnU', '2021-05-27 10:21:39'),
(34, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE0NiwiZXhwIjoxNjI0NjkyMTQ2fQ.LN0QiI8R_l6zxR1gZKvEIwjcZsfDu5UlxcsQ_le2tXI', '2021-05-27 10:22:26'),
(35, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE1MywiZXhwIjoxNjI0NjkyMTUzfQ.ZSAnN2ryX_TeI1z4JIKPiIJ1-_zn01n_EpI5pF7EVDk', '2021-05-27 10:22:33'),
(36, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE2MywiZXhwIjoxNjI0NjkyMTYzfQ.i3SUdlUBBMAVLWD-gAuLFvRh7DpXkMq_OyY7ezlcm3s', '2021-05-27 10:22:43'),
(37, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE3MiwiZXhwIjoxNjI0NjkyMTcyfQ.ySVBEbmZ3eF8OTuQaghVRc8DUY6p3j19vv3JE6bZb7A', '2021-05-27 10:22:52'),
(38, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE5NywiZXhwIjoxNjI0NjkyMTk3fQ.u6uE22cxGuSWbQS-yPQUcbk1MWxU9oitgvmDHLH47Nw', '2021-05-27 10:23:17'),
(39, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDIzMiwiZXhwIjoxNjI0NjkyMjMyfQ.utGNd9y7tPLwL4YhIlwZ7ky6h4y63dmEuyNC8tJx18o', '2021-05-27 10:23:52'),
(40, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDI2MiwiZXhwIjoxNjI0NjkyMjYyfQ.A7mqDlOS5wu67QRPhSkVCVL8tl4n4tOnF39FKrQTDuc', '2021-05-27 10:24:22'),
(41, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDQyOCwiZXhwIjoxNjI0NjkyNDI4fQ.PFKbOpoqWJZ_z1QvrZhfbV2KkPxVS3nC7beEiy0MyOc', '2021-05-27 10:27:08'),
(42, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDQ2NCwiZXhwIjoxNjI0NjkyNDY0fQ.13V9fVCeZWiEqI6oJIR71Mr3naNc0EWBf1HrBrQc9Vw', '2021-05-27 10:27:44'),
(43, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDU4MywiZXhwIjoxNjI0NjkyNTgzfQ.CJfOyeK2zufVl8EZoS1x8XiLeXHmaEFWjhQytRDKmBY', '2021-05-27 10:29:43'),
(44, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDY4MywiZXhwIjoxNjI0NjkyNjgzfQ.o3S7gTjdWz-gJ0tk63NlwZpVr7e2nxuaosfO7gHkFIk', '2021-05-27 10:31:23'),
(45, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDY5MSwiZXhwIjoxNjI0NjkyNjkxfQ.uNw_JkfyP0siAJ3os1iW8-Tcwu0-D91jX3CAkFG5PSI', '2021-05-27 10:31:31'),
(46, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDc4NywiZXhwIjoxNjI0NjkyNzg3fQ.o0nynrypSLFMYlcL7fw5lPRJ3MaLHtqxW2IbfxwZafk', '2021-05-27 10:33:07'),
(47, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDkwNywiZXhwIjoxNjI0NjkyOTA3fQ.geqnrMMYSrBvwOuEhYQ85pojgGBm9Lh7NnSwmEfcNp8', '2021-05-27 10:35:07'),
(48, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMjA4OSwiZXhwIjoxNjI0Njk0MDg5fQ.Q_k9G9VL3v1EcWLopoWEcOCnrth_3OIM1XVM9hzllac', '2021-05-27 10:54:49'),
(110, 10, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEwLCJpYXQiOjE2MjM0MTU3NjUsImV4cCI6MTYyMzUwMjE2NX0.P1Qld8kNRHSe9Xf1Z6Zh8Lst3LAakWX30mpW5_7DW7k', '2021-06-11 15:49:25'),
(111, 11, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjExLCJpYXQiOjE2MjM0MTU4MzMsImV4cCI6MTYyMzUwMjIzM30.9TaVCAsw3kE17mRtY0CqQ6wJW2rKqIHjLZ_Z_WcUjdY', '2021-06-11 15:50:33'),
(112, 12, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEyLCJpYXQiOjE2MjM0MTU4NzQsImV4cCI6MTYyMzUwMjI3NH0.HeO5zz9jMe-rM9GbTfkW9aMkw8qsCPIl3odxJE1V2z0', '2021-06-11 15:51:14'),
(113, 13, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEzLCJpYXQiOjE2MjM0MTYwMjksImV4cCI6MTYyMzUwMjQyOX0.xS6outos8SY-VtmirVh9K8a4d4J-YyHn75xUBTy2Pd0', '2021-06-11 15:53:49'),
(115, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMzQxNjQ3NSwiZXhwIjoxNjIzNTAyODc1fQ.FV8M0Ivv6LKOcUTObbOnno0AP9TnMPJ7s4a5bncgd6w', '2021-06-11 16:01:15'),
(121, 16, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjE2LCJpYXQiOjE2MjM0MTgwOTksImV4cCI6MTYyMzUwNDQ5OX0.K55HhqrvSpvOcJZhPNt2XuNgaUaBYleNiHq6S810qTM', '2021-06-11 16:28:19'),
(129, 1, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTYyMzgzMTMzNiwiZXhwIjoxNjIzOTE3NzM2fQ.zLYiLTQ78YcAKaCJZ1uLDt-MW5ILrtQTpTxbyy9NXf8', '2021-06-16 11:15:36'),
(162, 8, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjgsImlhdCI6MTYyNDk2MTcwMywiZXhwIjoxNjI1MDQ4MTAzfQ.fyvf09IHqcZi3pGlTh75vOYrXDYAdKaRQKFY9BCSL2E', '2021-06-29 13:15:03');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `name` varchar(64) NOT NULL,
  `fax` varchar(64) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `password` varchar(256) NOT NULL,
  `avatar` varchar(128) DEFAULT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `name`, `fax`, `phone`, `password`, `avatar`, `createdOn`) VALUES
(1, 'amandaalame@gmail.com', 'Amanda Alame', '5863438773', '3139124798', '$2b$10$yFc7Sg7WNxrC5t5cUAs5JupzNdWj/MrYHz8L1/MFX/zBncdsfwiga', NULL, '2021-06-17 12:17:33'),
(2, 'amerzeni@gmail.com', 'Amer M. Zeni, MD', '5863438773', '3178093538', '$2b$10$S/yR/u8iiHMSRMLORIHY1.UEIL84iYS9yrIF1avdPdSmzCYgG1p/C', NULL, '2021-06-17 12:17:33'),
(3, 'alex.conley81@gmail.com', 'Alexandria C. Glenn, MD', '5863438773', '3136631981', '$2b$10$60xfzqj0gT2SKijWWPNhT.LzU9kcRTlx7fzOtu9C4ExUS.U64Liya', NULL, '2021-06-17 12:17:33'),
(4, 'ameralame@gmail.com', 'Amer M. Alame, MD', '5863438773', '3133337372', '$2b$10$CZrMZGcyseWfStNUjTuPaeikfcKbHijcX3je7ulrumg9dD9fJT3SW', NULL, '2021-06-17 12:17:33'),
(5, 'doris@harperclinic.org', 'Doris Ferries', '5863438773', '5863438717', '$2b$10$j/FzjmXK8vew8jCjEPOLT.J0JlBWIWZSWOfBmpSrihgCz5ymbSGxu', NULL, '2021-06-17 12:17:33'),
(6, 'elie@live.com', 'Elie Kattar', '', '8182757850', '$2b$10$oCEI8/b7mo81iN2og41R/.4eIxGwUxhpP.SmVOpegsEm.It8/KnVy', NULL, '2021-06-17 12:17:33'),
(7, 'mtasker122@gmail.com', 'Megan Tasker, DO', '5865800006', '2486319953', '$2b$10$yizbEYd6rmlRU2TLVpv1Z.17b9A.OaAb/k1CQNvlnZtPYeP0.vZd.', NULL, '2021-06-17 12:17:33'),
(8, 'louai@galen.com', 'Louai', '1115556666', '1119678473', '$2b$10$EsZ9mnrwa1NriplRBe4ZmuSD.U0DdjzPBowEvsvhJdAVrsxOxlDoe', NULL, '2021-06-17 12:17:33'),
(20, 'demo.physician1@mail.com', 'Demo Physician One', '1113334456', '1116574897', '$2b$10$EyH6pmGtia3tvYv.VFlVwudJvRwcqClKAd8KK64t/Guc0pkiNuT3G', NULL, '2021-06-17 12:19:12'),
(21, 'userrr@mail.com', 'asfsaf', '1112142142', '1115573825', '$2b$10$6LBn3B4Kr7NEEUVlYLxUo.5yXgWksOhwk9oaZY4CBZlpRCR95S8z.', NULL, '2021-06-25 12:06:03');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `role` varchar(32) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `userId`, `role`, `createdOn`) VALUES
(80, 1, 'admin', '2021-06-17 12:17:33'),
(81, 2, 'physician', '2021-06-17 12:17:33'),
(82, 3, 'physician', '2021-06-17 12:17:33'),
(83, 4, 'physician', '2021-06-17 12:17:33'),
(84, 5, 'coordinator', '2021-06-17 12:17:33'),
(85, 6, 'admin', '2021-06-17 12:17:33'),
(86, 7, 'physician', '2021-06-17 12:17:33'),
(88, 20, 'physician', '2021-06-17 12:20:13'),
(91, 8, 'admin', '2021-06-25 11:45:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blood_thinner`
--
ALTER TABLE `blood_thinner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carrier`
--
ALTER TABLE `carrier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diagnosis`
--
ALTER TABLE `diagnosis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eventTypeId` (`eventTypeId`,`priorityId`,`facilityId`,`physicianId`,`coordinatorId`,`patientId`,`diagnosisId`,`bloodThinnerId`),
  ADD KEY `facilityId` (`facilityId`),
  ADD KEY `patientId` (`patientId`),
  ADD KEY `coordinatorId` (`coordinatorId`),
  ADD KEY `physicianId` (`physicianId`),
  ADD KEY `priorityId` (`priorityId`),
  ADD KEY `diagnosisId` (`diagnosisId`),
  ADD KEY `bloodThinnerId` (`bloodThinnerId`),
  ADD KEY `carrierId` (`carrierId`);

--
-- Indexes for table `event_checklist`
--
ALTER TABLE `event_checklist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eventId` (`eventId`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `event_note`
--
ALTER TABLE `event_note`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`),
  ADD KEY `eventId` (`eventId`);

--
-- Indexes for table `event_procedure`
--
ALTER TABLE `event_procedure`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eventId` (`eventId`),
  ADD KEY `procedureId` (`procedureId`);

--
-- Indexes for table `event_type`
--
ALTER TABLE `event_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility`
--
ALTER TABLE `facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility_physician`
--
ALTER TABLE `facility_physician`
  ADD PRIMARY KEY (`id`),
  ADD KEY `facilityId` (`facilityId`),
  ADD KEY `physicianId` (`physicianId`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eventId` (`eventId`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carrierId` (`carrierId`);

--
-- Indexes for table `patient_allergy`
--
ALTER TABLE `patient_allergy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patientId` (`patientId`);

--
-- Indexes for table `patient_assessment`
--
ALTER TABLE `patient_assessment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patientId` (`patientId`);

--
-- Indexes for table `patient_goal`
--
ALTER TABLE `patient_goal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patientId` (`patientId`);

--
-- Indexes for table `patient_health_concern`
--
ALTER TABLE `patient_health_concern`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patientId` (`patientId`);

--
-- Indexes for table `patient_immunization`
--
ALTER TABLE `patient_immunization`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patientId` (`patientId`);

--
-- Indexes for table `patient_implanted_device`
--
ALTER TABLE `patient_implanted_device`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patientId` (`patientId`);

--
-- Indexes for table `patient_medication`
--
ALTER TABLE `patient_medication`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patientId` (`patientId`);

--
-- Indexes for table `patient_smoking_status`
--
ALTER TABLE `patient_smoking_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patientId` (`patientId`);

--
-- Indexes for table `priority`
--
ALTER TABLE `priority`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procedures`
--
ALTER TABLE `procedures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refresh`
--
ALTER TABLE `refresh`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`),
  ADD KEY `token` (`token`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blood_thinner`
--
ALTER TABLE `blood_thinner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `carrier`
--
ALTER TABLE `carrier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=790;

--
-- AUTO_INCREMENT for table `diagnosis`
--
ALTER TABLE `diagnosis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `event_checklist`
--
ALTER TABLE `event_checklist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `event_note`
--
ALTER TABLE `event_note`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `event_procedure`
--
ALTER TABLE `event_procedure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT for table `event_type`
--
ALTER TABLE `event_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `facility`
--
ALTER TABLE `facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `facility_physician`
--
ALTER TABLE `facility_physician`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=411;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `patient_allergy`
--
ALTER TABLE `patient_allergy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `patient_assessment`
--
ALTER TABLE `patient_assessment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `patient_goal`
--
ALTER TABLE `patient_goal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `patient_health_concern`
--
ALTER TABLE `patient_health_concern`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `patient_immunization`
--
ALTER TABLE `patient_immunization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `patient_implanted_device`
--
ALTER TABLE `patient_implanted_device`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `patient_medication`
--
ALTER TABLE `patient_medication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `patient_smoking_status`
--
ALTER TABLE `patient_smoking_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `priority`
--
ALTER TABLE `priority`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `procedures`
--
ALTER TABLE `procedures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `refresh`
--
ALTER TABLE `refresh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `event_ibfk_1` FOREIGN KEY (`facilityId`) REFERENCES `facility` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_2` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_3` FOREIGN KEY (`coordinatorId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_4` FOREIGN KEY (`physicianId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `event_checklist`
--
ALTER TABLE `event_checklist`
  ADD CONSTRAINT `event_checklist_ibfk_1` FOREIGN KEY (`eventId`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_checklist_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `event_note`
--
ALTER TABLE `event_note`
  ADD CONSTRAINT `event_note_ibfk_1` FOREIGN KEY (`eventId`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_note_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `event_procedure`
--
ALTER TABLE `event_procedure`
  ADD CONSTRAINT `event_procedure_ibfk_1` FOREIGN KEY (`eventId`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_procedure_ibfk_2` FOREIGN KEY (`procedureId`) REFERENCES `procedures` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `facility_physician`
--
ALTER TABLE `facility_physician`
  ADD CONSTRAINT `facility_physician_ibfk_1` FOREIGN KEY (`facilityId`) REFERENCES `facility` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `facility_physician_ibfk_2` FOREIGN KEY (`physicianId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_ibfk_1` FOREIGN KEY (`eventId`) REFERENCES `event` (`id`) ON DELETE SET NULL ON UPDATE NO ACTION;

--
-- Constraints for table `patient`
--
ALTER TABLE `patient`
  ADD CONSTRAINT `patient_ibfk_1` FOREIGN KEY (`carrierId`) REFERENCES `carrier` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `patient_allergy`
--
ALTER TABLE `patient_allergy`
  ADD CONSTRAINT `patient_allergy_ibfk_1` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `patient_assessment`
--
ALTER TABLE `patient_assessment`
  ADD CONSTRAINT `patient_assessment_ibfk_1` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `patient_goal`
--
ALTER TABLE `patient_goal`
  ADD CONSTRAINT `patient_goal_ibfk_1` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `patient_health_concern`
--
ALTER TABLE `patient_health_concern`
  ADD CONSTRAINT `patient_health_concern_ibfk_1` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `patient_immunization`
--
ALTER TABLE `patient_immunization`
  ADD CONSTRAINT `patient_immunization_ibfk_1` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `patient_implanted_device`
--
ALTER TABLE `patient_implanted_device`
  ADD CONSTRAINT `patient_implanted_device_ibfk_1` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `patient_medication`
--
ALTER TABLE `patient_medication`
  ADD CONSTRAINT `patient_medication_ibfk_1` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `patient_smoking_status`
--
ALTER TABLE `patient_smoking_status`
  ADD CONSTRAINT `patient_smoking_status_ibfk_1` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
