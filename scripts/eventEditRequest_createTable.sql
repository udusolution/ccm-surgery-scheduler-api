CREATE TABLE `event_edit_request` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `eventId` int(11) NOT NULL,
    `requesterId` int(11) NOT NULL,
    `status` enum('pending','approved','rejected') NOT NULL DEFAULT 'pending',
    `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
    `updatedOn` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
    PRIMARY KEY (`id`),
    KEY `eventId` (`eventId`,`requesterId`),
    KEY `requesterId` (`requesterId`),
    CONSTRAINT `event_edit_request_ibfk_1` FOREIGN KEY (`eventId`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
    CONSTRAINT `event_edit_request_ibfk_2` FOREIGN KEY (`requesterId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;