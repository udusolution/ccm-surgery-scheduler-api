-- Add cascade delete to day_note_mention when day_note is deleted
ALTER TABLE `day_note_mention`
ADD CONSTRAINT `fk_id_noteId`
FOREIGN KEY (`noteId`)
REFERENCES `day_note` (`id`) ON DELETE CASCADE;