-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 21, 2021 at 02:55 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `galen2`
--

-- --------------------------------------------------------

--
-- Table structure for table `blood_thinner`
--

CREATE TABLE `blood_thinner` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` text DEFAULT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blood_thinner`
--

INSERT INTO `blood_thinner` (`id`, `code`, `name`, `description`, `createdOn`) VALUES
(26, 'Plavix', 'Plavix 75mg', 'Patient needs to stop taking Plavix 75mg 7 days before surgery.', '2021-06-17 12:17:33'),
(27, 'ASA325', 'Aspirin 325mg', 'Patient needs to stop taking Aspirin 325mg 7 days before surgery.', '2021-06-17 12:17:33'),
(28, 'ASA81', 'Aspirin 81mg', 'No need to stop aspirin 81mg. Patient to continue taking normally.', '2021-06-17 12:17:33'),
(29, 'Eliquis', 'Eliquis', 'Patient needs to stop taking Eliquis 3 days before surgery.', '2021-06-17 12:17:33'),
(30, 'Coumadin', 'Coumadin', 'Patient needs to stop taking Coumadin 4 days before surgery.', '2021-06-17 12:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `carrier`
--

CREATE TABLE `carrier` (
  `id` int(11) NOT NULL,
  `code` varchar(256) NOT NULL,
  `name` varchar(512) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `carrier`
--

INSERT INTO `carrier` (`id`, `code`, `name`, `createdOn`) VALUES
(633, 'AAA', 'AAA OF MICHIGAN', '2021-06-17 12:17:33'),
(634, 'AAR01', 'AARP', '2021-06-17 12:17:33'),
(635, 'ABS', 'ABS', '2021-06-17 12:17:33'),
(636, 'ABSO', 'ABSOLUTE TOTAL CARE', '2021-06-17 12:17:33'),
(637, 'ACI', 'ACI', '2021-06-17 12:17:33'),
(638, 'AET05', 'AETNA BETTER HEALTH OF MICHIGAN', '2021-06-17 12:17:33'),
(639, 'AET07C1', 'AETNA', '2021-06-17 12:17:33'),
(640, 'AETSS', 'AETNA SENIOR SUP INS', '2021-06-17 12:17:33'),
(641, 'AIG', 'AIG', '2021-06-17 12:17:33'),
(642, 'ALL01', 'ALLIED BENEFIT SYSTEM', '2021-06-17 12:17:33'),
(643, 'ALL08', 'ALL SAVERS INSURANCE', '2021-06-17 12:17:33'),
(644, 'AME14', 'AMERIHEALTH ADMINISTRATORS', '2021-06-17 12:17:33'),
(645, 'AME35', 'AMERICAS CHOICE HEALTHPLAN', '2021-06-17 12:17:33'),
(646, 'AMHLT', 'AMERIHEALTH VIP', '2021-06-17 12:17:33'),
(647, 'APL', 'AMERICAN PUBLIC LIFE', '2021-06-17 12:17:33'),
(648, 'APOS', 'APOSTROPHE', '2021-06-17 12:17:33'),
(649, 'APWU', 'APWU', '2021-06-17 12:17:33'),
(650, 'ASR01', 'ASR PHYSICIANS CARE', '2021-06-17 12:17:33'),
(651, 'ASRM', 'ASRM', '2021-06-17 12:17:33'),
(652, 'BANFID', 'BANKERS FIDELITY', '2021-06-17 12:17:33'),
(653, 'BANKERS', 'BANKERS LIFE & CASUALTY', '2021-06-17 12:17:33'),
(654, 'BCC', 'BLUE CROSS COMPLETE', '2021-06-17 12:17:33'),
(655, 'BCN', 'BLUE CARE NETWORK HMO', '2021-06-17 12:17:33'),
(656, 'BNKFID', 'BANKERS FIDELITY', '2021-06-17 12:17:33'),
(657, 'CALP', 'CALPERS', '2021-06-17 12:17:33'),
(658, 'CDB', 'CUSTOM DESIGN BENEFITS', '2021-06-17 12:17:33'),
(659, 'CHA03', 'CHAMPVA', '2021-06-17 12:17:33'),
(660, 'CHEST', 'CHESTERFIELD RESOURCES INC', '2021-06-17 12:17:33'),
(661, 'CIG02', 'CIGNA', '2021-06-17 12:17:33'),
(662, 'CIG08', 'CIGNA HEALTH PLANS', '2021-06-17 12:17:33'),
(663, 'CITZ', 'CITIZENS', '2021-06-17 12:17:33'),
(664, 'CLCH', 'CLAIM CHOICE', '2021-06-17 12:17:33'),
(665, 'COL', 'COLONIAL PENN', '2021-06-17 12:17:33'),
(666, 'COLHLTH', 'COLONIAL HEALTH', '2021-06-17 12:17:33'),
(667, 'COM28', 'COMMUNITY CARE ASSOCIATES', '2021-06-17 12:17:33'),
(668, 'COMP', 'COMP RISK SERV - REVIEW WORKS', '2021-06-17 12:17:33'),
(669, 'CONSUMER', 'CONSUMERS MUTUAL INS CO', '2021-06-17 12:17:33'),
(670, 'CONWE', 'CONTINENTAL WESTERN', '2021-06-17 12:17:33'),
(671, 'COR', 'CORE SOURCE', '2021-06-17 12:17:33'),
(672, 'CORP', 'CORPORATE BENEFIT SERVICES OF AMERI', '2021-06-17 12:17:33'),
(673, 'COU03V1', 'COVENTRY HEALTH CARE / FIRST HEALTH', '2021-06-17 12:17:33'),
(674, 'CSHL', 'CENTRAL STATES HEALTH AND LIFE', '2021-06-17 12:17:33'),
(675, 'EGP', 'EGP ENTERPRISE GROUP', '2021-06-17 12:17:33'),
(676, 'EHIM', 'EHIM', '2021-06-17 12:17:33'),
(677, 'EMPLOY', 'EMPLOYEE BENEFIT LOGISTIC', '2021-06-17 12:17:33'),
(678, 'EQUIP', 'EQUIPOINT', '2021-06-17 12:17:33'),
(679, 'FARM', 'FARM BUREAU', '2021-06-17 12:17:33'),
(680, 'FCPC', 'FCPC', '2021-06-17 12:17:33'),
(681, 'FID03', 'FIDELIS SECURE CARE', '2021-06-17 12:17:33'),
(682, 'FREE', 'FREEDOM LIFE', '2021-06-17 12:17:33'),
(683, 'GBI', 'GALLAGHER BASSETT', '2021-06-17 12:17:33'),
(684, 'GEHA', 'GEHA', '2021-06-17 12:17:33'),
(685, 'GIL01', 'GILSBAR', '2021-06-17 12:17:33'),
(686, 'GLO01', 'GLOBAL CARE', '2021-06-17 12:17:33'),
(687, 'GOLD2', 'GOLDEN RULE UHC', '2021-06-17 12:17:33'),
(688, 'GRI', 'GRAND RIVER INSURANCE', '2021-06-17 12:17:33'),
(689, 'GRPRES', 'GROUP RESOURCES', '2021-06-17 12:17:33'),
(690, 'HEA41D', 'HEALTH NET', '2021-06-17 12:17:33'),
(691, 'HEA45C1', 'HEALTH PLAN OF MICHIGAN', '2021-06-17 12:17:33'),
(692, 'HEALT', 'HEALTHSCOPE ADMINISTRATORS', '2021-06-17 12:17:33'),
(693, 'HLA', 'HARTFORD LIFE/MERCER HEALTH', '2021-06-17 12:17:33'),
(694, 'HPN', 'HEALTH PLAN OF NEVADA', '2021-06-17 12:17:33'),
(695, 'HRTFD', 'THE HARTFORD', '2021-06-17 12:17:33'),
(696, 'HSPR', 'HEALTH SPRING', '2021-06-17 12:17:33'),
(697, 'HUM01C1', 'HUMANA COMMERCIAL', '2021-06-17 12:17:33'),
(698, 'HUMA1', 'HUMANA MEDICARE', '2021-06-17 12:17:33'),
(699, 'IHCSOL', 'IHC SOLUTIONS', '2021-06-17 12:17:33'),
(700, 'JPFAR', 'JP FARLEY CORP', '2021-06-17 12:17:33'),
(701, 'KEY', 'KEY BENEFIT ADMINISTRATORS', '2021-06-17 12:17:33'),
(702, 'L', 'LOOMIS', '2021-06-17 12:17:33'),
(703, 'LEG', 'LEGION LIMIT MEDICAL', '2021-06-17 12:17:33'),
(704, 'LIB', 'LIBERTY MUTUAL', '2021-06-17 12:17:33'),
(705, 'LIBUN', 'LIBERTY  UNION LIFE', '2021-06-17 12:17:33'),
(706, 'LUM', 'LUMICO', '2021-06-17 12:17:33'),
(707, 'MAN', 'MANHATTAN LIFE', '2021-06-17 12:17:33'),
(708, 'MCL01', 'MCLAREN HEALTH PLANS', '2021-06-17 12:17:33'),
(709, 'MCL02', 'MCLAREN MEDICAID', '2021-06-17 12:17:33'),
(710, 'MCRADV', 'DO NOT USE', '2021-06-17 12:17:33'),
(711, 'MCWF', 'MOTOR CITY WELFARE FUND', '2021-06-17 12:17:33'),
(712, 'MED', 'LIFE MEDICO CORP LIFE INS', '2021-06-17 12:17:33'),
(713, 'MED13', 'MEDICARE PLUS BLUE', '2021-06-17 12:17:33'),
(714, 'MEDI', 'MEDISHARE', '2021-06-17 12:17:33'),
(715, 'MEDICO', 'MEDICO CORP LIFE INS', '2021-06-17 12:17:33'),
(716, 'MEDMUT', 'MEDICAL MUTUAL', '2021-06-17 12:17:33'),
(717, 'MEEMIC', 'MEEMIC AUTO', '2021-06-17 12:17:33'),
(718, 'MER02', 'MERITAIN HEALTH', '2021-06-17 12:17:33'),
(719, 'MERID1', 'MERIDIAN HEALTH PLAN', '2021-06-17 12:17:33'),
(720, 'MIC02C1', 'BLUE CROSS BLUE SHIELD', '2021-06-17 12:17:33'),
(721, 'MIC03', 'DO NOT USE', '2021-06-17 12:17:33'),
(722, 'MIC04C1', 'MICHIGAN MEDICARE', '2021-06-17 12:17:33'),
(723, 'MIC07C', 'MEDICAID', '2021-06-17 12:17:33'),
(724, 'MICH', 'MICHIGAN COMPLETE HEALTH', '2021-06-17 12:17:33'),
(725, 'MID06', 'MIDWEST HAP EMPOWERED', '2021-06-17 12:17:33'),
(726, 'MOL12', 'MOLINA', '2021-06-17 12:17:33'),
(727, 'MULT1', 'MULTIPLAN', '2021-06-17 12:17:33'),
(728, 'MUT01', 'MUTUAL OF OMAHA', '2021-06-17 12:17:33'),
(729, 'MWV', 'MEDICAID WEST VIRGINIA', '2021-06-17 12:17:33'),
(730, 'NASI', 'NATIONAL AUTOMATIC SPRINKLER IND', '2021-06-17 12:17:33'),
(731, 'NAT11', 'NATIONAL ASSOCIATION OF LETTER', '2021-06-17 12:17:33'),
(732, 'NEB', 'NEBRASKA MEDICAID', '2021-06-17 12:17:33'),
(733, 'NEIHBP', 'NATIONAL ELEVATOR INDUSTRY', '2021-06-17 12:17:33'),
(734, 'NIPPON', 'NIPPON LIFE INS', '2021-06-17 12:17:33'),
(735, 'OPTIMED', 'OPTIMED', '2021-06-17 12:17:33'),
(736, 'OXF', 'OXFORD UHC', '2021-06-17 12:17:33'),
(737, 'PACE', 'PACE SOUTHEASTERN MICHIGAN', '2021-06-17 12:17:33'),
(738, 'PACIF', 'PACIFICARE', '2021-06-17 12:17:33'),
(739, 'PAI01', 'PAI', '2021-06-17 12:17:33'),
(740, 'PAL', 'PAN AMERICAN LIFE', '2021-06-17 12:17:33'),
(741, 'PARA', 'PARADIGM MGNT SERVICE LLC', '2021-06-17 12:17:33'),
(742, 'PARAM', 'PARAMOUNT INSURANCE', '2021-06-17 12:17:33'),
(743, 'PHCS', 'PHCS', '2021-06-17 12:17:33'),
(744, 'PHIL', 'PHILADELPHIA AMERICAN LIFE', '2021-06-17 12:17:33'),
(745, 'PIF', 'PAINTERS INSURANCE FUND', '2021-06-17 12:17:33'),
(746, 'PRE18', 'PREFERRED PROVIDER', '2021-06-17 12:17:33'),
(747, 'PREM', 'PREMIER ACCESS', '2021-06-17 12:17:33'),
(748, 'PRI10V1', 'PRIORITY HEALTH', '2021-06-17 12:17:33'),
(749, 'PROGR', 'PROGRESSIVE AUTO INSURANCE', '2021-06-17 12:17:33'),
(750, 'RAIL', 'RAILROAD MEDICARE', '2021-06-17 12:17:33'),
(751, 'RELI', 'RELIANCE STANDARD LIFE', '2021-06-17 12:17:33'),
(752, 'RMA', 'RELIANCE MEDICARE ADV', '2021-06-17 12:17:33'),
(753, 'SHEET', 'SHEET METAL WORKERS', '2021-06-17 12:17:33'),
(754, 'SHP', 'SECURITY HEALTH PLAN', '2021-06-17 12:17:33'),
(755, 'SOMPO', 'SOMPO GLOBAL RISK', '2021-06-17 12:17:33'),
(756, 'STATE', 'STATE FARM GEORGIA', '2021-06-17 12:17:33'),
(757, 'STAWEL', 'STAYWELL HEALTH PLAN', '2021-06-17 12:17:33'),
(758, 'STFR', 'STATE FARM', '2021-06-17 12:17:33'),
(759, 'STFRM', 'STATE FARM DETROIT', '2021-06-17 12:17:33'),
(760, 'STONE', 'STONEBRIDGE LIFE', '2021-06-17 12:17:33'),
(761, 'STUDENT', 'STUDENT RESOURCES', '2021-06-17 12:17:33'),
(762, 'SUP', 'SUPERIOR HEALTH PLAN', '2021-06-17 12:17:33'),
(763, 'TOT03C', 'TOTAL HEALTH CARE', '2021-06-17 12:17:33'),
(764, 'TRAN', 'TRANS AMERICA', '2021-06-17 12:17:33'),
(765, 'TRANSAM', 'TRANSAMERICA FINANCIAL', '2021-06-17 12:17:33'),
(766, 'TRI05', 'TRICARE FOR LIFE', '2021-06-17 12:17:33'),
(767, 'TRIEAST', 'TRICARE EAST', '2021-06-17 12:17:33'),
(768, 'TRMARK', 'TRUSTMARK HEALTH BENEFITS', '2021-06-17 12:17:33'),
(769, 'UMR', 'UMR BEAUMONT', '2021-06-17 12:17:33'),
(770, 'UNI20V1', 'UNITED HEALTHCARE', '2021-06-17 12:17:33'),
(771, 'UNIF', 'UNIFIED LIFE INS COMP', '2021-06-17 12:17:33'),
(772, 'UNITAM', 'UNITED AMERICAN', '2021-06-17 12:17:33'),
(773, 'UPMC', 'UPMC INS SERVICES', '2021-06-17 12:17:33'),
(774, 'USAA', 'USAA LIFE INS COMP', '2021-06-17 12:17:33'),
(775, 'USHL', 'USHL FOR SMARTHEALTH', '2021-06-17 12:17:33'),
(776, 'UWL', 'UNITED WORLD LIFE', '2021-06-17 12:17:33'),
(777, 'VACCN', 'VA CCN OPTUM', '2021-06-17 12:17:33'),
(778, 'VADET', 'VETERANS - FEE FOR SERVICE', '2021-06-17 12:17:33'),
(779, 'VANI', 'VA NORTHERN INDIANA HEALTHCARE SYST', '2021-06-17 12:17:33'),
(780, 'VARIP', 'VARIPRO', '2021-06-17 12:17:33'),
(781, 'VAWPS', 'WPS MVH-CCN', '2021-06-17 12:17:33'),
(782, 'VCH', 'VETERANS CHOICE VACAA', '2021-06-17 12:17:33'),
(783, 'VMC', 'VMC', '2021-06-17 12:17:33'),
(784, 'VOCC', 'VHA OFFICE OF COMMUNITY CARE', '2021-06-17 12:17:33'),
(785, 'WASH', 'WASHINGTON NATIONAL', '2021-06-17 12:17:33'),
(786, 'WEA', 'WEA TRUST', '2021-06-17 12:17:33'),
(787, 'WEL04', 'WELLCARE', '2021-06-17 12:17:33'),
(788, 'WEST', 'WESTERN UNITED LIFE', '2021-06-17 12:17:33'),
(789, 'WPSVAP', 'WPS VAPC3', '2021-06-17 12:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis`
--

CREATE TABLE `diagnosis` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `diagnosis`
--

INSERT INTO `diagnosis` (`id`, `code`, `name`, `createdOn`) VALUES
(16, 'K29.70', 'Gastritis', '2021-06-17 12:17:33'),
(17, 'K40.0', 'Inguinal Hernia – Bilateral', '2021-06-17 12:17:33'),
(18, 'K40.90', 'Inguinal Hernia – Unilateral', '2021-06-17 12:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `id` int(11) NOT NULL,
  `dateFrom` datetime NOT NULL,
  `dateTo` datetime NOT NULL,
  `isMoveUpRequested` tinyint(1) NOT NULL DEFAULT 0,
  `eventTypeId` int(11) NOT NULL,
  `eventTypeCode` varchar(256) NOT NULL,
  `eventTypeName` varchar(256) NOT NULL,
  `priorityId` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `priorityText` varchar(256) NOT NULL,
  `facilityId` int(11) NOT NULL,
  `facilityCode` varchar(256) NOT NULL,
  `facilityName` varchar(256) NOT NULL,
  `facilityAddress` varchar(512) NOT NULL,
  `physicianId` int(11) NOT NULL,
  `physicianName` varchar(256) NOT NULL,
  `physicianPhone` varchar(64) DEFAULT NULL,
  `physicianFax` varchar(64) DEFAULT NULL,
  `coordinatorId` int(11) NOT NULL,
  `coordinatorName` varchar(256) NOT NULL,
  `patientId` int(11) NOT NULL,
  `patientName` varchar(128) DEFAULT NULL,
  `patientLastName` varchar(128) DEFAULT NULL,
  `patientDoB` datetime DEFAULT NULL,
  `patientGender` varchar(64) DEFAULT NULL,
  `patientAddress` varchar(512) DEFAULT NULL,
  `patientPreferredPhone` varchar(256) DEFAULT NULL,
  `patientSSN` varchar(128) DEFAULT NULL,
  `insuranceInfo` text DEFAULT NULL,
  `authorizationRequired` varchar(64) DEFAULT NULL,
  `diagnosisId` int(11) DEFAULT NULL,
  `diagnosisCode` varchar(64) DEFAULT NULL,
  `diagnosisDescription` varchar(512) DEFAULT NULL,
  `comment` text DEFAULT NULL,
  `medicalClearanceStatus` enum('not_required','pending','accepted','rejected') NOT NULL DEFAULT 'not_required',
  `cardiacClearanceStatus` enum('not_required','pending','accepted','rejected') DEFAULT 'not_required',
  `otherClearanceStatus` enum('not_required','pending','accepted','rejected') NOT NULL DEFAULT 'not_required',
  `otherClearance` text DEFAULT NULL,
  `medicalClearancePhysician` varchar(512) DEFAULT NULL,
  `cardiacClearancePhysician` varchar(512) DEFAULT NULL,
  `otherClearancePhysician` varchar(512) DEFAULT NULL,
  `bloodThinnerId` int(11) DEFAULT NULL,
  `bloodThinnerCode` varchar(128) DEFAULT NULL,
  `bloodThinnerName` varchar(256) DEFAULT NULL,
  `bloodThinnerDescription` varchar(512) DEFAULT NULL,
  `status` varchar(64) NOT NULL DEFAULT 'pending',
  `isEmergency` tinyint(4) NOT NULL DEFAULT 0,
  `isDraft` tinyint(4) NOT NULL DEFAULT 0,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedOn` datetime NOT NULL DEFAULT current_timestamp(),
  `bsgOn` datetime DEFAULT NULL,
  `wfcOn` datetime DEFAULT NULL,
  `confirmedOn` datetime DEFAULT NULL,
  `completedOn` datetime DEFAULT NULL,
  `reminders` int(11) NOT NULL DEFAULT 0,
  `latestReminderOn` datetime DEFAULT NULL,
  `carrierId` int(11) DEFAULT NULL,
  `carrierCode` varchar(256) DEFAULT NULL,
  `carrierName` varchar(512) DEFAULT NULL,
  `subscriberId` text DEFAULT NULL,
  `groupName` text DEFAULT NULL,
  `groupNumber` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `dateFrom`, `dateTo`, `isMoveUpRequested`, `eventTypeId`, `eventTypeCode`, `eventTypeName`, `priorityId`, `priority`, `priorityText`, `facilityId`, `facilityCode`, `facilityName`, `facilityAddress`, `physicianId`, `physicianName`, `physicianPhone`, `physicianFax`, `coordinatorId`, `coordinatorName`, `patientId`, `patientName`, `patientLastName`, `patientDoB`, `patientGender`, `patientAddress`, `patientPreferredPhone`, `patientSSN`, `insuranceInfo`, `authorizationRequired`, `diagnosisId`, `diagnosisCode`, `diagnosisDescription`, `comment`, `medicalClearanceStatus`, `cardiacClearanceStatus`, `otherClearanceStatus`, `otherClearance`, `medicalClearancePhysician`, `cardiacClearancePhysician`, `otherClearancePhysician`, `bloodThinnerId`, `bloodThinnerCode`, `bloodThinnerName`, `bloodThinnerDescription`, `status`, `isEmergency`, `isDraft`, `createdOn`, `updatedOn`, `bsgOn`, `wfcOn`, `confirmedOn`, `completedOn`, `reminders`, `latestReminderOn`, `carrierId`, `carrierCode`, `carrierName`, `subscriberId`, `groupName`, `groupNumber`) VALUES
(33, '2021-06-24 12:23:00', '2021-06-24 14:38:00', 0, 14, 'ENDO', 'Endoscopy', 18, 1, 'Routine - Elective', 25, 'HSP1', 'Ascension - St. John', '22101 Moross Rd, Detroit, MI 48236', 20, 'Demo Physician One', '1116574897', '1113334456', 8, 'Louai', 20, 'John William Doe', NULL, '1995-07-24 00:00:00', 'female', 'Main Street 123/Detroit/Michigan/11223355', '1112223355', '111-22-3333', NULL, NULL, 17, 'K40.0', 'Inguinal Hernia – Bilateral', NULL, 'not_required', 'pending', 'accepted', NULL, NULL, 'Example Cardiac Physician', 'Other Medical Physiciann', 27, 'ASA325', 'Aspirin 325mg', 'Patient needs to stop taking Aspirin 325mg 7 days before surgery.', 'confirmed', 0, 0, '2021-06-17 12:23:49', '2021-06-17 12:23:49', '2021-06-17 12:28:33', '2021-06-17 13:19:11', '2021-06-17 13:19:12', NULL, 0, NULL, 641, 'AIG', 'AIG', '111', 'Group One', 'G1');

-- --------------------------------------------------------

--
-- Table structure for table `event_checklist`
--

CREATE TABLE `event_checklist` (
  `id` int(11) NOT NULL,
  `eventId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `description` text NOT NULL,
  `step` int(11) NOT NULL DEFAULT 0,
  `groupName` varchar(512) DEFAULT NULL,
  `assigneeId` int(11) DEFAULT NULL,
  `dueDate` datetime NOT NULL DEFAULT current_timestamp(),
  `status` enum('tbd','done','na') NOT NULL DEFAULT 'tbd',
  `createdOn` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event_checklist`
--

INSERT INTO `event_checklist` (`id`, `eventId`, `userId`, `description`, `step`, `groupName`, `assigneeId`, `dueDate`, `status`, `createdOn`) VALUES
(21, 33, 8, 'Paperwork prepared for medical clearance', 0, 'Medical Clearance', 7, '2021-06-25 10:27:00', 'done', '2021-06-18 07:28:48'),
(22, 33, 8, 'Paperwork prepared for medical clearance', 0, 'Cardiac Clearance', 7, '2021-06-10 00:00:00', 'done', '2021-06-18 08:24:58'),
(23, 33, 8, 'Patient organized medical clearance appointment', 0, 'Medical Clearance', 7, '2021-06-26 00:00:00', 'tbd', '2021-06-18 08:33:23'),
(24, 33, 8, 'Medical clearance form filled out by PCP', 0, 'Medical Clearance', 7, '2021-06-30 00:00:00', 'tbd', '2021-06-18 10:25:42'),
(25, 33, 8, 'Medical clearance reviewed for abnormalities', 0, 'Cardiac Clearance', 1, '2021-06-24 00:00:00', 'tbd', '2021-06-18 11:00:16');

-- --------------------------------------------------------

--
-- Table structure for table `event_note`
--

CREATE TABLE `event_note` (
  `id` int(11) NOT NULL,
  `eventId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `note` text NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event_note`
--

INSERT INTO `event_note` (`id`, `eventId`, `userId`, `note`, `createdOn`) VALUES
(12, 33, 8, 'This is an example note', '2021-06-17 12:28:29');

-- --------------------------------------------------------

--
-- Table structure for table `event_procedure`
--

CREATE TABLE `event_procedure` (
  `id` int(11) NOT NULL,
  `eventId` int(11) NOT NULL,
  `procedureId` int(11) NOT NULL,
  `procedureCode` varchar(256) NOT NULL,
  `procedureName` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event_procedure`
--

INSERT INTO `event_procedure` (`id`, `eventId`, `procedureId`, `procedureCode`, `procedureName`, `createdOn`) VALUES
(31, 33, 21, '45378', 'Colonoscopy', '2021-06-21 11:10:18');

-- --------------------------------------------------------

--
-- Table structure for table `event_type`
--

CREATE TABLE `event_type` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event_type`
--

INSERT INTO `event_type` (`id`, `code`, `name`, `createdOn`) VALUES
(13, 'OR', 'Main OR', '2021-06-17 12:17:33'),
(14, 'ENDO', 'Endoscopy', '2021-06-17 12:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `facility`
--

CREATE TABLE `facility` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `address` varchar(256) NOT NULL,
  `schedulingOfficePhone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `facility`
--

INSERT INTO `facility` (`id`, `code`, `name`, `address`, `schedulingOfficePhone`, `fax`, `createdOn`) VALUES
(25, 'HSP1', 'Ascension - St. John', '22101 Moross Rd, Detroit, MI 48236', '3133433366', '3133433366', '2021-06-17 12:17:33'),
(26, 'HSP2', 'Ascension - Macomb', '11800 Twelve Mile Rd, Warren, MI 48093', '5865735000', '5865735000', '2021-06-17 12:17:33'),
(27, 'HSP3', 'Ascension - Oakland', '27351 Dequindre Rd, Madison Heights, MI 48071', '2489677000', '2489677000', '2021-06-17 12:17:33'),
(28, 'HSP4', 'Beaumont Hospital - Grosse Pointe', '468 Cadieux Rd, Grosse Pointe, MI 48230', '3134731000', '3134731000', '2021-06-17 12:17:33'),
(29, 'HSP5', 'Beaumont Hospital - Dearborn', '18101 Oakwood Blvd, Dearborn, MI 48124', '3135937000', '3135937000', '2021-06-17 12:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `facility_physician`
--

CREATE TABLE `facility_physician` (
  `id` int(11) NOT NULL,
  `facilityId` int(11) NOT NULL,
  `physicianId` int(11) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `facility_physician`
--

INSERT INTO `facility_physician` (`id`, `facilityId`, `physicianId`, `createdOn`) VALUES
(5, 25, 20, '2021-06-17 12:19:16'),
(6, 26, 20, '2021-06-17 12:19:20'),
(7, 27, 20, '2021-06-17 12:19:22'),
(8, 28, 20, '2021-06-17 12:19:24'),
(9, 29, 20, '2021-06-17 12:19:25');

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE `history` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `actionType` varchar(64) NOT NULL,
  `description` varchar(512) DEFAULT NULL,
  `extra1` text DEFAULT NULL,
  `extra2` int(11) DEFAULT NULL,
  `extra3` int(11) DEFAULT NULL,
  `extra4` int(11) DEFAULT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `userId`, `actionType`, `description`, `extra1`, `extra2`, `extra3`, `extra4`, `createdOn`) VALUES
(1, 3, 'AddMasterData', 'Added master data entry \"Urgent\" to master data \"priority\"', NULL, NULL, NULL, NULL, '2021-06-02 10:34:20'),
(2, 3, 'AddMasterData', 'Added master data entry \"Diagnosis 2\" to \"diagnosis\"', NULL, NULL, NULL, NULL, '2021-06-02 10:37:18'),
(3, 3, 'AddMasterData', 'Added master data entry \"Blood Thinner New\" to \"blood_thinner\"', NULL, NULL, NULL, NULL, '2021-06-02 10:40:52'),
(4, 3, 'DeleteMasterData', 'Deleted master data entry from \"blood_thinner\"', NULL, NULL, NULL, NULL, '2021-06-02 10:40:54'),
(5, 3, 'AddEvent', 'Added event #6 for patient \"undefined\"', NULL, NULL, NULL, NULL, '2021-06-03 16:23:53'),
(6, 3, 'AddEvent', 'Added event #7 for patient #1', NULL, NULL, NULL, NULL, '2021-06-04 09:43:13'),
(7, 3, 'AddEvent', 'Added event #8 for patient #1', NULL, NULL, NULL, NULL, '2021-06-04 12:40:33'),
(8, 3, 'AddEvent', 'Added event #9 for patient #1', NULL, NULL, NULL, NULL, '2021-06-04 12:42:28'),
(9, 3, 'AddMasterData', 'Added master data entry \"Emergency Room 1\" to \"facility_room\"', NULL, NULL, NULL, NULL, '2021-06-04 13:26:57'),
(10, 3, 'AddEvent', 'Added event #10 for patient #1', NULL, NULL, NULL, NULL, '2021-06-04 13:46:49'),
(11, 3, 'AddEvent', 'Added event #11 for patient #1', NULL, NULL, NULL, NULL, '2021-06-08 13:11:24'),
(12, 3, 'AddMasterData', 'Added master data entry \"Procedure 1\" to \"procedures\"', NULL, NULL, NULL, NULL, '2021-06-09 10:46:32'),
(13, 3, 'AddMasterData', 'Added master data entry \"Procedure 2\" to \"procedures\"', NULL, NULL, NULL, NULL, '2021-06-09 10:46:38'),
(14, 3, 'AddMasterData', 'Added master data entry \"Procedure 3\" to \"procedures\"', NULL, NULL, NULL, NULL, '2021-06-09 10:46:44'),
(15, 3, 'AddMasterData', 'Added master data entry \"Procedure 4\" to \"procedures\"', NULL, NULL, NULL, NULL, '2021-06-09 10:46:50'),
(16, 3, 'AddMasterData', 'Added master data entry \"Procedure 5\" to \"procedures\"', NULL, NULL, NULL, NULL, '2021-06-09 10:46:55'),
(17, 3, 'AddEvent', 'Added event #12 for patient #1', NULL, NULL, NULL, NULL, '2021-06-10 10:23:55'),
(18, 3, 'AddMasterData', 'Added master data entry \"Aspirin\" to \"blood_thinner\"', NULL, NULL, NULL, NULL, '2021-06-10 11:04:31'),
(19, 3, 'AddEvent', 'Added event #13 for patient #1', NULL, NULL, NULL, NULL, '2021-06-11 13:32:04'),
(20, 18, 'AddEvent', 'Added event #14 for patient #10', NULL, NULL, NULL, NULL, '2021-06-11 16:23:27'),
(21, 18, 'AddEvent', 'Added event #15 for patient #9', NULL, NULL, NULL, NULL, '2021-06-11 16:25:27'),
(22, 18, 'AddEvent', 'Added event #16 for patient #9', NULL, NULL, NULL, NULL, '2021-06-14 12:12:15'),
(23, 18, 'AddEvent', 'Added event #17 for patient #9', NULL, NULL, NULL, NULL, '2021-06-14 12:13:54'),
(24, 1, 'AddEvent', 'Added event #18 for patient #10', NULL, NULL, NULL, NULL, '2021-06-14 13:57:50'),
(25, 1, 'AddEvent', 'Added event #19 for patient #10', NULL, NULL, NULL, NULL, '2021-06-14 13:58:27'),
(26, 1, 'AddEvent', 'Added event #20 for patient #10', NULL, NULL, NULL, NULL, '2021-06-15 11:25:29'),
(27, 1, 'AddEvent', 'Added event #21 for patient #10', NULL, NULL, NULL, NULL, '2021-06-15 11:27:11'),
(28, 1, 'AddEvent', 'Added event #22 for patient #16', NULL, NULL, NULL, NULL, '2021-06-15 12:08:29'),
(29, 1, 'AddEvent', 'Added event #24 for patient #10', NULL, NULL, NULL, NULL, '2021-06-16 08:43:19'),
(30, 1, 'AddEvent', 'Added event #25 for patient #10', NULL, NULL, NULL, NULL, '2021-06-16 08:44:07'),
(31, 1, 'AddEvent', 'Added event #26 for patient #10', NULL, NULL, NULL, NULL, '2021-06-16 11:44:30'),
(32, 1, 'AddEvent', 'Added event #27 for patient #10', NULL, NULL, NULL, NULL, '2021-06-16 11:44:58'),
(33, 1, 'AddEvent', 'Added event #28 for patient #10', NULL, NULL, NULL, NULL, '2021-06-16 11:46:14'),
(34, 1, 'AddMasterData', 'Added master data entry \"Carrier 1\" to \"carrier\"', NULL, NULL, NULL, NULL, '2021-06-16 12:16:58'),
(35, 1, 'AddMasterData', 'Added master data entry \"asfasf\" to \"carrier\"', NULL, NULL, NULL, NULL, '2021-06-16 12:17:12'),
(36, 1, 'DeleteMasterData', 'Deleted master data entry from \"carrier\"', NULL, NULL, NULL, NULL, '2021-06-16 12:17:16'),
(37, 1, 'AddMasterData', 'Added master data entry \"Carrier 2\" to \"carrier\"', NULL, NULL, NULL, NULL, '2021-06-16 12:17:26'),
(38, 1, 'AddMasterData', 'Added master data entry \"Carrier 3\" to \"carrier\"', NULL, NULL, NULL, NULL, '2021-06-16 12:17:34'),
(39, 1, 'AddEvent', 'Added event #29 for patient #17', NULL, NULL, NULL, NULL, '2021-06-16 12:40:18'),
(40, 1, 'AddEvent', 'Added event #30 for patient #19', NULL, NULL, NULL, NULL, '2021-06-16 13:40:34'),
(41, 1, 'AddEvent', 'Added event #31 for patient #19', NULL, NULL, NULL, NULL, '2021-06-16 13:42:12'),
(42, 1, 'AddMasterData', 'Added master data entry \"safasfasf\" to \"event_type\"', NULL, NULL, NULL, NULL, '2021-06-16 15:14:15'),
(43, 1, 'DeleteMasterData', 'Deleted master data entry from \"event_type\"', NULL, NULL, NULL, NULL, '2021-06-16 15:14:19'),
(44, 1, 'AddEvent', 'Added event #32 for patient #10', NULL, NULL, NULL, NULL, '2021-06-17 08:29:05'),
(45, 8, 'AddEvent', 'Added event #33 for patient #20', NULL, NULL, NULL, NULL, '2021-06-17 12:23:49');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `firstName` varchar(256) DEFAULT NULL,
  `middleName` varchar(256) DEFAULT NULL,
  `lastName` varchar(256) DEFAULT NULL,
  `address1` varchar(256) NOT NULL,
  `address2` varchar(256) NOT NULL,
  `city` varchar(32) NOT NULL,
  `state` varchar(32) NOT NULL,
  `zipcode` varchar(32) NOT NULL,
  `dateofbirth` date NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `email` varchar(256) DEFAULT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `ssn` varchar(64) DEFAULT NULL,
  `sex` varchar(64) DEFAULT NULL,
  `carrierId` int(11) DEFAULT NULL,
  `carrierCode` varchar(256) DEFAULT NULL,
  `carrierName` varchar(512) DEFAULT NULL,
  `subscriberId` text DEFAULT NULL,
  `groupName` text DEFAULT NULL,
  `groupNumber` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `name`, `firstName`, `middleName`, `lastName`, `address1`, `address2`, `city`, `state`, `zipcode`, `dateofbirth`, `createdOn`, `email`, `phone`, `ssn`, `sex`, `carrierId`, `carrierCode`, `carrierName`, `subscriberId`, `groupName`, `groupNumber`) VALUES
(20, 'John William Doe', 'John', 'William', 'Doe', 'Main Street 123', 'Bld 3', 'Detroit', 'Michigan', '11223343', '1984-02-09', '2021-06-17 12:23:24', 'john.doe@mail.com', '1112223355', '111-22-3333', 'male', 670, 'CONWE', 'CONTINENTAL WESTERN', '111', 'Group One', 'G111');

-- --------------------------------------------------------

--
-- Table structure for table `patient_allergy`
--

CREATE TABLE `patient_allergy` (
  `id` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `status` varchar(128) NOT NULL,
  `allergyDate` datetime NOT NULL,
  `reaction` varchar(512) NOT NULL,
  `rxNormId` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `priority`
--

CREATE TABLE `priority` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `priority` int(11) NOT NULL DEFAULT 3
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `priority`
--

INSERT INTO `priority` (`id`, `code`, `name`, `createdOn`, `priority`) VALUES
(18, 'Elective', 'Routine - Elective', '2021-06-17 12:17:33', 1),
(19, 'PR124', 'Elective w/ RTMU', '2021-06-17 12:17:33', 3),
(20, 'Stat', 'Emergency / STAT', '2021-06-17 12:17:33', 5);

-- --------------------------------------------------------

--
-- Table structure for table `procedures`
--

CREATE TABLE `procedures` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `procedures`
--

INSERT INTO `procedures` (`id`, `code`, `name`, `createdOn`) VALUES
(21, '45378', 'Colonoscopy', '2021-06-17 12:17:33'),
(22, '45380', 'Colonoscopy w/ Biopsies', '2021-06-17 12:17:33'),
(23, '45990', 'Examination Under Anesthesia', '2021-06-17 12:17:33');

-- --------------------------------------------------------

--
-- Table structure for table `refresh`
--

CREATE TABLE `refresh` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `token` varchar(512) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `refresh`
--

INSERT INTO `refresh` (`id`, `userId`, `token`, `createdOn`) VALUES
(26, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjA5NjQyNCwiZXhwIjoxNjI0Njg4NDI0fQ.6K-UGRqUqDBx0U3fGCnHFJ5bOiSUalhX_UoCTyuwP1c', '2021-05-27 09:20:24'),
(27, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjA5NjU4MiwiZXhwIjoxNjI0Njg4NTgyfQ.YFNaRs793Rla-5sdilcVjxs2pj-0gOalkK7TLOZX_pQ', '2021-05-27 09:23:02'),
(28, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjA5Njk5MywiZXhwIjoxNjI0Njg4OTkzfQ.hOoZJ8vmOYOidl3bb1H16bi0W4JXLEyjP8-11v6HLJo', '2021-05-27 09:29:53'),
(30, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjA5OTg2MCwiZXhwIjoxNjI0NjkxODYwfQ.vnFOY9yt-SlYsblidhdcv1wpDz0FHrF17m_d9j8d-j8', '2021-05-27 10:17:40'),
(31, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDA1NCwiZXhwIjoxNjI0NjkyMDU0fQ.Cu4m5oL1t4hj7L0NCNgfUUgL3njbe_ZjYStWOWIqDqo', '2021-05-27 10:20:54'),
(32, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDA1NywiZXhwIjoxNjI0NjkyMDU3fQ.HYsO7kVqzsDiJCXroqUS7z76rp_aRjDDHZcLVWBaNNs', '2021-05-27 10:20:57'),
(33, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDA5OSwiZXhwIjoxNjI0NjkyMDk5fQ.fyk9s8Z-h9We7-MvQ5uu-JnKNqynWKA7BWRQiOFJAnU', '2021-05-27 10:21:39'),
(34, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE0NiwiZXhwIjoxNjI0NjkyMTQ2fQ.LN0QiI8R_l6zxR1gZKvEIwjcZsfDu5UlxcsQ_le2tXI', '2021-05-27 10:22:26'),
(35, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE1MywiZXhwIjoxNjI0NjkyMTUzfQ.ZSAnN2ryX_TeI1z4JIKPiIJ1-_zn01n_EpI5pF7EVDk', '2021-05-27 10:22:33'),
(36, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE2MywiZXhwIjoxNjI0NjkyMTYzfQ.i3SUdlUBBMAVLWD-gAuLFvRh7DpXkMq_OyY7ezlcm3s', '2021-05-27 10:22:43'),
(37, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE3MiwiZXhwIjoxNjI0NjkyMTcyfQ.ySVBEbmZ3eF8OTuQaghVRc8DUY6p3j19vv3JE6bZb7A', '2021-05-27 10:22:52'),
(38, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE5NywiZXhwIjoxNjI0NjkyMTk3fQ.u6uE22cxGuSWbQS-yPQUcbk1MWxU9oitgvmDHLH47Nw', '2021-05-27 10:23:17'),
(39, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDIzMiwiZXhwIjoxNjI0NjkyMjMyfQ.utGNd9y7tPLwL4YhIlwZ7ky6h4y63dmEuyNC8tJx18o', '2021-05-27 10:23:52'),
(40, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDI2MiwiZXhwIjoxNjI0NjkyMjYyfQ.A7mqDlOS5wu67QRPhSkVCVL8tl4n4tOnF39FKrQTDuc', '2021-05-27 10:24:22'),
(41, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDQyOCwiZXhwIjoxNjI0NjkyNDI4fQ.PFKbOpoqWJZ_z1QvrZhfbV2KkPxVS3nC7beEiy0MyOc', '2021-05-27 10:27:08'),
(42, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDQ2NCwiZXhwIjoxNjI0NjkyNDY0fQ.13V9fVCeZWiEqI6oJIR71Mr3naNc0EWBf1HrBrQc9Vw', '2021-05-27 10:27:44'),
(43, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDU4MywiZXhwIjoxNjI0NjkyNTgzfQ.CJfOyeK2zufVl8EZoS1x8XiLeXHmaEFWjhQytRDKmBY', '2021-05-27 10:29:43'),
(44, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDY4MywiZXhwIjoxNjI0NjkyNjgzfQ.o3S7gTjdWz-gJ0tk63NlwZpVr7e2nxuaosfO7gHkFIk', '2021-05-27 10:31:23'),
(45, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDY5MSwiZXhwIjoxNjI0NjkyNjkxfQ.uNw_JkfyP0siAJ3os1iW8-Tcwu0-D91jX3CAkFG5PSI', '2021-05-27 10:31:31'),
(46, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDc4NywiZXhwIjoxNjI0NjkyNzg3fQ.o0nynrypSLFMYlcL7fw5lPRJ3MaLHtqxW2IbfxwZafk', '2021-05-27 10:33:07'),
(47, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDkwNywiZXhwIjoxNjI0NjkyOTA3fQ.geqnrMMYSrBvwOuEhYQ85pojgGBm9Lh7NnSwmEfcNp8', '2021-05-27 10:35:07'),
(48, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMjA4OSwiZXhwIjoxNjI0Njk0MDg5fQ.Q_k9G9VL3v1EcWLopoWEcOCnrth_3OIM1XVM9hzllac', '2021-05-27 10:54:49'),
(110, 10, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEwLCJpYXQiOjE2MjM0MTU3NjUsImV4cCI6MTYyMzUwMjE2NX0.P1Qld8kNRHSe9Xf1Z6Zh8Lst3LAakWX30mpW5_7DW7k', '2021-06-11 15:49:25'),
(111, 11, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjExLCJpYXQiOjE2MjM0MTU4MzMsImV4cCI6MTYyMzUwMjIzM30.9TaVCAsw3kE17mRtY0CqQ6wJW2rKqIHjLZ_Z_WcUjdY', '2021-06-11 15:50:33'),
(112, 12, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEyLCJpYXQiOjE2MjM0MTU4NzQsImV4cCI6MTYyMzUwMjI3NH0.HeO5zz9jMe-rM9GbTfkW9aMkw8qsCPIl3odxJE1V2z0', '2021-06-11 15:51:14'),
(113, 13, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEzLCJpYXQiOjE2MjM0MTYwMjksImV4cCI6MTYyMzUwMjQyOX0.xS6outos8SY-VtmirVh9K8a4d4J-YyHn75xUBTy2Pd0', '2021-06-11 15:53:49'),
(115, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMzQxNjQ3NSwiZXhwIjoxNjIzNTAyODc1fQ.FV8M0Ivv6LKOcUTObbOnno0AP9TnMPJ7s4a5bncgd6w', '2021-06-11 16:01:15'),
(121, 16, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjE2LCJpYXQiOjE2MjM0MTgwOTksImV4cCI6MTYyMzUwNDQ5OX0.K55HhqrvSpvOcJZhPNt2XuNgaUaBYleNiHq6S810qTM', '2021-06-11 16:28:19'),
(129, 1, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjEsImlhdCI6MTYyMzgzMTMzNiwiZXhwIjoxNjIzOTE3NzM2fQ.zLYiLTQ78YcAKaCJZ1uLDt-MW5ILrtQTpTxbyy9NXf8', '2021-06-16 11:15:36'),
(130, 8, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjgsImlhdCI6MTYyMzkyMTI0NSwiZXhwIjoxNjI0MDA3NjQ1fQ.8LVZ_VWOors3eN7FlGFPNSmP-DaQBb4DMyNllZpEc7s', '2021-06-17 12:14:05'),
(131, 8, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjgsImlhdCI6MTYyNDAxMTg3MCwiZXhwIjoxNjI0MDk4MjcwfQ.vYs2XkMhhzi-nOd_8fvJXxO3Tdpw95_AaydhxeAN1gc', '2021-06-18 13:24:30'),
(132, 8, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjgsImlhdCI6MTYyNDI1MTY3MiwiZXhwIjoxNjI0MzM4MDcyfQ.hlCYum3ha1gIwHxG273jbv1OrO7tV6DcE3fC7SQzwtw', '2021-06-21 08:01:12');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `name` varchar(64) NOT NULL,
  `fax` varchar(64) DEFAULT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `password` varchar(256) NOT NULL,
  `avatar` varchar(128) DEFAULT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `name`, `fax`, `phone`, `password`, `avatar`, `createdOn`) VALUES
(1, 'amandaalame@gmail.com', 'Amanda Alame', '5863438773', '3139124798', '$2b$10$yFc7Sg7WNxrC5t5cUAs5JupzNdWj/MrYHz8L1/MFX/zBncdsfwiga', NULL, '2021-06-17 12:17:33'),
(2, 'amerzeni@gmail.com', 'Amer M. Zeni, MD', '5863438773', '3178093538', '$2b$10$S/yR/u8iiHMSRMLORIHY1.UEIL84iYS9yrIF1avdPdSmzCYgG1p/C', NULL, '2021-06-17 12:17:33'),
(3, 'alex.conley81@gmail.com', 'Alexandria C. Glenn, MD', '5863438773', '3136631981', '$2b$10$60xfzqj0gT2SKijWWPNhT.LzU9kcRTlx7fzOtu9C4ExUS.U64Liya', NULL, '2021-06-17 12:17:33'),
(4, 'ameralame@gmail.com', 'Amer M. Alame, MD', '5863438773', '3133337372', '$2b$10$CZrMZGcyseWfStNUjTuPaeikfcKbHijcX3je7ulrumg9dD9fJT3SW', NULL, '2021-06-17 12:17:33'),
(5, 'doris@harperclinic.org', 'Doris Ferries', '5863438773', '5863438717', '$2b$10$j/FzjmXK8vew8jCjEPOLT.J0JlBWIWZSWOfBmpSrihgCz5ymbSGxu', NULL, '2021-06-17 12:17:33'),
(6, 'elie@live.com', 'Elie Kattar', '', '8182757850', '$2b$10$oCEI8/b7mo81iN2og41R/.4eIxGwUxhpP.SmVOpegsEm.It8/KnVy', NULL, '2021-06-17 12:17:33'),
(7, 'mtasker122@gmail.com', 'Megan Tasker, DO', '5865800006', '2486319953', '$2b$10$yizbEYd6rmlRU2TLVpv1Z.17b9A.OaAb/k1CQNvlnZtPYeP0.vZd.', NULL, '2021-06-17 12:17:33'),
(8, 'louai@galen.com', 'Louai', '', '8182757851', '$2b$10$EsZ9mnrwa1NriplRBe4ZmuSD.U0DdjzPBowEvsvhJdAVrsxOxlDoe', NULL, '2021-06-17 12:17:33'),
(20, 'demo.physician1@mail.com', 'Demo Physician One', '1113334456', '1116574897', '$2b$10$EyH6pmGtia3tvYv.VFlVwudJvRwcqClKAd8KK64t/Guc0pkiNuT3G', NULL, '2021-06-17 12:19:12');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `role` varchar(32) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `userId`, `role`, `createdOn`) VALUES
(80, 1, 'admin', '2021-06-17 12:17:33'),
(81, 2, 'physician', '2021-06-17 12:17:33'),
(82, 3, 'physician', '2021-06-17 12:17:33'),
(83, 4, 'physician', '2021-06-17 12:17:33'),
(84, 5, 'coordinator', '2021-06-17 12:17:33'),
(85, 6, 'admin', '2021-06-17 12:17:33'),
(86, 7, 'physician', '2021-06-17 12:17:33'),
(87, 8, 'admin', '2021-06-17 12:17:33'),
(88, 20, 'physician', '2021-06-17 12:20:13');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blood_thinner`
--
ALTER TABLE `blood_thinner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carrier`
--
ALTER TABLE `carrier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diagnosis`
--
ALTER TABLE `diagnosis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eventTypeId` (`eventTypeId`,`priorityId`,`facilityId`,`physicianId`,`coordinatorId`,`patientId`,`diagnosisId`,`bloodThinnerId`),
  ADD KEY `facilityId` (`facilityId`),
  ADD KEY `patientId` (`patientId`),
  ADD KEY `coordinatorId` (`coordinatorId`),
  ADD KEY `physicianId` (`physicianId`),
  ADD KEY `priorityId` (`priorityId`),
  ADD KEY `diagnosisId` (`diagnosisId`),
  ADD KEY `bloodThinnerId` (`bloodThinnerId`),
  ADD KEY `carrierId` (`carrierId`);

--
-- Indexes for table `event_checklist`
--
ALTER TABLE `event_checklist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eventId` (`eventId`),
  ADD KEY `userId` (`userId`);

--
-- Indexes for table `event_note`
--
ALTER TABLE `event_note`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`),
  ADD KEY `eventId` (`eventId`);

--
-- Indexes for table `event_procedure`
--
ALTER TABLE `event_procedure`
  ADD PRIMARY KEY (`id`),
  ADD KEY `eventId` (`eventId`),
  ADD KEY `procedureId` (`procedureId`);

--
-- Indexes for table `event_type`
--
ALTER TABLE `event_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility`
--
ALTER TABLE `facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility_physician`
--
ALTER TABLE `facility_physician`
  ADD PRIMARY KEY (`id`),
  ADD KEY `facilityId` (`facilityId`),
  ADD KEY `physicianId` (`physicianId`);

--
-- Indexes for table `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`),
  ADD KEY `carrierId` (`carrierId`);

--
-- Indexes for table `patient_allergy`
--
ALTER TABLE `patient_allergy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patientId` (`patientId`);

--
-- Indexes for table `priority`
--
ALTER TABLE `priority`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procedures`
--
ALTER TABLE `procedures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refresh`
--
ALTER TABLE `refresh`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`),
  ADD KEY `token` (`token`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blood_thinner`
--
ALTER TABLE `blood_thinner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `carrier`
--
ALTER TABLE `carrier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=790;

--
-- AUTO_INCREMENT for table `diagnosis`
--
ALTER TABLE `diagnosis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `event_checklist`
--
ALTER TABLE `event_checklist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `event_note`
--
ALTER TABLE `event_note`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `event_procedure`
--
ALTER TABLE `event_procedure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `event_type`
--
ALTER TABLE `event_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `facility`
--
ALTER TABLE `facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `facility_physician`
--
ALTER TABLE `facility_physician`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `history`
--
ALTER TABLE `history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `patient_allergy`
--
ALTER TABLE `patient_allergy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `priority`
--
ALTER TABLE `priority`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `procedures`
--
ALTER TABLE `procedures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `refresh`
--
ALTER TABLE `refresh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `event_ibfk_1` FOREIGN KEY (`facilityId`) REFERENCES `facility` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_10` FOREIGN KEY (`carrierId`) REFERENCES `carrier` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_2` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_3` FOREIGN KEY (`coordinatorId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_4` FOREIGN KEY (`physicianId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_6` FOREIGN KEY (`priorityId`) REFERENCES `priority` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_7` FOREIGN KEY (`diagnosisId`) REFERENCES `diagnosis` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_8` FOREIGN KEY (`eventTypeId`) REFERENCES `event_type` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_ibfk_9` FOREIGN KEY (`bloodThinnerId`) REFERENCES `blood_thinner` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `event_checklist`
--
ALTER TABLE `event_checklist`
  ADD CONSTRAINT `event_checklist_ibfk_1` FOREIGN KEY (`eventId`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_checklist_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `event_note`
--
ALTER TABLE `event_note`
  ADD CONSTRAINT `event_note_ibfk_1` FOREIGN KEY (`eventId`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_note_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `event_procedure`
--
ALTER TABLE `event_procedure`
  ADD CONSTRAINT `event_procedure_ibfk_1` FOREIGN KEY (`eventId`) REFERENCES `event` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `event_procedure_ibfk_2` FOREIGN KEY (`procedureId`) REFERENCES `procedures` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `facility_physician`
--
ALTER TABLE `facility_physician`
  ADD CONSTRAINT `facility_physician_ibfk_1` FOREIGN KEY (`facilityId`) REFERENCES `facility` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `facility_physician_ibfk_2` FOREIGN KEY (`physicianId`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `patient`
--
ALTER TABLE `patient`
  ADD CONSTRAINT `patient_ibfk_1` FOREIGN KEY (`carrierId`) REFERENCES `carrier` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `patient_allergy`
--
ALTER TABLE `patient_allergy`
  ADD CONSTRAINT `patient_allergy_ibfk_1` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
