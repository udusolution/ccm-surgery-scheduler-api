CREATE TABLE `event_edit_whitelist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `allowerId` int(11) NOT NULL,
  `allowedId` int(11) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedOn` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `allowerId` (`allowerId`,`allowedId`),
  KEY `allowedId` (`allowedId`),
  CONSTRAINT `event_edit_whitelist_ibfk_1` FOREIGN KEY (`allowerId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `event_edit_whitelist_ibfk_2` FOREIGN KEY (`allowedId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;