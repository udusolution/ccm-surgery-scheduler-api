-- Add index to day_note_mention noteId foreign key
ALTER TABLE `day_note_mention` 
ADD INDEX `note_id_index` (`noteId`);