-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 31, 2021 at 03:44 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `galen`
--

-- --------------------------------------------------------

--
-- Table structure for table `blood_thinner`
--

CREATE TABLE `blood_thinner` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blood_thinner`
--

INSERT INTO `blood_thinner` (`id`, `code`, `name`, `createdOn`) VALUES
(3, 'BT123', 'Blood Thinner', '2021-05-31 08:57:21');

-- --------------------------------------------------------

--
-- Table structure for table `diagnosis`
--

CREATE TABLE `diagnosis` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `diagnosis`
--

INSERT INTO `diagnosis` (`id`, `code`, `name`, `createdOn`) VALUES
(1, 'K29.70', 'Gastritis', '2021-05-30 15:24:45');

-- --------------------------------------------------------

--
-- Table structure for table `event_type`
--

CREATE TABLE `event_type` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `event_type`
--

INSERT INTO `event_type` (`id`, `code`, `name`, `createdOn`) VALUES
(1, 'EV123', 'Surgery', '2021-05-30 15:04:18'),
(2, 'EV321', 'Endoscopy', '2021-05-30 15:04:30');

-- --------------------------------------------------------

--
-- Table structure for table `facility`
--

CREATE TABLE `facility` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `address` varchar(256) NOT NULL,
  `schedulingOfficePhone` varchar(32) NOT NULL,
  `fax` varchar(32) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `facility`
--

INSERT INTO `facility` (`id`, `code`, `name`, `address`, `schedulingOfficePhone`, `fax`, `createdOn`) VALUES
(1, 'HSP123', 'Saint Terez', 'Hadat Beirut', '1234567890', '0987654321', '2021-05-30 13:34:56');

-- --------------------------------------------------------

--
-- Table structure for table `insurance`
--

CREATE TABLE `insurance` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `insurance`
--

INSERT INTO `insurance` (`id`, `code`, `name`, `createdOn`) VALUES
(1, 'INS124', 'Medicare', '2021-05-30 15:09:40');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `address1` varchar(256) NOT NULL,
  `address2` varchar(256) NOT NULL,
  `city` varchar(32) NOT NULL,
  `state` varchar(32) NOT NULL,
  `zipcode` varchar(32) NOT NULL,
  `dateofbirth` date NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `name`, `address1`, `address2`, `city`, `state`, `zipcode`, `dateofbirth`, `createdOn`) VALUES
(1, 'Amelia Watson', 'Main Street 123', 'Bld 123', 'Detroit', 'Michigan', '12345678', '1995-05-24', '2021-05-28 16:12:35'),
(2, 'John Doe', 'Add1', 'Add2', 'Boston', 'Washington', '12234556', '1990-02-13', '2021-05-29 10:27:44'),
(3, 'John Doe', 'Add1', 'Add2', 'Boston', 'Washington', '12234556', '2021-04-29', '2021-05-29 10:29:38');

-- --------------------------------------------------------

--
-- Table structure for table `patient_allergy`
--

CREATE TABLE `patient_allergy` (
  `id` int(11) NOT NULL,
  `patientId` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `status` varchar(128) NOT NULL,
  `allergyDate` datetime NOT NULL,
  `reaction` varchar(512) NOT NULL,
  `rxNormId` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient_allergy`
--

INSERT INTO `patient_allergy` (`id`, `patientId`, `name`, `status`, `allergyDate`, `reaction`, `rxNormId`) VALUES
(1, 1, 'Peanut Allergy', 'active', '2020-03-29 00:00:00', 'Swelling', 'RX123'),
(2, 1, 'Milk Allergy', 'active', '2017-11-06 00:00:00', 'Fever', 'RX321'),
(3, 1, 'Adrenaline Allergy', 'serious', '2018-03-26 00:00:00', 'Low blood pressure, light headedness', 'RX456');

-- --------------------------------------------------------

--
-- Table structure for table `priority`
--

CREATE TABLE `priority` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp(),
  `priority` int(11) NOT NULL DEFAULT 3
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `priority`
--

INSERT INTO `priority` (`id`, `code`, `name`, `createdOn`, `priority`) VALUES
(1, 'PR123', 'High', '2021-05-30 15:33:24', 3);

-- --------------------------------------------------------

--
-- Table structure for table `procedures`
--

CREATE TABLE `procedures` (
  `id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `name` varchar(256) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `procedures`
--

INSERT INTO `procedures` (`id`, `code`, `name`, `createdOn`) VALUES
(1, '45378', 'Colonoscopy', '2021-05-30 15:15:19');

-- --------------------------------------------------------

--
-- Table structure for table `refresh`
--

CREATE TABLE `refresh` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `token` varchar(512) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `refresh`
--

INSERT INTO `refresh` (`id`, `userId`, `token`, `createdOn`) VALUES
(1, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjMsImlhdCI6MTYyMjAwNzgzOSwiZXhwIjoxNjIyMDExNDM5fQ.uqHq6TlmF0jwDwYu11OuZlD_7HDLpxtYfmqQ_SAv8LU', '2021-05-26 00:00:00'),
(2, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjMsImlhdCI6MTYyMjAwODI2MiwiZXhwIjoxNjIyMDExODYyfQ.y-yOudsgYF3Z53VbWSzR3xS9gVjsI7BxoaxNoZN446A', '2021-05-26 08:51:02'),
(3, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjMsImlhdCI6MTYyMjAwODI2NCwiZXhwIjoxNjIyMDExODY0fQ.xp06691NZ3URFjel01ztNZgci4oyh1xcYBR6Y4RPZDU', '2021-05-26 08:51:04'),
(4, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjMsImlhdCI6MTYyMjAwODI2NSwiZXhwIjoxNjIyMDExODY1fQ.yv65iJ3o-DZfb1BhtEIFsGYQtqitVrJjCjeTCV7ICXg', '2021-05-26 08:51:05'),
(5, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjMsImlhdCI6MTYyMjAwODI2NiwiZXhwIjoxNjIyMDExODY2fQ.dls5MYfp-rblqs7HqiiVii-jY7eSTgg6kUtCQaL8m1I', '2021-05-26 08:51:06'),
(6, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjMsImlhdCI6MTYyMjAwODI3NSwiZXhwIjoxNjIyMDExODc1fQ.9zevMJB7N3MYeBiAAlL4Uz_mL9wyihAze6wqQKKemK0', '2021-05-26 08:51:15'),
(7, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjMsImlhdCI6MTYyMjAwODM0NywiZXhwIjoxNjIyMDExOTQ3fQ.eXa-ApUEPT1rOu6IXDneU7Dm-CkpRDkEV0iE6FwkpTk', '2021-05-26 08:52:27'),
(8, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOjMsImlhdCI6MTYyMjAxMTYzNywiZXhwIjoxNjI0NjAzNjM3fQ.gEAYAuEfSyLjGhhRuYWgmyUaRGdiZfklu0J_ZdY0720', '2021-05-26 09:47:17'),
(9, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjAxMjE1NSwiZXhwIjoxNjI0NjA0MTU1fQ.a6Bn8QROxrth-TDNg75WtrjEVhJA_bfcbarNTr5VPUQ', '2021-05-26 09:55:55'),
(10, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjAxMjI4MCwiZXhwIjoxNjI0NjA0MjgwfQ.8hOVSMHU4IDJO_UEagMhgiJstkiH20os6e3xNsHrtwI', '2021-05-26 09:58:00'),
(11, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjAyMjE1MiwiZXhwIjoxNjI0NjE0MTUyfQ.dO_0cBpzMsq1Tfz_TWXMcYVXvP3ABDebCZ797RkLkr4', '2021-05-26 12:42:32'),
(12, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjAyMjE2NSwiZXhwIjoxNjI0NjE0MTY1fQ.1InFvmLgAZAo2vixn4grviayz61axjoEbqVvJnouyFA', '2021-05-26 12:42:45'),
(13, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjAyMjE2NiwiZXhwIjoxNjI0NjE0MTY2fQ.yt7fLtlx0ZrWgkiTDEg-rAYfFOBWJJ8vp9biYdDdrDc', '2021-05-26 12:42:46'),
(14, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjAyMjE4NiwiZXhwIjoxNjI0NjE0MTg2fQ.C8grQ_zBS7hKlz0Zh4TJeksZ6Yg0FqSH_fzQPFLroNM', '2021-05-26 12:43:06'),
(15, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjAyMjE4NywiZXhwIjoxNjI0NjE0MTg3fQ.yxQtowkPIbilRGylPifk9SNkHXYYwk3JU6dqlTmQ_WE', '2021-05-26 12:43:07'),
(16, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjAyMjI3NywiZXhwIjoxNjI0NjE0Mjc3fQ.772yD4WH_QXJxrrVmQj3iSgzQOwwhjGz9s2eto0-UEk', '2021-05-26 12:44:37'),
(17, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjAyMjUxMiwiZXhwIjoxNjI0NjE0NTEyfQ.3DeTOOb99y_Fpz6T8YLytdIQSSwkdxOfCC8vHlO_iPg', '2021-05-26 12:48:32'),
(18, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjAyMjU3OSwiZXhwIjoxNjI0NjE0NTc5fQ.24U-ijNRW57Kk6gUERacrnCxeh36RB2evCWv5wi9ENA', '2021-05-26 12:49:39'),
(19, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjAyMjY1MywiZXhwIjoxNjI0NjE0NjUzfQ.J1KRfZuH93_fV-EH3V9MYz0qmVKvs6LkevlSfVWoEZ8', '2021-05-26 12:50:53'),
(20, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjAyMjY3MCwiZXhwIjoxNjI0NjE0NjcwfQ.4tBuDX3MIwwLq-JeVkSIchS-6mMgvhmMJjOlWtlo18g', '2021-05-26 12:51:10'),
(21, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjAzNjUzMywiZXhwIjoxNjI0NjI4NTMzfQ.-tbu6cI4RFWhhePRA5SVW9ZEm3rPT7oVav13UteO0zc', '2021-05-26 16:42:13'),
(22, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjAzNjU5MCwiZXhwIjoxNjI0NjI4NTkwfQ.pPgTSBiTrA3c6mzL-saCTFJUmLR_5jZUZFRODX9LwWs', '2021-05-26 16:43:10'),
(23, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjAzNjYxMSwiZXhwIjoxNjI0NjI4NjExfQ.jmQC5Y1lQFYGCPrBjRE8DfcrCFzORDmWX8zwT_DN9Eo', '2021-05-26 16:43:31'),
(24, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjA5NDk3NSwiZXhwIjoxNjI0Njg2OTc1fQ.YZzGKtOUI8mbrG1BLX6XrMKseSeDVPniifknYdHSOUE', '2021-05-27 08:56:15'),
(25, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjA5NTY5NywiZXhwIjoxNjI0Njg3Njk3fQ.gw1eP3JIyPC7BDhOCiD1i5HJjJ7sUOfYIVMiQW89BxU', '2021-05-27 09:08:17'),
(26, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjA5NjQyNCwiZXhwIjoxNjI0Njg4NDI0fQ.6K-UGRqUqDBx0U3fGCnHFJ5bOiSUalhX_UoCTyuwP1c', '2021-05-27 09:20:24'),
(27, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjA5NjU4MiwiZXhwIjoxNjI0Njg4NTgyfQ.YFNaRs793Rla-5sdilcVjxs2pj-0gOalkK7TLOZX_pQ', '2021-05-27 09:23:02'),
(28, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjA5Njk5MywiZXhwIjoxNjI0Njg4OTkzfQ.hOoZJ8vmOYOidl3bb1H16bi0W4JXLEyjP8-11v6HLJo', '2021-05-27 09:29:53'),
(29, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjA5NzExMSwiZXhwIjoxNjI0Njg5MTExfQ.JYQzOV65azMpWUKIgEzzirvm2Rl4aSlWGzz2kjwPCwc', '2021-05-27 09:31:51'),
(30, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjA5OTg2MCwiZXhwIjoxNjI0NjkxODYwfQ.vnFOY9yt-SlYsblidhdcv1wpDz0FHrF17m_d9j8d-j8', '2021-05-27 10:17:40'),
(31, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDA1NCwiZXhwIjoxNjI0NjkyMDU0fQ.Cu4m5oL1t4hj7L0NCNgfUUgL3njbe_ZjYStWOWIqDqo', '2021-05-27 10:20:54'),
(32, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDA1NywiZXhwIjoxNjI0NjkyMDU3fQ.HYsO7kVqzsDiJCXroqUS7z76rp_aRjDDHZcLVWBaNNs', '2021-05-27 10:20:57'),
(33, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDA5OSwiZXhwIjoxNjI0NjkyMDk5fQ.fyk9s8Z-h9We7-MvQ5uu-JnKNqynWKA7BWRQiOFJAnU', '2021-05-27 10:21:39'),
(34, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE0NiwiZXhwIjoxNjI0NjkyMTQ2fQ.LN0QiI8R_l6zxR1gZKvEIwjcZsfDu5UlxcsQ_le2tXI', '2021-05-27 10:22:26'),
(35, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE1MywiZXhwIjoxNjI0NjkyMTUzfQ.ZSAnN2ryX_TeI1z4JIKPiIJ1-_zn01n_EpI5pF7EVDk', '2021-05-27 10:22:33'),
(36, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE2MywiZXhwIjoxNjI0NjkyMTYzfQ.i3SUdlUBBMAVLWD-gAuLFvRh7DpXkMq_OyY7ezlcm3s', '2021-05-27 10:22:43'),
(37, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE3MiwiZXhwIjoxNjI0NjkyMTcyfQ.ySVBEbmZ3eF8OTuQaghVRc8DUY6p3j19vv3JE6bZb7A', '2021-05-27 10:22:52'),
(38, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDE5NywiZXhwIjoxNjI0NjkyMTk3fQ.u6uE22cxGuSWbQS-yPQUcbk1MWxU9oitgvmDHLH47Nw', '2021-05-27 10:23:17'),
(39, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDIzMiwiZXhwIjoxNjI0NjkyMjMyfQ.utGNd9y7tPLwL4YhIlwZ7ky6h4y63dmEuyNC8tJx18o', '2021-05-27 10:23:52'),
(40, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDI2MiwiZXhwIjoxNjI0NjkyMjYyfQ.A7mqDlOS5wu67QRPhSkVCVL8tl4n4tOnF39FKrQTDuc', '2021-05-27 10:24:22'),
(41, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDQyOCwiZXhwIjoxNjI0NjkyNDI4fQ.PFKbOpoqWJZ_z1QvrZhfbV2KkPxVS3nC7beEiy0MyOc', '2021-05-27 10:27:08'),
(42, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDQ2NCwiZXhwIjoxNjI0NjkyNDY0fQ.13V9fVCeZWiEqI6oJIR71Mr3naNc0EWBf1HrBrQc9Vw', '2021-05-27 10:27:44'),
(43, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDU4MywiZXhwIjoxNjI0NjkyNTgzfQ.CJfOyeK2zufVl8EZoS1x8XiLeXHmaEFWjhQytRDKmBY', '2021-05-27 10:29:43'),
(44, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDY4MywiZXhwIjoxNjI0NjkyNjgzfQ.o3S7gTjdWz-gJ0tk63NlwZpVr7e2nxuaosfO7gHkFIk', '2021-05-27 10:31:23'),
(45, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDY5MSwiZXhwIjoxNjI0NjkyNjkxfQ.uNw_JkfyP0siAJ3os1iW8-Tcwu0-D91jX3CAkFG5PSI', '2021-05-27 10:31:31'),
(46, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDc4NywiZXhwIjoxNjI0NjkyNzg3fQ.o0nynrypSLFMYlcL7fw5lPRJ3MaLHtqxW2IbfxwZafk', '2021-05-27 10:33:07'),
(47, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMDkwNywiZXhwIjoxNjI0NjkyOTA3fQ.geqnrMMYSrBvwOuEhYQ85pojgGBm9Lh7NnSwmEfcNp8', '2021-05-27 10:35:07'),
(48, 4, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjQsImlhdCI6MTYyMjEwMjA4OSwiZXhwIjoxNjI0Njk0MDg5fQ.Q_k9G9VL3v1EcWLopoWEcOCnrth_3OIM1XVM9hzllac', '2021-05-27 10:54:49'),
(49, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwMjEwOSwiZXhwIjoxNjI0Njk0MTA5fQ.U-tliARLLAIu82V5hv1DtW1npyuvsVw_2hZVHXrhad4', '2021-05-27 10:55:09'),
(50, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwMjE4MSwiZXhwIjoxNjI0Njk0MTgxfQ.8QOWQzEjHHh8PlJWnoIQTfkWifgJJ08ltH3a11WESXg', '2021-05-27 10:56:21'),
(51, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNDY4NCwiZXhwIjoxNjI0Njk2Njg0fQ.2keZu_ETg0sKd34jwKpQVvOhs5tggcdI_cvc9vyWDXo', '2021-05-27 11:38:04'),
(52, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNDc2NywiZXhwIjoxNjI0Njk2NzY3fQ.9lyeiFPzyO2rKGsKUz1T726BPhEEpfr60gDWBQ4SzhM', '2021-05-27 11:39:27'),
(53, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNDgxOSwiZXhwIjoxNjI0Njk2ODE5fQ.26YnSparvjVcdm7Ocea7W_Q36SG22iY_KFk8540uxyw', '2021-05-27 11:40:19'),
(54, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNDgzNiwiZXhwIjoxNjI0Njk2ODM2fQ.2l4M88BmizDZqwotU5MLfi4btzqSOi9poiItGAmltfQ', '2021-05-27 11:40:36'),
(55, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNDk4NiwiZXhwIjoxNjI0Njk2OTg2fQ.3NAuIKL4hA_bV2MqQT2p14JloTjcR0f9uLnplq6F56Y', '2021-05-27 11:43:06'),
(56, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNTAyOCwiZXhwIjoxNjI0Njk3MDI4fQ.TGLq8b7LKUUJjjNB-bbIZQ3b69m76vNCINxoJrWwgao', '2021-05-27 11:43:48'),
(57, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNTU0NCwiZXhwIjoxNjI0Njk3NTQ0fQ.nX8-BoiD7OwYsq6VAt4fWwW6RK0I4UjiNM7JLSSX2C8', '2021-05-27 11:52:24'),
(58, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNTc4MywiZXhwIjoxNjIyMTA1Nzk4fQ.NwHRdjN9lAa0YAAe2e0qy9pxGVgKTi-VSgJjHH6EMTE', '2021-05-27 11:56:23'),
(59, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNTkwMiwiZXhwIjoxNjIyMTA1OTE3fQ.nX2MY08AJ6KHLPNSqA9NzWWf7URsyxWiJvIactDL--c', '2021-05-27 11:58:22'),
(60, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNjA5NCwiZXhwIjoxNjIyMTA2MTA5fQ.IXfQOrjD2Us_SnUZiUspasHNsYyCnyeHBVRKc5aXhDw', '2021-05-27 12:01:34'),
(61, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNjI0NiwiZXhwIjoxNjIyMTA2MjYxfQ.1C6yTHkzy0545EJmQjUGhaD0GjqBPWsB8X4n3BreFQA', '2021-05-27 12:04:06'),
(62, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNjM3MiwiZXhwIjoxNjIyMTA2Mzg3fQ.BilxxFSWqwpY6U8Pt1mnYgbdCCNDocQFOwzXZ0IaGF4', '2021-05-27 12:06:12'),
(63, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNjQ1OSwiZXhwIjoxNjIyMTA2NDc0fQ.OzDmDpbMwBcv_NFpakoXLdAgS7ObTZajiHHDN69DAM0', '2021-05-27 12:07:39'),
(64, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNjU5NiwiZXhwIjoxNjIyMTA2NjExfQ.uRsOlzk4j63zRA9A5N8NFzqIZHEuoRQyC4vPi9YUms8', '2021-05-27 12:09:56'),
(65, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNjg5OSwiZXhwIjoxNjIyMTA2OTE0fQ.N19NBcWNPm1u2msnIakuDuE3X2fbnTiwv9WOoQ67eb8', '2021-05-27 12:14:59'),
(66, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNzEyMywiZXhwIjoxNjIyMTA3MTM4fQ.I7TNVrXKugXnbOheSP1dNljg2h48wyZQ-rPWPBO_sBk', '2021-05-27 12:18:43'),
(67, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNzM5NywiZXhwIjoxNjIyMTA3NDEyfQ.CWpE1hp9P2HuNUPBE-y0wdPqaNSlwaUzGg5S9X6Vw5M', '2021-05-27 12:23:17'),
(68, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNzQ0OCwiZXhwIjoxNjIyMTA3NDYzfQ.QYd_hSjIn_INw_KGeBJ5Z18Zh86tWNB3jFTrdH2UU_k', '2021-05-27 12:24:08'),
(69, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNzc3NywiZXhwIjoxNjIyMTA3NzkyfQ.ewFb-kADWIdkL1tV3ofowvqAx0jMbdzxWlif2tbxO84', '2021-05-27 12:29:37'),
(70, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwNzg5MiwiZXhwIjoxNjIyMTA3OTA3fQ.4VeynTmc04dnrYU2TXt9RHjWxI12RDIfy45GFhlI1dg', '2021-05-27 12:31:32'),
(71, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwODEzNCwiZXhwIjoxNjIyMTA4MTQ5fQ.uV5fgoVxK2Z3aXw4TsefoXiSGjUfbs9xxua9sxTOM98', '2021-05-27 12:35:34'),
(72, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwODIwOCwiZXhwIjoxNjIyMTA4MjIzfQ.w2oS--WnobobsuF2-bQWCgg1liSSQeblCgZEwl0AIxQ', '2021-05-27 12:36:48'),
(73, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwODQwNywiZXhwIjoxNjIyMTA4NDIyfQ.4Xn3Y6VaZjW_Qu1geZtVYhJG8Ov8WiGYw4ZGyLSyTeo', '2021-05-27 12:40:07'),
(74, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwODQzOCwiZXhwIjoxNjIyMTA4NDUzfQ.7lCxkcknEE_Ko1AKHUwcM64_UPyTeeb8GwsZT5MG9fI', '2021-05-27 12:40:38'),
(75, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwODc0MywiZXhwIjoxNjIyMTA4NzUzfQ.FeI0N9warbPwG-hGtr0TwIj7VlSbHuCfTPiMeLnX6uI', '2021-05-27 12:45:43'),
(76, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjEwODc3MCwiZXhwIjoxNjIyMTk1MTcwfQ.1bCfqsP4qAwMAKbxFKhFrlzapjEnFYxM1D600b3RI2o', '2021-05-27 12:46:10'),
(77, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjExMDAxMCwiZXhwIjoxNjIyMTk2NDEwfQ.Z6kjjYRUviEar8hBpiuNthGnEOrcW4_pCAGrPi-xybQ', '2021-05-27 13:06:50'),
(78, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjExMDAzMiwiZXhwIjoxNjIyMTk2NDMyfQ.gHvCDj6df2MxUZxJrfgSLxXkE9HCk3ukeUM3Tx_Xddw', '2021-05-27 13:07:12'),
(79, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjExMDEzNywiZXhwIjoxNjIyMTk2NTM3fQ.498k1h3pTQY4kq6pDAPMHNfSleigXPgWp4sgInCzbZ4', '2021-05-27 13:08:57'),
(80, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjE5NjMxNCwiZXhwIjoxNjIyMjgyNzE0fQ.Q1tk_SSN_z1mj9pOd1LfGqymysfq1H6zjrIrlANZn-4', '2021-05-28 13:05:14'),
(81, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjIwNTIzNSwiZXhwIjoxNjIyMjkxNjM1fQ.NgdwzOYqePrcMZXOG9I5xb_0sdQYHvzc-33jLULGbL4', '2021-05-28 15:33:55'),
(82, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjI4OTYyMCwiZXhwIjoxNjIyMzc2MDIwfQ.Kf3h2FM6XLkEQC-XGvBTCf0QCjXil4JfTjLEJjhTyls', '2021-05-29 15:00:20'),
(83, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjI5NTIyMSwiZXhwIjoxNjIyMzgxNjIxfQ._ubrLuOOxiOCK5UQBYTjRylOVKYGe52rrD5L_zEkapE', '2021-05-29 16:33:41'),
(84, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjM3NjA3MiwiZXhwIjoxNjIyNDYyNDcyfQ.dzhH_5n7pdiROG6KU23em7F577eiiCry05aSUma8oyI', '2021-05-30 15:01:12'),
(85, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjQ1NjU1OCwiZXhwIjoxNjIyNTQyOTU4fQ.5qmod5fZ2yr72X7hRGtsxHBi29EVF0XR4oR383-eGtw', '2021-05-31 13:22:38'),
(86, 3, 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjMsImlhdCI6MTYyMjQ1NjYxOCwiZXhwIjoxNjIyNTQzMDE4fQ.VRZIO3wZhIiVOrfV-KY7u1NrHtt2QMuFEBz1PRv4LfU', '2021-05-31 13:23:38');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `phone` varchar(32) DEFAULT NULL,
  `password` varchar(256) NOT NULL,
  `avatar` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `phone`, `password`, `avatar`) VALUES
(1, 'user1@mail.com', '1234567890', '$2b$10$yFc7Sg7WNxrC5t5cUAs5JupzNdWj/MrYHz8L1/MFX/zBncdsfwiga', NULL),
(2, 'user2@mail.com', '1234567891', '$2b$10$8MtPf2IXeFXOcHbQRcQyUumOjfZ3n/ugtrID6SWjlZwmTNqCZqcN.', NULL),
(3, 'user3@mail.com', '1234567897', '$2b$10$VeFW/IJe8LxoiRPDXVzWMOwdYqUx0.Tsq0Tkxkf8kJKv/INue7eoa', NULL),
(4, 'user4@mail.com', '1234567894', '$2b$10$btFaK1cJrdN9.cI3clA9W.MKg1UGPrzIsFkjzFB6p1fPGHxs1FSLC', NULL),
(5, 'user8@mail.com', '70123456', '$2b$10$gMwjr2X1hVxwb5HEpK.nGONilhP6tLv7eoYQsPf3w134P5AjlIiV6', NULL),
(6, 'user9@mail.com', '70123457', '$2b$10$NVfMcQIHto.ScESG.z4BFuQtzs91i.gTKI0LchTnp/6/bbnuA1o3W', NULL),
(7, 'user10@mail.com', '1234567899', '$2b$10$GPY8E9ngLoIUK5n1HQEBfutnFah0ko5s.hK3oYjReobFiNaezwHhW', NULL),
(8, 'user11@mail.com', '70123452', '$2b$10$K7fpE1Bz/rvh6hlxE5SegO74Er.liaTwGLcXo3d0oObmCv8aYsk2a', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  `role` varchar(32) NOT NULL,
  `createdOn` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `userId`, `role`, `createdOn`) VALUES
(24, 3, 'coordinator', '2021-05-31 15:37:36'),
(25, 3, 'admin', '2021-05-31 15:37:36'),
(26, 7, 'coordinator', '2021-05-31 16:29:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blood_thinner`
--
ALTER TABLE `blood_thinner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diagnosis`
--
ALTER TABLE `diagnosis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_type`
--
ALTER TABLE `event_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facility`
--
ALTER TABLE `facility`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `insurance`
--
ALTER TABLE `insurance`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_allergy`
--
ALTER TABLE `patient_allergy`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patientId` (`patientId`);

--
-- Indexes for table `priority`
--
ALTER TABLE `priority`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `procedures`
--
ALTER TABLE `procedures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `refresh`
--
ALTER TABLE `refresh`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`),
  ADD KEY `token` (`token`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blood_thinner`
--
ALTER TABLE `blood_thinner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `diagnosis`
--
ALTER TABLE `diagnosis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `event_type`
--
ALTER TABLE `event_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `facility`
--
ALTER TABLE `facility`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `insurance`
--
ALTER TABLE `insurance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `patient_allergy`
--
ALTER TABLE `patient_allergy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `priority`
--
ALTER TABLE `priority`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `procedures`
--
ALTER TABLE `procedures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `refresh`
--
ALTER TABLE `refresh`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `patient_allergy`
--
ALTER TABLE `patient_allergy`
  ADD CONSTRAINT `patient_allergy_ibfk_1` FOREIGN KEY (`patientId`) REFERENCES `patient` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
