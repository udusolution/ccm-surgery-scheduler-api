/**
 * @description data-access with DB for user data
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');
const Enums = require('../common/enums');


/**
 * add new user
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} email user email
 * @param {string} name user's name
 * @param {string} password user password (hashed)
 * @param {string} phone user phone number
 * @param {string} fax user fax number
 * @returns {Promise<number>} new user ID
 */
async function add(log, reqId, email, name, password, phone, fax) {

    const fnName = 'repos/user/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            email,
            name,
            password,
            phone,
            fax
        }

        // SQL
        const sql = `
            INSERT INTO user
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add user`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return result.insertId;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * update a user
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user's ID
 * @param {string?} email new email
 * @param {string?} name new name
 * @param {string?} phone new phone number
 * @param {string?} fax new fax number
 */
async function update(log, reqId, userId, { email, name, phone, fax, officePhone, officeAddress, color }) {

    const fnName = 'repos/user/update';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {};
        if(email) params.email = email;
        if(name) params.name = name;
        if(phone) params.phone = phone;
        if(fax) params.fax = fax;
        if(color) params.color = color;
        if(officePhone !== null && officePhone !== undefined) params.officePhone = officePhone;
        if(officeAddress !== null && officeAddress !== undefined) params.officeAddress = officeAddress;

        // SQL
        const sql = `
            UPDATE user
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update user`);
        // -> Query
        await con.query(sql, [params, userId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}

/**
 * update a user
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user's ID
 * @param {string?} email new email
 * @param {string?} name new name
 * @param {string?} phone new phone number
 * @param {string?} fax new fax number
 */
async function updateFavoriteEventType(log, reqId, userId, favoriteEventTypeId) {

    const fnName = 'repos/user/updateFavoriteEventType';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            favoriteEventTypeId: !favoriteEventTypeId || favoriteEventTypeId == 0 ? null : favoriteEventTypeId
        };

        // SQL
        const sql = `
            UPDATE user
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update user favorite event type`);
        // -> Query
        await con.query(sql, [params, userId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * update a user's roles
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user's ID
 * @param {string[]} roles user roles
 */
async function updateRoles(log, reqId, userId, roles) {

    const fnName = 'repos/user/updateRoles';
    log.info(reqId, `${fnName} ### start`);

    try {


        log.info(reqId, `${fnName} ### [query] delete user current roles`);
        // -> Delete User Current Roles
        await con.query(`DELETE FROM user_role WHERE userId = ?`, userId);

        // Gather New Role Insert Promises
        let promises = [];
        for(let i = 0; i < roles.length; i++) {
            const role = roles[i];
            promises.push(con.query(`INSERT INTO user_role SET ?`, [{userId, role}]));
        }

        log.info(reqId, `${fnName} ### [query] add user roles`);
        // -> Insert User Roles
        if(promises.length) await Promise.all(promises);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * find a user by his ID
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user's ID
 * @returns {Promise<any>} found user
 */
async function findById(log, reqId, userId) {

    const fnName = 'repos/user/findById';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT u.*
            FROM user as u
            WHERE u.id = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get user`);
        // -> Query
        const [users] = await con.query(sql, [userId]);

        if(users.length) {
            
            // End
            log.info(reqId, `${fnName} ### end [success]`);
            return users[0];
        }
        else {
            
            // Null
            log.info(reqId, `${fnName} ### end [no data]`);
            return null;
        }
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * find a user by his email
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} email user's email
 * @returns {Promise<any>} found user
 */
async function findByEmail(log, reqId, email) {

    const fnName = 'repos/user/findByEmail';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT u.*
            FROM user as u
            WHERE u.email = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get user`);
        // -> Query
        const [users] = await con.query(sql, [email]);

        if(users.length) {
            
            // End
            log.info(reqId, `${fnName} ### end [success]`);
            return users[0];
        }
        else {
            
            // Null
            log.info(reqId, `${fnName} ### end [no data]`);
            return null;
        }
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}

/**
 * find a user by his forgot password token
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} token user's forgot password token
 * @returns {Promise<any>} found user
 */
async function findByForgotPasswordToken(log, reqId, token) {

    const fnName = 'repos/user/findByForgotPasswordToken';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM user
            WHERE forgotPasswordToken = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get user`);
        // -> Query
        const [users] = await con.query(sql, [token]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return users[0];
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * check if an email is already taken by a user
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} email email to check
 * @returns {Promise<boolean>} whether the email is taken or not
 */
async function isEmailTaken(log, reqId, email) {

    const fnName = 'repos/user/isEmailTaken';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM user as u
            WHERE u.email = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get user with email`);
        // -> Query
        const [users] = await con.query(sql, [email]);

        if(users && users[0]) {
            
            // End
            log.info(reqId, `${fnName} ### end [taken]`);
            return true;
        }
        else {
            
            // Not Taken
            log.info(reqId, `${fnName} ### end [not taken]`);
            return false;
        }
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * check if a phone number is already taken by a user
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} phone phone to check
 * @returns {Promise<boolean>} whether the phone is taken or not
 */
async function isPhoneTaken(log, reqId, phone) {

    const fnName = 'repos/user/isPhoneTaken';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM user as u
            WHERE u.phone = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get user with phone`);
        // -> Query
        const [users] = await con.query(sql, [phone]);

        if(users.length && users[0]) {
            
            // End
            log.info(reqId, `${fnName} ### end [taken]`);
            return true;
        }
        else {
            
            // Not Taken
            log.info(reqId, `${fnName} ### end [not taken]`);
            return false;
        }
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get a user's details and roles
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user's ID
 * @returns {Promise<{user: any, roles: string[]}>} user's details and roles
 */
async function getDetails(log, reqId, userId) {

    const fnName = 'repos/user/getDetails';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT u.*, ur.role
            FROM user AS u
            LEFT JOIN user_role AS ur ON ur.userId = u.id
            WHERE u.id = ?
        `;

        log.info(reqId, `${fnName} ### [query] get user with details`);
        // -> Query
        const [users] = await con.query(sql, [userId]);

        if(users.length) {

            // Format result
            const result = {
                user: users[0],
                roles: users[0].role ? users.map(u => u.role) : []
            }
            
            // End
            log.info(reqId, `${fnName} ### end [success]`);
            return result;
        }
        else {
            
            // Null
            log.info(reqId, `${fnName} ### end [no data]`);
            return null;
        }
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * delete a user
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user's ID
 */
async function deleteUser(log, reqId, userId) {

    const fnName = 'repos/user/deleteUser';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            UPDATE user
            SET isDeleted = 1
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete user`);
        // -> Query
        await con.query(sql, [userId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * delete a user's scheduled productivity reports
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user's ID
 */
async function deleteUserScheduledProductivityReports(log, reqId, userId) {

    const fnName = 'repos/user/deleteUserScheduledProductivityReports';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM productivity_report
            WHERE coordinatorId = ?
            OR physicianId = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete user scheduled productivity reports`);
        // -> Query
        await con.query(sql, [userId, userId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * search users [paginated]
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} searchCol column to search in 
 * @param {string} searchQuery column search query
 * @param {string} sortCol column to sort by
 * @param {boolean} sortIsAsc whether the sorting is ascending (if not the descending)
 * @param {number} skip entries to skip
 * @param {number} limit maximum entries to fetch (page size)
 * @returns {Promise<any[]>} found users
 */
async function search(log, reqId, { searchCol, searchQuery, sortCol, sortIsAsc, skip = 0, limit }) {

    const fnName = 'repos/users/search';
    log.info(reqId, `${fnName} ### start`);

    try {

        const params = [];
        const whereClauses = [];

        if(searchCol) {
            whereClauses.push(`u.${searchCol} LIKE ?`);
            params.push(`%${searchQuery}%`);
        }

        // SQL
        const sql = `
            SELECT u.*, (SELECT GROUP_CONCAT(role) FROM user_role WHERE userId = u.id GROUP BY userId) AS user_roles
            FROM user AS u
            WHERE u.isDeleted = 0
            ${searchCol ? `AND ${whereClauses.join(' AND ')}` : ''}
            ${sortCol && sortIsAsc != null ? `ORDER BY ${sortCol} ${sortIsAsc == true ? 'ASC' : 'DESC'}` : ''}
            ${!sortCol ? `
                ORDER BY 
                CASE
                    WHEN user_roles LIKE "%${Enums.UserRoles.Admin}" THEN 1
                    WHEN user_roles LIKE "%${Enums.UserRoles.Physician}" THEN 2
                    WHEN user_roles LIKE "%${Enums.UserRoles.Coordinator}" THEN 3
                    ELSE 4
                END
            ` : ''}
            LIMIT ${skip},${limit}
        `;

        log.info(reqId, `${fnName} ### [query] search users`);
        // -> Query
        const [result] = await con.query(sql, [...params]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return result;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * number of users that match a search query
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} searchCol column to search in 
 * @param {string} searchQuery column search query
 * @returns {Promise<number>} number of users that match the search query
 */
async function searchCount(log, reqId, { searchCol, searchQuery }) {

    const fnName = 'repos/users/searchCount';
    log.info(reqId, `${fnName} ### start`);

    try {

        const params = [];
        const whereClauses = [];

        if(searchCol) {
            whereClauses.push([`${searchCol} LIKE ?`]);
            params.push(`%${searchQuery}%`);
        }

        // SQL
        const sql = `
            SELECT COUNT(*) as count
            FROM user
            WHERE isDeleted = 0
            ${searchCol ? `AND ${whereClauses.join(' AND ')}` : ''}
        `;

        log.info(reqId, `${fnName} ### [query] get matching users count`);
        // -> Query
        const [result] = await con.query(sql, [...params]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return result[0].count;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * search users having role [paginated]
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} searchCol column to search in 
 * @param {string} searchQuery column search query
 * @param {string} sortCol column to sort by
 * @param {boolean} sortIsAsc whether the sorting is ascending (if not the descending)
 * @param {number} skip entries to skip
 * @param {number} limit maximum entries to fetch (page size)
 * @param {string} role role to filter by that a user must have
 * @returns {Promise<any[]>} found users
 */
async function searchWithRole(log, reqId, { searchCol, searchQuery, sortCol, sortIsAsc, skip = 0, limit, role }) {

    const fnName = 'repos/users/searchWithRole';
    log.info(reqId, `${fnName} ### start`);

    try {

        const params = [];
        const whereClauses = [];

        if(searchCol) {
            whereClauses.push(`u.${searchCol} LIKE ?`);
            params.push(`%${searchQuery}%`);
        }

        if(role) {
            whereClauses.push(`ur.role = ?`);
            params.push(role);
        }

        // SQL
        const sql = `
            SELECT u.*, (SELECT GROUP_CONCAT(role) FROM user_role WHERE userId = u.id GROUP BY userId) AS user_roles
            FROM user AS u
            LEFT JOIN user_role AS ur ON ur.userId = u.id
            WHERE u.isDeleted = 0
            ${whereClauses.length > 0 ? `AND ${whereClauses.join(' AND ')}` : ''}
            GROUP BY u.id
            ${sortCol && sortIsAsc != null ? `ORDER BY ${sortCol} ${sortIsAsc == true ? 'ASC' : 'DESC'}` : ''}
            LIMIT ${skip},${limit}
        `;

        log.info(reqId, `${fnName} ### [query] search users`);
        // -> Query
        const [results] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * number of users (with role) that match a search query
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} searchCol column to search in 
 * @param {string} searchQuery column search query
 * @param {string} role role to filter by that a user must have
 * @returns {Promise<number>} number of users that match the search query
 */
async function searchWithRoleCount(log, reqId, { searchCol, searchQuery, role }) {

    const fnName = 'repos/users/searchWithRoleCount';
    log.info(reqId, `${fnName} ### start`);

    try {

        const params = [];
        const whereClauses = [];

        if(searchCol) {
            whereClauses.push(`u.${searchCol} LIKE ?`);
            params.push(`%${searchQuery}%`);
        }

        if(role) {
            whereClauses.push(`ur.role = ?`);
            params.push(role);
        }

        // SQL
        const sql = `
            SELECT COUNT(*) as count
            FROM user AS u
            ${role ? `JOIN user_role AS ur ON ur.userId = u.id` : ''}
            WHERE u.isDeleted = 0
            ${whereClauses.length > 0 ? `AND ${whereClauses.join(' AND ')}` : ''}
        `;

        log.info(reqId, `${fnName} ### [query] get matching users count`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return result[0].count;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * get all the users that have a given role
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} role role
 * @returns {Promise<any[]>} users with role
 */
async function getWithRole(log, reqId, role) {

    const fnName = 'repos/users/getWithRole';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT u.*
            FROM user AS u
            JOIN user_role AS ur ON ur.userId = u.id
            WHERE ur.role = ?
            AND u.isDeleted = 0
        `;

        log.info(reqId, `${fnName} ### [query] search users with role "${role}"`);
        // -> Query
        const [result] = await con.query(sql, role);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return result;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * get all the physicians with their details
 * @param {*} log logger
 * @param {string} reqId request ID
 * @returns {Promise<any[]>} physicians
 */
async function getPhysiciansWithDetails(log, reqId) {

    const fnName = 'repos/users/getPhysiciansWithDetails';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT u.*, ur.role, fp.facilityId AS facilityId, f.name AS facilityName
            FROM user AS u
            JOIN user_role AS ur ON ur.userId = u.id
            LEFT JOIN facility_physician AS fp ON fp.physicianId = u.id
            LEFT JOIN facility AS f ON fp.facilityId = f.id
            WHERE ur.role = "${Enums.UserRoles.Physician}"
            AND u.isDeleted = 0
        `;

        log.info(reqId, `${fnName} ### [query] get physicians with roles and associated facilities`);
        // -> Query
        const [results] = await con.query(sql);

        const final = [];
        for(let phy of results) {
            if(!final.some(f => f.id == phy.id)) {
                final.push({
                    ...phy,
                    roles: results.filter(r => r.id == phy.id)?.map(r => r.role) ?? [],
                    facilityAssociations: results.filter(r => r.id == phy.id)?.map(r => { return { facilityId: r.facilityId, facilityName: r.facilityName } }) ?? []
                })
            }
        }

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return final;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * get all users
 * @param {*} log logger
 * @param {string} reqId request ID
 * @returns {Promise<any[]>} users
 */
async function getAll(log, reqId) {

    const fnName = 'repos/users/getAll';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM user
            WHERE isDeleted = 0
        `;

        log.info(reqId, `${fnName} ### [query] get all users`);
        // -> Query
        const [results] = await con.query(sql);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}

/**
 * get all users with their roles
 * @param {*} log logger
 * @param {string} reqId request ID
 * @returns {Promise<any[]>} users
 */
 async function getAllWithRoles(log, reqId) {

    const fnName = 'repos/users/getAllWithRoles';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT u.* , GROUP_CONCAT(ur.role) as roles
            FROM user u
                     LEFT JOIN user_role ur
                               ON u.id = ur.userId
            WHERE u.isDeleted = 0
            GROUP BY u.id
        `;

        log.info(reqId, `${fnName} ### [query] get all users`);
        // -> Query
        const [results] = await con.query(sql);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results.map(u => {
            u.roles = u.roles?.split(',') || [];
            return u;
        });
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * get a user with their whitelist
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user ID
 */
 async function getWithWhitelist(log, reqId, userId) {

    const fnName = 'repos/users/getWithWhitelist';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT u.*, w.allowedId AS allowedId
            FROM user AS u
            LEFT JOIN event_edit_whitelist AS w ON w.allowerId = u.id
            WHERE u.id = ?
        `;

        log.info(reqId, `${fnName} ### [query] get user with whitelist`);
        // -> Query
        const [results] = await con.query(sql, [userId]);

        if(!results[0]) {
            log.info(reqId, `${fnName} ### end [success]`);
            return null;
        }
        const final = results[0];
        final.whitelist = results?.filter(r => r.allowedId)?.map(r => r.allowedId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return final;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
        
    }
}


/**
 * update a user's signature
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user's ID
 * @param {string} signature signature image file path
 */
 async function setSignature(log, reqId, userId, signature) {

    const fnName = 'repos/user/setSignature';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            signature
        };

        // SQL
        const sql = `
            UPDATE user
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update user signature`);
        // -> Query
        await con.query(sql, [params, userId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * change a user's password
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user's ID
 * @param {string} hashedNewPassword hashed new password
 */
 async function changePassword(log, reqId, userId, hashedNewPassword) {

    const fnName = 'repos/user/changePassword';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            password: hashedNewPassword
        };

        // SQL
        const sql = `
            UPDATE user
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update user password`);
        // -> Query
        await con.query(sql, [params, userId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * change a user's password reset token
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} email user's email
 * @param {string} forgotPasswordToken forgot password token
 */
 async function changePasswordResetToken(log, reqId, email, forgotPasswordToken) {

    const fnName = 'repos/user/changePasswordResetToken';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            forgotPasswordToken
        };

        // SQL
        const sql = `
            UPDATE user
            SET ?
            WHERE email = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] update user forgot password token`);
        // -> Query
        await con.query(sql, [params, email]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * lock a user's account
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user's ID
 */
async function lock(log, reqId, userId) {

    const fnName = 'repos/user/lock';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            UPDATE user
            SET isLocked = 1
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] lock the user's account`);
        // -> Query
        await con.query(sql, [userId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * unlock a user's account
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user's ID
 */
 async function unlock(log, reqId, userId) {

    const fnName = 'repos/user/unlock';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            UPDATE user
            SET isLocked = 0
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] unlock the user's account`);
        // -> Query
        await con.query(sql, [userId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}

/**
 * update a user's push notification subscription
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user's ID
 * @param {*} pnSub user's push notification subscription
 */
 async function updatePnSub(log, reqId, userId, pnSub) {

    const fnName = 'repos/user/updatePnSub';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = [
            JSON.stringify(pnSub),
            userId
        ]

        // SQL
        const sql = `
            UPDATE user
            SET pnSub = ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update the user's push notification subscription`);
        // -> Query
        await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    update,
    updateRoles,
    findById,
    findByEmail,
    isEmailTaken,
    isPhoneTaken,
    getDetails,
    search,
    searchCount,
    searchWithRole,
    searchWithRoleCount,
    getWithRole,
    getPhysiciansWithDetails,
    getAll,
    getAllWithRoles,
    updateFavoriteEventType,
    setSignature,
    changePassword,
    changePasswordResetToken,
    findByForgotPasswordToken,
    deleteUser,
    deleteUserScheduledProductivityReports,
    getWithWhitelist,
    lock,
    unlock,
    updatePnSub
}