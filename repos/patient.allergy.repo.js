/**
 * @description data-access with DB for patient allergy data
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add patient allergy
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number?} id ID of the allergy
 * @param {number} patientId patient ID
 * @param {string} name allergy name
 * @param {string} status allergy status
 * @param {string} allergyDate allergy date
 * @param {string} reaction allergy reaction
 * @param {string} rxNormId allergy rxNormId
 * @returns {Promise<any>} the newly created allergy
 */
async function add(log, reqId, { id = null, patientId, name, status, allergyDate, reaction, rxNormId }) {

    const fnName = 'repos/patient/allergy/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            patientId,
            name, 
            status, 
            allergyDate, 
            reaction, 
            rxNormId
        }
        if(id) params.id = id;

        // SQL
        const sql = `
            INSERT INTO patient_allergy
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add patient allergy`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all the patient's allergies
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} patientId patient ID
 * @returns {Promise<any[]>} array of patient's allergies
 */
async function getForPatient(log, reqId, patientId) {

    const fnName = 'repos/patient/allergy/getForPatient';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM patient_allergy
            WHERE patientId = ?
        `;

        log.info(reqId, `${fnName} ### [query] get patient allergies`);
        // -> Query
        const [allergies] = await con.query(sql, [patientId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return allergies;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * update a patient allergy column value
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} rowId allergy ID
 * @param {string} col column name
 * @param {*} val new column value
 */
async function updateCol(log, reqId, rowId, col, val) {

    const fnName = 'repos/patient/allergy/updateCol';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Update Params
        const updateParams = {};
        updateParams[col] = val;

        // SQL
        const sql = `
            UPDATE patient_allergy
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update patient allergy`);
        // -> Query
        await con.query(sql, [updateParams, rowId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * delete a patient allergy
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} rowId allergy ID
 */
async function deleteRow(log, reqId, rowId) {

    const fnName = 'repos/patient/allergy/deleteRow';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM patient_allergy
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete patient allergy`);
        // -> Query
        await con.query(sql, [rowId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    getForPatient,
    updateCol,
    deleteRow
}