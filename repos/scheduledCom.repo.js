/**
 * @description data-access with DB for scheduled communications (master files)
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add email scheduled communication
 * @param {*} log logger
 * @param {string} reqId request ID
 * @returns {Promise<any>} the newly created scheduled communication
 */
async function addEmailScheduledCommunication(log, reqId, { templateId, whenToSend, rule, forWhom }) {

    const fnName = 'repos/scheduledCom/addEmailScheduledCommunication';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            templateId,
            whenToSend
        }
        if(rule) params.rule = rule;
        if(forWhom) params.forWhom = forWhom;

        // SQL
        const sql = `
            INSERT INTO email_scheduled_com
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add email scheduled communications`);
        // -> Query
        const [result] = await con.query(sql, [params]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all email scheduled communication
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function getAllEmailScheduledCommunications(log, reqId) {

    const fnName = 'repos/scheduledCom/getAllEmailScheduledCommunications';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT 
                esc.*,
                e.name AS templateName
            FROM email_scheduled_com AS esc
            JOIN email AS e ON esc.templateId = e.id
        `;

        log.info(reqId, `${fnName} ### [query] get all email scheduled communications`);
        // -> Query
        const [results] = await con.query(sql);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * add sms scheduled communication
 * @param {*} log logger
 * @param {string} reqId request ID
 * @returns {Promise<any>} the newly created scheduled communication
 */
async function addSmsScheduledCommunication(log, reqId, { templateId, whenToSend, rule, forWhom }) {

    const fnName = 'repos/scheduledCom/addSmsScheduledCommunication';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            templateId,
            whenToSend
        }
        if(rule) params.rule = rule;
        if(forWhom) params.forWhom = forWhom;

        // SQL
        const sql = `
            INSERT INTO sms_scheduled_com
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add sms scheduled communications`);
        // -> Query
        const [result] = await con.query(sql, [params]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all sms scheduled communication
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function getAllSmsScheduledCommunications(log, reqId) {

    const fnName = 'repos/scheduledCom/getAllSmsScheduledCommunications';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT 
                ssc.*,
                s.name AS templateName
            FROM sms_scheduled_com AS ssc
            JOIN sms AS s ON ssc.templateId = s.id
        `;

        log.info(reqId, `${fnName} ### [query] get all sms scheduled communications`);
        // -> Query
        const [results] = await con.query(sql);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    addEmailScheduledCommunication,
    getAllEmailScheduledCommunications,
    addSmsScheduledCommunication,
    getAllSmsScheduledCommunications
}