/**
 * @description data-access with DB for event scheduled email communications
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add scheduled email communication to event
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function add(log, reqId, { eventId, templateId, templateName, templateSubject, templateBody, whenToSend, attachments, forWhom = 'patient' }) {

    const fnName = 'repos/event/scheduledCom/email/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            eventId, 
            templateId, 
            templateName,
            templateSubject,
            templateBody,
            whenToSend,
            forWhom
        }

        // SQL
        const sql = `
            INSERT INTO event_email_com
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add scheduled email communication to event`);
        // -> Query
        const [result] = await con.query(sql, params);

        // -> Add Attachments If Any
        if(attachments?.length > 0) {

            const attachmentParams = [
                ...attachments.map(a => [result.insertId, a])
            ]
    
            // SQL
            const sql = `
                INSERT INTO event_email_com_attachment (comId, file)
                VALUES ?
            `;
    
            log.info(reqId, `${fnName} ### [query] add attachments`);
            // -> Query
            await con.query(sql, [attachmentParams]);
        }

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * update scheduled event email communication
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function updateOne(log, reqId, { id, templateSubject, templateBody }) {

    const fnName = 'repos/event/scheduledCom/email/updateOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            templateSubject,
            templateBody
        }

        // SQL
        const sql = `
            UPDATE event_email_com
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update scheduled event email communication`);
        // -> Query
        await con.query(sql, [params, id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * update scheduled event email communication's "when to send"
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function updateWhenToSend(log, reqId, id, whenToSend) {

    const fnName = 'repos/event/scheduledCom/email/updateOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            whenToSend
        }

        // SQL
        const sql = `
            UPDATE event_email_com
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update scheduled event email communication's "when to send"`);
        // -> Query
        await con.query(sql, [params, id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * remove scheduled email communication from event
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function remove(log, reqId, id) {

    const fnName = 'repos/event/scheduledCom/email/remove';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM event_email_com
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] remove scheduled email communication from event`);
        // -> Query
        await con.query(sql, [id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * remove scheduled email communication attachment from the scheduled event email com
 * @param {*} log logger
 * @param {string} reqId request ID
 */
 async function removeAttachment(log, reqId, id) {

    const fnName = 'repos/event/scheduledCom/email/removeAttachment';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM event_email_com_attachment
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] remove scheduled email communication attachment from the scheduled event email com`);
        // -> Query
        await con.query(sql, [id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get an event's scheduled email communications and with their attachments
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function getForEvent(log, reqId, eventId, forPatientOnly = false) {

    const fnName = 'repos/event/scheduledCom/email/getForEvent';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT e.*, ea.file AS templateFile
            FROM event_email_com AS e
            LEFT JOIN event_email_com_attachment AS ea ON ea.comId = e.id
            WHERE e.eventId = ?
            ${forPatientOnly ? `AND e.forWhom = 'patient'` : ''}
            ORDER BY e.templateName ASC
        `;

        log.info(reqId, `${fnName} ### [query] get event email scheduled communications and with their attachments`);
        // -> Query
        const [results] = await con.query(sql, [eventId]);

        // Format the result
        const final = [];
        for(let i = 0; i < results.length; i++) {
            const com = results[i];

            // Check if final already has this template
            if(final.some(f => f.id == com.id)) continue;

            // Push this template to the final array, and include its attachments
            final.push({
                ...com,
                attachments: results.filter(t => (t.id == com.id) && t.templateFile).map(t => t.templateFile)
            })
        }

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return final;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get an event's scheduled email communications and with their attachments
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function getForEventWithFiles(log, reqId, eventId) {

    const fnName = 'repos/event/scheduledCom/email/getForEventWithFiles';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT e.*, ea.file AS templateFile, ea.id AS fileId
            FROM event_email_com AS e
            LEFT JOIN event_email_com_attachment AS ea ON ea.comId = e.id
            WHERE e.eventId = ?
        `;

        log.info(reqId, `${fnName} ### [query] get event email scheduled communications and with their attachments`);
        // -> Query
        const [results] = await con.query(sql, [eventId]);

        // Format the result
        const final = [];
        for(let i = 0; i < results.length; i++) {
            const com = results[i];

            // Check if final already has this template
            if(final.some(f => f.id == com.id)) continue;

            // Push this template to the final array, and include its attachments
            final.push({
                ...com,
                attachments: results
                    .filter(t => (t.id == com.id) && t.templateFile)
                    .map(t => { 
                        return { id: t.fileId, file: t.templateFile }
                    })
            })
        }

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return final;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get an event scheduled email com with its attachments
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id com id
 */
async function findById(log, reqId, id) {

    const fnName = 'repos/event/scheduledCom/email/findById';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT e.*, ea.id AS fileId, ea.file AS templateFile
            FROM event_email_com AS e
            LEFT JOIN event_email_com_attachment AS ea ON ea.comId = e.id
            WHERE e.id = ?
        `;

        log.info(reqId, `${fnName} ### [query] get an event scheduled email com with its attachments`);
        // -> Query
        const [results] = await con.query(sql, [id]);

        // Format the result
        let final = null;
        if(results[0]) {
            final = { ...results[0] };
            if(results.filter(t => t.templateFile).length > 0) {
                final.attachments = results
                    .filter(t => t.templateFile)
                    .map(t => {
                        return {
                            id: t.fileId,
                            file: t.templateFile
                        }
                    })
            }
        }

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return final;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * add email template attachments
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function addAttachments(log, reqId, rows) {

    const fnName = 'repos/event/scheduledCom/email/addAttachments';
    log.info(reqId, `${fnName} ### start ### rows: ${JSON.stringify(rows)}`);

    try {

        // Params
        const params = [
            ...rows.map(r => [r.comId, r.file])
        ]

        // SQL
        const sql = `
            INSERT INTO event_email_com_attachment (comId, file)
            VALUES ?
        `;

        log.info(reqId, `${fnName} ### [query] add email template attachments`);
        // -> Query
        await con.query(sql, [params]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    remove,
    getForEvent,
    removeAttachment,
    updateOne,
    updateWhenToSend,
    addAttachments,
    findById,
    getForEventWithFiles
}