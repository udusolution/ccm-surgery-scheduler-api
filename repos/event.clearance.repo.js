/**
 * @description data-access with DB for event clearances
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add event clearance
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId Event ID
 * @param {number?} clearanceId Master files clearance id (optional)
 * @param {string} clearanceName clearance name
 * @param {string} clearanceDescription clearance description
 * @param {string} physicianName clearance physician name
 * @param {number} assigneeId ID of the user that this item is assigned to
 * @param {Date | string} dueDate due date of this item
 * @param {string?} status status of the clearance
 * @returns {Promise<any>} the newly created event clearance
 */
async function add(log, reqId, { eventId, clearanceId = null, clearanceName, clearanceDescription, physicianName, assigneeId, dueDate, status = null }) {

    const fnName = 'repos/event/clearance/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            eventId, 
            clearanceName, 
            clearanceDescription,
            assigneeId,
            dueDate
        }
        if(clearanceId) params.clearanceId = clearanceId;
        if(status) params.status = status;
        if(physicianName) params.physicianName = physicianName;

        // SQL
        const sql = `
            INSERT INTO event_clearance
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add event clearance`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * update event clearance item
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function update(log, reqId, { id, clearanceName, clearanceDescription, physicianName, assigneeId, dueDate, status, rejectionReason, statusUpdatedOn, statusUpdaterId }) {

    const fnName = 'repos/event/clearance/update';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {};
        if(clearanceName) params.clearanceName = clearanceName;
        if(clearanceDescription) params.clearanceDescription = clearanceDescription;
        if(physicianName != null) params.physicianName = physicianName;
        if(assigneeId) params.assigneeId = assigneeId;
        if(dueDate) params.dueDate = dueDate;
        if(status) params.status = status;
        if(rejectionReason) params.rejectionReason = rejectionReason;
        if(statusUpdatedOn) params.statusUpdatedOn = statusUpdatedOn;
        if(statusUpdaterId) params.statusUpdaterId = statusUpdaterId;

        // SQL
        const sql = `
            UPDATE event_clearance
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update event clearance ### update params: ${JSON.stringify(params)}`);
        // -> Query
        await con.query(sql, [params, id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all the event's clearances
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 * @returns {Promise<any[]>} array of event's clearances
 */
async function getForEvent(log, reqId, eventId) {

    const fnName = 'repos/event/clearance/getForEvent';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT c.*, u.name AS assigneeName, statusUpdater.name AS statusUpdaterName
            FROM event_clearance AS c
            LEFT JOIN user AS u ON c.assigneeId = u.id
            LEFT JOIN user AS statusUpdater ON c.statusUpdaterId = statusUpdater.id
            WHERE eventId = ?
        `;

        log.info(reqId, `${fnName} ### [query] get event clearances`);
        // -> Query
        const [results] = await con.query(sql, eventId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get event clearance by ID
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id event clearance ID
 * @returns {Promise<any>} event clearance
 */
async function findById(log, reqId, id) {

    const fnName = 'repos/event/clearance/findById';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM event_clearance
            WHERE id = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get event clearance`);
        // -> Query
        const [results] = await con.query(sql, id);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results[0];
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * delete an event's clearance item
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id event clearance's ID
 */
async function deleteOne(log, reqId, id) {

    const fnName = 'repos/event/clearance/deleteOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM event_clearance
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete event clearance`);
        // -> Query
        await con.query(sql, id);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    update,
    getForEvent,
    findById,
    deleteOne
}