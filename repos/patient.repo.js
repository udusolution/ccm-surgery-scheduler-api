/**
 * @description data-access with DB for patient data
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add a new patient
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number?} id patient ID
 * @param {string} address1 patient's address 1
 * @param {string} address2 patient's address 2
 * @param {string} city patient's city
 * @param {string} state patient's state
 * @param {string} zipcode patient's zipcode
 * @param {string} dateofbirth patient's dateofbirth
 * @returns {Promise<any>} new patient
 */
async function add(log, reqId, { 
    id = null, 
    firstName, 
    middleName, 
    lastName,
    address1, 
    address2, 
    city, 
    state, 
    zipcode, 
    dateofbirth, 
    email, 
    phone, 
    ssn, 
    sex, 
    carrierId,
    carrierCode,
    carrierName,
    subscriberId,
    groupName,
    groupNumber,
    carrierId2,
    carrierCode2,
    carrierName2,
    subscriberId2,
    groupName2,
    groupNumber2
}) {

    const fnName = 'repos/patient/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            firstName,
            middleName,
            lastName,
            name: `${firstName} ${middleName} ${lastName}`,
            address1, 
            city, 
            state, 
            zipcode, 
            dateofbirth,
            email, 
            phone,
            ssn,
            sex,
            carrierId,
            carrierCode,
            carrierName,
            subscriberId,
            groupName,
            groupNumber
        }
        if(id) params.id = id;
        if(address2) params.address2 = address2;
        if(carrierId2) params.carrierId2 = carrierId2;
        if(carrierCode2) params.carrierCode2 = carrierCode2;
        if(carrierName2) params.carrierName2 = carrierName2;
        if(subscriberId2) params.subscriberId2 = subscriberId2;
        if(groupName2) params.groupName2 = groupName2;
        if(groupNumber2) params.groupNumber2 = groupNumber2;

        // SQL
        const sql = `
            INSERT INTO patient
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add patient`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { id: result.insertId, ...params };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * search patients [paginated]
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} searchCol column to search in 
 * @param {string} searchQuery column search query
 * @param {string} sortCol column to sort by
 * @param {boolean} sortIsAsc whether the sorting is ascending (if not the descending)
 * @param {number} skip entries to skip
 * @param {number} limit maximum entries to fetch (page size)
 * @returns {Promise<any[]>} found patients
 */
async function search(log, reqId, { searchCol, searchQuery, sortCol, sortIsAsc, skip = 0, limit }) {

    const fnName = 'repos/patient/search';
    log.info(reqId, `${fnName} ### start`);

    try {

        const params = [];
        const whereClauses = [];

        if(searchCol) {
            whereClauses.push([`${searchCol} LIKE ?`]);
            params.push(`%${searchQuery}%`);
        }

        // SQL
        const sql = `
            SELECT *
            FROM patient
            ${searchCol ? `WHERE ${whereClauses.join(' AND ')}` : ''}
            ${sortCol && sortIsAsc != null ? `ORDER BY ${sortCol} ${sortIsAsc == true ? 'ASC' : 'DESC'}` : ''}
            LIMIT ${skip},${limit}
        `;

        log.info(reqId, `${fnName} ### [query] search patients`);
        // -> Query
        const [result] = await con.query(sql, [...params]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return result;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * number of patients that match a search query
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} searchCol column to search in 
 * @param {string} searchQuery column search query
 * @returns {Promise<number>} number of patients that match the search query
 */
async function searchCount(log, reqId, { searchCol, searchQuery }) {

    const fnName = 'repos/patient/searchCount';
    log.info(reqId, `${fnName} ### start`);

    try {

        const params = [];
        const whereClauses = [];

        if(searchCol) {
            whereClauses.push([`${searchCol} LIKE ?`]);
            params.push(`%${searchQuery}%`);
        }

        // SQL
        const sql = `
            SELECT COUNT(*) as count
            FROM patient
            ${searchCol ? `WHERE ${whereClauses.join(' AND ')}` : ''}
        `;

        log.info(reqId, `${fnName} ### [query] get matching patients count`);
        // -> Query
        const [result] = await con.query(sql, [...params]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return result[0].count;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * total number of patients
 * @param {*} log logger
 * @param {string} reqId request ID
 * @returns {Promise<number>}
 */
async function totalCount(log, reqId) {

    const fnName = 'repos/patient/totalCount';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT COUNT(*) as count
            FROM patient
        `;

        log.info(reqId, `${fnName} ### [query] get all patients count`);
        // -> Query
        const [result] = await con.query(sql);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return result[0].count;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * search patients using a query string
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} query general search query
 * @param {number} skip entries to skip
 * @param {number} limit maximum entries to fetch
 * @returns {Promise<any[]>} found patients
 * @deprecated
 */
async function searchAdvanced_deprecated(log, reqId, { query, skip = 0, limit = 10 }) {

    const fnName = 'repos/patient/searchAdvanced';
    log.info(reqId, `${fnName} ### start`);

    try {

        const params = [];
        const whereClauses = [];
        let orderClause = 'ORDER BY name ASC';
        let alteredOrderClause = false;

        // -- Name
        // If query length is 4, match first 2 letters of first name and first 2 letters of last name
        // EG: if name is "John Doe" then query "jodo" should match it
        if(query.length == 4) {
            const queryFirstHalf = query.substr(0, 2);
            const querySecondHalf = query.substr(2, 2);
            whereClauses.push(`(firstName LIKE ? AND lastName LIKE ?)`);
            params.push(`${queryFirstHalf}%`);
            params.push(`${querySecondHalf}%`);
        }
        else {
            whereClauses.push(`name LIKE ?`);
            params.push(`%${query}%`);

            orderClause = `
                ORDER BY CASE
                    WHEN name LIKE ? THEN 1
                    WHEN name LIKE ? THEN 2
                    WHEN name LIKE ? THEN 4
                    ELSE 3
                END
            `;
            alteredOrderClause = true;
        }

        // -- Email
        whereClauses.push(`email LIKE ?`);
        params.push(`%${query}%`);

        // -- Phone
        whereClauses.push(`phone LIKE ?`);
        params.push(`%${query}%`);

        // -- SSN
        whereClauses.push(`ssn LIKE ?`);
        params.push(`%${query}%`);

        // -- Address
        whereClauses.push(`concat_ws(' ',address1,address2,city,state,zipcode) LIKE ?`);
        params.push(`%${query}%`);

        // If order clause was altered to be used for name search relevance, add the query to its params
        if(alteredOrderClause) {
            params.push(query);
            params.push(`${query}%`);
            params.push(`%${query}`);
        }

        // SQL
        const sql = `
            SELECT *
            FROM patient
            WHERE ${whereClauses.join(' OR ')}
            ${orderClause}
            LIMIT ${skip},${limit}
        `;

        log.info(reqId, `${fnName} ### [query] search patients`);
        // -> Query
        const [results] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success] ### ${results.length} result(s) found`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * search patients using a query string
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} query general search query
 * @param {number} skip entries to skip
 * @param {number} limit maximum entries to fetch
 * @returns {Promise<any[]>} found patients
 */
async function searchAdvanced(log, reqId, { query, skip = 0, limit = 10 }) {

    const fnName = 'repos/patient/searchAdvanced';
    log.info(reqId, `${fnName} ### start`);

    try {

        if(!query) {
            log.info(reqId, `${fnName} ### end [no query specified]`);
            return [];
        }

        let initialsSearch = null;

        // If query is 4 characters, use as name initial detectors
        if(query?.trim()?.length == 4) {
            initialsSearch = {
                first: query.trim().substr(0, 2),
                last: query.trim().substr(2, 2),
            }
        }

        // Prepare Query
        const preparedQuery = query
            .split(' ')
            .filter(w => w)
            .map(w => w.trim())
            .map(w => `${w}*`)
            .join(' ');

        // SQL
        const sql = `
            SELECT *, MATCH(firstName,middleName,lastName,email,phone,ssn) AGAINST(? IN BOOLEAN MODE) AS relevance
            FROM patient
            HAVING relevance > 0 ${initialsSearch ? `OR (firstName LIKE "${initialsSearch.first}%" AND lastName LIKE "${initialsSearch.last}%")` : ''}
            ORDER BY relevance DESC
            LIMIT ${skip},${limit}
        `;

        log.info(reqId, `${fnName} ### [query] search patients`);
        // -> Query
        const [results] = await con.query(sql, [preparedQuery]);

        // End
        log.info(reqId, `${fnName} ### end [success] ### ${results.length} result(s) found`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * get patient by ID
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} patientId patient ID
 * @returns {Promise<any>|null} patient details, null if not found
 */
async function findById(log, reqId, patientId) {

    const fnName = 'repos/patient/findById';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM patient
            WHERE id = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get patient`);
        // -> Query
        const [result] = await con.query(sql, patientId);

        // End
        log.info(reqId, `${fnName} ### end [success] ### Patient found: ${JSON.stringify(result[0])}`);
        return result[0] || null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * update patient data
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} patientId patient ID
 * @param {*} params update params
 */
async function updateOne(log, reqId, patientId, { 
    firstName, 
    middleName, 
    lastName,
    address1, 
    address2, 
    city, 
    state, 
    zipcode, 
    dateofbirth, 
    email, 
    phone, 
    ssn, 
    sex, 
    carrierId, 
    carrierCode, 
    carrierName, 
    subscriberId, 
    groupName, 
    groupNumber,
    carrierId2,
    carrierCode2,
    carrierName2,
    subscriberId2,
    groupName2,
    groupNumber2
}) {

    const fnName = 'repos/patient/updateOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Update params
        const params = {};
        if(firstName) params.firstName = firstName;
        if(middleName) params.middleName = middleName;
        if(lastName) params.lastName = lastName;
        if(firstName && middleName && lastName) params.name = `${firstName} ${middleName} ${lastName}`;
        if(address1) params.address1 = address1;
        if(address2) params.address2 = address2;
        if(city) params.city = city;
        if(state) params.state = state;
        if(zipcode) params.zipcode = zipcode;
        if(dateofbirth) params.dateofbirth = dateofbirth;
        if(email) params.email = email;
        if(phone) params.phone = phone;
        if(ssn) params.ssn = ssn;
        if(sex) params.sex = sex;
        if(carrierId) params.carrierId = carrierId;
        if(carrierCode) params.carrierCode = carrierCode;
        if(carrierName) params.carrierName = carrierName;
        if(subscriberId) params.subscriberId = subscriberId;
        if(groupName) params.groupName = groupName;
        if(groupNumber) params.groupNumber = groupNumber;
        params.carrierId2 = carrierId2;
        params.carrierCode2 = carrierCode2;
        params.carrierName2 = carrierName2;
        params.subscriberId2 = subscriberId2;
        params.groupName2 = groupName2;
        params.groupNumber2 = groupNumber2;

        // SQL
        const sql = `
            UPDATE patient
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update patient`);
        // -> Query
        await con.query(sql, [params, patientId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * delete patient by ID
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} patientId patient ID
 */
 async function deleteOne(log, reqId, patientId) {

    const fnName = 'repos/patient/deleteOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM patient
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete patient`);
        // -> Query
        await con.query(sql, patientId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}
 
 
/**
 * Exports
 */
module.exports = {
    add,
    search,
    searchCount,
    totalCount,
    searchAdvanced,
    findById,
    updateOne,
    deleteOne
}