/**
 * @description data-access with DB for event facility boarding slip emails
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add record of email sent to facility with event boarding slip
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId Event ID
 * @param {number} userId User ID who is sending the email
 * @param {string} facilitySchedulingEmail Master files facility scheduling email
 * @param {string} emailBody email body
 * @param {string} bsFile boarding slip file path
 * @returns {Promise<any>} the newly created record
 */
async function addRecord(log, reqId, { eventId, userId, facilitySchedulingEmail, emailBody, bsFile }) {

    const fnName = 'repos/event/facilityEmail/addRecord';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            eventId,
            userId,
            facilitySchedulingEmail,
            emailBody,
            bsFile
        }

        // SQL
        const sql = `
            INSERT INTO event_facility_bs_email
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add record of email sent to facility with event boarding slip`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all the event's boarding slip facility emails
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 * @returns {Promise<any[]>} array of event's boarding slip facility emails
 */
async function getForEvent(log, reqId, eventId) {

    const fnName = 'repos/event/facilityEmail/getForEvent';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT e.*, u.name AS userName
            FROM event_facility_bs_email AS e
            LEFT JOIN user AS u ON e.userId = u.id
            WHERE eventId = ?
            ORDER BY id DESC
        `;

        log.info(reqId, `${fnName} ### [query] get all the event's boarding slip facility emails`);
        // -> Query
        const [results] = await con.query(sql, [eventId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    addRecord,
    getForEvent,
}