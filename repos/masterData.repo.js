/**
 * @description data-access with DB for master data
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');
const Enums = require('../common/enums');


/**
 * add a master data entry
 * @param {*} log logger
 * @param {string} reqId request Id
 * @param {string} table the master data pre-defined table to insert the entry into
 * @param {*} data the entry data, mapping to the table columns
 * @returns {Promise<any>} the newly created entry
 */
async function add(log, reqId, table, data) {

    const fnName = 'repos/masterData/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = data;
        if(data.id) params.id = data.id;

        // SQL
        const sql = `
            INSERT INTO ${table}
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add master data (${table})`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all the entries in a master data table
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} table the master data table to fetch
 * @returns {Promise<[*]>} all the entries of the master data table
 */
async function getTable(log, reqId, table) {

    const fnName = 'repos/masterData/getTable';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Sorting
        let sorting = '';
        if(table == Enums.MasterDataTables.Carrier) sorting = 'ORDER BY name ASC';
        if(table == Enums.MasterDataTables.Procedures) sorting = 'ORDER BY name ASC';
        if(table == Enums.MasterDataTables.Email) sorting = 'ORDER BY name ASC';
        if(table == Enums.MasterDataTables.Sms) sorting = 'ORDER BY name ASC';

        // SQL
        const sql = `
            SELECT *
            FROM ${table}
            ${sorting}
        `;

        log.info(reqId, `${fnName} ### [query] get master data`);
        // -> Query
        const [results] = await con.query(sql);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get an entry in a master data table
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} table the master data table to fetch from
 * @param {string} rowId the master data entry id to fetch
 * @returns {Promise<any>} the master data table entry
 */
async function getRow(log, reqId, table, rowId) {

    const fnName = 'repos/masterData/getRow';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM ${table}
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] get master data row`);
        // -> Query
        const [results] = await con.query(sql, [rowId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results[0];
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}

/**
 * get entries in a master data table
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} table the master data table to fetch from
 * @param {number[]} rowIds the master data entry ids to fetch
 * @returns {Promise<any[]>} the master data table entries
 */
async function getRows(log, reqId, table, rowIds) {

    const fnName = 'repos/masterData/getRows';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM ${table}
            WHERE id IN (?)
        `;

        log.info(reqId, `${fnName} ### [query] get master data rows`);
        // -> Query
        const [results] = await con.query(sql, [rowIds]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all the entries in multiple master data tables
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string[]} tables the master data tables to fetch
 * @returns {Promise<[[*]]>} all the entries of the master data table, with the table name being the key to its entries
 */
async function getTables(log, reqId, tables) {

    const fnName = 'repos/masterData/getTables';
    log.info(reqId, `${fnName} ### start`);

    try {

        // -> Map each table to a promise to fetch its master data
        const fetchPromises = tables.map(table => {
            // Sorting
            let sorting = '';
            if(table == Enums.MasterDataTables.Carrier) sorting = 'ORDER BY name ASC';
            if(table == Enums.MasterDataTables.Procedures) sorting = 'ORDER BY name ASC';
            if(table == Enums.MasterDataTables.Email) sorting = 'ORDER BY name ASC';
            if(table == Enums.MasterDataTables.Sms) sorting = 'ORDER BY name ASC';

            return con.query(`SELECT * FROM ${table} ${sorting}`)
        });

        log.info(reqId, `${fnName} ### [query] fetch specified master data`);
        // -> Await all Promises
        const promiseResults = await Promise.all(fetchPromises);

        // -> Place each resulting master data table into its key in the result object
        const masterData = {};
        for(let i = 0; i < promiseResults.length; i++) {
            masterData[tables[i]] = promiseResults[i][0];
        }

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return masterData;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * update a column in a master data entry
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} table master data table
 * @param {number} rowId entry (row) ID
 * @param {string} col predefined column name
 * @param {*} val new value to update the column
 */
async function updateCol(log, reqId, table, rowId, col, val) {

    const fnName = 'repos/masterData/updateCol';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Update Params
        const updateParams = {};
        updateParams[col] = val;

        // SQL
        const sql = `
            UPDATE ${table}
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update master data entry column (${table}.${col} = ${val})`);
        // -> Query
        await con.query(sql, [updateParams, rowId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * delete an entry of a master data table
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} table master data table
 * @param {number} rowId entry (row) ID
 */
async function deleteRow(log, reqId, table, rowId) {

    const fnName = 'repos/masterData/deleteRow';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM ${table}
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete master data entry (${table}:${rowId})`);
        // -> Query
        await con.query(sql, [rowId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    getTable,
    getTables,
    getRow,
    getRows,
    updateCol,
    deleteRow
}