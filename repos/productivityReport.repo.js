/**
 * @description data-access with DB for scheduled productivity reports
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');
const Enums = require('../common/enums');
const moment = require('moment');


/**
 * add a scheduled productivity report
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function addOne(log, reqId, { 
    coordinatorId = null,
    physicianId = null,
    facilityId = null,
    sendEvery,
    hour = null,
    sendTo
}) {

    const fnName = 'repos/productivityReports/addOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Note Params
        const params = {
            sendEvery,
            sendTo
        }
        if(coordinatorId) params.coordinatorId = coordinatorId;
        if(physicianId) params.physicianId = physicianId;
        if(facilityId) params.facilityId = facilityId;
        if(hour) params.hour = hour;

        // SQL
        const sql = `
            INSERT INTO productivity_report
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add scheduled productivity report`);
        // -> Query
        await con.query(sql, [params]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
        
    }
}


/**
 * update a scheduled productivity report
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function updateOne(log, reqId, { 
    id,
    coordinatorId = null,
    physicianId = null,
    facilityId = null,
    sendEvery = null,
    hour = null,
    sendTo = null
}) {

    const fnName = 'repos/productivityReports/updateOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Note Params
        const params = {};
        if(sendEvery) params.sendEvery = sendEvery;
        if(sendTo) params.sendTo = sendTo;
        if(coordinatorId) {
            params.coordinatorId = coordinatorId;
            params.physicianId = null;
        }
        else if(physicianId) {
            params.physicianId = physicianId;
            params.coordinatorId = null;
        }
        if(facilityId) params.facilityId = facilityId;
        if(hour) params.hour = hour;

        // SQL
        const sql = `
            UPDATE productivity_report
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update scheduled productivity report`);
        // -> Query
        await con.query(sql, [params, id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
        
    }
}


/**
 * get all scheduled productivity reports
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function getAll(log, reqId) {

    const fnName = 'repos/productivityReports/getAll';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT pr.*, coord.name AS coordinatorName, phy.name AS physicianName
            FROM productivity_report AS pr
            LEFT JOIN user AS coord ON pr.coordinatorId = coord.id
            LEFT JOIN user AS phy ON pr.physicianId = phy.id
        `;

        log.info(reqId, `${fnName} ### [query] get all scheduled productivity reports`);
        // -> Query
        const [results] = await con.query(sql);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
        
    }
}


/**
 * delete a scheduled productivity report
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id day note ID
 */
 async function deleteOne(log, reqId, id) {

    const fnName = 'repos/productivityReports/deleteOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM productivity_report
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete scheduled productivity report`);
        // -> Query
        await con.query(sql, [id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
        
    }
}


/**
 * Exports
 */
module.exports = {
    addOne,
    updateOne,
    getAll,
    deleteOne
}