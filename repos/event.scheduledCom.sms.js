/**
 * @description data-access with DB for event scheduled sms communications
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add scheduled sms communication to event
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function add(log, reqId, { eventId, templateId, templateName, templateBody, whenToSend, forWhom = 'patient' }) {

    const fnName = 'repos/event/scheduledCom/sms/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            eventId, 
            templateId, 
            templateName,
            templateBody,
            whenToSend,
            forWhom
        }

        // SQL
        const sql = `
            INSERT INTO event_sms_com
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add scheduled sms communication to event`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * update scheduled event sms communication
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function updateOne(log, reqId, { id, templateBody }) {

    const fnName = 'repos/event/scheduledCom/sms/updateOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            templateBody
        }

        // SQL
        const sql = `
            UPDATE event_sms_com
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update scheduled event sms communication`);
        // -> Query
        await con.query(sql, [params, id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * update scheduled event sms communication's "when to send"
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function updateWhenToSend(log, reqId, id, whenToSend) {

    const fnName = 'repos/event/scheduledCom/sms/updateOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            whenToSend
        }

        // SQL
        const sql = `
            UPDATE event_sms_com
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update scheduled event sms communication's "when to send"`);
        // -> Query
        await con.query(sql, [params, id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * remove scheduled sms communication from event
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function remove(log, reqId, id) {

    const fnName = 'repos/event/scheduledCom/sms/remove';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM event_sms_com
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] remove scheduled sms communication from event`);
        // -> Query
        await con.query(sql, [id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get an event's scheduled sms communications
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function getForEvent(log, reqId, eventId, forPatientOnly = false) {

    const fnName = 'repos/event/scheduledCom/sms/getForEvent';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM event_sms_com
            WHERE eventId = ?
            ${forPatientOnly ? `AND forWhom = 'patient'` : ''}
            ORDER BY templateName ASC
        `;

        log.info(reqId, `${fnName} ### [query] get event sms scheduled communications`);
        // -> Query
        const [results] = await con.query(sql, [eventId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get an event scheduled sms com with its attachments
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id com id
 */
async function findById(log, reqId, id) {

    const fnName = 'repos/event/scheduledCom/sms/findById';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM event_sms_com
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] get an event scheduled sms com`);
        // -> Query
        const [results] = await con.query(sql, [id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results[0];
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    remove,
    getForEvent,
    updateOne,
    updateWhenToSend,
    findById
}