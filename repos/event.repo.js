/**
 * @description data-access with DB for events
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');
const Enums = require('../common/enums');


/**
 * add a new event
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {Date} dateFrom starting date-time of the event
 * @param {Date} dateTo end date-time of the event
 * @param {number} eventTypeId event type ID
 * @param {string} eventTypeCode event type code
 * @param {string} eventTypeName event type name
 * @param {number} priorityId priority ID
 * @param {number} priority priority
 * @param {string} priorityText priority name
 * @param {number} facilityId facility ID
 * @param {string} facilityCode facility code
 * @param {string} facilityName facility name
 * @param {string} facilityAddress facility address
 * @param patientAddress1
 * @param patientAddress2
 * @param patientCity
 * @param patientState
 * @param patientZipCode
 * @param {number} physicianId physician ID
 * @param {string} physicianName physician name
 * @param physicianPhone
 * @param physicianFax
 * @param physicianOfficePhone
 * @param physicianOfficeAddress
 * @param {number} coordinatorId coordinator ID
 * @param {number} coordinatorName coordinator name
 * @param {number} patientId patient ID
 * @param patientName
 * @param patientFirstName
 * @param patientMiddleName
 * @param patientLastName
 * @param patientDoB
 * @param patientGender
 * @param patientPreferredPhone
 * @param patientEmail
 * @param patientSSN
 * @param isEmergency
 * @param carrierId
 * @param carrierCode
 * @param carrierName
 * @param subscriberId
 * @param groupName
 * @param groupNumber
 * @param insuranceAuthorizationStatus
 * @param carrierId2
 * @param carrierCode2
 * @param carrierName2
 * @param subscriberId2
 * @param groupName2
 * @param groupNumber2
 * @returns {Promise<any>} newly inserted event
 */
async function add(log, reqId, {
    dateFrom,
    dateTo,
    eventTypeId,
    eventTypeCode,
    eventTypeName,
    priorityId,
    priority,
    priorityText,
    facilityId,
    facilityCode,
    facilityName,
    facilityAddress,
    patientAddress1,
    patientAddress2,
    patientCity,
    patientState,
    patientZipCode,
    physicianId,
    physicianName,
    physicianPhone,
    physicianFax,
    physicianOfficePhone,
    physicianOfficeAddress,
    coordinatorId,
    coordinatorName,
    patientId,
    patientName,
    patientFirstName,
    patientMiddleName,
    patientLastName,
    patientDoB,
    patientGender,
    patientPreferredPhone,
    patientEmail,
    patientSSN,
    isEmergency,
    carrierId,
    carrierCode,
    carrierName,
    subscriberId,
    groupName,
    groupNumber,
    insuranceAuthorizationStatus,
    carrierId2,
    carrierCode2,
    carrierName2,
    subscriberId2,
    groupName2,
    groupNumber2
}) {

    const fnName = 'repos/events/add';
    log.info(reqId, `${fnName} ### start`);

    try {
        
        // Params
        const params = {
            dateFrom,
            dateTo,
            eventTypeId,
            eventTypeCode,
            eventTypeName,
            priorityId,
            priority,
            priorityText,
            facilityId,
            facilityCode,
            facilityName,
            facilityAddress,
            patientAddress1,
            patientAddress2,
            patientCity,
            patientState,
            patientZipCode,
            physicianId,
            physicianName,
            physicianPhone,
            physicianFax,
            physicianOfficePhone,
            physicianOfficeAddress,
            coordinatorId,
            coordinatorName,
            patientId,
            patientName,
            patientFirstName,
            patientMiddleName,
            patientLastName,
            patientDoB,
            patientGender,
            patientPreferredPhone,
            patientEmail,
            patientSSN,
            carrierId,
            carrierCode,
            carrierName,
            subscriberId,
            groupName,
            groupNumber
        }
        if(isEmergency) params.isEmergency = 1;
        if(insuranceAuthorizationStatus) params.insuranceAuthorizationStatus = insuranceAuthorizationStatus;
        if(carrierId2) params.carrierId2 = carrierId2;
        if(carrierCode2) params.carrierCode2 = carrierCode2;
        if(carrierName2) params.carrierName2 = carrierName2;
        if(subscriberId2) params.subscriberId2 = subscriberId2;
        if(groupName2) params.groupName2 = groupName2;
        if(groupNumber2) params.groupNumber2 = groupNumber2;
        params.status = Enums.EventStatuses.WaitingForConfirmation;

        // SQL
        const sql = `
            INSERT INTO event
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add event`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { id: result.insertId, ...params };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * search events [paginated]
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} searchCol column to search in 
 * @param {string} searchQuery column search query
 * @param {string} sortCol column to sort by
 * @param {boolean} sortIsAsc whether the sorting is ascending (if not the descending)
 * @param {number} skip entries to skip
 * @param {number} limit maximum entries to fetch (page size)
 * @param {number} physicianId physician ID to filter by
 * @param {number} coordinatorId coordinator ID to filter by
 * @param {number} patientId patient ID to filter by
 * @param {number} facilityId facility ID to filter by
 * @param {Date|string} fromDate get events that start after this date time
 * @param {Date|string} toDate get events that start before this date time
 * @returns {Promise<any[]>} found events
 */
async function search(log, reqId, { 
    searchCol = null, 
    searchQuery = null, 
    sortCol = null, 
    sortIsAsc = null, 
    skip = 0, 
    limit = 10, 
    physicianId = null, 
    coordinatorId = null, 
    patientId = null, 
    facilityId = null, 
    fromDate = null, 
    toDate = null,
    status = null,
    isEmergency = null,
    eventTypeId = null,
    showOnlyMyCancelled = false
}, userId) {

    const fnName = 'repos/event/search';
    log.info(reqId, `${fnName} ### start`);

    try {

        const params = [];
        const whereClauses = [];
        let hasWhere = false;

        // Search Column
        if(searchCol) {
            hasWhere = true;
            whereClauses.push(`${searchCol} LIKE ?`);
            params.push(`%${searchQuery}%`);
        }

        // Filter by Physician
        if(physicianId) {
            hasWhere = true;
            whereClauses.push(`physicianId IN (?)`);
            params.push(Array.isArray(physicianId) ? physicianId : [physicianId]);
        }

        // Filter by Coordinator
        if(coordinatorId) {
            hasWhere = true;
            whereClauses.push(`coordinatorId IN (?)`);
            params.push(Array.isArray(coordinatorId) ? coordinatorId : [coordinatorId]);
        }

        // Filter by Patient
        if(patientId) {
            hasWhere = true;
            whereClauses.push(`patientId IN (?)`);
            params.push(Array.isArray(patientId) ? patientId : [patientId]);
        }

        // Filter by Facility
        if(facilityId) {
            hasWhere = true;
            whereClauses.push(`facilityId IN (?)`);
            params.push(Array.isArray(facilityId) ? facilityId : [facilityId]);
        }
        
        // From Date
        if(fromDate) {
            hasWhere = true;
            whereClauses.push(`dateFrom > ?`);
            params.push(fromDate);
        }

        // To Date
        if(toDate) {
            hasWhere = true;
            whereClauses.push(`dateFrom < ?`);
            params.push(toDate);
        }

        // Status
        if(status) {
            hasWhere = true;
            if(Array.isArray(status)) whereClauses.push(`status IN (?) ${showOnlyMyCancelled ? `AND (status != ? OR (status = ? AND canceledBy = ?))` : ''}`);
            else whereClauses.push(`status = ? ${showOnlyMyCancelled ? `AND (status != ? OR (status = ? AND canceledBy = ?))` : ''}`);
            params.push(status);
            if(showOnlyMyCancelled) params.push(Enums.EventStatuses.Cancelled, Enums.EventStatuses.Cancelled, userId);
        }

        // Is Emergency / RTMU
        if(isEmergency) {
            hasWhere = true;
            if(Array.isArray(isEmergency)) whereClauses.push(`isEmergency IN (?)`);
            else whereClauses.push(`isEmergency = ?`);
            params.push(isEmergency);
        }

        // Event Type
        if(eventTypeId) {
            hasWhere = true;
            if(Array.isArray(eventTypeId)) whereClauses.push(`eventTypeId IN (?)`);
            else whereClauses.push(`eventTypeId = ?`);
            params.push(eventTypeId);
        }

        // Sort
        let sorting = '';
        if(sortCol) {
            sorting = `ORDER BY ${sortCol} ${sortIsAsc == true ? 'ASC' : 'DESC'}`
        }
        else {
            sorting = `
            ORDER BY CASE
                WHEN dateFrom > NOW() THEN (UNIX_TIMESTAMP(dateFrom))
                WHEN dateFrom < NOW() THEN (100000000000 - UNIX_TIMESTAMP(dateFrom))
                ELSE 1
            END
            `
        }

        // SQL
        const sql = `
            SELECT 
                e.*, 
                et.color AS color, 
                et.shortname AS eventTypeShortname, 
                p.sex AS patientSex, 
                p.dateofbirth AS patientDoB,
                authUpdater.name AS insuranceAuthorizationStatusUpdaterName,
                carr.authorizationUrl AS authorizationUrl,
                carr.authorizationPhone AS authorizationPhone,
                medUpdater.name AS medicalClearanceStatusUpdaterName, 
                cardUpdater.name AS cardiacClearanceStatusUpdaterName, 
                othUpdater.name AS otherClearanceStatusUpdaterName, 
                (SELECT COUNT(*) FROM event_procedure AS proc WHERE proc.eventId = e.id) AS procedureCount,
                (SELECT GROUP_CONCAT(proc.procedureCode SEPARATOR \', \') FROM event_procedure AS proc WHERE proc.eventId = e.id) AS procedureCodes,
                (SELECT GROUP_CONCAT(clr.status SEPARATOR \',\') FROM event_clearance AS clr WHERE clr.eventId = e.id) AS clearanceStatuses
            FROM event AS e
            LEFT JOIN event_type AS et ON e.eventTypeId = et.id
            LEFT JOIN patient AS p ON e.patientId = p.id
            LEFT JOIN user AS authUpdater ON e.insuranceAuthorizationStatusUpdaterId = authUpdater.id
            LEFT JOIN user AS medUpdater ON e.medicalClearanceUpdaterId = medUpdater.id
            LEFT JOIN user AS cardUpdater ON e.cardiacClearanceUpdaterId = cardUpdater.id
            LEFT JOIN user AS othUpdater ON e.otherClearanceUpdaterId = othUpdater.id
            LEFT JOIN carrier AS carr ON e.carrierId = carr.id
            ${hasWhere ? `WHERE ${whereClauses.join(' AND ')}` : ''}
            ${sorting}
            LIMIT ${skip},${limit}
        `;

        log.info(reqId, `${fnName} ### [query] search events`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return result;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * number of events that match a search query
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} searchCol column to search in 
 * @param {string} searchQuery column search query
 * @param {number} physicianId physician ID to filter by
 * @param {number} coordinatorId coordinator ID to filter by
 * @param {number} patientId patient ID to filter by
 * @param {number} facilityId facility ID to filter by
 * @param {Date|string} fromDate get events that start after this date time
 * @param {Date|string} toDate get events that start before this date time
 * @returns {Promise<number>} number of events that match the search query
 */
async function searchCount(log, reqId, { 
    searchCol = null, 
    searchQuery = null,
    physicianId = null, 
    coordinatorId = null, 
    patientId = null, 
    facilityId = null,
    fromDate = null, 
    toDate = null,
    status = null,
    isEmergency = null,
    eventTypeId = null,
    showOnlyMyCancelled = false
}, userId) {

    const fnName = 'repos/event/searchCount';
    log.info(reqId, `${fnName} ### start`);

    try {

        const params = [];
        const whereClauses = [];
        let hasWhere = false;

        // Search Column
        if(searchCol) {
            hasWhere = true;
            whereClauses.push(`${searchCol} LIKE ?`);
            params.push(`%${searchQuery}%`);
        }

        // Filter by Physician
        if(physicianId) {
            hasWhere = true;
            whereClauses.push(`physicianId IN (?)`);
            params.push(Array.isArray(physicianId) ? physicianId : [physicianId]);
        }

        // Filter by Coordinator
        if(coordinatorId) {
            hasWhere = true;
            whereClauses.push(`coordinatorId IN (?)`);
            params.push(Array.isArray(coordinatorId) ? coordinatorId : [coordinatorId]);
        }

        // Filter by Patient
        if(patientId) {
            hasWhere = true;
            whereClauses.push(`patientId IN (?)`);
            params.push(Array.isArray(patientId) ? patientId : [patientId]);
        }

        // Filter by Facility
        if(facilityId) {
            hasWhere = true;
            whereClauses.push(`facilityId IN (?)`);
            params.push(Array.isArray(facilityId) ? facilityId : [facilityId]);
        }
        
        // From Date
        if(fromDate) {
            hasWhere = true;
            whereClauses.push(`dateFrom > ?`);
            params.push(fromDate);
        }

        // To Date
        if(toDate) {
            hasWhere = true;
            whereClauses.push(`dateFrom < ?`);
            params.push(toDate);
        }

        // Status
        if(status) {
            hasWhere = true;
            if(Array.isArray(status)) whereClauses.push(`status IN (?) ${showOnlyMyCancelled ? `AND (status != ? OR (status = ? AND canceledBy = ?))` : ''}`);
            else whereClauses.push(`status = ? ${showOnlyMyCancelled ? `AND (status != ? OR (status = ? AND canceledBy = ?))` : ''}`);
            params.push(status);
            if(showOnlyMyCancelled) params.push(Enums.EventStatuses.Cancelled, Enums.EventStatuses.Cancelled, userId);
        }

        // Is Emergency / RTMU
        if(isEmergency) {
            hasWhere = true;
            if(Array.isArray(isEmergency)) whereClauses.push(`isEmergency IN (?)`);
            else whereClauses.push(`isEmergency = ?`);
            params.push(isEmergency);
        }

        // Event Type
        if(eventTypeId) {
            hasWhere = true;
            if(Array.isArray(eventTypeId)) whereClauses.push(`eventTypeId IN (?)`);
            else whereClauses.push(`eventTypeId = ?`);
            params.push(eventTypeId);
        }

        // SQL
        const sql = `
            SELECT COUNT(*) AS count
            FROM event
            ${hasWhere ? `WHERE ${whereClauses.join(' AND ')}` : ''}
        `;

        log.info(reqId, `${fnName} ### [query] get matching events count`);
        // -> Query
        const [result] = await con.query(sql, [...params]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return result[0].count;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * get events that intersect with a given start and end times, and possibly patient or physician
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} dateFrom event's start date-time
 * @param {string} dateTo event's end date-time
 * @param {number} patientId patient ID to match intersecting events with
 * @param {number} physicianId physician ID to match intersecting events with
 * @returns {Promise<any[]>} events that intersect
 */
async function eventsInPeriod(log, reqId, dateFrom, dateTo, patientId = null, physicianId = null, ignore = null) {

    const fnName = 'repos/event/eventsInPeriod';
    log.info(reqId, `${fnName} ### start`);

    try {

        const params = [dateFrom, dateFrom, dateTo, dateTo, dateFrom, dateTo];
        let extraWhere = false;
        const extraWhereClauses = [];

        // Extra Where Clauses
        if(patientId) {
            extraWhere = true;
            extraWhereClauses.push('patientId = ?');
            params.push(patientId);
        }
        if(physicianId) {
            extraWhere = true;
            extraWhereClauses.push('physicianId = ?');
            params.push(physicianId);
        }
        if(ignore) {
            params.push(ignore);
        }

        // SQL
        const sql = `
            SELECT *
            FROM event
            WHERE ((dateFrom < ? AND dateTo > ?)
            OR (dateFrom < ? AND dateTo > ?)
            OR (dateFrom > ? AND dateTo < ?))
            ${extraWhere ? `AND (${extraWhereClauses.join(' OR ')})` : ''}
            ${ignore ? `AND id NOT IN (?)` : ''}
        `;

        log.info(reqId, `${fnName} ### [query] get intersecting events count`);
        // -> Query
        const [results] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success] ### intersecting events: ${JSON.stringify(results)}`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * update an event
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 * @param {*} params update params
 * @returns {Promise<null>}
 */
async function update(log, reqId, eventId, params) {

    const fnName = 'repos/event/update';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Check if empty params
        if(Object.keys(params).length == 0) {
            log.info(reqId, `${fnName} ### end ### nothing to update`);
            return;
        }

        // SQL
        const sql = `
            UPDATE event
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update event`);
        // -> Query
        await con.query(sql, [params, eventId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * get an event
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 * @returns {Promise<any>} event
 */
async function findById(log, reqId, eventId) {

    const fnName = 'repos/event/findById';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT 
                e.*, 
                medAssignee.name AS medicalClearanceAssigneeName, 
                cardAssignee.name AS cardiacClearanceAssigneeName, 
                othAssignee.name AS otherClearanceAssigneeName,
                medUpdater.name AS medicalClearanceStatusUpdaterName, 
                cardUpdater.name AS cardiacClearanceStatusUpdaterName, 
                othUpdater.name AS otherClearanceStatusUpdaterName, 
                authUpdater.name AS insuranceAuthorizationStatusUpdaterName,
                carr.authorizationUrl AS authorizationUrl,
                carr.authorizationPhone AS authorizationPhone,
                carr2.authorizationUrl AS authorizationUrl2,
                carr2.authorizationPhone AS authorizationPhone2,
                f.schedulingEmail AS facilitySchedulingEmail,
                f.eBoardingLink AS eBoardingLink,
                phy.signature AS physicianSignature
            FROM event AS e
            LEFT JOIN user AS medAssignee ON e.medicalClearanceAssigneeId = medAssignee.id
            LEFT JOIN user AS cardAssignee ON e.cardiacClearanceAssigneeId = cardAssignee.id
            LEFT JOIN user AS othAssignee ON e.otherClearanceAssigneeId = othAssignee.id
            LEFT JOIN user AS medUpdater ON e.medicalClearanceUpdaterId = medUpdater.id
            LEFT JOIN user AS cardUpdater ON e.cardiacClearanceUpdaterId = cardUpdater.id
            LEFT JOIN user AS othUpdater ON e.otherClearanceUpdaterId = othUpdater.id
            LEFT JOIN user AS authUpdater ON e.insuranceAuthorizationStatusUpdaterId = authUpdater.id
            LEFT JOIN user AS phy ON e.physicianId = phy.id
            LEFT JOIN carrier AS carr ON e.carrierCode = carr.code
            LEFT JOIN carrier AS carr2 ON e.carrierCode2 = carr2.code
            LEFT JOIN patient AS patient ON e.patientId = patient.id
            LEFT JOIN facility AS f ON e.facilityId = f.id
            WHERE e.id = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get event`);
        // -> Query
        const [results] = await con.query(sql, eventId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results[0];
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * set event as Boarding Slip Generated and update timestamp
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 */
async function setAsBSG(log, reqId, eventId) {

    const fnName = 'repos/event/setAsBSG';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            UPDATE event
            SET status = "${Enums.EventStatuses.BoardingSlipGenerated}", bsgON = NOW(), statusUpdatedOn = NOW()
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] set event as Boarding Slip Generated`);
        // -> Query
        await con.query(sql, eventId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * set event as Waiting For Confirmation and update timestamp
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 */
async function setAsWFC(log, reqId, eventId) {

    const fnName = 'repos/event/setAsWFC';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            UPDATE event
            SET status = "${Enums.EventStatuses.WaitingForConfirmation}", wfcON = NOW(), statusUpdatedOn = NOW()
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] set event as Waiting For Confirmation`);
        // -> Query
        await con.query(sql, eventId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * set event as Confirmed and update timestamp
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 */
async function setAsConfirmed(log, reqId, eventId) {

    const fnName = 'repos/event/setAsConfirmed';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            UPDATE event
            SET status = "${Enums.EventStatuses.Confirmed}", confirmedON = NOW(), statusUpdatedOn = NOW()
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] set event as Confirmed`);
        // -> Query
        await con.query(sql, eventId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * set event as Rejected and update timestamp
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 */
async function setAsRejected(log, reqId, eventId) {

    const fnName = 'repos/event/setAsRejected';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            UPDATE event
            SET status = "${Enums.EventStatuses.Rejected}"
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] set event as Rejected`);
        // -> Query
        await con.query(sql, eventId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * set event as Completed and update timestamp
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 */
async function setAsCompleted(log, reqId, eventId) {

    const fnName = 'repos/event/setAsCompleted';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            UPDATE event
            SET status = "${Enums.EventStatuses.Completed}", completedON = NOW(), statusUpdatedOn = NOW()
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] set event as Completed`);
        // -> Query
        await con.query(sql, eventId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * delete event
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 */
async function deleteEvent(log, reqId, eventId) {

    const fnName = 'repos/event/deleteEvent';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM event
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete event`);
        // -> Query
        await con.query(sql, eventId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * delete event
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 * @param {string} cancelReason cancellation reason
 * @param {number} userId user ID of the user that cancelled this event
 * @param {string?} cancelNote cancellation note
 */
async function cancelEvent(log, reqId, eventId, cancelReason, userId, cancelNote = null) {

    const fnName = 'repos/event/cancelEvent';
    log.info(reqId, `${fnName} ### start`);

    try {

        const params = {
            status: Enums.EventStatuses.Cancelled,
            cancelReason: cancelReason,
            canceledBy: userId,
            statusUpdatedOn: new Date()
        }
        if(cancelNote) params.cancelNote = cancelNote;
        else params.cancelNote = null;

        // SQL
        const sql = `
            UPDATE event
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] cancel event`);
        // -> Query
        await con.query(sql, [params, eventId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * check if a user has been granted edit permission for an event (returns null if the user does not have an edit request to begin with)
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 * @param {number} userId user ID
 * @returns {Promise<Boolean | null>}
 */
 async function userWasGrantedEditPerm(log, reqId, eventId, userId) {

    const fnName = 'repos/event/userWasGrantedEditPerm';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM event_edit_request
            WHERE eventId = ?
            AND requesterId = ?
        `;

        log.info(reqId, `${fnName} ### [query] get event edit request`);
        // -> Query
        const [editRequest] = await con.query(sql, [eventId, userId]);

        const hasPerm = !editRequest[0]
            ? null
            : editRequest[0].status == Enums.EventEditRequestStatuses.Approved
                ? true
                : false;

        // End
        log.info(reqId, `${fnName} ### end [success] ### hasPerm: ${hasPerm}, edit request: ${JSON.stringify(editRequest[0])}`);
        return hasPerm;
        
    }
    catch(e) {
        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    search,
    searchCount,
    eventsInPeriod,
    update,
    findById,
    setAsBSG,
    setAsWFC,
    setAsConfirmed,
    setAsRejected,
    setAsCompleted,
    deleteEvent,
    cancelEvent,
    userWasGrantedEditPerm
}