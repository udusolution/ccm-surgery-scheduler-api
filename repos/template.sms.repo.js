/**
 * @description data-access with DB for sms templates
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add sms template
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function add(log, reqId, { name, body, tags }) {

    const fnName = 'repos/templates/sms/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            name,
            body
        }
        if(tags) params.tags = tags;

        // SQL
        const sql = `
            INSERT INTO sms
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add sms template`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all sms template and with their attachments
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function getAll(log, reqId) {

    const fnName = 'repos/templates/sms/getAll';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM sms
        `;

        log.info(reqId, `${fnName} ### [query] get all sms template`);
        // -> Query
        const [results] = await con.query(sql);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get an sms template
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id template id
 */
async function findById(log, reqId, id) {

    const fnName = 'repos/templates/sms/findById';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM sms
            WHERE id = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get sms template`);
        // -> Query
        const [results] = await con.query(sql, [id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results[0];
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    getAll,
    findById,
}