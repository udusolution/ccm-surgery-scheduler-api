/**
 * @description data-access with DB for patient data
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add a patient data entry
 * @param {*} log logger
 * @param {string} reqId request Id
 * @param {string} table the patient data pre-defined table to insert the entry into
 * @param {*} data the entry data, mapping to the table columns
 * @returns {Promise<any>} the newly created entry
 */
async function add(log, reqId, table, data) {

    const fnName = 'repos/patientData/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = data;
        if(data.id) params.id = data.id;

        // SQL
        const sql = `
            INSERT INTO ${table}
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add patient data (${table})`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all the entries in a patient data table
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} table the patient data table to fetch
 * @returns {Promise<[*]>} all the entries of the patient data table
 */
async function getTable(log, reqId, table) {

    const fnName = 'repos/patientData/getTable';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM ${table}
        `;

        log.info(reqId, `${fnName} ### [query] get patient data`);
        // -> Query
        const [results] = await con.query(sql);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all the entries in multiple patient data tables
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string[]} tables the patient data tables to fetch
 * @returns {Promise<[[*]]>} all the entries of the patient data table, with the table name being the key to its entries
 */
async function getTables(log, reqId, tables) {

    const fnName = 'repos/patientData/getTables';
    log.info(reqId, `${fnName} ### start`);

    try {

        // -> Map each table to a promise to fetch its patient data
        const fetchPromises = tables.map(table => con.query(`SELECT * FROM ${table}`));

        log.info(reqId, `${fnName} ### [query] fetch specified patient data`);
        // -> Await all Promises
        const promiseResults = await Promise.all(fetchPromises);

        // -> Place each resulting patient data table into its key in the result object
        const patientData = {};
        for(let i = 0; i < promiseResults.length; i++) {
            patientData[tables[i]] = promiseResults[i][0];
        }

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return patientData;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * update a column in a patient data entry
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} table patient data table
 * @param {number} rowId entry (row) ID
 * @param {string} col predefined column name
 * @param {*} val new value to update the column
 */
async function updateCol(log, reqId, table, rowId, col, val) {

    const fnName = 'repos/patientData/updateCol';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Update Params
        const updateParams = {};
        updateParams[col] = val;

        // SQL
        const sql = `
            UPDATE ${table}
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update patient data entry column (${table}.${col} = ${val})`);
        // -> Query
        await con.query(sql, [updateParams, rowId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * delete an entry of a patient data table
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} table patient data table
 * @param {number} rowId entry (row) ID
 */
async function deleteRow(log, reqId, table, rowId) {

    const fnName = 'repos/patientData/deleteRow';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM ${table}
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete patient data entry (${table}:${rowId})`);
        // -> Query
        await con.query(sql, [rowId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    getTable,
    getTables,
    updateCol,
    deleteRow
}