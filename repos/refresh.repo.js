/**
 * @description data-access with DB for Refresh tokens data
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add a refresh token
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} refreshToken refresh token
 * @param {number} userId user ID for the user that the refresh token belongs to
 */
async function add(log, reqId, refreshToken, userId) {

    const fnName = 'repos/refresh/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            token: refreshToken,
            userId: userId
        }

        // SQL
        const sql = `
            INSERT INTO refresh
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] insert refresh token`);
        // -> Query
        await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * delete a user's refresh tokens
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user ID
 */
async function deleteUserTokens(log, reqId, userId) {

    const fnName = 'repos/refresh/deleteUserTokens';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM refresh
            WHERE userId = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete user refresh tokens for user with ID ${userId}`);
        // -> Query
        await con.query(sql, [userId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * check if a refresh token exists for a specific user
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} refreshToken refresh token
 * @param {number} userId user ID
 * @returns {Promise<boolean>}
 */
async function doesExist(log, reqId, refreshToken, userId) {

    const fnName = 'repos/refresh/doesExist';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM refresh
            WHERE token = ?
            AND userId = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] check if refresh token exists for this userId ### refreshToken: ${refreshToken}, userId: ${userId}`);
        // -> Query
        const [tokens] = await con.query(sql, [refreshToken, userId]);

        if(tokens && tokens[0]) {

            // Exists
            log.info(reqId, `${fnName} ### end [exists]`);
            return true;
        }
        else {

            // Does not exist
            log.info(reqId, `${fnName} ### end [does not exist]`);
            return false;
        }
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    deleteUserTokens,
    doesExist
}