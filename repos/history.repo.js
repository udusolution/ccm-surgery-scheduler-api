/**
 * @description data-access with DB for history table
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add a history entry
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user ID of the user that performed the action
 * @param {number?} eventId event ID if the action was performed for an event
 * @param {string} actionType action type (ENUM)
 * @param {string} description action description
 * @param {string} extra1 extra info 1
 * @param {string} extra2 extra info 2
 * @param {string} extra3 extra info 3
 * @param {string} extra4 extra info 4
 * @param {number?} patientId patient ID if the action was performed on a patient
 * @returns {Promise<number>} ID of newly inserted history row
 */
async function add(log, reqId, userId, eventId = null, actionType, description = null, extra1 = null, extra2 = null, extra3 = null, extra4 = null, patientId = null) {

    const fnName = 'repos/history/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            userId,
            actionType
        }
        if(eventId) params.eventId = eventId;
        if(patientId) params.patientId = patientId;
        if(description) params.description = description;
        if(extra1) params.extra1 = extra1;
        if(extra2) params.extra2 = extra2;
        if(extra3) params.extra3 = extra3;
        if(extra4) params.extra4 = extra4;

        // SQL
        const sql = `
            INSERT INTO history
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add history entry`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return result.insertId;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        // Swallow the Error
        // throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get history entries [paginated]
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} skip number of results to skip
 * @param {number} limit maximum results to return (page size)
 * @param {number} userId user ID to filter for his history only
 * @returns {Promise<any[]>} list of history entries
 */
async function getEntries(log, reqId, skip, limit, userId = null) {

    const fnName = 'repos/history/getEntries';
    log.info(reqId, `${fnName} ### start`);

    try {

        let params = [];
        if(userId) params.push(userId);

        // SQL
        const sql = `
            SELECT h.*, u.email AS userEmail, u.name AS userName, e.physicianName, e.patientName, e.facilityName, e.dateFrom
            FROM history AS h
            LEFT JOIN user AS u ON h.userId = u.id
            LEFT JOIN event AS e ON h.eventId = e.id
            ${userId ? 'WHERE h.userId = ?' : ''}
            ORDER BY h.id DESC
            LIMIT ${skip},${limit}
        `;

        log.info(reqId, `${fnName} ### [query] get history entries`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return result;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * search history entries [paginated]
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} skip entries to skip
 * @param {number} limit maximum entries to fetch (page size)
 * @param {Date|string} fromDate get events that start after this date time
 * @param {Date|string} toDate get events that start before this date time
 * @param {number} userId user ID to filter by
 * @param {number} eventId event ID to filter by
 * @param {number} patientId patient ID to filter by
 * @returns {Promise<any[]>} found history entries
 */
async function search(log, reqId, { 
    skip = 0, 
    limit = 10,
    fromDate = null, 
    toDate = null,
    userId = null,
    eventId = null,
    patientId = null,
    types = null
}) {

    const fnName = 'repos/history/search';
    log.info(reqId, `${fnName} ### start`);

    try {

        const params = [];
        const whereClauses = [];
        let hasWhere = false;

        // Filter by User
        if(userId) {
            hasWhere = true;
            whereClauses.push(`h.userId = ?`);
            params.push(userId);
        }

        // Filter by Event
        if(eventId) {
            hasWhere = true;
            whereClauses.push(`h.eventId = ?`);
            params.push(eventId);
        }

        // Filter by Patient
        if(patientId) {
            hasWhere = true;
            whereClauses.push(`h.patientId = ?`);
            params.push(patientId);
        }
        
        // From Date
        if(fromDate) {
            hasWhere = true;
            whereClauses.push(`h.createdOn > ?`);
            params.push(fromDate);
        }

        // To Date
        if(toDate) {
            hasWhere = true;
            whereClauses.push(`h.createdOn < ?`);
            params.push(toDate);
        }

        // Types
        if(types) {
            hasWhere = true;
            whereClauses.push(`h.actionType IN (?)`);
            params.push(types);
        }

        // SQL
        const sql = `
            SELECT h.*, u.email AS userEmail, u.name AS userName, e.physicianName, e.patientName, e.facilityName, e.dateFrom
            FROM history AS h
            LEFT JOIN user AS u ON h.userId = u.id
            LEFT JOIN event AS e ON h.eventId = e.id
            ${hasWhere ? `WHERE ${whereClauses.join(' AND ')}` : ''}
            ORDER BY h.id DESC
            LIMIT ${skip},${limit}
        `;

        log.info(reqId, `${fnName} ### [query] search history`);
        // -> Query
        const [results] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    getEntries,
    search
}