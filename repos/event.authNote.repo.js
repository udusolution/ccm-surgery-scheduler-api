/**
 * @description data-access with DB for event auth notes
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add event auth note
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId Event ID
 * @param {number} userId User ID
 * @param {string} note note text
 * @returns {Promise<any>} the newly created auth note
 */
async function add(log, reqId, { eventId, userId, note }) {

    const fnName = 'repos/event/authNote/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            eventId, 
            userId, 
            note
        }

        // SQL
        const sql = `
            INSERT INTO event_auth_note
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add event auth note`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all the event's auth notes
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId patient ID
 * @returns {Promise<any[]>} array of event's auth notes
 */
async function getForEvent(log, reqId, eventId) {

    const fnName = 'repos/event/authNote/getForEvent';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT n.*, u.email AS userEmail, u.name AS userName
            FROM event_auth_note AS n
            JOIN user AS u ON n.userId = u.id
            WHERE n.eventId = ?
        `;

        log.info(reqId, `${fnName} ### [query] get event auth notes`);
        // -> Query
        const [results] = await con.query(sql, eventId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get by ID
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} noteId note ID
 * @returns {Promise<any>} note
 */
async function findById(log, reqId, noteId) {

    const fnName = 'repos/event/authNote/findById';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM event_auth_note
            WHERE id = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get event auth note`);
        // -> Query
        const [results] = await con.query(sql, noteId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results[0];
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * delete an event auth note
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} noteId note ID
 */
async function deleteOne(log, reqId, noteId) {

    const fnName = 'repos/event/authNote/deleteOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM event_auth_note
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete event auth note`);
        // -> Query
        await con.query(sql, [noteId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    getForEvent,
    findById,
    deleteOne
}