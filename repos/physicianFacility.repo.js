/**
 * @description data-access with DB for physician facilities
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add association between a facility and a physician
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} physicianId physician ID
 * @param {number} facilityId facility ID
 */
async function addAssociation(log, reqId, physicianId, facilityId) {

    const fnName = 'repos/event/physicianFacility/addAssociation';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            physicianId, 
            facilityId
        }

        // SQL
        const sql = `
            INSERT INTO facility_physician
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add facility physician association`);
        // -> Query
        await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all the physician's facilities
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} physicianId physician ID
 * @returns {Promise<any[]>} array of physician's facilities
 */
async function getForPhysician(log, reqId, physicianId) {

    const fnName = 'repos/event/physicianFacility/getForPhysician';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT fp.*, f.id AS facilityId, f.code AS facilityCode, f.name AS facilityName
            FROM facility_physician AS fp
            JOIN facility AS f ON fp.facilityId = f.id
            WHERE fp.physicianId = ?
        `;

        log.info(reqId, `${fnName} ### [query] get physician facilities`);
        // -> Query
        const [results] = await con.query(sql, physicianId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * delete an association between a physician and a facility
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} associationId facility physician association's ID
 */
async function deleteAssociation(log, reqId, associationId) {

    const fnName = 'repos/event/physicianFacility/deleteAssociation';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM facility_physician
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete the association between the physician and the facility`);
        // -> Query
        await con.query(sql, associationId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    addAssociation,
    getForPhysician,
    deleteAssociation
}