/**
 * @description data-access with DB for event procedures
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add event procedure
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId Event ID
 * @param {number} procedureId Procedure ID
 * @param {string} procedureCode procedure code
 * @param {string} procedureName procedure name
 * @returns {Promise<any>} the newly created procedure
 */
async function add(log, reqId, { eventId, procedureId, procedureCode, procedureName }) {

    const fnName = 'repos/event/procedure/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            eventId, 
            procedureId, 
            procedureCode, 
            procedureName
        }

        // SQL
        const sql = `
            INSERT INTO event_procedure
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add event procedure`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * update event procedure
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id Event procedure ID
 * @param {string} procedureCode procedure code
 * @param {string} procedureName procedure name
 */
async function updateOne(log, reqId, { id, procedureCode, procedureName }) {

    const fnName = 'repos/event/procedure/updateOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {}
        if(procedureCode !== undefined) params.procedureCode = procedureCode;
        if(procedureName) params.procedureName = procedureName;

        if(Object.keys(params).length == 0) {
            log.info(reqId, `${fnName} ### end ### nothing to update`);
            return;
        }

        // SQL
        const sql = `
            UPDATE event_procedure
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update event procedure ### update params: ${params}`);
        // -> Query
        await con.query(sql, [params, id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * add multiple event procedure
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId Event ID
 * @param {[{id: number, code: string, name: string}]} procedures array of procedures to insert
 */
async function addMany(log, reqId, eventId, procedures) {

    const fnName = 'repos/event/procedure/addMany';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const values = procedures.map(p => [eventId, p.id || null, p.code || null, p.name]);

        // SQL
        const sql = `
            INSERT INTO event_procedure (eventId, procedureId, procedureCode, procedureName)
            VALUES ?
        `;

        log.info(reqId, `${fnName} ### [query] add event procedures`);
        // -> Query
        await con.query(sql, [values]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all the event's procedures
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId patient ID
 * @returns {Promise<any[]>} array of event's procedures
 */
async function getForEvent(log, reqId, eventId) {

    const fnName = 'repos/event/procedure/getForEvent';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM event_procedure
            WHERE eventId = ?
        `;

        log.info(reqId, `${fnName} ### [query] get event procedures`);
        // -> Query
        const [results] = await con.query(sql, eventId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get event's procedure
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id event procedure ID
 * @returns {Promise<any>} event's procedures
 */
async function findById(log, reqId, id) {

    const fnName = 'repos/event/procedure/findById';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM event_procedure
            WHERE id = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get event procedure`);
        // -> Query
        const [results] = await con.query(sql, [id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results[0];
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * delete an event's procedure
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventProcedureId event's procedure ID
 */
async function deleteOne(log, reqId, eventProcedureId) {

    const fnName = 'repos/event/procedure/deleteOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM event_procedure
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete event procedure`);
        // -> Query
        await con.query(sql, eventProcedureId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * delete an event's procedures
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 */
async function deleteForEvent(log, reqId, eventId) {

    const fnName = 'repos/event/procedure/deleteForEvent';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM event_procedure
            WHERE eventId = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete event procedures`);
        // -> Query
        await con.query(sql, eventId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    updateOne,
    addMany,
    getForEvent,
    findById,
    deleteOne,
    deleteForEvent,
}