/**
 * @description data-access with DB for event checklist
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add event checklist item
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId Event ID
 * @param {number} userId User ID that created the item
 * @param {string} description item description
 * @param {string} groupName item's group that it belongs to
 * @param {number} assigneeId ID of the user that this item is assigned to
 * @param {Date | string} dueDate due date of this item
 * @returns {Promise<any>} the newly created checklist item
 */
async function add(log, reqId, { eventId, userId, description, groupName, assigneeId, dueDate }) {

    const fnName = 'repos/event/checklist/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            eventId, 
            userId, 
            description,
            groupName, 
            assigneeId, 
            dueDate
        }

        // SQL
        const sql = `
            INSERT INTO event_checklist
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add event checklist item`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * update event checklist item
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} itemId Checklist item ID
 * @param {number} userId User ID that created the item
 * @param {string} description item description
 * @param {string} groupName item's group that it belongs to
 * @param {number} assigneeId ID of the user that this item is assigned to
 * @param {Date | string} dueDate due date of this item
 */
async function update(log, reqId, itemId, { description, assigneeId, dueDate, status }) {

    const fnName = 'repos/event/checklist/update';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {};
        if(description) params.description = description;
        if(assigneeId) params.assigneeId = assigneeId;
        if(dueDate) params.dueDate = dueDate;
        if(status) params.status = status;

        // SQL
        const sql = `
            UPDATE event_checklist
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update event checklist item ### update params: ${JSON.stringify(params)}`);
        // -> Query
        await con.query(sql, [params, itemId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all the event's checklist items
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId patient ID
 * @returns {Promise<any[]>} array of event's checklist items
 */
async function getForEvent(log, reqId, eventId) {

    const fnName = 'repos/event/checklist/getForEvent';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT c.*, u.name AS assigneeName
            FROM event_checklist AS c
            LEFT JOIN user AS u ON c.assigneeId = u.id
            WHERE eventId = ?
        `;

        log.info(reqId, `${fnName} ### [query] get event checklist items`);
        // -> Query
        const [results] = await con.query(sql, eventId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get checklist item by ID
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} itemId checklist item ID
 * @returns {Promise<any>} checklist item
 */
async function findById(log, reqId, itemId) {

    const fnName = 'repos/event/checklist/findById';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM event_checklist
            WHERE id = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get event checklist item`);
        // -> Query
        const [results] = await con.query(sql, itemId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results[0];
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * delete an event's checklist item
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} itemId event's checklist item ID
 */
async function deleteOne(log, reqId, itemId) {

    const fnName = 'repos/event/checklist/deleteOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM event_checklist
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete event checklist item`);
        // -> Query
        await con.query(sql, itemId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    update,
    getForEvent,
    findById,
    deleteOne
}