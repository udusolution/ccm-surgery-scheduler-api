/**
 * @description data-access with DB for event blood thinners
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add event blood thinner
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId Event ID
 * @param {number?} bloodThinnerId Master files blood thinner id (optional)
 * @param {string} bloodThinnerName blood thinner name
 * @param {string} bloodThinnerDescription blood thinner description
 * @returns {Promise<any>} the newly created event blood thinner
 */
async function add(log, reqId, { eventId, bloodThinnerId = null, bloodThinnerName, bloodThinnerCode, bloodThinnerDescription }) {

    const fnName = 'repos/event/bloodThinner/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            eventId, 
            bloodThinnerName,
            bloodThinnerCode
        }
        if(bloodThinnerId) params.bloodThinnerId = bloodThinnerId;
        if(bloodThinnerDescription) params.bloodThinnerDescription = bloodThinnerDescription;

        // SQL
        const sql = `
            INSERT INTO event_blood_thinner
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add event blood thinner`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * update event blood thinner
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id Event blood thinner ID
 * @param {string} bloodThinnerName blood thinner name
 * @param {string} bloodThinnerDescription blood thinner description
 */
async function updateOne(log, reqId, { id, bloodThinnerName, bloodThinnerCode, bloodThinnerDescription, isActive }) {

    const fnName = 'repos/event/bloodThinner/updateOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {}
        if(bloodThinnerName) params.bloodThinnerName = bloodThinnerName;
        if(bloodThinnerCode) params.bloodThinnerCode = bloodThinnerCode;
        if(bloodThinnerDescription) params.bloodThinnerDescription = bloodThinnerDescription;
        if(isActive !== null) params.isActive = isActive;

        if(Object.keys(params).length == 0) {
            log.info(reqId, `${fnName} ### end ### nothing to update`);
            return;
        }

        // SQL
        const sql = `
            UPDATE event_blood_thinner
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update event blood thinner ### update params: ${JSON.stringify(params)}`);
        // -> Query
        await con.query(sql, [params, id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all the event's blood thinners
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 * @returns {Promise<any[]>} array of event's blood thinners
 */
async function getForEvent(log, reqId, eventId) {

    const fnName = 'repos/event/bloodThinner/getForEvent';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM event_blood_thinner
            WHERE eventId = ?
        `;

        log.info(reqId, `${fnName} ### [query] get event blood thinners`);
        // -> Query
        const [results] = await con.query(sql, [eventId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get event blood thinner by ID
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id event blood thinner ID
 * @returns {Promise<any>} event blood thinner
 */
async function findById(log, reqId, id) {

    const fnName = 'repos/event/bloodThinner/findById';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM event_blood_thinner
            WHERE id = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get event blood thinner`);
        // -> Query
        const [results] = await con.query(sql, [id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results[0];
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * delete an event's blood thinner item
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id event blood thinner's ID
 */
async function deleteOne(log, reqId, id) {

    const fnName = 'repos/event/bloodThinner/deleteOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM event_blood_thinner
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete event blood thinner`);
        // -> Query
        await con.query(sql, [id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * delete an event's blood thinners
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 */
async function deleteAllFromEvent(log, reqId, eventId) {

    const fnName = 'repos/event/bloodThinner/deleteAllFromEvent';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM event_blood_thinner
            WHERE eventId = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete event blood thinners`);
        // -> Query
        await con.query(sql, [eventId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    updateOne,
    getForEvent,
    findById,
    deleteOne,
    deleteAllFromEvent
}