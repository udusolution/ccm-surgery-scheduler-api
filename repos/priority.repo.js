/**
 * @description data-access with DB for priorities
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * set a priority as default
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} rowId priority ID
 */
async function setAsDefault(log, reqId, rowId) {

    const fnName = 'repos/priority/setAsDefault';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            UPDATE priority
            SET isDefault = 
                CASE
                    WHEN id = ? THEN 1
                    ELSE 0
                END
        `;

        log.info(reqId, `${fnName} ### [query] set priority as default`);
        // -> Query
        const [results] = await con.query(sql, [rowId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * Exports
 */
module.exports = {
    setAsDefault
}