/**
 * @description data-access with DB for event diagnosis
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add event diagnosis
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId Event ID
 * @param {number} diagnosisId diagnosis ID
 * @param {string} diagnosisCode diagnosis code
 * @param {string} diagnosisDescription diagnosis name
 * @returns {Promise<any>} the newly created diagnosis
 */
async function add(log, reqId, { eventId, diagnosisId, diagnosisCode, diagnosisDescription }) {

    const fnName = 'repos/event/diagnosis/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            eventId,
            diagnosisDescription
        }
        if(diagnosisId) params.diagnosisId = diagnosisId;
        if(diagnosisCode) params.diagnosisCode = diagnosisCode;

        // SQL
        const sql = `
            INSERT INTO event_diagnosis
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add event diagnosis`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * update event diagnosis
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id Event diagnosis ID
 * @param {string} diagnosisCode diagnosis code
 * @param {string} diagnosisDescription diagnosis name
 */
async function updateOne(log, reqId, { id, diagnosisCode, diagnosisDescription }) {

    const fnName = 'repos/event/diagnosis/updateOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {}
        if(diagnosisCode != undefined) params.diagnosisCode = diagnosisCode;
        if(diagnosisDescription != undefined) params.diagnosisDescription = diagnosisDescription;

        if(Object.keys(params).length == 0) {
            log.info(reqId, `${fnName} ### end ### nothing to update`);
            return;
        }

        // SQL
        const sql = `
            UPDATE event_diagnosis
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update event diagnosis`);
        // -> Query
        await con.query(sql, [params, id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return null;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all the event's diagnosis
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 * @returns {Promise<any[]>} array of event's diagnosis
 */
async function getForEvent(log, reqId, eventId) {

    const fnName = 'repos/event/diagnosis/getForEvent';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM event_diagnosis
            WHERE eventId = ?
        `;

        log.info(reqId, `${fnName} ### [query] get event diagnosis`);
        // -> Query
        const [results] = await con.query(sql, eventId);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get an event's diagnosis
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id event diagnosis ID
 * @returns {Promise<any>} event's diagnosis
 */
async function findById(log, reqId, id) {

    const fnName = 'repos/event/diagnosis/findById';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT *
            FROM event_diagnosis
            WHERE id = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get event diagnosis`);
        // -> Query
        const [results] = await con.query(sql, id);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results[0];
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * delete an event's diagnosis
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventDiagnosisId event's diagnosis ID
 */
async function deleteOne(log, reqId, eventDiagnosisId) {

    const fnName = 'repos/event/diagnosis/deleteOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM event_diagnosis
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete event diagnosis`);
        // -> Query
        await con.query(sql, [eventDiagnosisId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    updateOne,
    getForEvent,
    findById,
    deleteOne,
}