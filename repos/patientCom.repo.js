/**
 * @description data-access with DB for patient coms
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add event note
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId Event ID
 * @param {number} userId User ID
 * @param {string} note note text
 * @returns {Promise<any>} the newly created note
 */
async function addRecord(log, reqId, { userId, eventId, patientEmail, patientPhone, emailSubject, emailBody, emailAttachments, smsBody }) {

    const fnName = 'repos/patientCom/addRecord';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            userId,
            eventId
        }
        if(patientEmail)        params.patientEmail         = patientEmail;
        if(patientPhone)        params.patientPhone         = patientPhone;
        if(emailSubject)        params.emailSubject         = emailSubject;
        if(emailBody)           params.emailBody            = emailBody;
        if(emailAttachments)    params.emailAttachments     = emailAttachments;
        if(smsBody)             params.smsBody              = smsBody;

        // SQL
        const sql = `
            INSERT INTO patient_com_history
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add patient com record`);
        // -> Query
        const [result] = await con.query(sql, [params]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get for event
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId Event ID
 * @returns {Promise<any>} the newly created note
 */
async function getForEvent(log, reqId, eventId) {

    const fnName = 'repos/patientCom/getForEvent';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT pch.*, u.name AS userName
            FROM patient_com_history AS pch
            LEFT JOIN user AS u ON pch.userId = u.id
            WHERE eventId = ?
            ORDER BY id ASC
        `;

        log.info(reqId, `${fnName} ### [query] get event sent patient communications`);
        // -> Query
        const [results] = await con.query(sql, [eventId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    addRecord,
    getForEvent
}