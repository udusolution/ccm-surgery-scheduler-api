/**
 * @description data-access with DB for event edit requests
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');
const Enums = require('../common/enums');


/**
 * add an event edit request
 * @param {*} log logger
 * @param {*} reqId request ID
 */
async function add(log, reqId, { eventId, requesterId }) {

    const fnName = 'repos/eventEditRequest/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            eventId,
            requesterId
        }

        // SQL
        const sql = `
            INSERT INTO event_edit_request
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add event edit request`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all the event's edit requests
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 * @returns {Promise<any[]>} array of event's edit requests
 */
async function getForEvent(log, reqId, eventId) {

    const fnName = 'repos/eventEditRequest/getForEvent';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT 
                er.*,
                e.patientFirstName AS patientFirstName,
                e.patientLastName AS patientLastName, 
                e.dateFrom AS eventDateFrom,
                e.physicianName AS physicianName,
                e.coordinatorName AS coordinatorName,
                e.facilityName AS facilityName,
                requester.name AS requesterName
            FROM event_edit_request AS er
            JOIN event AS e ON er.eventId = e.id
            JOIN user AS requester ON requester.id = er.requesterId
            WHERE e.id = ?
        `;

        log.info(reqId, `${fnName} ### [query] get event edit requests`);
        // -> Query
        const [results] = await con.query(sql, [eventId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}

/**
 * get all the event's edit requests
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} eventId event ID
 * @returns {Promise<any[]>} array of event's edit requests
 */
async function getForCoordinator(log, reqId, coordinatorId) {

    const fnName = 'repos/eventEditRequest/getForCoordinator';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT 
                er.*,
                e.patientFirstName AS patientFirstName,
                e.patientLastName AS patientLastName, 
                e.dateFrom AS eventDateFrom,
                e.physicianName AS physicianName,
                e.coordinatorName AS coordinatorName,
                e.facilityName AS facilityName,
                requester.name AS requesterName
            FROM event_edit_request AS er
            JOIN event AS e ON er.eventId = e.id
            JOIN user AS requester ON requester.id = er.requesterId
            WHERE e.coordinatorId = ?
        `;

        log.info(reqId, `${fnName} ### [query] get event edit requests`);
        // -> Query
        const [results] = await con.query(sql, [coordinatorId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get by ID
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id event edit request ID
 * @returns {Promise<any>|null} event edit request
 */
async function findById(log, reqId, id) {

    const fnName = 'repos/eventEditRequest/findById';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT 
                er.*,
                e.patientFirstName AS patientFirstName,
                e.patientLastName AS patientLastName, 
                e.dateFrom AS eventDateFrom,
                e.physicianName AS physicianName,
                e.coordinatorName AS coordinatorName,
                e.coordinatorId AS coordinatorId,
                e.facilityName AS facilityName,
                requester.name AS requesterName
            FROM event_edit_request AS er
            JOIN event AS e ON er.eventId = e.id
            JOIN user AS requester ON requester.id = er.requesterId
            WHERE er.id = ?
            LIMIT 1
        `;

        log.info(reqId, `${fnName} ### [query] get event edit request`);
        // -> Query
        const [results] = await con.query(sql, [id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results[0];
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * approve an event's edit request
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id event's edit request ID
 */
async function approveRequest(log, reqId, id) {

    const fnName = 'repos/eventEditRequest/approveRequest';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            UPDATE event_edit_request
            SET status = ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] approve event edit request`);
        // -> Query
        await con.query(sql, [Enums.EventEditRequestStatuses.Approved, id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}

/**
 * reject an event's edit request
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id event's edit request ID
 */
async function rejectRequest(log, reqId, id) {

    const fnName = 'repos/eventEditRequest/rejectRequest';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            UPDATE event_edit_request
            SET status = ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] reject event edit request`);
        // -> Query
        await con.query(sql, [Enums.EventEditRequestStatuses.Rejected, id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    getForEvent,
    getForCoordinator,
    findById,
    approveRequest,
    rejectRequest
}