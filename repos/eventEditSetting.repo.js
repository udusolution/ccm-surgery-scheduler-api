/**
 * @description data-access with DB for event edit settings
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');
const Enums = require('../common/enums');


/**
 * update a user's event edit setting
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user ID
 * @param {('allow-all'|'allow-none'|'upon-request')} setting setting value
 */
async function updateUserSetting(log, reqId, userId, setting) {

    const fnName = 'repos/eventEditSetting/updateUserSetting';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            UPDATE user
            SET eventEditPerm = ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update user event edit permissions setting`);
        // -> Query
        await con.query(sql, [setting, userId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * update a user's event edit whitelist (note that this is a greedy edit, meaning the array of whitelisted users will be added and the old ones removed)
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user ID
 * @param {number[]} whitelistedIds whitelisted user IDs
 */
 async function updateUserWhitelist(log, reqId, userId, whitelistedIds) {

    const fnName = 'repos/eventEditSetting/updateUserWhitelist';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Delete Previous Whitelist
        await con.query(`DELETE FROM event_edit_whitelist WHERE allowerId = ?`, [userId]);

        // If no new whitelist IDs provided, return
        if(!whitelistedIds || whitelistedIds?.length == 0) {
            log.info(reqId, `${fnName} ### end [success]`);
            return;
        }

        // Params
        const params = [
            ...whitelistedIds.map(id => [userId, id])
        ]

        // SQL
        const sql = `
            INSERT INTO event_edit_whitelist (allowerId, allowedId)
            VALUES ?
        `;

        log.info(reqId, `${fnName} ### [query] update user event edit whitelist`);
        // -> Query
        await con.query(sql, [params]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get a user's event edit whitelist
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} userId user ID
 */
 async function getUserWhitelist(log, reqId, userId) {

    const fnName = 'repos/eventEditSetting/getUserWhitelist';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT whitelist.*, u.name AS allowedName
            FROM event_edit_whitelist AS whitelist
            JOIN user AS u ON u.id = whitelist.allowedId
            WHERE allowerId = ?
        `;

        log.info(reqId, `${fnName} ### [query] get user event edit whitelist`);
        // -> Query
        const [results] = await con.query(sql, [userId]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    updateUserSetting,
    updateUserWhitelist,
    getUserWhitelist
}