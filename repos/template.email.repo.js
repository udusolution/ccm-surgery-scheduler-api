/**
 * @description data-access with DB for email templates
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * add email template
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function add(log, reqId, { name, subject, body, tags }) {

    const fnName = 'repos/templates/email/add';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Params
        const params = {
            name, 
            subject, 
            body
        }
        if(tags) params.tags = tags;

        // SQL
        const sql = `
            INSERT INTO email
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add email template`);
        // -> Query
        const [result] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return { ...params, id: result.insertId };
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * add email template attachments
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function addAttachments(log, reqId, rows) {

    const fnName = 'repos/templates/email/addAttachments';
    log.info(reqId, `${fnName} ### start ### rows: ${JSON.stringify(rows)}`);

    try {

        // Params
        const params = [
            ...rows.map(r => [r.emailId, r.file])
        ]

        // SQL
        const sql = `
            INSERT INTO email_attachment (emailId, file)
            VALUES ?
        `;

        log.info(reqId, `${fnName} ### [query] add email template attachments`);
        // -> Query
        await con.query(sql, [params]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get all email template and with their attachments
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function getAll(log, reqId) {

    const fnName = 'repos/templates/email/getAll';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT e.*, ea.file AS templateFile
            FROM email AS e
            LEFT JOIN email_attachment AS ea ON ea.emailId = e.id
            ORDER BY e.name ASC
        `;

        log.info(reqId, `${fnName} ### [query] get all email template and with their attachments`);
        // -> Query
        const [results] = await con.query(sql);

        // Format the result
        const final = [];
        for(let i = 0; i < results.length; i++) {
            const template = results[i];

            // Check if final already has this template
            if(final.some(f => f.id == template.id)) continue;

            // Push this template to the final array, and include its attachments
            final.push({
                ...template,
                attachments: results.filter(t => (t.id == template.id) && t.templateFile).map(t => t.templateFile)
            })
        }

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return final;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * get an email template with its attachments
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id template id
 */
async function findById(log, reqId, id) {

    const fnName = 'repos/templates/email/findById';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT e.*, ea.id AS fileId, ea.file AS templateFile
            FROM email AS e
            LEFT JOIN email_attachment AS ea ON ea.emailId = e.id
            WHERE e.id = ?
        `;

        log.info(reqId, `${fnName} ### [query] get email template and with its attachments`);
        // -> Query
        const [results] = await con.query(sql, [id]);

        // Format the result
        let final = null;
        if(results[0]) {
            final = { ...results[0] };
            if(results.filter(t => t.templateFile).length > 0) {
                final.attachments = results
                    .filter(t => t.templateFile)
                    .map(t => {
                        return {
                            id: t.fileId,
                            file: t.templateFile
                        }
                    })
            }
        }

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return final;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}


/**
 * Exports
 */
module.exports = {
    add,
    addAttachments,
    getAll,
    findById
}