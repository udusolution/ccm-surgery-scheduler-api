/**
 * @description data-access with DB for day notes
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');
const Enums = require('../common/enums');
const moment = require('moment');


/**
 * add a day note
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function addOne(log, reqId, { userId, day, note, mentions, recurrences = null, hourFrom = null, hourTo = null }) {

    const fnName = 'repos/dayNote/addOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Note Params
        const noteParams = {
            userId,
            day,
            note
        }
        if(hourFrom) noteParams.hourFrom = hourFrom;
        if(hourTo) noteParams.hourTo = hourTo;
        if(recurrences) noteParams.recurrences = recurrences;
        switch(recurrences) {
            case Enums.DayNoteRecurrence.Daily:
                break;
            case Enums.DayNoteRecurrence.Weekly:
                noteParams.rDayOfWeek = moment.utc(day).isoWeekday(); // 1-7 1:Monday 7:Sunday
                break;
            case Enums.DayNoteRecurrence.Monthly:
                noteParams.rDayOfMonth = moment.utc(day).date(); // 1-31
                break;
            case Enums.DayNoteRecurrence.Yearly:
                noteParams.rDayOfMonth = moment.utc(day).date(); // 1-31
                noteParams.rMonth = moment.utc(day).month() + 1; // 0-11, +1 so 1-12
                break;
        }

        // SQL
        const sql = `
            INSERT INTO day_note
            SET ?
        `;

        log.info(reqId, `${fnName} ### [query] add day note`);
        // -> Query
        const [results] = await con.query(sql, [noteParams]);

        if(mentions?.length > 0) {

            const noteId = results.insertId;

            const mentionsSql = `
                INSERT INTO day_note_mention (userId, noteId)
                VALUES ?
            `

            const mentionsParam = [
                ...mentions.map(mention => [mention, noteId])
            ]

            log.info(reqId, `${fnName} ### [query] add day note mentions`);
            // -> Query
            await con.query(mentionsSql, [mentionsParam]);
        }

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results.insertId;
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }

    }
}


/**
 * get a day's notes
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function getForDay(log, reqId, day) {

    const fnName = 'repos/dayNote/getForDay';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Note Params
        const params = [
            moment.utc(day).hour(0).minute(0).second(0).toDate(),
            moment.utc(day).add(1, 'day').hour(0).minute(0).second(0).toDate(),
            moment.utc(day).hour(12).minute(0).second(0).toDate(),
            moment.utc(day).hour(12).minute(0).second(0).toDate(),
            moment.utc(day).hour(12).minute(0).second(0).toDate(),
            moment.utc(day).hour(12).minute(0).second(0).toDate(),
        ]

        // SQL
        const sql = `
            SELECT note.*, mention.id AS mentionId, poster.id AS posterId, poster.name AS posterName, mentioned.id AS mentionedId, mentioned.name AS mentionedName, mentioned.color AS mentionedColor
            FROM day_note AS note
            LEFT JOIN day_note_mention AS mention ON mention.noteId = note.id
            LEFT JOIN user AS mentioned ON mention.userId = mentioned.id
            LEFT JOIN user AS poster ON note.userId = poster.id
            WHERE 
                (note.recurrences = "${Enums.DayNoteRecurrence.Once}" AND note.day > ? AND note.day < ?)
                OR
                (note.recurrences = "${Enums.DayNoteRecurrence.Daily}")
                OR
                (note.recurrences = "${Enums.DayNoteRecurrence.Weekly}" AND WEEKDAY(?) + 1 = note.rDayOfWeek)
                OR
                (note.recurrences = "${Enums.DayNoteRecurrence.Monthly}" AND DAY(?) = note.rDayOfMonth)
                OR
                (note.recurrences = "${Enums.DayNoteRecurrence.Yearly}" AND MONTH(?) = note.rMonth AND DAY(?) = note.rDayOfMonth)
            ORDER BY note.createdOn ASC
        `;

        log.info(reqId, `${fnName} ### [query] add day note`);
        // -> Query
        const [results] = await con.query(sql, params);

        let final = [];
        if(results[0]) {

            for(let i = 0; i < results.length; i++) {

                const note = results[i];
                if(!final.some(f => f.id == note.id)) {
                    final.push({
                        ...note,
                        mentions: results
                            .filter(t => t.mentionedName)
                            .filter(t => t.id == note.id)
                            .map(t => {
                                return {
                                    mentionedId: t.mentionedId,
                                    mentionedName: t.mentionedName,
                                    mentionedColor: t.mentionedColor
                                }
                            })
                    })
                }
            }
        }

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return final;

    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }

    }
}

/**
 * get a month's notes
 * @param {*} log logger
 * @param {string} reqId request ID
 */
async function getForMonth(log, reqId, day) {

    const fnName = 'repos/dayNote/getForMonth';
    log.info(reqId, `${fnName} ### start`);

    try {

        // Note Params
        const params = [
            moment.utc(day).add(15, 'days').format('YYYY'),
            moment.utc(day).add(15, 'days').format('MM'),
            moment.utc(day).hour(12).minute(0).second(0).toDate(),
        ]

        // SQL
        const sql = `
            SELECT note.*, mention.id AS mentionId, poster.id AS posterId, poster.name AS posterName, mentioned.id AS mentionedId, mentioned.name AS mentionedName, mentioned.color AS mentionedColor
            FROM day_note AS note
            LEFT JOIN day_note_mention AS mention ON mention.noteId = note.id
            LEFT JOIN user AS mentioned ON mention.userId = mentioned.id
            LEFT JOIN user AS poster ON note.userId = poster.id
            WHERE (
                (note.recurrences = "${Enums.DayNoteRecurrence.Once}" AND YEAR(note.day) = ? AND MONTH(note.day) = ?)
                OR
                (note.recurrences = "${Enums.DayNoteRecurrence.Daily}")
                OR
                (note.recurrences = "${Enums.DayNoteRecurrence.Weekly}")
                OR
                (note.recurrences = "${Enums.DayNoteRecurrence.Monthly}")
                OR
                (note.recurrences = "${Enums.DayNoteRecurrence.Yearly}" AND MONTH(?) = note.rMonth)
            )
            ORDER BY note.createdOn ASC
        `;

        log.info(reqId, `${fnName} ### [query] get month's notes`);
        // -> Query
        const [results] = await con.query(sql, params);

        let final = [];
        if(results[0]) {

            for(let i = 0; i < results.length; i++) {

                const note = results[i];
                if(!final.some(f => f.id == note.id)) {
                    final.push({
                        ...note,
                        mentions: results
                            .filter(t => t.mentionedName)
                            .filter(t => t.id == note.id)
                            .map(t => {
                                return {
                                    mentionedId: t.mentionedId,
                                    mentionedName: t.mentionedName,
                                    mentionedColor: t.mentionedColor
                                }
                            })
                    })
                }
            }
        }

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return final;

    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }

    }
}


/**
 * delete a day note
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id day note ID
 * @note if the note is recurrent, it is deleted and all the recurrences are gone too
 */
async function deleteOne(log, reqId, id) {

    const fnName = 'repos/dayNote/deleteOne';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            DELETE FROM day_note
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] delete day note`);
        // -> Query
        await con.query(sql, [id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;

    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);

    }
}

/**
 * find a day note
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {number} id day note ID
 */
async function findById(log, reqId, id) {

    const fnName = 'repos/dayNote/findById';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            SELECT * FROM day_note
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] get day note`);
        // -> Query
        const [results] = await con.query(sql, [id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results[0];

    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);

    }
}

async function update(log, reqId, id, params) {
    const fnName = 'repos/dayNote/update';
    log.info(reqId, `${fnName} ### start`);

    try {

        // SQL
        const sql = `
            UPDATE day_note
            SET ?
            WHERE id = ?
        `;

        log.info(reqId, `${fnName} ### [query] update day note`);
        // -> Query
        await con.query(sql, [params, id]);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return;
    }
    catch(e) {
        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);
        throw new Errors.DbErrors.DbError(e.message, e);
    }
}

/**
 * Exports
 */
module.exports = {
    addOne,
    getForDay,
    getForMonth,
    deleteOne,
    findById,
    update,
}