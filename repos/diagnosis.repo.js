/**
 * @description data-access with DB for user data
 */


const con = require('../config/db/db');
const Errors = require('../common/errors/_CustomErrors');


/**
 * search diagnosis
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} query search query
 * @param {number} limit maximum entries to fetch (page size)
 * @returns {Promise<any[]>} found diagnosis
 */
async function search(log, reqId, { query, limit }) {

    const fnName = 'repos/diagnosis/search';
    log.info(reqId, `${fnName} ### start`);

    try {

        const params = [
            query,
            limit
        ]

        // SQL
        const sql = `
            SELECT *, MATCH(code,description,tags) AGAINST(? IN BOOLEAN MODE) AS relevance
            FROM diagnosis
            HAVING relevance > 0
            ORDER BY relevance DESC
            LIMIT ?
        `;

        log.info(reqId, `${fnName} ### [query] search diagnosis`);
        // -> Query
        const [results] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * number of diagnosis that match a search query
 * @param {*} log logger
 * @param {string} reqId request ID
 * @param {string} query search query
 * @returns {Promise<number>} number of diagnosis that match the search query
 */
async function searchCount(log, reqId, { query }) {

    const fnName = 'repos/diagnosis/searchCount';
    log.info(reqId, `${fnName} ### start`);

    try {

        const params = [
            `%${query}%`,
            `%${query}%`,
            `%${query}%`
        ]

        // SQL
        const sql = `
            SELECT COUNT(*) as count
            FROM diagnosis
            WHERE code LIKE ?
            OR description LIKE ?
            OR tags LIKE ?
        `;

        log.info(reqId, `${fnName} ### [query] count matching diagnosis`);
        // -> Query
        const [results] = await con.query(sql, params);

        // End
        log.info(reqId, `${fnName} ### end [success]`);
        return results[0].count;
        
    }
    catch(e) {

        log.warn(reqId, `${fnName} ### end [failure] ### error: ${JSON.stringify(e)}`);

        if(e instanceof Errors.DbErrors.DbError) {
            throw e;
        }
        else {
            throw new Errors.DbErrors.DbError(e.message, e);
        }
        
    }
}


/**
 * Exports
 */
module.exports = {
    search,
    searchCount
}