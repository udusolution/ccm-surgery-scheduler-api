/**
 * @description global error handler for the app
 */


const Errors = require('./common/errors/_CustomErrors');
const Enums = require('./common/enums');
const M = require('./resources/languages/messages');


function handler(err, req, res, next) {

    const { log, reqId } = req;
    const fnName = 'globalErrorHandler';
    log.info(reqId, `${fnName} ### start`);

    // () DB Errors
    if(err instanceof Errors.DbErrors.DbError) {
        log.warn(reqId, `${fnName} ### DB error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.InternalServerError).json({ message: M.failure.generic() });
    }

    // () Encryption Errors
    else if(err instanceof Errors.EncErrors.EncError) {
        log.warn(reqId, `${fnName} ### Encryption error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.Retry).json({ message: M.failure.generic() });
    }

    // () Validation Errors
    else if(err instanceof Errors.ValidationErrors.ValidationError) {
        log.info(reqId, `${fnName} ### Validation error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.BadRequest).json({ message: err.message, errors: err.errors });
    }

    // () Sanity Errors
    else if(err instanceof Errors.CommonErrors.SanityError) {
        log.info(reqId, `${fnName} ### Sanity error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.InvalidOperation).json({ message: err.userMessage });
    }

    // () Not Found Errors
    else if(err instanceof Errors.CommonErrors.NotFoundError) {
        log.info(reqId, `${fnName} ### Not Found error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.NotFound).json({ message: err.userMessage });
    }

    // () Auth Errors
    else if(err instanceof Errors.AuthErrors.UserNotFoundAuthError) {
        log.info(reqId, `${fnName} ### Auth error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.Invalid).json({ message: M.failure.unauthorized() });
    }
    else if(err instanceof Errors.AuthErrors.FailedRolesAuthError) {
        log.info(reqId, `${fnName} ### Auth error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.Forbidden).json({ message: M.failure.unauthorized() });
    }
    else if(err instanceof Errors.AuthErrors.InvalidPasswordAuthError) {
        log.info(reqId, `${fnName} ### Auth error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.Invalid).json({ message: err.message || M.failure.unauthorized() });
    }
    else if(err instanceof Errors.AuthErrors.InvalidAccessTokenAuthError) {
        log.info(reqId, `${fnName} ### Auth error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.Unauthorized).json({ message: M.failure.unauthorized() });
    }
    else if(err instanceof Errors.AuthErrors.InvalidRefreshTokenAuthError) {
        log.info(reqId, `${fnName} ### Auth error: ${JSON.stringify(err)}`);
        console.log("REFRESH FAILED")
        res.status(Enums.HttpCodes.InvalidRefreshToken).json({ message: M.failure.unauthorized() });
    }
    else if(err instanceof Errors.AuthErrors.InvalidTokenAuthError) {
        log.info(reqId, `${fnName} ### Auth error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.Unauthorized).json({ message: M.failure.unauthorized() });
    }
    else if(err instanceof Errors.AuthErrors.MissingAccessTokenAuthError) {
        log.info(reqId, `${fnName} ### Auth error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.Unauthorized).json({ message: M.failure.unauthorized() });
    }
    else if(err instanceof Errors.AuthErrors.ForbiddenAuthError) {
        log.info(reqId, `${fnName} ### Auth error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.Forbidden).json({ message: M.failure.forbidden() });
    }
    else if(err instanceof Errors.AuthErrors.AccountLockedAuthError) {
        log.info(reqId, `${fnName} ### Auth error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.Forbidden).json({ message: M.failure.accountLocked() });
    }
    else if(err instanceof Errors.AuthErrors.AuthError) {
        log.info(reqId, `${fnName} ### Auth error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.Unauthorized).json({ message: M.failure.unauthorized() });
    }

    // () MD Errors
    else if(err instanceof Errors.MdErrors.UnauthorizedMdError) {
        log.warn(reqId, `${fnName} ### MD error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.Retry).json({ message: M.failure.generic() });
    }
    else if(err instanceof Errors.MdErrors.ForbiddenMdError) {
        log.warn(reqId, `${fnName} ### MD error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.Retry).json({ message: M.failure.generic() });
    }
    else if(err instanceof Errors.MdErrors.TooManyRequestsMdError) {
        log.info(reqId, `${fnName} ### MD error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.TooManyRequests).json({ message: M.failure.tooManyRequests() });
    }
    else if(err instanceof Errors.MdErrors.MdError) {
        log.warn(reqId, `${fnName} ### MD error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.Retry).json({ message: M.failure.tooManyRequests() });
    }

    // () JWT Errors
    else if(err instanceof Errors.JwtErrors.JwtError) {
        log.info(reqId, `${fnName} ### JWT Error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.InternalServerError).json({ message: M.failure.generic() });
    }

    // () Reserved Time Error
    else if(err instanceof Errors.CommonErrors.ReservedTimeError) {
        log.info(reqId, `${fnName} ### Reserved Time Error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.Invalid).json({ message: err.userMessage ?? M.failure.reservedTime(), data: { intersectingEvents: err.intersectingEvents } });
    }

    // () Address Lookup Error
    else if(err instanceof Errors.AddressLookupErrors.AddressLookupError) {
        log.info(reqId, `${fnName} ### Address Lookup Error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.Retry).json({ message: M.failure.generic() });
    }

    // () File Upload Error
    else if(err instanceof Errors.FileUploadErrors.FileUploadError) {
        log.warn(reqId, `${fnName} ### File Upload Error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.InternalServerError).json({ message: err.message || M.failure.failedToUploadFile() });
    }

    // () Invalid Phone Number Sms Error
    else if(err instanceof Errors.SmsErrors.InvalidPhoneNumberSmsError) {
        log.info(reqId, `${fnName} ### Sms Error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.Retry).json({ message: M.failure.invalidPhoneNumber() });
    }
    else if(err instanceof Errors.SmsErrors.SmsError) {
        log.warn(reqId, `${fnName} ### Sms Error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.Retry).json({ message: M.failure.smsFailure() });
    }

    // () Other Error
    else {
        log.warn(reqId, `${fnName} ### Error message: ${err.message}, Error: ${JSON.stringify(err)}`);
        res.status(Enums.HttpCodes.InternalServerError).json({ message: M.failure.generic() });
    }

    // End
    next();
}


/**
 * Exports
 */
module.exports = handler;